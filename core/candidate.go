package core

import (
	pb "github.com/guyu96/go-timbre/protobuf"
	"github.com/mbilal92/noise"
)

//RegisterCandidateTx returns the candidate registration transaction
func RegisterCandidateTx(payerID []byte, payerPrivateKey noise.PrivateKey, nonce uint64) (*Transaction, error) {
	tBody := &pb.Transaction_Body{
		PayerId: payerID,
		Nonce:   nonce,
	}
	payerSign, err := signTransactionBody(tBody, payerPrivateKey)
	if err != nil {
		return nil, err
	}
	newPbTransaction := &pb.Transaction{
		Body:     tBody,
		Type:     TxBecomeCandidate,
		PayerSig: payerSign,
	}

	newTransaction := &Transaction{
		pbTransaction: newPbTransaction,
	}
	return newTransaction, nil
}

//QuitCandidateTx returns the quit candidate transaction
func QuitCandidateTx(payerID []byte, payerPrivateKey noise.PrivateKey, nonce uint64) (*Transaction, error) {
	tBody := &pb.Transaction_Body{
		PayerId: payerID,
		Nonce:   nonce,
	}
	payerSign, err := signTransactionBody(tBody, payerPrivateKey)
	if err != nil {
		return nil, err
	}
	newPbTransaction := &pb.Transaction{
		Body:     tBody,
		Type:     TxQuitCandidate,
		PayerSig: payerSign,
	}

	newTransaction := &Transaction{
		pbTransaction: newPbTransaction,
	}
	return newTransaction, nil
}
