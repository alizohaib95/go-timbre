package core

import (
	"time"

	"github.com/golang/protobuf/proto"
	pb "github.com/guyu96/go-timbre/protobuf"
	"github.com/mbilal92/noise"
)

//Vote struct to wrap protobuf Vote
type Vote struct {
	pbVote *pb.Vote
}

//NewVote creates new vote instance
func NewVote(id string, voterPrivateKey noise.PrivateKey, votedCand []string) (*Vote, error) {

	newVote := Vote{
		pbVote: &pb.Vote{
			VoteCand:  votedCand,
			Id:        id,
			TimeStamp: time.Now().UnixNano(),
		},
	}
	err := newVote.SignVote(voterPrivateKey) //Signs the vote
	if err != nil {
		return nil, err
	}
	return &newVote, nil
}

//SignVote signs the vote
func (v *Vote) SignVote(voterPrivateKey noise.PrivateKey) error {
	voteBytes, err := proto.Marshal(v.pbVote)
	if err != nil {
		return err
	}
	v.pbVote.VoterSig = voterPrivateKey.SignB(voteBytes)
	return nil
}

//GetProtoVote returns the protoVote
func (v *Vote) GetProtoVote() *pb.Vote {
	return v.pbVote
}

//ToBytes convert the vote to bytes
func (v *Vote) ToBytes() ([]byte, error) {
	return proto.Marshal(v.pbVote)
}

//VoteFromBytes convert the bytes back to vote object
func VoteFromBytes(voteBytes []byte) (*Vote, error) {
	vote := new(Vote)
	vote.pbVote = new(pb.Vote)
	if err := proto.Unmarshal(voteBytes, vote.pbVote); err != nil {
		return nil, err
	}
	return vote, nil
}

//GetID returns the id of the vote
func (v *Vote) GetID() string {
	return v.pbVote.GetId()
}

//ResourceUtilized returns the resource used object
func (v *Vote) ResourceUtilized() *ResourceUsage {
	voteSize, _ := v.ToBytes()
	gasBurnt := &ResourceUsage{
		cpuUsage: 10,
		netUsage: int64(len(voteSize)),
	}
	return gasBurnt
}

//DeepCopyVotes deep copies vote in another array
func DeepCopyVotes(votes []*pb.Vote) []*pb.Vote {
	copy := make([]*pb.Vote, len(votes))
	for i, vote := range votes {
		var copied pb.Vote = *vote
		copy[i] = &copied
	}
	return copy
}
