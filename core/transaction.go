package core

import (
	"time"

	"github.com/golang/protobuf/proto"
	pb "github.com/guyu96/go-timbre/protobuf"
	"github.com/mbilal92/noise"
)

//Transaction is the protopuf transaction wrapper
type Transaction struct {
	pbTransaction *pb.Transaction
}

//MakeTestTransaction creates transaction for testing pupose
func MakeTestTransaction(payerID, recipientID []byte, amount int64, nonce uint64, payerPrivateKey noise.PrivateKey) (*Transaction, error) {

	tBody := &pb.Transaction_Body{
		RecipientId: recipientID,
		Decibels:    amount,
		Nonce:       nonce,
		PayerId:     payerID,
	}
	payerSign, err := signTransactionBody(tBody, payerPrivateKey)
	if err != nil {
		return nil, err
	}

	newPbTransaction := &pb.Transaction{
		Body:      tBody,
		PayerSig:  payerSign,
		TimeStamp: time.Now().UnixNano(),
		Type:      TxTransfer, //Corrosponding to the transfer type transaction
	}

	newTransaction := &Transaction{
		pbTransaction: newPbTransaction,
	}

	// fmt.Println("Test transaction has been created")
	return newTransaction, nil
}

//signTransactionBody signs the transaction body using private key of the payer
func signTransactionBody(transactionBody *pb.Transaction_Body, payerPrivateKey noise.PrivateKey) ([]byte, error) {
	tBodyBytes, err := proto.Marshal(transactionBody) //This marshals the transaction
	if err != nil {
		return nil, err
	}

	tBodySign := payerPrivateKey.SignB(tBodyBytes)

	return tBodySign, nil
}

//GetTxDecibels returns the decibles in the transaction
func (t *Transaction) GetTxDecibels() int64 {
	return t.pbTransaction.GetBody().GetDecibels()
}

//GetProtoTransaction returns proto transaction
func (t *Transaction) GetProtoTransaction() *pb.Transaction {
	return t.pbTransaction
}

//ToBytes convert it into bytes
func (t *Transaction) ToBytes() ([]byte, error) {
	return proto.Marshal(t.pbTransaction)
}

//TransactionFromBytes converts back to the transaction type from bytes
func TransactionFromBytes(transBytes []byte) (*Transaction, error) {
	trans := new(Transaction)
	trans.pbTransaction = new(pb.Transaction)
	if err := proto.Unmarshal(transBytes, trans.pbTransaction); err != nil {
		return nil, err
	}
	return trans, nil
}

//ResourceUtilized returns the resource used object
func (t *Transaction) ResourceUtilized() *ResourceUsage {
	txSize, _ := t.ToBytes()
	gasBurnt := &ResourceUsage{
		cpuUsage: 10,
		netUsage: int64(len(txSize)),
	}
	return gasBurnt
}
