package core

import (
	"time"

	"github.com/golang/protobuf/proto"
	pb "github.com/guyu96/go-timbre/protobuf"
	"github.com/mbilal92/noise"
)

//Podf is the struct against proof of double forgery
type Podf struct {
	pbPodf *pb.PoDF
}

//NewPodf returns the new proof of double forgery instance
func NewPodf(h1, h2 *pb.BlockHeader, nonce uint64, pk []byte, sk noise.PrivateKey) (*Podf, error) {

	newPodf := &Podf{
		pbPodf: &pb.PoDF{
			Header1:   h1,
			Header2:   h2,
			TimeStamp: time.Now().UnixNano(),
			Nonce:     nonce,
			PublicKey: pk,
		},
	}
	newPodf.SignPodf(sk)

	return newPodf, nil
}

//SignPodf signs the podf
func (p *Podf) SignPodf(mpk noise.PrivateKey) error {
	podfBytes, err := proto.Marshal(p.pbPodf)
	if err != nil {
		return err
	}
	p.pbPodf.Signature = mpk.SignB(podfBytes)
	return nil
}

//GetProtoPodf return the proto podf
func (p *Podf) GetProtoPodf() *pb.PoDF {
	return p.pbPodf
}

//ToBytes conver to byte
func (p *Podf) ToBytes() ([]byte, error) {
	podfBytes, err := proto.Marshal(p.pbPodf)
	if err != nil {
		return []byte{}, err
	}
	return podfBytes, nil
}

//PodfFromBytes returns the podf instance from the bytes
func PodfFromBytes(pBytes []byte) (*Podf, error) {
	var newPodf = new(Podf)
	newPodf.pbPodf = new(pb.PoDF)

	if err := proto.Unmarshal(pBytes, newPodf.pbPodf); err != nil {
		return nil, err
	}
	return newPodf, nil
}
