package blockstate

import (
	"encoding/hex"
	"errors"

	"github.com/guyu96/go-timbre/storage"
)

const (
	cacheSize = 10
)

var (
	blockStateBucket []byte
)

func init() {
	blockStateBucket = []byte("Block_state")
}

//StateStore manages the state of the block
type StateStore struct {
	db          *storage.Database // database connection
	cachedState []*BlockState     //For now not usin cache
	Stored      map[string]int64
}

//NewStateStore returns the new state store
func NewStateStore(database *storage.Database) *StateStore {

	store := &StateStore{
		db:          database,
		cachedState: make([]*BlockState, cacheSize),
		Stored:      make(map[string]int64),
	}

	return store
}

//PersistBlockState persists the blockstate to the Db
func (ss *StateStore) PersistBlockState(blockState *BlockState) error {
	stateBytes, err := blockState.ToBytes()
	if err != nil {
		return err
	}
	err = ss.db.Put(blockStateBucket, blockState.State.Hash, stateBytes)
	if err != nil {
		return err
	}

	ss.Stored[hex.EncodeToString(blockState.State.Hash)] = 1

	return nil
}

//CheckState checks if the state of the block exists
func (ss *StateStore) CheckState(blockHash []byte) bool {
	if _, ok := ss.Stored[hex.EncodeToString(blockHash)]; ok {
		return true
	}
	return false
}

//GetBlockState gets the blockstate from the db
func (ss *StateStore) GetBlockState(blockHash []byte) (*BlockState, error) {
	if ss.CheckState(blockHash) {
		stateBytes, err := ss.db.Get(blockStateBucket, blockHash)
		if err != nil {
			return nil, err
		}
		blockState, err := StateFromBytes(stateBytes)
		if err != nil {
			return nil, err
		}
		return blockState, nil
	}

	return nil, errors.New("Block state doesn't exist in the Db")
}

//RemoveState removes the state from the Db
func (ss *StateStore) RemoveState(blockHash []byte) {
	if ss.CheckState(blockHash) {
		ss.db.Delete(blockStateBucket, blockHash)
	}
	delete(ss.Stored, hex.EncodeToString(blockHash)) //deleting it from the map function

}
