package core

const (
	//TxTransfer is corrosponding to the retrieval transaction
	TxTransfer = "Transfer"
	//TxBecomeCandidate is for announcing the intention to become delegate
	TxBecomeCandidate = "Become_Candidate"
	//TxQuitCandidate is for announcing to quit from being candidate
	TxQuitCandidate = "Quit_Transaction"
)

//Epoch entatils the feature requirement to run an epoch on the node
type Epoch struct {
	Duration           int64 //Duration (int) represents the height of the block after whichh epoch should end
	RollBackPercentage int   //After an epoch ends the rollback percentage is the amount the user will be able to claim back
	Inflation          int   //Percentage inflation after an epoch
	EpochNum           int32
}

//TestEpoch basically set the features for the test epoch
var TestEpoch *Epoch = &Epoch{
	Duration:           1000,
	RollBackPercentage: 10,
	Inflation:          0,
}

//SetEpochNum sets the epoch number
func (e Epoch) SetEpochNum(num int32) {
	e.EpochNum = num
}
