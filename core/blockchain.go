package core

import (
	"bytes"
	"container/list"
	"encoding/hex"
	"errors"
	"fmt"
	"sync"

	"github.com/guyu96/go-timbre/log"
	pb "github.com/guyu96/go-timbre/protobuf"
	"github.com/guyu96/go-timbre/storage"
	"github.com/mbilal92/noise"
)

var (
	// BlockBucket is the name for block storage bucket.
	BlockBucket []byte
)

func init() {
	BlockBucket = []byte("block-bucket")
}

// Blockchain implements functions for persisting and updating the blockchain.
type Blockchain struct {
	db            *storage.Database // database for blockchain persistence
	genesis       *Block            // the genesis block
	mainTailIndex int               // the index of the main tail block that has the maximum fork lengths in tails

	tails  []*Block // a list of tail blocks corresponding to different forks
	muTail sync.RWMutex

	forkLengths   []int // a list of fork lengths, one for each tail block in tails
	muForkLengths sync.RWMutex

	mainForkMap  map[int64][]byte //A key value pair key-> Block height Value-> The hash
	muMainforMap sync.RWMutex

	IrreversibleBlock             int64
	Last2by3MinedCommitteeMembers *list.List
}

// InitBlockchain initializes a blockchain using the given genesis block.
func InitBlockchain(db *storage.Database, genesis *Block) (*Blockchain, error) {
	if !genesis.IsValid() {
		// TODO: use pkg/errors package to wrap errors.
		return nil, errors.New("genesis block is invalid")
	}
	// db.DeleteBucket(BlockBucket) //Delete the bucket, so that it creates new block every time it starts
	if db.HasBucket(BlockBucket) == false {
		log.Info().Msgf("Initializing block bucket")
		db.NewBucket(BlockBucket)
	}
	// if err := db.NewBucket(BlockBucket); err != nil {
	// 	return nil, err
	// }
	bc := &Blockchain{
		db:                            db,
		genesis:                       genesis,
		tails:                         []*Block{genesis},
		forkLengths:                   []int{0},
		mainTailIndex:                 0,
		mainForkMap:                   make(map[int64][]byte),
		IrreversibleBlock:             int64(0),
		Last2by3MinedCommitteeMembers: list.New(), //[]string{}
	}
	if err := bc.putBlock(genesis); err != nil {
		return nil, errors.New("error saving genesis block")
	}
	return bc, nil
}

func (bc *Blockchain) ResetBlockChain(db *storage.Database, genesis *Block) (*Blockchain, error) {
	db.DeleteBucket(BlockBucket) //Deleting bucket to make node every time:- TODO:Change it to restore state
	err := db.NewBucket(BlockBucket)
	if err != nil {
		log.Info().Err(err)
		return nil, err
	}

	return InitBlockchain(db, genesis)
}

// CreateBlockchain creates a new blockchain. This method should be called to start a new blockchain with a new genesis block.
func CreateBlockchain(db *storage.Database, minerPublicKey noise.PublicKey, minerPrivateKey noise.PrivateKey) (*Blockchain, error) {
	genesis, err := NewGenesisBlock(minerPublicKey, 0, []*pb.Transaction{})
	if err != nil {
		return nil, errors.New("error creating genesis block")
	}
	if err := genesis.Sign(minerPrivateKey); err != nil {
		return nil, errors.New("error signing genesis block")
	}
	return InitBlockchain(db, genesis)
}

//TODO: populate MainFOrkMap in this function (done)
// LoadBlockchain loads the blockchain from disk.
func LoadBlockchain(db *storage.Database) (*Blockchain, error) {
	bc := &Blockchain{db: db}
	// Find the genesis block and populate the parent-child, child-parent, and visited maps.
	parentChild := make(map[string][]byte) // byte slice cannot be used as map keys
	childParent := make(map[string][]byte)
	visited := make(map[string]bool)
	var genesis *Block
	err := db.ForEach(BlockBucket, func(blockHash, blockBytes []byte) error {
		block, err := NewBlockFromBytes(blockBytes)
		if err != nil {
			return err
		}
		if !block.IsValid() {
			return errors.New("error loading blockchain - invalid block")
		}
		blockHashStr := string(blockHash)
		parentHashStr := string(block.ParentHash())
		if block.IsGenesis() {
			if genesis != nil {
				return fmt.Errorf("error setting genesis block - duplicate genesis blocks")
			}
			genesis = block
		} else {
			parentChild[parentHashStr] = blockHash
			childParent[blockHashStr] = block.ParentHash()
		}
		visited[blockHashStr] = false
		return nil
	})
	if err != nil {
		return nil, err
	}
	if genesis == nil {
		return nil, errors.New("error loading blockchain - no genesis block")
	}
	// Find tail blocks
	tails := make([]*Block, 0)
	for _, child := range parentChild {
		// The child is a tail block if it is not a parent.
		if _, exist := parentChild[string(child)]; !exist {
			block, err := bc.GetBlockByHash(child)
			if err != nil {
				return nil, err
			}
			tails = append(tails, block)
		}
	}
	// Blockchain only has genesis block.
	if len(tails) == 0 {
		tails = append(tails, genesis)
	}
	// Find the longest fork. Choose the first one in case of ties.
	forkLengths := make([]int, len(tails))
	mainTailIndex := -1
	mainForkLength := -1
	for i, tail := range tails {
		blockHash := tail.Hash()
		blockHashStr := string(blockHash)
		visited[blockHashStr] = true
		currentForkLength := 0
		for {
			parentHash, exist := childParent[string(blockHash)]
			if !exist {
				break
			}
			blockHash = parentHash
			blockHashStr = string(blockHash)
			visited[blockHashStr] = true
			currentForkLength++
		}
		if !bytes.Equal(blockHash, genesis.Hash()) {
			return nil, errors.New("error loading blockchain - tail block does not have genesis block as root")
		}
		forkLengths[i] = currentForkLength
		if currentForkLength > mainForkLength {
			mainForkLength = currentForkLength
			mainTailIndex = i
		}
	}
	for blockHash, visited := range visited {
		if !visited {
			block, _ := bc.GetBlockByHash([]byte(blockHash))
			return nil, fmt.Errorf("error loading blockchain - %s not in any fork", block)
		}
	}
	bc.muTail.Lock()
	bc.genesis = genesis
	bc.tails = tails
	bc.muTail.Unlock()

	bc.muForkLengths.Lock()
	bc.forkLengths = forkLengths
	bc.muForkLengths.Unlock()

	bc.setMainTailIndex(mainTailIndex)
	bc.mainForkMap = make(map[int64][]byte)
	bc.updateMainForkMap(bc.GetMainTail())
	bc.IrreversibleBlock = int64(0)
	bc.Last2by3MinedCommitteeMembers = list.New()
	return bc, nil
}

func (bc *Blockchain) setMainTailIndex(i int) {
	// bc.mu.RLock()
	bc.muTail.Lock()
	bc.mainTailIndex = i
	bc.muTail.Unlock()
	// bc.mu.Unlock()
}

func (bc *Blockchain) updateMainForkMap(tail *Block) error {
	bc.muForkLengths.Lock()
	defer bc.muForkLengths.Unlock()

	var err error
	if tail == nil {
		return errors.New("Tail is currently empty")
	}
	walk := tail

	bc.muMainforMap.Lock()
	defer bc.muMainforMap.Unlock()

	for {
		if bytes.Equal(bc.mainForkMap[walk.Height()], walk.Hash()) {
			break
		}
		bc.mainForkMap[walk.Height()] = walk.Hash()
		if walk.ParentHash() == nil {
			// log.Info().Msgf("Walk Exhausted.")
			break
		}
		walk, err = bc.GetBlockByHash(walk.ParentHash())
		if err != nil {
			log.Info().Msgf("Could not find block.")
			return err
		}
	}
	return nil
}

// func (bc *Blockchain) Validate(blocks []*Block) error {
func (bc *Blockchain) Validate(block *Block) error {
	// blocksLength := len(blocks)
	// if blocksLength == 0 {
	// 	return nil
	// }

	// headBlock := blocks[0]
	// headBlock := block

	// parentBlock, err := bc.GetBlockByHash(headBlock.ParentHash())
	parentBlock, err := bc.GetBlockByHash(block.ParentHash())
	// First verify if the blocks is a valid contiguous chain of blocks.
	// if err != nil || !parentBlock.IsValidParentOf(headBlock) {
	if err != nil || !parentBlock.IsValidParentOf(block) {
		// return fmt.Errorf("append error - failed to get valid parent block of incoming head %s %s", headBlock, parentBlock)
		return fmt.Errorf("append error - failed to get valid parent block of incoming head %s %s", block, parentBlock)
	}
	// for i, block := range blocks {
	if !block.IsValid() {
		return fmt.Errorf("append error - %s is invalid", block)
	}
	if bc.Contains(block) {
		return fmt.Errorf("append error - %s already exists", block)
	}
	// if i > 0 && !blocks[i-1].IsValidParentOf(block) {
	// return fmt.Errorf("append error - incoming blocks are not contiguous at index %d", i)
	// }
	// }

	return nil
}

// Append appends blocks to the blockchain. The input blocks should be a contiguous chain of blocks sorted in ascending order by block height.
// func (bc *Blockchain) Append(blocks []*Block) error {
func (bc *Blockchain) Append(block *Block) error {

	// blocksLength := len(blocks)
	// if blocksLength == 0 {
	if block == nil {
		return nil
	}

	// headBlock := blocks[0]
	// tailBlock := blocks[blocksLength-1]

	headBlock := block
	tailBlock := block

	parentBlock, err := bc.GetBlockByHash(headBlock.ParentHash())
	// fmt.Println("Validating in Append")
	// err = bc.Validate(blocks)
	err = bc.Validate(block)
	if err != nil {
		return err
	}
	// fmt.Println("Done Validating in Append")
	// Append blocks to blockchain and update fork information
	// if err = bc.batchPutBlocks(blocks); err != nil {
	if err = bc.putBlock(block); err != nil {
		return err
	}
	parentTailIndex := bc.indexInTails(parentBlock)
	// newForkLength := parentBlock.HeightInt() + blocksLength
	newForkLength := parentBlock.HeightInt() + 1
	if parentTailIndex == -1 {
		bc.muTail.Lock()
		bc.tails = append(bc.tails, tailBlock)
		bc.muTail.Unlock()
		bc.muForkLengths.Lock()
		bc.forkLengths = append(bc.forkLengths, newForkLength)
		bc.muForkLengths.Unlock()
	} else {
		bc.muTail.Lock()
		bc.tails[parentTailIndex] = tailBlock
		bc.muTail.Unlock()
		bc.muForkLengths.Lock()
		bc.forkLengths[parentTailIndex] = newForkLength
		bc.muForkLengths.Unlock()
	}

	// bc.UpdateMainTailIndex() // --->This has moved to syncer to revert the blocks and using the wallet manager htere
	// err = bc.updateMainForkMap(bc.GetMainTail())
	// if err != nil {
	// log.Error().Msgf("Could not update main fork map")
	// }
	return nil
}

// GetBlockByHash retrieves a block on the blockchain by block hash.
func (bc *Blockchain) GetBlockByHash(blockHash []byte) (*Block, error) {
	if bc == nil {
		return nil, errors.New("BlockChain Not Initilized")
	}
	blockBytes, err := bc.db.Get(BlockBucket, blockHash)
	if err != nil {
		return nil, err
	}
	block, err := NewBlockFromBytes(blockBytes)
	if err != nil {
		return nil, err
	}
	return block, nil
}

//GetBlockByHeight returns the block in the main fork for the given height
func (bc *Blockchain) GetBlockByHeight(height int64) (*Block, error) {
	var err error
	// walk := bc.GetMainTail()
	bc.muMainforMap.RLock()
	hash, ok := bc.mainForkMap[height]
	bc.muMainforMap.RUnlock()

	if !ok {
		return nil, errors.New(fmt.Sprintf("Block for height %v does not exist in main fork map", height))
	}

	block, err := bc.GetBlockByHash(hash)

	if err != nil {
		return nil, err
	}

	return block, nil

}

//BlockByHeight returns the parent of the main fork
func (bc *Blockchain) BlockHashByHeight(height int64) ([]byte, error) {
	bc.muMainforMap.RLock()
	defer bc.muMainforMap.RUnlock()

	if _, ok := bc.mainForkMap[height]; !ok {
		return []byte{}, errors.New("Block of height doesn't exist in Main Fork")
	}
	return bc.mainForkMap[height], nil
}

//BinaryDealSearch is a binary search lookup to find the deal in a block
func (bc *Blockchain) BinaryDealSearch(ts int64) (*pb.Deal, int64, error) {
	low := 0
	lastMid := -1
	// fmt.Println("bc.mainForkMap: ", bc.mainForkMap) //This is the height of the genesis block
	bc.muMainforMap.RLock()
	high := len(bc.mainForkMap) - 1 //This is the max height of the main tail
	bc.muMainforMap.RUnlock()

	for low <= high {
		median := low + (high-low)/2
		block, err := bc.GetBlockByHeight(int64(median))
		if err != nil {
			return nil, 0, err
		}

		bTs := block.GetTimestamp()

		// fmt.Println("Median: ", median, "bts: ", bTs)
		targetDeal, err := CheckDealFromBlock(block, ts)

		if targetDeal != nil { //Case where deal was found in the block
			log.Info().Msgf("Ref Deal found!!!! %d", block.Height())
			return targetDeal, block.Height(), nil

		} else if bTs > ts { //When deal was not found and the timestamp of the block is smaller
			high = median - 1
		} else {
			low = median + 1 //Otherwise
		}

		if lastMid == median {
			break
		}
		lastMid = median //In case it runs into infinite loop
	}

	return nil, 0, nil

}

//LinearDealSearch search for the deal in blockchain linearly
func (bc *Blockchain) LinearDealSearch(ts int64) (*pb.Deal, int64, error) {

	tail := bc.GetMainTail()
	for tail != nil && tail.IsGenesis() != true && tail.GetTimestamp() >= ts {
		targetDeal, _ := CheckDealFromBlock(tail, ts)
		if targetDeal != nil {
			return targetDeal, tail.Height(), nil
		}
		if tail != nil {
			tail, _ = bc.GetBlockByHash(tail.ParentHash())
		} else {
			return nil, 0, nil
		}
	}

	return nil, 0, nil
}

//CheckDealFromBlock checks the deal which matches with the timestamp
func CheckDealFromBlock(b *Block, dts int64) (*pb.Deal, error) {
	if b != nil {
		deals := b.GetDeals()

		for _, deal := range deals {
			if deal.GetTimestamp() == dts {
				return deal, nil
			}
		}
	}
	return nil, errors.New("Deal not found in the block")
}

// func getDealHash(deal *pb.Deal) []byte {
// 	dealBytes, _ := proto.Marshal(deal)
// 	dealHash := crypto.Sha256(dealBytes)
// 	return dealHash
// }

//FindLastVerification finds the similar last verification and returns the timestamp of the verification
func (bc *Blockchain) FindLastVerification(dealHeight int64, dealHash []byte) (int64, error) {

	tail := bc.GetMainTail()
	// Tail height -1 is because blocks are first added to the blockchain and then states are updated
	for i := tail.Height() - 1; i >= dealHeight; i-- { //Iterator shouldn't look beyond the deal height since verification happens after the deal
		block, err := bc.GetBlockByHeight(i)
		if err != nil {
			return 0, err
		}
		chPrPair := block.GetChPrPair()

		for _, proof := range chPrPair {
			res := bytes.Compare(proof.GetDealhash(), dealHash) //looking for the verification proof corrosponding to the deal
			if res == 0 {
				fmt.Println("Congrats. Last verification found!!!!!!!!!!!!!!") //Its a match
				return proof.Timestamp, nil
			}
		}
	}
	return 0, nil
}

// Contains returns true if the blockchain contains the block with hash blockHash.
func (bc *Blockchain) Contains(block *Block) bool {
	if block == nil || bc.db == nil {
		return false
	}
	_, err := bc.db.Get(BlockBucket, block.Hash())
	return err == nil
}

// Genesis returns the blockchain's genesis block.
func (bc *Blockchain) Genesis() *Block {
	return bc.genesis
}

// GetMainTail returns the blockchain's main tail block.
func (bc *Blockchain) GetMainTail() *Block {

	if bc.mainTailIndex == -1 {
		log.Error().Msgf("Main Tail is not set")
		return nil
	}
	bc.muTail.RLock()
	defer bc.muTail.RUnlock()

	return bc.tails[bc.mainTailIndex]
}

// Length returns the length of the blockchain, i.e., the length of the blockchain's longest fork.
func (bc *Blockchain) Length() int {
	if bc == nil {
		return 0
	}
	bc.muForkLengths.RLock()
	defer bc.muForkLengths.RUnlock()
	return bc.forkLengths[bc.mainTailIndex]
}

// NumForks returns the number of forks that the blockchain has.
func (bc *Blockchain) NumForks() int {
	bc.muTail.RLock()
	defer bc.muTail.RUnlock()
	return len(bc.tails)
}

func (bc *Blockchain) String() string {
	// TODO: use string builder instead.
	if bc != nil {
		s := fmt.Sprintf("Genesis: %s\n", bc.genesis)
		s += fmt.Sprintf("Tails:\n")
		for i, tail := range bc.tails {
			s += "\t"
			bc.muForkLengths.RLock()
			forkLength := bc.forkLengths[i]
			bc.muForkLengths.RUnlock()
			if i == bc.mainTailIndex {
				s += "MAIN "
			} else {
				s += "     " // for alignment (monospaced font only)
			}
			s += fmt.Sprintf("%d %s\n", forkLength, tail)
		}
		return s
	}

	s := "Block Chain not initilized!!"
	return s
}

// indexInTails returns the index of block in tails or -1 if block is not a tail block.
func (bc *Blockchain) indexInTails(block *Block) int {
	bc.muTail.RLock()
	t := bc.tails
	bc.muTail.RUnlock()

	for i, tail := range t {
		if block.IsSameAs(tail) {
			return i
		}
	}
	return -1
}

func (bc *Blockchain) FindCommonAncestor(block *Block) (*Block, error) {
	var err error

	// bc.mu.RLock()
	mainTail := bc.GetMainTail()
	// bc.mu.RUnlock()

	//   				/->block
	// ->->->/->->->->->tail
	for block.Height() > mainTail.Height() { // Bring the two to the same height or the newtail is smaller
		block, err = bc.GetBlockByHash(block.ParentHash())
		if err != nil {
			return nil, err
		}
	}

	// Traversing back to see if the parents of two are the same, if same return
	for {
		mainChainBlock, err := bc.GetBlockByHeight(block.Height())
		if err != nil {
			log.Info().Msgf("Could not find the parent block on main chain")
			return nil, err
		}

		if bytes.Equal(mainChainBlock.Hash(), block.Hash()) {
			break
		}
		block, err = bc.GetBlockByHash(block.ParentHash())
		if err != nil {
			log.Info().Msgf("Could not find the parent block on the chain")
			return nil, err
		}
	}
	return block, nil

}

func (bc *Blockchain) Forked(newTail *Block, newTailIndex int) ([]*Block, []*Block, error) {
	var err error
	mailTail := bc.GetMainTail()

	commonParent, err := bc.FindCommonAncestor(newTail) //Find common ancestor b/w newTail and oldMainTail
	if err != nil {
		log.Info().Msgf("Unable to find blocks to revert")
		return nil, nil, err
	}

	// These are in decreasing order which is correct in form to roll back
	// Block#17, Block#16 ...
	blocksToRollBack, err := bc.loadBlocksFromTo(mailTail, commonParent)
	if err != nil {
		return nil, nil, err
	}

	// These are in decreasing order, should be made ascending
	newerMailTailBlocks, err := bc.loadBlocksFromTo(newTail, commonParent)
	if err != nil {
		log.Info().Msgf("Unable to find blocks for new tail")
		return nil, nil, err
	}
	// Chain has moved to new main tail
	bc.setMainTailIndex(newTailIndex)

	log.Info().Msgf("Fork was successful")
	return blocksToRollBack, newerMailTailBlocks, nil
}

//FindLongestForkTail returns the tail of the longest fork
func (bc *Blockchain) FindLongestForkTail() *Block {

	longestTail := bc.GetMainTail()
	bc.muTail.RLock()
	t := bc.tails
	bc.muTail.RUnlock()

	for _, tail := range t {
		if tail.Height() > longestTail.Height() {
			longestTail = tail
		}
	}
	return longestTail
}

// updateMainTailIndex finds the longest fork and change the main tail if necessary.
func (bc *Blockchain) UpdateMainTailIndex() ([]*Block, []*Block, bool, error) {
	var err error
	var blocksToRollBack []*Block
	var blocksToProcess []*Block
	forkOccured := false
	bc.muTail.RLock()
	t := bc.tails
	bc.muTail.RUnlock()

	mainForkLength := bc.Length()
	for i, tail := range t {
		bc.muForkLengths.RLock()
		forkLength := bc.forkLengths[i]
		bc.muForkLengths.RUnlock()

		if forkLength > mainForkLength {
			blocksToRollBack, blocksToProcess, err = bc.Forked(tail, i) // i is the new main tail to be
			if err != nil {
				log.Info().Msgf("Fork switch was not successful")
				return nil, nil, false, err
			}
			forkOccured = true
			mainForkLength = forkLength
			// } else if forkLength < mainForkLength+21 { // remove chain
			// 	tailsToRemove = append(tailsToRemove, bc.tails[i])
			//}
		}
	}

	// log.Info().Msgf("\ntailsToRemove %v", tailsToRemove)
	bc.updateMainForkMap(bc.GetMainTail())
	// log.Info().Msgf("updateMainForkMap: bc.mainForkMap %v", bc.mainForkMap)
	// bc.muTail.RLock()
	// t = bc.tails
	// bc.muTail.RUnlock()

	for _, tail := range t {
		if tail.Height() < bc.IrreversibleBlock {
			bc.DeleteFork(tail)
		}
	}

	return blocksToRollBack, blocksToProcess, forkOccured, nil
}

// putBlock puts a block in the blockchain database WITHOUT checking if block is valid.
func (bc *Blockchain) putBlock(block *Block) error {
	blockBytes, err := block.ToBytes()
	if err != nil {
		return err
	}
	return bc.db.Put(BlockBucket, block.Hash(), blockBytes)
}

// batchPutBlocks puts multiple blocks in the blockchain database WITHOUT checking if block is valid
func (bc *Blockchain) batchPutBlocks(blocks []*Block) error {
	blockHashList := make([][]byte, len(blocks))
	blockBytesList := make([][]byte, len(blocks))
	for i, block := range blocks {
		blockBytes, err := block.ToBytes()
		if err != nil {
			return err
		}
		// bc.mainForkMap[block.Height()] = block.Hash()
		blockHashList[i] = block.Hash()
		blockBytesList[i] = blockBytes
	}
	return bc.db.BatchPut(BlockBucket, blockHashList, blockBytesList)
}

//GetMainForkHeight returns the height of the mainForkMap
func (bc *Blockchain) GetMainForkHeight() int {
	return len(bc.mainForkMap)
}

// loadBlocksFromTo return list of blocks including the from(block)
func (bc *Blockchain) loadBlocksFromTo(from, to *Block) ([]*Block, error) {
	var err error
	var blocks []*Block

	if bytes.Equal(from.Hash(), to.Hash()) {
		return nil, nil
	}
	for !bytes.Equal(from.Hash(), to.Hash()) {
		blocks = append(blocks, from)
		from, err = bc.GetBlockByHash(from.ParentHash())
		if err != nil {
			log.Info().Msgf("Could not find parent block")
			return nil, err
		}
	}
	return blocks, nil
}

func (bc *Blockchain) PrintFromTail(tail *Block) {
	var err error
	log.Info().Msgf("================================")
	for {
		log.Info().Msgf("Height: %d  |  Hash %s", tail.Height(), hex.EncodeToString(tail.Hash()))
		tail, err = bc.GetBlockByHash(tail.ParentHash())
		if err != nil {
			break
		}
	}
	log.Info().Msgf("================================")

}

func (bc *Blockchain) PrintMainFork() {
	log.Info().Msg("================================")
	for k, v := range bc.mainForkMap {
		log.Info().Msgf("Height[%d] Hash[%s]", k, hex.EncodeToString(v))
	}
	log.Info().Msgf("================================")
}

func (bc *Blockchain) GetTails() []*Block {
	// bc.mu.RLock()
	// defer bc.mu.RUnlock()
	bc.muTail.RLock()
	t := bc.tails
	bc.muTail.RUnlock()

	blocks := make([]*Block, 0, len(t))
	for _, tail := range t {
		blocks = append(blocks, tail)
	}
	return blocks
}

func (bc *Blockchain) IsInMainFork(block *Block) bool {
	bc.muMainforMap.RLock()
	defer bc.muMainforMap.RUnlock()
	hash, ok := bc.mainForkMap[block.Height()]
	isHashSame := bytes.Equal(hash, block.Hash())
	return ok && isHashSame
}

func (bc *Blockchain) RemoveBlock(block *Block) error {

	// This should be uncommented. Reason being, if there is
	// a block added to the chain, the transactions should be validated first.
	// This function is to be only called when removing forks.
	if bc.IsInMainFork(block) {
		log.Error().Msgf("Cannot remove block on main tail")
		return errors.New("Cannot remove block on main tail")
	}

	parentBlock, err := bc.GetBlockByHash(block.ParentHash())
	if err != nil {
		return err
	}

	tIndex := bc.indexInTails(block)
	newForkLength := parentBlock.HeightInt()
	if tIndex == -1 {
		log.Error().Msgf("Cannot delete block that is not a tail")
		return errors.New("Cannot delete block that is not a tail")
	} else {

		if bc.IsInMainFork(parentBlock) { // Delete the other tail
			bc.muTail.Lock()
			bc.tails = removeNthTail(bc.tails, tIndex)
			bc.muTail.Unlock()
			bc.muForkLengths.Lock()
			bc.forkLengths = removeNthTailLength(bc.forkLengths, tIndex)
			bc.muForkLengths.Unlock()
			m := 0
			mi := 0
			for i, e := range bc.tails {
				if i == 0 || e.HeightInt() > m {
					m = e.HeightInt()
					mi = i
				}
			}
			bc.setMainTailIndex(mi)
		} else {
			bc.muTail.Lock()
			bc.tails[tIndex] = parentBlock
			bc.muTail.Unlock()
			bc.muForkLengths.Lock()
			bc.forkLengths[tIndex] = newForkLength
			bc.muForkLengths.Unlock()
		}

	}
	err = bc.db.Delete(BlockBucket, block.Hash())
	// err = bc.removeBlockFromLRU(block)
	if err != nil {
		return err
	}
	return nil
}

func (bc *Blockchain) DeleteFork(tail *Block) error {
	_, err := bc.FindCommonAncestor(tail)
	if err != nil {
		log.Error().Msgf("No common ancestor found")
		return err
	}

	walk := tail
	for !bc.IsInMainFork(walk) {
		parentHash := walk.ParentHash()
		bc.RemoveBlock(walk)
		walk, err = bc.GetBlockByHash(parentHash)
		if err != nil {
			log.Error().Msgf("Cannot find parent")
			return err
		}
	}

	// index := bc.indexInTails(tail)

	return nil
}

// Utility Function
func removeNthTail(t []*Block, i int) []*Block {
	// s := t
	// s[len(s)-1], s[i] = s[i], s[len(s)-1]
	// return s[:len(s)-1]
	return append(t[:i], t[i+1:]...)
}

func removeNthTailLength(s []int, i int) []int {
	// s[len(s)-1], s[i] = s[i], s[len(s)-1]
	// return s[:len(s)-1]
	return append(s[:i], s[i+1:]...)
}

func (bc *Blockchain) GetChainByTail(tail *Block) ([]*Block, error) {

	var err error
	block := bc.GetMainTail()
	chain := []*Block{tail}

	for {
		if block.IsGenesis() {
			break
		}
		block, err = bc.GetBlockByHash(block.ParentHash())
		if err != nil {
			return nil, err
		}
		chain = append(chain, block)
	}

	// Reverse the order of chain slice.
	for i, j := 0, len(chain)-1; i < j; i, j = i+1, j-1 {
		chain[i], chain[j] = chain[j], chain[i]
	}
	return chain, nil
}

//BinaryRoundSearch seaerch the blocks by round and return the first block found by height
func (bc *Blockchain) BinaryRoundSearch(targetRound int64) (int64, error) {
	low := 0
	lastMid := -1
	// fmt.Println("bc.mainForkMap: ", bc.mainForkMap) //This is the height of the genesis block
	high := len(bc.mainForkMap) //This is the max height of the main tail
	for low <= high {
		median := low + (high-low)/2
		block, err := bc.GetBlockByHeight(int64(median))
		if err != nil {
			return 0, err
		}

		bRound := block.GetRoundNum()

		// fmt.Println("Median: ", median, "bts: ", bTs)
		// targetDeal, err := CheckDealFromBlock(block, ts)

		if bRound == targetRound { //Case where deal was found in the block
			log.Info().Msgf("Target block found!!!! %d", block.Height())
			return block.Height(), nil

		} else if bRound > targetRound { //When deal was not found and the timestamp of the block is smaller
			high = median - 1
		} else {
			low = median + 1 //Otherwise
		}

		if lastMid == median {
			break
		}
		lastMid = median //In case it runs into infinite loop
	}

	return 0, nil
}

//GetLastMiner returns the last miner by height of the block
func (bc *Blockchain) GetLastMiner(refH, refRound int64, refPk string) (*Block, string, error) {

	for i := refH - 1; i >= 0; i-- {
		// fmt.Println("I is", i)
		b, err := bc.GetBlockByHeight(i)
		if err != nil {
			return nil, "", err
		}
		bMiner := b.MinerPublicKey().String()
		if bMiner != refPk || refRound != b.GetRoundNum() {
			return b, bMiner, nil
		}
	}
	return nil, "", nil //Against the first miner
}

//GetFirstBlockOfMiner returns the first block of the miner
func (bc *Blockchain) GetFirstBlockOfMiner(refB *Block, refH int64, refPk string) (*Block, error) {

	for i := refH - 1; i >= 0; i-- {
		b, err := bc.GetBlockByHeight(i)
		if err != nil {
			return nil, err
		}
		bMiner := b.MinerPublicKey().String()
		if bMiner != refPk || refB.GetRoundNum() != b.GetRoundNum() {
			if i == refH-1 {
				return refB, nil
			}
			reqBlock, err := bc.GetBlockByHeight(i + 1)
			if err != nil {
				return nil, err
			}
			return reqBlock, nil
		}
	}
	return nil, errors.New("Not found")
}

//CheckLastMinerBlocks checks if last miner has produced all the blocks
func (bc *Blockchain) CheckLastMinerBlocks(curMiner string, maxBlockPerMiner int64) error {
	var err error
	tail := bc.GetMainTail()
	if tail == nil || tail.GetRoundNum() == 1 { //Exempt the first round
		return nil
	}
	for tail.MinerPublicKey().String() != curMiner {
		parent := tail.ParentHash()
		tail, err = bc.GetBlockByHash(parent)
		if err != nil {
			return err
		}
	}
	//Got the block with the minerPK != currentMiner
	lastMBheight := tail.Height() - maxBlockPerMiner - 1
	firstMinerB, err := bc.GetBlockByHeight(lastMBheight)
	if err != nil {
		return err
	}
	if tail.MinerPublicKey().String() == firstMinerB.MinerPublicKey().String() {
		return nil
	}
	return errors.New("Yet to receive blocks of last miner")
}

func (bc *Blockchain) GetLastNBlocks(n int) []*pb.Block {

	nBlocks := make([]*pb.Block, 0)
	tailHeight := bc.GetMainTail().Height()

	for i := n; i > 0; i-- {
		val, ok := bc.mainForkMap[tailHeight]
		if !ok {
			log.Error().Msgf("Value for height does not exist in main fork map")
			break
		} else {
			b, err := bc.GetBlockByHash(val)
			if err != nil {
				log.Error().Err(err)
			}
			nBlocks = append(nBlocks, b.PbBlock)

		}

		tailHeight--
	}

	return nBlocks
}

func (bc *Blockchain) UpdateIrreversibleBlockNumber(minerAdd string, ComitteeSize uint32, NumberOfToMineBlockPerSlot int) {

	size2by3 := int((2 * ComitteeSize) / 3)
	log.Info().Msgf("bc.ComitteeSize ComitteeSize %v size2by3 %v", ComitteeSize, size2by3)

	// if bc.Last2by3MinedCommitteeMembers.Len() == 0 || bc.Last2by3MinedCommitteeMembers.Len() < size2by3 {
	// bc.Last2by3MinedCommitteeMembers.PushBack(minerAdd)
	// return
	// }

	if bc.Last2by3MinedCommitteeMembers.Len() == 0 || bc.Last2by3MinedCommitteeMembers.Len() < size2by3*NumberOfToMineBlockPerSlot {
		bc.Last2by3MinedCommitteeMembers.PushBack(minerAdd)
		return
	}
	// } else {

	bc.Last2by3MinedCommitteeMembers.Remove(bc.Last2by3MinedCommitteeMembers.Front())
	bc.Last2by3MinedCommitteeMembers.PushBack(minerAdd)
	// }

	keys := make(map[string]bool)
	list := []string{}
	for e := bc.Last2by3MinedCommitteeMembers.Front(); e != nil; e = e.Next() {
		entry := e.Value.(string)
		// log.Info().Msgf("bc.IrreversibleBlock entry: %v", entry)
		if _, value := keys[entry]; !value {
			keys[entry] = true
			list = append(list, entry)
		}
	}

	// keys := make(map[string]bool)
	// keys1 := make(map[string]bool)
	// list := []string{}
	// for e := bc.Last2by3MinedCommitteeMembers.Front(); e != nil; e = e.Next() {
	// 	entry := e.Value.(string)
	// 	log.Info().Msgf("bc.IrreversibleBlock entry: %v", entry)
	// 	if _, value := keys[entry]; !value {
	// 		keys[entry] = true
	// 	} else {
	// 		if _, value1 := keys1[entry]; !value1 {
	// 			keys1[entry] = true
	// 			list = append(list, entry)
	// 		}
	// 	}
	// }

	// if len(list) >= size2by3 && bc.GetMainForkHeight()-int(bc.IrreversibleBlock) >= size2by3 {
	if len(list) >= size2by3 {
		bc.IrreversibleBlock += 1
	}

	log.Info().Msgf("bc.IrreversibleBlock UPdated: %v size2by3 %v bc.GetMainForkHeight() %v diff %v", bc.IrreversibleBlock, size2by3, bc.GetMainForkHeight(), bc.GetMainForkHeight()-int(bc.IrreversibleBlock))
}

func (bc *Blockchain) UpdateIrreversibleBlockNumberWhileReverting(minerAdd string) {

	lastMinerAddress := bc.Last2by3MinedCommitteeMembers.Back()
	if lastMinerAddress != nil {
		bc.Last2by3MinedCommitteeMembers.Remove(lastMinerAddress)
		if lastMinerAddress.Value.(string) != minerAdd {
			log.Error().Msgf("UpdateIrreversibleBlockNumberWhileReverting Invalid Miner %v got deleted in Bc.Last2by3MinedCommitteeMembers insted of %v ", lastMinerAddress.Value.(string), minerAdd)
		}
	} else {
		bc.IrreversibleBlock -= 1
	}

	log.Info().Msgf("bc.IrreversibleBlock revert UPdated: %v", bc.IrreversibleBlock)
}

func (bc *Blockchain) GetLastMinedCommitteeMemberLength() int {
	return bc.Last2by3MinedCommitteeMembers.Len()
}
