package rpc

import (
	"context"
	"encoding/hex"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"sort"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/guyu96/go-timbre/core"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	"github.com/guyu96/go-timbre/protobuf"
	"github.com/guyu96/go-timbre/roles"
	rpcpb "github.com/guyu96/go-timbre/rpc/pb"
	"github.com/guyu96/go-timbre/wallet"
	"github.com/mbilal92/noise"
	"golang.org/x/crypto/bcrypt"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Bucket for user login/reiger
var (
	userBucket      []byte // name for node info storage bucket
	modBucket       rpcpb.Moderators
	firstMod        bool
	modRules        string
	postToVotes     map[string][]*rpcpb.Vote
	postToModStatus map[string][]*rpcpb.ModStatus
	// postToModStatus
	jwtKey []byte
)

func init() {
	userBucket = []byte("user-bucket")
	firstMod = false
	jwtKey = []byte("alizohaib")
	postToVotes = make(map[string][]*rpcpb.Vote)
	postToModStatus = make(map[string][]*rpcpb.ModStatus)
}

type User struct {
	PasswordHash string
	IsModerator  bool
	Echoes       []string
	Filters      []string
	Following    []string
	Followers    []string
	Id           string
}

type Api struct {
	node *net.Node
}

func NewAPI(node *net.Node) *Api {
	return &Api{
		node: node,
	}
}

// TODO: Error handling for each
func (s *Api) GetBlock(ctx context.Context, req *rpcpb.GetBlockRequest) (*rpcpb.Block, error) {

	var block *core.Block
	var err error

	if s.node.Bc == nil {
		return nil, status.Error(codes.NotFound, "Not Found")
	}

	if req.Hash != "" {
		by, err := hex.DecodeString(req.Hash)
		block, err = s.node.Bc.GetBlockByHash(by)
		if err != nil || block == nil {
			return nil, status.Error(codes.InvalidArgument, "Incorrect Argument")
		}
	}

	if req.Height == 0 {
		block = s.node.Bc.Genesis()
	}

	if req.Height != 0 {
		block, err = s.node.Bc.GetBlockByHeight(req.Height)
		if err != nil || block == nil {
			return nil, status.Error(codes.InvalidArgument, "Incorrect Argument")
		}
	}

	if block == nil {
		return nil, status.Error(codes.InvalidArgument, "Not Found")
	}

	return &rpcpb.Block{
		Height:         block.Height(),
		Hash:           hex.EncodeToString(block.Hash()),
		ParentHash:     hex.EncodeToString(block.ParentHash()),
		Timestamp:      block.GetTimestamp(),
		MinerPublicKey: hex.EncodeToString(block.PbBlock.Header.GetMinerPublicKey()),
		RoundNum:       block.GetRoundNum(),
	}, nil

}

func (s *Api) GetNBlocks(ctx context.Context, req *rpcpb.GetNBlocksRequest) (*rpcpb.Blocks, error) {
	if s.node.Bc == nil {
		return nil, status.Error(codes.Internal, "Blockchain is empty")
	}
	blocks := s.node.Bc.GetLastNBlocks(5)

	return &rpcpb.Blocks{
		Blocks: blocks,
	}, nil
}

// asd
func (s *Api) GetState(ctx context.Context, req *rpcpb.NonParamRequest) (*rpcpb.State, error) {

	if s.node.Bc == nil {
		return nil, nil
	}

	return &rpcpb.State{
		Tail:   s.node.Bc.GetMainTail().String(),
		Height: int64(s.node.Bc.Length()),
		Forks:  int64(s.node.Bc.NumForks()),
	}, nil
}

func (s *Api) GetAccount(ctx context.Context, req *rpcpb.NonParamRequest) (*rpcpb.Account, error) {

	return &rpcpb.Account{ //TODO:ChangeToPK -> review
		Address:  s.node.PublicKey().String(),
		Balance:  int64(s.node.Wm.MyWallet.GetBalance()),
		NodeType: s.node.GetNodeType(),
	}, nil
}

func (s *Api) SendStorageOffer(ctx context.Context, req *rpcpb.SendStorageOfferRequest) (*rpcpb.SendStorageOfferResponse, error) {

	if s.node.StorageProvider == nil {
		return nil, status.Error(codes.Internal, "Node is not a valid storage provider")
	}

	if req.Size < 200000000 {
		s.node.StorageProvider.SendStorageOffer(req.MinPrice, req.MaxDuration, req.Size)
	}

	return &rpcpb.SendStorageOfferResponse{
		Success: "1",
	}, nil
}

// func (s *Apu) RemovePostFromSP()
func (s *Api) RemovePostSP(ctx context.Context, req *rpcpb.RemovePostSpRequest) (*rpcpb.RemovePostSpResponse, error) {

	if s.node.StorageProvider == nil {
		return nil, status.Error(codes.Internal, "Node is not a valid storage provider")
	}

	deleted := s.node.StorageProvider.DeletePost(req.PostHash)
	return &rpcpb.RemovePostSpResponse{
		Deleted: deleted,
	}, nil
}

// Todo: Check if not miner
func (s *Api) GetCommittee(ctx context.Context, req *rpcpb.NonParamRequest) (*rpcpb.Candidates, error) {

	miners := s.node.Dpos.GetMinersAddresses()
	log.Info().Msgf("Asked for candidates......")
	return &rpcpb.Candidates{
		Candidates: miners,
	}, nil
}

func (s *Api) GetThread(ctx context.Context, req *rpcpb.GetThreadRequest) (*rpcpb.Thread, error) {

	go s.node.User.GetThread(req.Threadroothash) // Ask for the thread on the network
	thread, err := s.node.User.GetThreadFromCache(req.Threadroothash)
	if err != nil {
		log.Error().Msgf("Thread not found %v", req.Threadroothash)
		return nil, errors.New("Thread could not be found")
	}

	val, ok := postToVotes[req.Threadroothash]
	if ok {
		thread[0].Votes = val
	}

	val2, ok2 := postToModStatus[req.Threadroothash]
	if ok2 {
		thread[0].Tags = val2
	}

	return &rpcpb.Thread{Posts: thread}, nil
}

func (s *Api) GetPagedThreads(ctx context.Context, req *rpcpb.GetPagedThreadsRequest) (*rpcpb.Threads, error) {

	go s.node.User.GetThreads()
	threads := make([]*rpcpb.Thread, 0)

	count := 0
	s.node.ThreadBaseMap.Range(func(key interface{}, val interface{}) bool {
		count++

		thread, err := s.node.User.GetThreadFromCache(key.(string))
		hash := key.(string)
		val, ok := postToVotes[hash]
		if ok {
			thread[0].Votes = val.([]*rpcpb.Vote)
		}

		val2, ok2 := postToModStatus[hash]
		if ok2 {
			thread[0].Tags = val2
		}

		if err != nil {
			log.Info().Msgf("Thread could not be found. Moving to next thread")
			return true
		}

		if thread == nil {
			return true
		}

		threads = append(threads, &rpcpb.Thread{Posts: thread})

		return true
	})

	sort.SliceStable(threads, func(i, j int) bool {
		return threads[i].GetPosts()[0].GetTimestamp() < threads[j].GetPosts()[0].GetTimestamp()
	})

	var selectedThreads []*rpcpb.Thread
	for i, thread := range threads {
		if req.Start == req.End || (int32(i) >= req.Start && int32(i) < req.End) {
			selectedThreads = append(selectedThreads, thread)
		}
	}

	return &rpcpb.Threads{
		Threads: selectedThreads,
	}, nil

}

func (s *Api) GetThreads(ctx context.Context, req *rpcpb.NonParamRequest) (*rpcpb.Threads, error) {

	go s.node.User.GetThreads() // Asks for threads on the network
	// var threads []*rpcpb.Thread
	threads := make([]*rpcpb.Thread, 0)

	count := 0
	s.node.ThreadBaseMap.Range(func(key interface{}, val interface{}) bool {
		count++

		thread, err := s.node.User.GetThreadFromCache(key.(string))
		hash := key.(string)
		val, ok := postToVotes[hash]
		if ok {
			thread[0].Votes = val.([]*rpcpb.Vote)
		}

		val2, ok2 := postToModStatus[hash]
		if ok2 {
			thread[0].Tags = val2
		}

		if err != nil {
			log.Info().Msgf("Thread could not be found. Moving to next thread")
			return true
		}

		if thread == nil {
			return true
		}

		threads = append(threads, &rpcpb.Thread{Posts: thread})

		return true
	})

	log.Error().Msgf("Number of threads %v", count)

	// if len(threads) == 0 {
	// 	// No threads
	// 	return threads, nil
	// }

	return &rpcpb.Threads{
		Threads: threads,
	}, nil

}

func (s *Api) SendPost(ctx context.Context, req *rpcpb.SendPostRequest) (*rpcpb.PostHash, error) {
	phash, _ := hex.DecodeString(req.ParentPostHash)
	threadheadPosthashh, _ := hex.DecodeString(req.ThreadheadPostHash)
	fmt.Println("SENTTTT")
	h := s.node.User.SendPostRequest([]byte(req.Content), phash, threadheadPosthashh, req.MaxCost, req.MinDuration, s.node.PublicKey(), s.node.PrivateKey())
	return &rpcpb.PostHash{
		Hash: h,
	}, nil

}

func (s *Api) ChangeRole(ctx context.Context, req *rpcpb.ChangeRoleRequest) (*rpcpb.ChangeRoleResponse, error) {

	if req.Role == "SP" {
		if s.node.StorageProvider == nil {
			s.node.SetAndStartStorageProvider(roles.NewStorageProvider(s.node))
		}
	} else if req.Role == "MINER" {
		if s.node.Miner == nil {
			s.node.SetAndStartMiner(roles.NewMiner(s.node))
		}
	}
	return &rpcpb.ChangeRoleResponse{
		Success: 1,
	}, nil
}

func (s *Api) TransferMoney(ctx context.Context, req *rpcpb.TransferMoneyRequest) (*rpcpb.MoneyTransferResponse, error) {

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	log.Info().Msgf("TransferMoney: Just want to check: %s %d", req.Port, req.Amount)

	err := s.node.Services.DoAmountTrans(ctx, req.Port, req.Amount)
	if err != nil {
		log.Info().Err(err)
	}
	return &rpcpb.MoneyTransferResponse{
		Success: 1,
	}, nil
}

func (s *Api) GetStorageProviderStats(ctx context.Context, req *rpcpb.NonParamRequest) (*rpcpb.StorageProviderStats, error) {

	if s.node.StorageProvider == nil {
		return nil, status.Error(codes.Internal, "Node is not a valid storage provider")
	}

	stats := s.node.StorageProvider.GetStorageProviderStats()
	return stats, nil

}

func (s *Api) SendLastStorageOffer(ctx context.Context, req *rpcpb.NonParamRequest) (*protobuf.StorageOffer, error) {

	if s.node.StorageProvider != nil {
		return s.node.StorageProvider.GetLastStorageOffer(), nil
	} else {
		return nil, status.Error(codes.Internal, "Node is not a valid storage provider")
	}
}

// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------
// INDEXER FUNCTIONS -> Because the api file is the same for both client and indexer
// --------------------------------------------------------------------------------
// --------------------------------------------------------------------------------

func isAuth(token string) (*Claims, error) {

	claims := &Claims{}

	tkn, err := jwt.ParseWithClaims(token, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		return nil, err
	}

	if !tkn.Valid {
		return nil, err
	}
	return claims, nil
}

// These functions would only called via the indexer
func (s *Api) Register(ctx context.Context, req *rpcpb.RegisterRequest) (*rpcpb.User, error) {

	if !s.node.Db.HasBucket(userBucket) {
		return nil, status.Error(codes.Internal, "User Bucket not found")
	}

	val, _ := s.node.Db.Get(userBucket, []byte(req.Username))

	// User doesn't exist
	if val == nil {

		hash, err := bcrypt.GenerateFromPassword([]byte(req.Password), 2)
		if err != nil {
			return nil, status.Error(codes.Internal, "Could not hash the password")
		}

		isMod := false
		if firstMod == false {
			// firstMod = true //NOW EVERYONE WILL BE A MOD
			isMod = true
			// modBucket.Moderators = append(modBucket.Moderators, &rpcpb.Moderator{Username: req.Username})
			// fmt.Println("First Moderator was added")
		}

		hs := hex.EncodeToString(hash)
		user := User{PasswordHash: hs, IsModerator: isMod, Id: req.Username}

		encoded, err := json.Marshal(user)
		if err != nil {
			return nil, status.Error(codes.Internal, "Could not encde the password hash")
		}

		err = s.node.Db.Put(userBucket, []byte(req.Username), encoded)
		if err != nil {
			return nil, status.Error(codes.Internal, "Could not store user data")
		}

		// Generate keypair and save
		var keys net.Keypair
		keys.PublicKey, keys.PrivateKey, err = noise.GenerateKeys(nil)
		// keypair := kad.RandomKeys()
		// kad.PersistKeypairs("../Keys/Keys"+fmt.Sprintf("%s", req.Username)+".txt", []*kad.Keypair{keypair})
		noise.PersistKey("../Keys/Keys"+fmt.Sprintf("%s", req.Username)+".txt", keys.PrivateKey)

		wallet := wallet.NewWallet(req.Username)
		s.node.Wm.PutWalletByAddress(keys.PublicKey.String(), wallet)

		token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
			"username":    req.Username,
			"IsModerator": isMod,
		})

		tokenString, err := token.SignedString(jwtKey)

		if err != nil {
			return nil, status.Error(codes.Internal, "Could not generate token")
		}

		return &rpcpb.User{
			Token:       tokenString,
			IsModerator: isMod,
			Echoes:      nil,
			Filters:     nil,
			Following:   nil,
			Followers:   nil,
		}, nil
	}

	return nil, status.Error(codes.AlreadyExists, "User already exists")
}

type Claims struct {
	Username    string `json:"username"`
	IsModerator bool   `json:"IsModerator"`
	jwt.StandardClaims
}

func (s *Api) Login(ctx context.Context, req *rpcpb.LoginRequest) (*rpcpb.User, error) {

	if !s.node.Db.HasBucket(userBucket) {
		return nil, status.Error(codes.Internal, "User Bucket not found")
	}

	userjson, err := s.node.Db.Get(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}

	var user User
	err = json.Unmarshal(userjson, &user)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	hb, _ := hex.DecodeString(user.PasswordHash)
	err = bcrypt.CompareHashAndPassword(hb, []byte(req.Password))

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Incorrect password")
	}

	expirationTime := time.Now().Add(35 * time.Minute)

	claims := &Claims{
		Username:    req.Username,
		IsModerator: user.IsModerator,
		StandardClaims: jwt.StandardClaims{
			// In JWT, the expiry time is expressed as unix milliseconds
			ExpiresAt: expirationTime.Unix(),
		},
	}

	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)

	tokenString, err := token.SignedString([]byte("alizohaib"))

	if err != nil {
		return nil, err
	}

	return &rpcpb.User{
		Token:       tokenString,
		IsModerator: user.IsModerator,
		Echoes:      user.Echoes,
		Filters:     user.Filters,
		Following:   user.Following,
		Followers:   user.Followers,
	}, nil
}

func (s *Api) GetAccountViaUsername(ctx context.Context, req *rpcpb.GetAccountViaUsernameReq) (*rpcpb.Account, error) {

	path := "../Keys/Keys" + fmt.Sprintf("%s", req.Username) + ".txt"
	// Check if keypair file exists
	_, err := os.Stat(path)

	if err != nil {
		return nil, status.Error(codes.NotFound, "Keys File does not exist")
	}

	// Find public key
	// keysFromFile := kad.LoadKeypairs(path)
	// keys := keysFromFile[0]
	var keys net.Keypair
	keys.PrivateKey = noise.LoadKey(path)
	keys.PublicKey = keys.PrivateKey.Public()
	pkString := keys.PublicKey.String()

	// Find address
	w, err := s.node.Wm.GetWalletByAddress(pkString)
	if err != nil {
		return nil, status.Error(codes.NotFound, "Wallet not found for username")
	}

	return &rpcpb.Account{
		Address:  pkString,
		Balance:  int64(w.GetBalance()),
		NodeType: "Subscriber",
	}, nil
}

func (s *Api) SendPostViaIndexer(ctx context.Context, req *rpcpb.SendPostRequestViaIndexer) (*rpcpb.PostHash, error) {

	// GetPublicKeyVia username
	// keysFromFile := kad.LoadKeypairs("../Keys/Keys" + fmt.Sprintf("%s", req.Username) + ".txt")
	// keys := keysFromFile[0]
	var keys net.Keypair
	keys.PrivateKey = noise.LoadKey("../Keys/Keys" + fmt.Sprintf("%s", req.Username) + ".txt")
	keys.PublicKey = keys.PrivateKey.Public()
	// pk := keys.PublicKey[:] //keys.PublicKey()
	// privateKey := keys.PrivateKey[:]
	phash, _ := hex.DecodeString(req.ParentPostHash)
	threadheadPosthashh, _ := hex.DecodeString(req.ThreadheadPostHash)
	h := s.node.User.SendPostRequest([]byte(req.Content), phash, threadheadPosthashh, req.MaxCost, req.MinDuration, keys.PublicKey, keys.PrivateKey)
	return &rpcpb.PostHash{
		Hash: h,
	}, nil

}

func (s *Api) SendVoteViaIndexer(ctx context.Context, req *rpcpb.SendVoteRequestViaIndexer) (*rpcpb.Votes, error) {

	err := s.node.Services.DoPercentVoteDuplicate(ctx, req.PublicKey, req.Percent)

	if err != nil {
		return nil, status.Error(codes.NotFound, "Could not vote")
	}
	return &rpcpb.Votes{
		PublicKey: req.PublicKey,
	}, nil
}

func (s *Api) LikePost(ctx context.Context, req *rpcpb.LikeAPostRequest) (*rpcpb.LikeAPostResponse, error) {
	// s.node.User.UpdateLikes(req.Hash, req.Votes)
	// req.Hash
	claims, err := isAuth(req.Token)
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Unauthenticated Request")
	}

	at := -1
	for i, vote := range postToVotes[req.Hash] {
		// Vote already exists
		if vote.User == claims.Username {
			at = i
		}
	}
	// Vote already exists
	if at != -1 {
		if req.Votes == 0 {
			postToVotes[req.Hash] = append(postToVotes[req.Hash][:at], postToVotes[req.Hash][at+1:]...)
		} else {
			postToVotes[req.Hash][at] = &rpcpb.Vote{User: claims.Username, Vote: req.Votes}
		}
	} else {
		postToVotes[req.Hash] = append(postToVotes[req.Hash], &rpcpb.Vote{User: claims.Username, Vote: req.Votes})
	}

	return &rpcpb.LikeAPostResponse{
		Hash: req.Hash,
	}, nil
}

func (s *Api) TagPost(ctx context.Context, req *rpcpb.TagPostRequest) (*rpcpb.Thread, error) {

	claims, err := isAuth(req.Token)
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Unauthenticated Request")
	}

	// Check if user is a moderator
	userjson, err := s.node.Db.Get(userBucket, []byte(claims.Username))
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}
	var user User
	err = json.Unmarshal(userjson, &user)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	if !user.IsModerator {
		return nil, status.Error(codes.Unauthenticated, "User is not a moderator")
	}

	at := -1
	for i, modstatus := range postToModStatus[req.Hash] {
		// Vote already exists
		if modstatus.User == claims.Username {
			at = i
		}
	}

	// Tag already exists
	if at != -1 {
		// if req.Votes == 0 {
		// 	postToVotes[req.Hash] = append(postToVotes[req.Hash][:at], postToVotes[req.Hash][at+1:]...)
		// } else {
		postToModStatus[req.Hash][at] = &rpcpb.ModStatus{User: claims.Username, Tags: req.Tag}
		// }
	} else {
		postToModStatus[req.Hash] = append(postToModStatus[req.Hash], &rpcpb.ModStatus{User: claims.Username, Tags: req.Tag})
	}

	thread, err := s.node.User.GetThreadFromCache(req.Hash)
	if err != nil {
		log.Error().Msgf("Thread not found %v", req.Hash)
		return nil, errors.New("Thread could not be found")
	}

	val, ok := postToVotes[req.Hash]
	if ok {
		thread[0].Votes = val
	}

	val2, ok2 := postToModStatus[req.Hash]
	if ok2 {
		thread[0].Tags = val2
	}

	return &rpcpb.Thread{Posts: thread}, nil

}

func (s *Api) RegisterAsMod(ctx context.Context, req *rpcpb.RegisterAsModRequest) (*rpcpb.Moderators, error) {

	// Todo: addchecks if the user even exists in the db
	// Update Db
	userjson, err := s.node.Db.Get(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}
	var user User
	err = json.Unmarshal(userjson, &user)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	if user.IsModerator {
		return nil, status.Error(codes.Internal, "User is already a moderator")
	}

	err = s.node.Db.Delete(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Internal, "Cannot delete to update user from db")
	}

	user.IsModerator = true
	user.Filters = append(user.Filters, req.Filters)

	encoded, err := json.Marshal(user)

	if err != nil {
		return nil, status.Error(codes.Internal, "Could not encde the usr")
	}

	err = s.node.Db.Put(userBucket, []byte(req.Username), encoded)
	if err != nil {
		return nil, status.Error(codes.Internal, "Could not update user")
	}

	// modBucket.Moderators = append(modBucket.Moderators, &rpcpb.Moderator{Username: req.Username})

	return &modBucket, nil
}

func (s *Api) GetModerators(ctx context.Context, req *rpcpb.NonParamRequest) (*rpcpb.Moderators, error) {

	var allusers []*rpcpb.User
	s.node.Db.ForEach(userBucket, func(username, jsonuser []byte) error {

		var user2 User
		err := json.Unmarshal(jsonuser, &user2)

		var posts []string
		for hash, modstatuses := range postToModStatus {
			for _, ms := range modstatuses {
				if ms.User == user2.Id {
					posts = append(posts, hash)
				}
			}
		}

		allusers = append(allusers, &rpcpb.User{Id: user2.Id, IsModerator: user2.IsModerator, Followers: user2.Followers, Following: user2.Following, Filters: posts})

		if err != nil {
			return status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
		}

		return nil
	})
	return &rpcpb.Moderators{Moderators: allusers}, nil
}

func (s *Api) GetModeratorProf(ctx context.Context, req *rpcpb.GetModeratorProfReq) (*rpcpb.ModeratorStream, error) {

	if !s.node.Db.HasBucket(userBucket) {
		return nil, status.Error(codes.Internal, "User Bucket not found")
	}

	userjson, err := s.node.Db.Get(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}
	user := User{}
	err = json.Unmarshal(userjson, &user)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	if user.IsModerator {

		var posts []string
		for hash, modstatuses := range postToModStatus {
			for _, ms := range modstatuses {
				if ms.User == req.Username {
					posts = append(posts, hash)
				}
			}
		}

		var allposts []*rpcpb.Post

		for _, hash := range posts {
			thread, err := s.node.User.GetThreadFromCache(hash)
			if err != nil {
				log.Error().Msgf("Thread not found %v", hash)
				return nil, errors.New("Thread could not be found")
			}

			val, ok := postToVotes[hash]
			if ok {
				thread[0].Votes = val
			}

			val2, ok2 := postToModStatus[hash]
			if ok2 {
				thread[0].Tags = val2
			}

			allposts = append(allposts, thread[0])
		}

		var keys net.Keypair
		keys.PrivateKey = noise.LoadKey("../Keys/Keys" + fmt.Sprintf("%s", req.Username) + ".txt")
		keys.PublicKey = keys.PrivateKey.Public()

		return &rpcpb.ModeratorStream{
			ModeratorId:    req.Username,
			ModeratedPosts: allposts,
			Followers:      user.Followers,
			Echoes:         user.Echoes,
			Following:      user.Following,
			PublicKey:      keys.PublicKey.String(),
		}, nil
	}
	return nil, status.Error(codes.Unauthenticated, "User is not a moderator")

}

func (s *Api) AddRegexFilterToMod(ctx context.Context, req *rpcpb.AddRegexFilterToModRequest) (*rpcpb.Moderator, error) {

	userjson, err := s.node.Db.Get(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}
	var user User
	err = json.Unmarshal(userjson, &user)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	err = s.node.Db.Delete(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Internal, "Cannot delete to update user from db")
	}

	user.Filters = append(user.Filters, req.Filters)

	encoded, err := json.Marshal(user)

	if err != nil {
		return nil, status.Error(codes.Internal, "Could not encde the usr")
	}

	err = s.node.Db.Put(userBucket, []byte(req.Username), encoded)
	if err != nil {
		return nil, status.Error(codes.Internal, "Could not update user")
	}

	return &rpcpb.Moderator{
		Username: req.Username,
		Echoes:   user.Echoes,
		Filters:  user.Filters,
	}, nil

}

func RemoveIndex(s []string, index uint32) []string {
	return append(s[:index], s[index+1:]...)
}

func (s *Api) RemoveRegexFilterFromMod(ctx context.Context, req *rpcpb.RemoveRegexFilterFromModRequest) (*rpcpb.Moderator, error) {

	userjson, err := s.node.Db.Get(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}
	var user User
	err = json.Unmarshal(userjson, &user)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	err = s.node.Db.Delete(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Internal, "Cannot delete to update user from db")
	}

	user.Filters = RemoveIndex(user.Filters, req.Index)

	encoded, err := json.Marshal(user)

	if err != nil {
		return nil, status.Error(codes.Internal, "Could not encde the usr")
	}

	err = s.node.Db.Put(userBucket, []byte(req.Username), encoded)
	if err != nil {
		return nil, status.Error(codes.Internal, "Could not update user")
	}

	return &rpcpb.Moderator{
		Username: req.Username,
		Echoes:   user.Echoes,
		Filters:  user.Filters,
	}, nil

}

func contains(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func remove(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

func (s *Api) UpdateEchoesOnMod(ctx context.Context, req *rpcpb.UpdateEchoesOnModRequest) (*rpcpb.Moderator, error) {

	userjson, err := s.node.Db.Get(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}
	var user User
	err = json.Unmarshal(userjson, &user)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	err = s.node.Db.Delete(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Internal, "Cannot delete to update user from db")
	}

	if contains(user.Echoes, req.PostHash) {
		user.Echoes = remove(user.Echoes, req.PostHash)
	} else {
		user.Echoes = append(user.Echoes, req.PostHash)
	}

	encoded, err := json.Marshal(user)

	if err != nil {
		return nil, status.Error(codes.Internal, "Could not encde the usr")
	}

	err = s.node.Db.Put(userBucket, []byte(req.Username), encoded)
	if err != nil {
		return nil, status.Error(codes.Internal, "Could not update user")
	}

	return &rpcpb.Moderator{
		Username: req.Username,
		Echoes:   user.Echoes,
		Filters:  user.Filters,
	}, nil

}

func (s *Api) UpdateFollowing(ctx context.Context, req *rpcpb.UpdateFollowingRequest) (*rpcpb.Moderator, error) {

	userjson, err := s.node.Db.Get(userBucket, []byte(req.Username))

	userjson2, err := s.node.Db.Get(userBucket, []byte(req.Following))

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}
	var user User
	var user2 User
	err = json.Unmarshal(userjson, &user)
	err = json.Unmarshal(userjson2, &user2)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	err = s.node.Db.Delete(userBucket, []byte(req.Username))
	err = s.node.Db.Delete(userBucket, []byte(req.Following))
	if err != nil {
		return nil, status.Error(codes.Internal, "Cannot delete to update user from db")
	}

	if contains(user.Following, req.Following) {
		user.Following = remove(user.Following, req.Following)
		user2.Followers = remove(user2.Followers, req.Username)
	} else {
		user.Following = append(user.Following, req.Following)
		user2.Followers = append(user2.Followers, req.Username)
	}

	encoded, err := json.Marshal(user)
	encoded2, err := json.Marshal(user2)

	if err != nil {
		return nil, status.Error(codes.Internal, "Could not encde the usr")
	}

	err = s.node.Db.Put(userBucket, []byte(req.Username), encoded)
	err = s.node.Db.Put(userBucket, []byte(req.Following), encoded2)
	if err != nil {
		return nil, status.Error(codes.Internal, "Could not update user")
	}

	return &rpcpb.Moderator{
		Username:  req.Following,
		Echoes:    user2.Echoes,
		Filters:   user2.Filters,
		Followers: user2.Followers,
	}, nil

}

func (s *Api) GetAllFilters(ctx context.Context, req *rpcpb.AllFiltersRequest) (*rpcpb.AllFilters, error) {

	userjson, err := s.node.Db.Get(userBucket, []byte(req.Username))
	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Username doesn't exist")
	}
	var user User
	err = json.Unmarshal(userjson, &user)

	if err != nil {
		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	}

	var allFilters []string
	var allEchoes []string

	allFilters = append(allFilters, user.Filters...)
	allEchoes = append(allEchoes, user.Echoes...)

	var allusers []User

	s.node.Db.ForEach(userBucket, func(username, jsonuser []byte) error {

		var user2 User
		err = json.Unmarshal(jsonuser, &user2)

		allusers = append(allusers, user2)

		if err != nil {
			return status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
		}

		// allFilters = append(allFilters, user2.Filters...)
		// allEchoes = append(allEchoes, user2.Echoes...)

		return nil
	})
	sort.Slice(allusers, func(i, j int) bool {
		return len(allusers[i].Followers) > len(allusers[j].Followers)
	})

	if len(allusers) > 10 {
		allusers = allusers[:10]
	}

	for _, u := range allusers {
		allFilters = append(allFilters, u.Filters...)
		allEchoes = append(allEchoes, u.Echoes...)
	}
	// for _, u := range user.Following {
	// 	jsonuser, err := s.node.Db.Get(userBucket, []byte(u))

	// 	var user2 User
	// 	err = json.Unmarshal(jsonuser, &user2)

	// 	if err != nil {
	// 		return nil, status.Error(codes.Unauthenticated, "Cannot retrieve user from db")
	// 	}

	// 	allFilters = append(allFilters, user2.Filters...)
	// 	allEchoes = append(allEchoes, user2.Echoes...)
	// }

	return &rpcpb.AllFilters{
		Filters: allFilters,
		Echoes:  allEchoes,
	}, nil
}
