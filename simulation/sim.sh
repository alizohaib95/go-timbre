#!/bin/bash

# Change the path to your relevent go-timbre/simulation/main file --> home/$user/....
simpath="$PWD/scripts/"
portbase="50000"
comsize="2"

echo "Running simulation at" "$simpath"
pwd

gnome-terminal --working-directory="$simpath" -- /bin/bash -c "./node.sh"
# sleep 1
# gnome-terminal --working-directory="$simpath" -- /bin/bash -c "expect ./miner.sh $comsize 50006"
# sleep 1
# gnome-terminal --working-directory="$simpath" -- /bin/bash -c "expect ./miner.sh $comsize 50007"
# sleep 1
# gnome-terminal --working-directory="$simpath" -- /bin/bash -c "expect ./user.sh $comsize 50008"
# sleep 1
# gnome-terminal --working-directory="$simpath" -- /bin/bash -c "expect ./sp.sh $comsize 50009"
sleep 1
gnome-terminal --working-directory="$simpath" -- /bin/bash -c "expect ./indexer.sh $comsize 50010 8080"
sleep 1
gnome-terminal --working-directory="$simpath" -- /bin/bash -c "expect ./indexer.sh $comsize 50011 8081"
