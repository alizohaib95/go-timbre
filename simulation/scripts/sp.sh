# !/bin/expect -f

set timeout -1
set comsize [lindex $argv 0]
set portno [lindex $argv 1]

cd ../main

spawn go run main.go -p $portno -s

expect "Please enter the committee size:-\r"
send "$comsize\n"
expect ">\r"
send "sso\n"
interact