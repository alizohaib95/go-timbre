# !/bin/expect -f

set timeout -1
set comsize [lindex $argv 0]
set portno [lindex $argv 1]


cd ../main
# printf "2\n tc\n" | go run main.go -p $2 -m
spawn go run main.go -p $portno -m

# expect "Please enter the committee size:-\r"
# send "$comsize\n"
# expect "Could not find current blockchain tail\r"
# send "tc\n"
interact
