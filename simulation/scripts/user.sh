# !/bin/expect -f

set timeout -1
set comsize [lindex $argv 0]
set portno [lindex $argv 1]

cd ../main

spawn go run main.go -p $portno -u

expect "Please enter the committee size:-\r"
send "$comsize\n"
expect ">\r"
send "sp\n"
expect "Text to send:\r"
send "Simulation Test\n"
send "pb\n"
interact