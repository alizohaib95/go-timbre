# !/bin/expect -f

set timeout -1
set comsize [lindex $argv 0]
set portno [lindex $argv 1]
set indexno [lindex $argv 2]

cd ../main

spawn go run main.go -p $portno -i -pi $indexno

expect "Please enter the committee size:-\r"
send "$comsize\n"
interact