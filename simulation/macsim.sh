#!/bin/bash
PWD="/Users/mingyangwang/Documents/capstone/go-timbre/simulation/main"
osascript <<EOD
    tell app "Terminal"
        do script "cd $PWD && go run main.go"
    end tell
    tell app "Terminal"
        do script "cd $PWD && go run main.go -p 50007 -s"
    end tell
    tell app "Terminal"
        do script "cd $PWD && go run main.go -p 50008 -u"
    end tell
    tell app "Terminal"
        do script "cd $PWD && go run main.go -p 50009 -m"
    end tell
    tell app "Terminal"
        do script "cd $PWD && go run main.go -p 50010 -m"
    end tell
    tell app "Terminal"
        do script "cd $PWD && go run main.go -p 50011 -i -pi 8080"
    end tell
    tell app "Terminal"
        do script "cd $PWD && go run main.go -p 50012 -i -pi 8081"
    end tell
EOD