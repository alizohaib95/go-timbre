package roles

import (
	"bytes"
	"encoding/binary"
	"encoding/hex"
	"fmt"
	"math/rand"
	"sort"
	"sync"
	"time"

	"github.com/Nik-U/pbc"
	"github.com/golang/protobuf/proto"
	"github.com/guyu96/go-timbre/core"
	"github.com/guyu96/go-timbre/crypto"
	"github.com/guyu96/go-timbre/crypto/pos"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	"github.com/guyu96/go-timbre/protobuf"

	// "github.com/mbilal92/pbc"

	rpcpb "github.com/guyu96/go-timbre/rpc/pb"
)

const (
	spChanSize   = 32        // default storage provider channel buffer size
	defTotalSize = 200000000 // default storage size
	chunkSize    = 1000
)

var PostBucket = []byte("post-bucket")
var DealTagBucket = []byte("deal-tag-bucket")

// var DealBucket = []byte("deal-bucket")

// StorageProvider provides storage for posts in a Timbre forum.
type StorageProvider struct {
	node     *net.Node
	incoming chan net.Message
	// SpBlockTick      chan struct{}
	totalCapacity       uint32
	committedStorage    uint32
	usedStorage         uint32
	chunks              map[string]([]uint32)     // PostID -> List of chunk sizes
	Myactivedeals       map[string]*protobuf.Deal // Active Deals from blockchain
	ActiveDealMtx       sync.RWMutex              // ActiveDeal mutex
	NewMadeDeals        map[string]bool           // Active Deals from blockchain
	ProcessNewBlockMtx  sync.Mutex
	CurrentStorageOffer *protobuf.StorageOffer
}

func (sp *StorageProvider) GetStoredPostsHashes() []*rpcpb.SPost {

	hashes := make([]*rpcpb.SPost, 0)

	sp.node.Db.ForEach(PostBucket, func(postHash, v []byte) error {

		post := &protobuf.StoredPost{}
		err := proto.Unmarshal(v, post)
		if err != nil {
			log.Error().Msgf("Can not marshal post")
			// return
		}

		hashes = append(hashes, &rpcpb.SPost{
			Hash:    hex.EncodeToString(postHash),
			Size:    uint32(len(v)),
			Content: string(post.Content),
		})
		return nil
	})

	return hashes
}

func (sp *StorageProvider) GetNumberOfChunks() uint32 {
	count := 0
	for _, v := range sp.chunks {
		count = count + len(v)
	}
	return uint32(count)
}

func (sp *StorageProvider) GetLastStorageOffer() *protobuf.StorageOffer {
	return sp.CurrentStorageOffer
}

func (sp *StorageProvider) GetStorageProviderStats() *rpcpb.StorageProviderStats {

	postHashes := sp.GetStoredPostsHashes()
	var deals = make([]*rpcpb.Deal, 0)

	for dealhash, _ := range sp.Myactivedeals {

		deal := sp.Myactivedeals[dealhash]
		dealbytes, _ := proto.Marshal(deal)
		dealHash2 := crypto.Sha256(dealbytes)
		ExpiryTime := int64(deal.ExpiryTime)
		deals = append(deals, &rpcpb.Deal{
			DealHash:   hex.EncodeToString(dealHash2),
			ExpiryTime: ExpiryTime,
		})
	}

	return &rpcpb.StorageProviderStats{
		TotalCapacity:    sp.totalCapacity,
		CommittedStorage: sp.committedStorage,
		Chunks:           sp.GetNumberOfChunks(),
		UsedStorage:      sp.usedStorage,
		StoredPosts:      postHashes,
		ActiveDeals:      deals,
		LastOffer:        sp.CurrentStorageOffer,
	}
}

// NewStorageProvider creates a new sp.
func NewStorageProvider(node *net.Node) *StorageProvider {
	sp := &StorageProvider{
		node:     node,
		incoming: make(chan net.Message, spChanSize),
		// SpBlockTick:   make(chan struct{}, 5),
		totalCapacity: defTotalSize,
		chunks:        make(map[string]([]uint32)),
		// committedStorage: 0, // Leaving this out
		usedStorage:   0,
		Myactivedeals: make(map[string](*protobuf.Deal)),
		NewMadeDeals:  make(map[string]bool),
	}

	node.AddOutgoingChan(sp.incoming)

	if node.Db.HasBucket(PostBucket) {
		return sp
	}

	if err := node.Db.NewBucket(PostBucket); err != nil {
		panic(fmt.Sprintf("StroageProvider: Failed to init DB post-bucket: %s", err))
	}

	if err := node.Db.NewBucket(DealTagBucket); err != nil {
		panic(fmt.Sprintf("StroageProvider: Failed to init DB deal-tag-bucket: %s", err))
	}

	// if err := node.Db.NewBucket(DealBucket); err != nil {
	// 	panic(fmt.Sprintf("StroageProvider: Failed to init DB deal-tag-bucket: %s", err))
	// }

	return sp
}

//EmptyCaches empties the caches
func (sp *StorageProvider) EmptyCaches() {
	sp.chunks = make(map[string]([]uint32))
	sp.usedStorage = 0
	sp.Myactivedeals = make(map[string](*protobuf.Deal))
	sp.NewMadeDeals = make(map[string]bool)

}

//Node returns the node in the storage provider
func (sp *StorageProvider) Node() *net.Node {
	return sp.node
}

// Process starts processing incoming messages for Storage Provider.
func (sp *StorageProvider) Process() {
	// go sp.checkExpiredDeals()
	for msg := range sp.incoming {
		switch msg.Code {
		case net.MsgCodeStorageProviderRecievePostRequest:
			sp.storePost(msg)
		case net.MsgCodeGetPost:
			// sp.sendPost(msg) //This has moved to bp
		case net.MsgCodeReceiveVarificationTags:
			sp.onVarificationTagReceive(msg)
		// case net.MsgCodeSpChallengeCall:
		// 	sp.onChallengeCall(msg)
		case net.MsgCodeUpdateLikes:
			sp.UpdateLikes(msg)
		}
	}
}

// Retrieves post from database -> this has moved to bandwidth provider
func (sp *StorageProvider) sendPost(msg net.Message) {
	contentHash := msg.Data
	data, err := sp.node.Db.Get(PostBucket, contentHash)
	if err != nil {
		log.Info().Msgf("Unable to retrieve post")
		return
	}

	msgToSend := net.NewMessage(net.MsgCodePostContent, data)
	log.Info().Msgf("SP: --- sendPOst %v", len(msgToSend.Data))
	sp.node.Relay(msg.From, msgToSend)
}

func (sp *StorageProvider) DeletePost(postHash string) bool {
	by, err := hex.DecodeString(postHash)
	if err != nil {
		return false
	}
	err = sp.node.Db.Delete(PostBucket, by)
	if err == nil {
		return true
	}
	return false
}

// SendStorageOffer sends storage offer to miner's committee (broadcast for now)
func (sp *StorageProvider) SendStorageOffer(minPrice, maxDuration, size uint32) {

	if maxDuration == 0 {
		log.Error().Msg("StroageProvider: max Duration set to 0 Minutes.")
		return
	}

	offer := &protobuf.StorageOffer{
		Spid:        sp.node.PublicKey().ToByte(),
		KadID:       sp.node.GetNodeIDBytes(),
		MinPrice:    minPrice,
		MaxDuration: uint32(time.Duration(maxDuration) * time.Second),
		Size:        size,
	}

	if sp.CurrentStorageOffer != nil {
		if sp.CurrentStorageOffer.MinPrice > minPrice {
			sp.CurrentStorageOffer = offer
		}
	} else {
		sp.CurrentStorageOffer = offer
	}

	offerBytes, err := proto.Marshal(offer)
	if err != nil {
		log.Error().Msg("StroageProvider: StorageOffer marshal Error.")
		return
	}

	st := &protobuf.SignedStorageOffer{
		Offer: offer,
		Sig:   sp.node.PrivateKey().SignB(offerBytes),
	}

	stobytes, err := proto.Marshal(st)
	if err != nil {
		panic(fmt.Sprintf("StroageProvider: Failed to marshal StorageOffer: %s", err))
	}
	// sp.totalCapacity = size
	msg := net.NewMessage(net.MsgCodeStorageOfferNew, stobytes)
	sp.node.Broadcast(msg)
}

// storePost stores post to DB and send back signedInfoList
func (sp *StorageProvider) storePost(msg net.Message) {
	rl := new(protobuf.RequestList)
	if err := proto.Unmarshal(msg.Data, rl); err != nil {
		log.Info().Msgf(" RequestList Unmarshal Error:%v", err)
		return
	}

	sInfolist := new(protobuf.SignedInfoList)
	for _, threadPr := range rl.List {
		threadSInfolist := new(protobuf.ThreadSignedInfoList)
		for _, pr := range threadPr.List {
			rootHash := pr.SignedInfo.Info.GetMetadata().GetThreadRootPostHash()
			// parentHash := pr.SignedInfo.Info.GetMetadata().GetParentPostHash() This should be put elsewhere on the pr embed to the content maybe
			postInfobytes, err := proto.Marshal(pr.SignedInfo.Info)
			postMetaDataHash := crypto.Sha256(postInfobytes)

			post := &protobuf.StoredPost{
				Hash:           postMetaDataHash,
				ThreadHeadHash: rootHash,
				Content:        pr.Content,
				Timestamp:      time.Now().Unix(),
				Pid:            pr.SignedInfo.Info.GetMetadata().GetPid(),
			}

			postBytes, err := proto.Marshal(post)
			if err != nil {
				panic(fmt.Sprintf("StorageProvider: Failed to marshal post in storePosttoDB: %s", err))
			}

			if rootHash == nil { // Either a root post

				_, ok := sp.chunks[hex.EncodeToString(postMetaDataHash)]
				// If root post does not exist, allocate first chunk if space allows
				if !ok {

					if (sp.usedStorage + chunkSize) > sp.totalCapacity {
						// if (sp.usedStorage) > sp.totalCapacity {
						log.Error().Msg("Storage capacity reached.") // Find new storage provider
						newMsg := net.NewMessage(net.MsgCodeStorageProviderPostNotStored, msg.Data)
						sp.node.Relay(msg.From, newMsg)
						return
					}

					sp.chunks[hex.EncodeToString(postMetaDataHash)] = append(sp.chunks[hex.EncodeToString(postMetaDataHash)], uint32(len(postBytes)))
					sp.usedStorage = sp.usedStorage + chunkSize
					// sp.usedStorage = sp.usedStorage + pr.SignedInfo.Info.Metadata.GetContentSize()
				}
			} else { //reply post
				// Check if the root post for the reply post exists
				lchunks, ok := sp.chunks[hex.EncodeToString(rootHash)]
				if !ok {
					log.Error().Msgf("The root post has no allocated chunks here") // This should later take into account any dangling post
				} else {
					// Now either it is to be allocated a new chunk or added to an existing chunk
					lastAddedChunk := lchunks[len(lchunks)-1]

					// If adding to last chunk wont exceed the chunk size, then add to last
					if lastAddedChunk+uint32(len(postBytes)) <= chunkSize {
						sp.chunks[hex.EncodeToString(rootHash)][len(sp.chunks[hex.EncodeToString(rootHash)])-1] += uint32(len(postBytes))
					} else { //Allocate a new chunk if can be allocated

						if (sp.usedStorage + chunkSize) > sp.totalCapacity {
							log.Info().Msg("Storage capacity reached.") // Find new storage provider
							newMsg := net.NewMessage(net.MsgCodeStorageProviderPostNotStored, msg.Data)
							sp.node.Relay(msg.From, newMsg)
							return
						}
						sp.chunks[hex.EncodeToString(rootHash)] = append(sp.chunks[hex.EncodeToString(rootHash)], uint32(len(postBytes)))
						sp.usedStorage = sp.usedStorage + chunkSize
					}
				}

			}

			sp.node.Db.Put(PostBucket, postMetaDataHash, postBytes)
			// log.Info().Msgf("Post has been stored", hex.EncodeToString(postMetaDataHash))
			threadSInfolist.InfoList = append(threadSInfolist.InfoList, pr.SignedInfo)
		}

		InfoListBytes := crypto.HashStructs(threadSInfolist.InfoList) //proto.Marshal(sInfolist.InfoList)
		threadSInfolist.Spid = sp.node.PublicKey().ToByte()
		threadSInfolist.KadID = sp.node.GetNodeIDBytes()
		threadSInfolist.Sig = sp.node.PrivateKey().SignB(InfoListBytes)
		sInfolist.List = append(sInfolist.List, threadSInfolist)
	}

	signedInfoListBytes, _ := proto.Marshal(sInfolist) // TODO: error checking
	newMsg := net.NewMessage(net.MsgCodeStorageProviderPostStored, signedInfoListBytes)
	// log.Info().Msgf("SP: --- signedInfoList %v", len(newMsg.Data))
	sp.node.Relay(msg.From, newMsg)
}

func (sp *StorageProvider) onVarificationTagReceive(msg net.Message) {
	dealTagPair := new(protobuf.DealTagPair)
	if err := proto.Unmarshal(msg.Data, dealTagPair); err != nil {
		log.Info().Msgf(" DealTagPair Unmarshal Error:%v", err)
		return
	}

	// deal := dealTagPair.Deal
	// Dealbytes, _ := proto.Marshal(deal)
	// DealHash := crypto.Sha256(Dealbytes)
	// sp.node.Db.Put(DealBucket, DealHash, Dealbytes)
	// sp.DealMtx.Lock()
	sp.NewMadeDeals[string(dealTagPair.DealHash)] = true
	// sp.DealMtx.Unlock()
	// sp.node.Db.Put(DealBucket, DealHash, Dealbytes)

	// tags := pos.TagsFromByteArray(dealTagPair.Tags, sp.node.PosPairing)
	log.Info().Msgf("Deal made: %v", hex.EncodeToString(dealTagPair.DealHash))
	// log.Info().Msgf("Tags: Received%v", tags.String())

	sp.node.Db.Put(DealTagBucket, dealTagPair.DealHash, bytes.Join(dealTagPair.Tags, []byte("timbre")))
}

// func (sp *StorageProvider) onChallengeCall(msg net.Message) {
// 	dealChalPair := new(protobuf.DealChallPair)
// 	if err := proto.Unmarshal(msg.Data, dealChalPair); err != nil {
// 		log.Error().Msgf(" dealChalPair Unmarshal Error:%v", err)
// 		return
// 	}

// 	// log.Info().Msgf("dealChalPair: Dealhash for challenge %v", string(dealChalPair.DealHash))

// 	var content []byte
// 	dealBytes, err := sp.node.Db.Get(DealBucket, dealChalPair.DealHash)
// 	if err != nil {
// 		log.Error().Msgf("Could not read deadlhash from DB")
// 		return
// 	}
// 	log.Info().Msgf("SP --- Deal for ch %v", hex.EncodeToString(dealChalPair.DealHash))

// 	deal := new(protobuf.Deal)
// 	if err := proto.Unmarshal(dealBytes, deal); err != nil {
// 		log.Error().Msgf(" dealBytes Unmarshal Error:%v", err)
// 		return
// 	}

// 	// log.Info().Msgf("Deal: %v", deal.String())

// 	for _, infoPr := range deal.List {
// 		postInfobytes, err := proto.Marshal(infoPr.Info)
// 		postMetaDataHash := crypto.Sha256(postInfobytes)
// 		postBytes, err := sp.node.Db.Get(PostBucket, postMetaDataHash)
// 		if err != nil {
// 			log.Error().Msgf("Unable to get post from db")
// 			return
// 		}

// 		post := new(protobuf.StoredPost)
// 		if err2 := proto.Unmarshal(postBytes, post); err2 != nil {
// 			log.Error().Msgf("postBytes unmarshal error")
// 			return
// 		}
// 		// log.Info().Msgf("post: %v", post.String())

// 		content = append(content, post.Content...)
// 	}

// 	if len(content) < 150 {
// 		j := len(content)
// 		for i := 0; i < 150-j; i++ {
// 			content = append(content, []byte("0")...)
// 		}
// 	}

// 	// log.Info().Msgf("msgMap Data %v", string(content))
// 	pairing, err := pbc.NewPairingFromString(deal.PosParam)
// 	if err != nil {
// 		log.Error().Msgf("Could not generate pairing from string")
// 	}
// 	chl := new(pos.Chall)
// 	chl.FromProto(dealChalPair.Challenge, pairing)
// 	chlBytes, _ := proto.Marshal(dealChalPair.Challenge)
// 	// log.Info().Msgf("SP --- challenge: %v", chl.String())

// 	tagsBytes, _ := sp.node.Db.Get(DealTagBucket, dealChalPair.DealHash)
// 	tags := pos.TagsFromByteArray(bytes.Split(tagsBytes, []byte("timbre")), pairing)
// 	// log.Info().Msgf("SP --- Deal %v, Tags: %v, Data length %v, Data %v ", hex.EncodeToString(dealChalPair.DealHash), tags.String(), len(content), string(content))

// 	pbKey := new(pos.PublicKey)
// 	pbKey.FromProto(deal.PublicKey, pairing)
// 	// fmt.Println("PbKey: ", pbKey)

// 	prf := pos.Prove(content, sp.node.PosConfig, pairing, pbKey, tags, chl)
// 	protoPrf := prf.ToProto()
// 	protoPrfBytes, _ := proto.Marshal(protoPrf)
// 	sig := sp.node.PrivateKey().SignB(protoPrfBytes)
// 	// log.Info().Msgf("SP --- prf: %v", prf.String())

// 	challengeProofPair := &protobuf.ChallengeProofPair{
// 		ChallengeHash:  crypto.Sha256(chlBytes),
// 		Proof:          prf.ToProto(),
// 		Sig:            sig,
// 		Spid:           sp.node.PublicKey().ToByte(),
// 		Dealhash:       dealChalPair.DealHash,
// 		DealExpiryTime: dealChalPair.DealExpiryTime,
// 		DealStartTime:  dealChalPair.DealStartTime,
// 	}

// 	// log.Info().Msgf("challengeProofPair: %v", challengeProofPair.String())
// 	challengeProofPairBytes, _ := proto.Marshal(challengeProofPair)
// 	newMsg := net.NewMessage(net.MsgCodeSpChallengeProf, challengeProofPairBytes)
// 	// log.Info().Msgf("SP: --- chPrPair msg len %v", len(newMsg.Data))
// 	sp.node.Relay(msg.From, newMsg)

// }

func (sp *StorageProvider) checkExpiredDeals() {

	for {
		log.Info().Msgf("checkExpiredDeals")
		// select {
		// case <-sp.SpBlockTick:
		log.Info().Msgf("SP checkExpiredDeals")
		time.Sleep(sp.node.Dpos.TimeLeftToMine(time.Now().Unix()))

		currentTime := time.Now() //.Add(-1 * time.Second) // to make sure the deal with expiry time at exact time is not deleted now
		fmt.Printf("\t\tSP - Expired Deal Time %v\n", currentTime)
		sp.ActiveDealMtx.Lock()
		fmt.Printf("\t\tSP - checkExpiredDeals sp.ActiveDealMtx.Lock()\n")

		for dealhash, deal := range sp.Myactivedeals {
			ExpiryTime := time.Unix(0, int64(deal.ExpiryTime)) // wait extra for 1 minning round
			if currentTime.After(ExpiryTime) {                 // TODO: DPOS time for block window, change it to block window time
				for _, pr_item := range deal.List {
					hashbytes, _ := proto.Marshal(pr_item.Info)
					hash := crypto.Sha256(hashbytes)
					sp.node.Db.Delete(PostBucket, hash)
					// rootHash := hex.EncodeToString(pr_item.Info.GetMetadata(). // roothash is nil for root posts hence use the metadata hash for deletion
					// On post deletion -> remove chunks and restore usedStorage
					_, ok := sp.chunks[hex.EncodeToString(hash)]
					if ok {
						nChunks := len(sp.chunks[hex.EncodeToString(hash)])
						delete(sp.chunks, hex.EncodeToString(hash))
						sp.usedStorage -= uint32(nChunks) * chunkSize
					}
					_, ok = sp.chunks[hex.EncodeToString(pr_item.Info.Metadata.ThreadRootPostHash)]
					if ok {
						nChunks := len(sp.chunks[hex.EncodeToString(hash)])
						delete(sp.chunks, hex.EncodeToString(hash))
						sp.usedStorage -= uint32(nChunks) * chunkSize
					}
				}

				// sp.node.Db.Delete(DealBucket, []byte(dealhash))
				sp.node.Db.Delete(DealTagBucket, []byte(dealhash))
				// log.Info().Msgf("SP -- Removed Expired DealHahs %v Current Time: %v - ExpiryTime %v ", hex.EncodeToString([]byte(dealhash)), currentTime, ExpiryTime)
				delete(sp.Myactivedeals, dealhash)
			}
		}
		sp.ActiveDealMtx.Unlock()
		fmt.Printf("\t\tSP - checkExpiredDeals sp.ActiveDealMtx.Unlock()\n")
		// }
	}
}

func (sp *StorageProvider) UpdateLikes(msg net.Message) {

	spost := new(protobuf.StoredPost)
	if err := proto.Unmarshal(msg.Data, spost); err != nil {
		log.Info().Msgf(" StoredPost Unmarshal Error:%v", err)
		return
	}

	postRetrieved, err := sp.node.Db.Get(PostBucket, spost.Hash)
	if err != nil {
		log.Error().Msgf("Post not stored with sp")
		return
	}
	post := &protobuf.StoredPost{}
	err = proto.Unmarshal(postRetrieved, post)
	if err != nil {
		return
	}

	post.Likes = post.Likes + 1
	postBytes, err := proto.Marshal(post)
	if err != nil {
		log.Error().Msgf("Could not marshal post")
	}
	// Update the post
	err = sp.node.Db.Delete(PostBucket, spost.Hash)
	if err != nil {
		log.Error().Msgf("Could not delete update post")
	}
	err = sp.node.Db.Put(PostBucket, spost.Hash, postBytes)
	if err != nil {
		log.Error().Msgf("Could not update post")
	}

}

func (sp *StorageProvider) IssueChallenges(seed int64) {
	// fmt.Printf("seed %v for SP Challenge\n", seed)
	// fmt.Printf("\t sp IssueChallenges Time %v\n", time.Now())
	// time.Sleep(1 * time.Second) // sleep for some time so that Expired deals are removed first
	challengeIssuedDeal := make(map[string]bool)
	// fmt.Printf("\t\tNode - IssueChallenges sp.node.DealMtx.RLock()\n")
	// sp.node.DealMtx.RLock()
	// defer sp.node.DealMtx.RUnlock()

	dealhashKeys, expiredDeal := sp.node.GetActiveAndExpiredDealsHash()
	if dealhashKeys == nil {
		// fmt.Printf("\t\tNode - IssueChallenges sp.node.DealMtx.RUnLock()\n")
		return
	}

	// for index, dealhash := range dealhashKeys {
	// 	log.Info().Msgf("\t GenerateChallenges before deal: %v %v", index, hex.EncodeToString([]byte(dealhash)))
	// }

	sort.Strings(dealhashKeys)
	sort.Strings(expiredDeal)
	// for index, dealhash := range dealhashKeys {
	// 	log.Info().Msgf("\t IssueChallenges sorted deal: %v %v", index, hex.EncodeToString([]byte(dealhash)))
	// }

	NumberOfDealToVerifyL := NumberOfDealToVerify
	if NumberOfDealToVerifyL > len(dealhashKeys) {
		NumberOfDealToVerifyL = len(dealhashKeys)
	}

	randNum := rand.New(rand.NewSource(seed))
	pbc.SetRandRandom(randNum)

	currentTime := time.Now().Add(sp.node.Dpos.TimeLeftToMine(time.Now().Unix()))
	log.Info().Msgf("sp -- ISSUE CHAllenge active deals size: sp.Myactivedeals %v len %v, cap %v, NumberOfDealToVerifyL %v", len(sp.Myactivedeals), len(dealhashKeys), cap(dealhashKeys), NumberOfDealToVerifyL)
	// fmt.Printf("\t\tSP - IssueChallenges sp.DealMtx.Lock()\n")

	for i := 0; i < NumberOfDealToVerifyL; i++ {
		r := randNum.Intn(len(dealhashKeys))
		log.Info().Msgf("\t Seed %v  Random Number: %v", seed, r)
		dealhash := dealhashKeys[r]
		for {
			if _, ok := challengeIssuedDeal[dealhash]; !ok {
				break
			}
			r = randNum.Intn(len(dealhashKeys))
			log.Info().Msgf("\t Seed %v  Random Number: %v", seed, r)
			dealhash = dealhashKeys[r]
		}
		challengeIssuedDeal[dealhash] = true
		log.Info().Msgf("SP %v -- Issued Challenge DealHash %v \n", sp.node.Port, hex.EncodeToString([]byte(dealhash)))
		if dealhash != "" {
			deal := sp.node.GetDealbyHash(dealhash)
			if deal != nil {
				// ch, pairing := sp.generateChallenge(randNum, dealhash, deal, currentTime, len(dealhashKeys))
				ch, pairing := sp.generateChallenge(randNum, dealhash, deal, currentTime)
				_ = ch
				sp.ActiveDealMtx.RLock()
				if _, ok := sp.Myactivedeals[dealhash]; ok {
					// sp.generateProof(randNum, pairing, dealhash, deal, currentTime, ch, len(dealhashKeys))
					sp.generateProof(pairing, dealhash, deal, currentTime, ch)
				}
				sp.ActiveDealMtx.RUnlock()
			} else {
				log.Error().Msgf("\t Deal Not found for Hash %v for deals callenge", hex.EncodeToString([]byte(dealhash)))
			}
		} else {
			log.Error().Msgf("IssueChallenges DealHash empty %v", dealhash)
		}
	}

	for _, dealhash := range expiredDeal {
		r := randNum.Intn(len(dealhashKeys))
		log.Info().Msgf("\t Seed %v  Random Number: %v", seed, r)
		if dealhash != "" {
			deal := sp.node.GetDealbyHash(dealhash)
			if deal != nil {
				if _, ok := challengeIssuedDeal[dealhash]; !ok {
					// ch, pairing := sp.generateChallenge(randNum, dealhash, deal, currentTime, len(dealhashKeys))
					ch, pairing := sp.generateChallenge(randNum, dealhash, deal, currentTime)
					log.Info().Msgf("SP %v -- Issued Challenge Expired DealHash %v ", sp.node.Port, hex.EncodeToString([]byte(dealhash)))

					sp.ActiveDealMtx.RLock()
					if deal, ok := sp.Myactivedeals[dealhash]; ok {
						// sp.generateProof(randNum, pairing, dealhash, deal, currentTime, ch, len(dealhashKeys))
						sp.generateProof(pairing, dealhash, deal, currentTime, ch)
					}
					sp.ActiveDealMtx.RUnlock()
				}
			} else {
				log.Error().Msgf("\t Deal Not found for Hash %v for expired deals callenge", hex.EncodeToString([]byte(dealhash)))
			}
		} else {
			log.Error().Msgf("IssueChallenges exipred DealHahs empty %v", dealhash)
		}
	}

	// for dealhash, deal := range sp.Myactivedeals {
	// 	ExpiryTime := time.Unix(0, int64(deal.ExpiryTime)) //.Add(time.Second * 15) // wait extra for 1 minning round
	// 	if currentTime.After(ExpiryTime) {                 // TODO: DPOS time for block window, change it to block window time
	// 		log.Info().Msgf("SP %v -- Issued Challenge Expired DealHahs %v Current Time: %v - ExpiryTime %v ", sp.node.Port, hex.EncodeToString([]byte(dealhash)), currentTime, ExpiryTime)
	// 		if _, ok := challengeIssuedDeal[dealhash]; !ok {
	// 			sp.issueChallenge(randNum, dealhash, deal, currentTime)
	// 		}
	// 	}
	// }

	// sp.DealMtx.Unlock()
	// fmt.Printf("\t\tSP - IssueChallenges sp.DealMtx.Unlock()\n")
	// fmt.Printf("\t\tNode - IssueChallenges sp.node.DealMtx.RUnLock()\n")
}

// func (sp *StorageProvider) generateChallenge(randNum *rand.Rand, dealHash string, deal *protobuf.Deal, currentTime time.Time, lendeal int) (*pos.Chall, *pbc.Pairing) {
func (sp *StorageProvider) generateChallenge(randNum *rand.Rand, dealHash string, deal *protobuf.Deal, currentTime time.Time) (*pos.Chall, *pbc.Pairing) {
	// seed := 0

	// log.Info().Msgf("\t Ajeeb Seed 1 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	// log.Info().Msgf("Issuing challenge for DealHash: %v", hex.EncodeToString([]byte(dealHash)))
	contentSize := int(0)

	for _, infoPr := range deal.List {
		contentSize += int(infoPr.Info.Metadata.ContentSize)
	}

	// log.Info().Msgf("\t Ajeeb Seed 2 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	if contentSize < 150 {
		contentSize = 150
	}

	//TODO: keep challenge and challengeHash pairs for later verifaction
	// pairing, err := pbc.NewPairingFromString(deal.PosParam)
	// if err != nil {
	// 	log.Error().Msgf("Could not generate pairing from string")
	// }
	blockTagLen := pos.GetNumberofBlocksforTag(contentSize, sp.node.PosConfig)
	ch := pos.Challenge(randNum, blockTagLen, 3, sp.node.PosPairing)
	// ch := pos.Challenge(randNum, blockTagLen, 3, sp.node.PosPairing)
	// log.Info().Msgf("\t Ajeeb Seed 3 %v  Random Number: %v %t", 0, randNum.Intn(lendeal))

	return ch, sp.node.PosPairing //pairing
}

// func (sp *StorageProvider) generateProof(randNum *rand.Rand, pairing *pbc.Pairing, dealHash string, deal *protobuf.Deal, currentTime time.Time, ch *pos.Chall, lendeal int) {
func (sp *StorageProvider) generateProof(pairing *pbc.Pairing, dealHash string, deal *protobuf.Deal, currentTime time.Time, ch *pos.Chall) {
	// seed := 0
	// log.Info().Msgf("\t Ajeeb Seed 4 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	var content []byte

	for _, infoPr := range deal.List {
		postInfobytes, err := proto.Marshal(infoPr.Info)
		postMetaDataHash := crypto.Sha256(postInfobytes)
		postBytes, err := sp.node.Db.Get(PostBucket, postMetaDataHash)
		if err != nil {
			log.Error().Msgf("Unable to get post from db")
			return
		}

		post := new(protobuf.StoredPost)
		if err2 := proto.Unmarshal(postBytes, post); err2 != nil {
			log.Error().Msgf("postBytes unmarshal error")
			return
		}
		// log.Info().Msgf("post: %v", post.String())

		content = append(content, post.Content...)
	}

	if len(content) < 150 {
		j := 150 - len(content)
		for i := 0; i < j; i++ {
			content = append(content, []byte("0")...)
		}
	}

	// log.Info().Msgf("\t Ajeeb Seed 5 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	tagsBytes, _ := sp.node.Db.Get(DealTagBucket, []byte(dealHash))
	tags := pos.TagsFromByteArray(bytes.Split(tagsBytes, []byte("timbre")), sp.node.PosPairing)
	// tags := pos.TagsFromByteArray(bytes.Split(tagsBytes, []byte("timbre")), sp.node.PosPairing)
	// log.Info().Msgf("SP --- Deal %v, Tags: %v, Data length %v, Data %v ", hex.EncodeToString(dealChalPair.DealHash), tags.String(), len(content), string(content))

	// log.Info().Msgf("\t Ajeeb Seed 6 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	pbKey := new(pos.PublicKey)
	pbKey.FromProto(deal.PublicKey, sp.node.PosPairing)
	// fmt.Println("PbKey: ", pbKey)

	// log.Info().Msgf("\t Ajeeb Seed 7 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	prf := pos.Prove(content, sp.node.PosConfig, sp.node.PosPairing, pbKey, tags, ch)
	// log.Info().Msgf("\t Ajeeb Seed 8 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	protoPrf := prf.ToProto()
	protoPrfBytes, _ := proto.Marshal(protoPrf)
	sig := sp.node.PrivateKey().SignB(protoPrfBytes)
	// log.Info().Msgf("\t Ajeeb Seed 9 %v  Random Number: %v", 0, randNum.Intn(lendeal))

	bs := make([]byte, 4)
	// binary.LittleEndian.PutUint32(bs, 0)

	if prf.PsiIs0() {
		protoPrf.Psi = bs
	}

	if prf.YIs0() {
		protoPrf.Y = bs
	}

	if prf.TagIs0() {
		protoPrf.Tag = bs
	}

	timeforChPrPair := time.Now().UnixNano()

	ExpiryTime := time.Unix(0, int64(deal.ExpiryTime))
	if currentTime.After(ExpiryTime) {
		timeforChPrPair = deal.ExpiryTime
	}

	chPrPair := &protobuf.ChPrPair{
		Challenge:      ch.ToProto(),
		Proof:          protoPrf,
		Sig:            sig,
		Spid:           sp.node.PublicKey().ToByte(),
		Dealhash:       []byte(dealHash),
		DealExpiryTime: deal.ExpiryTime,
		Timestamp:      timeforChPrPair,
		DealStartTime:  deal.Timestamp,
	}

	// log.Info().Msgf("\t Ajeeb Seed 10 %v  Random Number: %v %t", 0, randNum.Intn(lendeal))

	// log.Info().Msgf("challengeProofPair: %v", challengeProofPair.String())
	challengeProofPairBytes, _ := proto.Marshal(chPrPair)
	newMsg := net.NewMessage(net.MsgCodeSpChallengeProf2, challengeProofPairBytes)
	// log.Info().Msgf("SP: --- chPrPair msg len %v", len(newMsg.Data))
	sp.node.Broadcast(newMsg)

	chl := new(pos.Chall)
	chl.FromProto(chPrPair.Challenge, sp.node.PosPairing)
	prf2 := new(pos.Proof)
	prf2.FromProto(chPrPair.Proof, sp.node.PosPairing)
	valid := pos.Verify([]byte(name), sp.node.PosPairing, pbKey, ch, prf)
	valid2 := pos.Verify([]byte(name), sp.node.PosPairing, pbKey, chl, prf2)

	// log.Info().Msgf("SP -- %v - Challenge: %v Proof %v \n  for Deal %v",	sp.node.Port, ch.String(), prf, hex.EncodeToString([]byte(dealHash)))

	// if ch.Equals(chl) {
	// 	log.Info().Msgf("SP -- Both Ch are equal")
	// } else {
	// 	log.Info().Msgf("SP -- Both Ch are Not equal")
	// }

	// if prf.Equals(prf2) {
	// 	log.Info().Msgf("SP -- Both prf are equal")
	// } else {
	// 	log.Info().Msgf("SP -- Both prf are Not equal")
	// }

	if !(valid2 && valid) {
		// 	log.Info().Msgf("SP -- %v **VALID** \n", sp.node.Port)
		// } else {
		log.Info().Msgf("\tSP --- prf. PSI: %v, Y:%v TAG %v \n content %v", protoPrf.Psi, protoPrf.Y, protoPrf.Tag, string(content))
		log.Info().Msgf("SP -- %v **Failed: %v** - Challenge: %v Proof %v  for Deal %v\n", sp.node.Port, valid, ch.String(), prf, hex.EncodeToString([]byte(dealHash)))
		log.Info().Msgf("SP2 -- %v **Failed: %v** - Challenge: %v Proof %v for Deal %v\n", sp.node.Port, valid2, chl.String(), prf2, hex.EncodeToString([]byte(dealHash)))
		// panic("SP: sp validation failed for deal")
	}

}

func (sp *StorageProvider) FilterMyDeals(deals []*protobuf.Deal) {
	// fmt.Printf("\tSP -- FilterMydeal incoming deals %v\n", len(deals))

	// fmt.Printf("\t\tSP - FilterMyDeals sp.ActiveDealMtx.Lock()\n")
	sp.ActiveDealMtx.Lock()
	for _, deal := range deals {
		dealBytes, _ := proto.Marshal(deal)
		dealhash := string(crypto.Sha256(dealBytes))
		if _, ok := sp.NewMadeDeals[dealhash]; ok {
			// fmt.Printf("SP -- FilterMydeal - My DEAL %v\n", hex.EncodeToString([]byte(dealhash)))
			sp.Myactivedeals[dealhash] = deal
			delete(sp.NewMadeDeals, dealhash)
		}
		// else {
		// fmt.Printf("SP -- FilterMydeal - Not My DEAL %v\n", hex.EncodeToString([]byte(dealhash)))
		// }

	}
	fmt.Printf("\tSP -- FilterMydeal Final my deals %v\n", len(sp.Myactivedeals))
	sp.ActiveDealMtx.Unlock()
	// fmt.Printf("\t\tSP - FilterMyDeals sp.ActiveDealMtx.Unlock()\n")

	// for dealhash, _ := range sp.NewMadeDeals { //TODO After a atleast 21 blocks has passed
	// 	delete(sp.NewMadeDeals, dealhash)
	// }
}

func (sp *StorageProvider) ProcessNewBlock(b *core.Block) {
	sp.ProcessNewBlockMtx.Lock()
	defer sp.ProcessNewBlockMtx.Unlock()
	sp.FilterMyDeals(b.GetDeals())
	sp.IssueChallenges(int64(binary.BigEndian.Uint64(b.Hash()[0:8])))
}

func (sp *StorageProvider) RemoveExpiredDealDataFromCahce(deal *protobuf.Deal) {
	for _, pr_item := range deal.List {
		hashbytes, _ := proto.Marshal(pr_item.Info)
		hash := crypto.Sha256(hashbytes)
		sp.node.Db.Delete(PostBucket, hash)
		rootHash := hex.EncodeToString(pr_item.Info.GetMetadata().GetThreadRootPostHash())
		// On post deletion -> remove chunks and restore usedStorage
		_, ok := sp.chunks[rootHash]
		if ok {
			nChunks := len(sp.chunks[rootHash])
			delete(sp.chunks, rootHash)
			sp.usedStorage -= uint32(nChunks) * chunkSize
		}
		_, ok = sp.chunks[hex.EncodeToString(hash)]
		if ok {
			nChunks := len(sp.chunks[hex.EncodeToString(hash)])
			delete(sp.chunks, hex.EncodeToString(hash))
			sp.usedStorage -= uint32(nChunks) * chunkSize
		}
	}

	dealbytes, _ := proto.Marshal(deal)
	dealHash := crypto.Sha256(dealbytes)
	sp.node.Db.Delete(DealTagBucket, dealHash)
	sp.ActiveDealMtx.Lock()
	delete(sp.Myactivedeals, string(dealHash))
	sp.ActiveDealMtx.Unlock()

	// log.Info().Msgf("SP -- Removed Expired DealHahs %v Remaining %v", hex.EncodeToString([]byte(dealHash)), len(sp.Myactivedeals))
}
