package roles

import (
	"encoding/hex"
	"fmt"
	"html"
	"net/http"
	"strings"

	"github.com/golang/protobuf/proto"
	"github.com/gorilla/websocket"
	"github.com/guyu96/go-timbre/crypto"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	pb "github.com/guyu96/go-timbre/protobuf"
	"github.com/mbilal92/noise"
)

const (
	indexerChanSize = 32 // default storage provider channel buffer size
)

var clients = make(map[*websocket.Conn]*Client) // connected clients
var broadcast = make(chan Message)
var upgrader = websocket.Upgrader{}
var posts []*Post

type Client struct {
	name      string
	connected bool
}

type Indexer struct {
	node     *net.Node
	incoming chan net.Message
	syncer   *Syncer
	poster   *User
}

type Message struct {
	Method  string
	Payload map[string]interface{}
}

type Post struct {
	Hash               string
	Content            string
	ParentHash         string
	ThreadHeadPostHash string
	Timestamp          int64
	Pid                string
}

func sendMessage(msg *Message) {
	for client := range clients {
		err := client.WriteJSON(msg)
		if err != nil {
			log.Printf("error: %v", err)
			client.Close()
			delete(clients, client)
		}
	}
}

// NewStorageProvider creates a new sp.
func NewIndexer(node *net.Node) *Indexer {
	indexer := &Indexer{
		node:     node,
		incoming: make(chan net.Message, indexerChanSize),
		poster:   NewUser(node),
	}
	node.AddOutgoingChan(indexer.incoming)

	// node.StartPoster(indexer.poster)
	// go node.Poster.Setup()
	// go node.Poster.Process()

	return indexer
}

func (indexer *Indexer) StartServer(syncer *Syncer, port uint16) {
	indexer.syncer = syncer
	upgrader.CheckOrigin = func(r *http.Request) bool { return true }

	http.HandleFunc("/printblock", func(w http.ResponseWriter, r *http.Request) {
		str := fmt.Sprintf("Syncer: BlockChain:\n %s", syncer.node.BlockChainString())
		fmt.Fprintf(w, "Hello, %q", html.EscapeString(str))
	})

	http.HandleFunc("/ws", func(w http.ResponseWriter, r *http.Request) {
		ws, err := upgrader.Upgrade(w, r, nil)
		if err != nil {
			panic(err)
		}

		log.Info().Msgf("Client connected")
		clients[ws] = &Client{connected: true}
		sendPosts()
		for {
			var msg Message
			err := ws.ReadJSON(&msg)
			if err != nil {
				log.Printf("error: %v", err)
				delete(clients, ws)
				break
			}
			payload := msg.Payload
			switch msg.Method {
			// case "updateClient":
			// payload := msg.payload.(Client)
			// clients[ws].name = msg.payload["name"].(string)
			// case "newPost":
			// 	log.Info().Msgf("Posting %s", payload["content"].(string))
			// 	txtTosend := strings.TrimSpace(payload["content"].(string))
			// 	if rep
			// 	indexer.poster.SendPostRequest([]byte(txtTosend), nil, 1, 5)
			// 	indexer.sp.SendStorageOffer(1, 30, 50) // storage offer values
			// 	log.Info().Msgf("Posted %s", payload["content"].(string))
			case "newPost":
				txtTosend := strings.TrimSpace(payload["content"].(string))
				replyTo := []byte("")
				threadHash := []byte("")
				if interface{}(payload["replyTo"]) != nil {
					r := payload["replyTo"].(string)
					reply, _ := hex.DecodeString(strings.TrimSpace(r))
					replyTo = reply
				}
				if interface{}(payload["threadHash"]) != nil {
					t := payload["threadHash"].(string)
					thread, _ := hex.DecodeString(strings.TrimSpace(t))
					threadHash = thread
				}

				// Require new message

				// indexer.sp.SendStorageOffer(1, 30, 50) // storage offer values
				hash := indexer.poster.SendPostRequest([]byte(txtTosend), []byte(replyTo), []byte(threadHash), 1, 30, noise.PublicKey{}, noise.PrivateKey{})
				// hash := indexer.poster.SendPostRequest([]byte(txtTosend), nil, nil, 1, 30)
				// postHashInBytes, _ := hex.DecodeString(postHash)

				log.Info().Msgf("Posted %s: %s", payload["content"].(string), hash)

				// sendPost(pr)
			}
		}
		defer ws.Close()
	})
	log.Info().Msgf("Indexer Server started on Port %v:", port)
	err := http.ListenAndServe(fmt.Sprintf(":%d", port), nil)
	if err != nil {
		panic(err)
	}
}

func (indexer *Indexer) onGetPostContent(msg net.Message) error {
	log.Info().Msgf("Post Content Heard")
	post := &pb.StoredPost{}
	err := proto.Unmarshal(msg.Data, post)
	if err != nil {
		return err
	}

	p := convertPost(post)
	posts = append(posts, p)
	sendPost(p)
	// if bp.postToSp[hex.EncodeToString(post.Hash)] == bp.node.GetNodeID().Address() {
	// 	return
	// }
	// bp.postCache.Add(hex.EncodeToString(post.Hash), msg.Data)
	return nil
}

func convertPost(p *pb.StoredPost) *Post {
	return &Post{
		ParentHash:         hex.EncodeToString(p.GetParentHash()),
		ThreadHeadPostHash: hex.EncodeToString(p.GetThreadHeadHash()),
		Hash:               hex.EncodeToString(p.GetHash()),
		Content:            string(p.GetContent()),
		Pid:                hex.EncodeToString(p.GetPid()),
		Timestamp:          p.GetTimestamp(),
	}
}

func sendPost(post *Post) {
	// meta := pr.GetSignedInfo().GetInfo().GetMetadata()
	// hash, _ := proto.Marshal(pr.SignedInfo.Info)
	// meta := pr
	msg := &Message{
		Method:  "newPost",
		Payload: map[string]interface{}{"Post": post},
	}
	sendMessage(msg)
}

func sendPosts() {
	msg := &Message{
		Method:  "allPosts",
		Payload: map[string]interface{}{"Posts": posts},
	}
	sendMessage(msg)
}

// func sendPost(pr *pb.PostRequest) {
// 	meta := pr.GetSignedInfo().GetInfo().GetMetadata()
// 	hash, _ := proto.Marshal(pr.SignedInfo.Info)

// 	post := &Post{
// 		ParentHash:         hex.EncodeToString(meta.GetParentPostHash()),
// 		ThreadHeadPostHash: hex.EncodeToString(meta.GetThreadRootPostHash()),
// 		Hash:               hex.EncodeToString(hash),
// 		Content:            string(pr.GetContent()),
// 		Pid:                hex.EncodeToString(meta.GetPid()),
// 		Timestamp:          meta.GetTimestamp(),
// 	}
// 	msg := &Message{
// 		Method:  "newPost",
// 		Payload: map[string]interface{}{"Post": post},
// 	}
// 	sendMessage(msg)
// }

func (indexer *Indexer) Process() {
	for msg := range indexer.incoming {
		switch msg.Code {
		case net.MsgCodeBlockNew:
			log.Info().Msgf("New block mined")
			block := new(pb.Block)
			proto.Unmarshal(msg.Data, block)

			for _, deal := range block.Data.GetDeals() {
				threadPr := deal.GetList()
				for _, pr := range threadPr {
					hashbytes, _ := proto.Marshal(pr.Info)
					hash := crypto.Sha256(hashbytes)

					log.Info().Msgf("Post retrieve %s", hex.EncodeToString(hash))
					msg := net.NewMessage(net.MsgCodeGetPost, hash)
					indexer.node.RequestAndResponse(msg, indexer.onGetPostContent, net.MsgCodePostContent)
				}
			}
		case net.MsgCodeStorageProviderPostStored:
			// log.Info().Msgf("Post content stored, asking now")
			// list := new(pb.SignedInfoList)
			// proto.Unmarshal(msg.Data, list)

		// block, _ := core.NewBlockFromBytes(msg.Data)
		case net.MsgCodePostContent:
			log.Info().Msgf("Post content event heard")
			// case net.MsgCodePostNew:
			// 	pr := &pb.PostRequest{}
			// 	err := proto.Unmarshal(msg.Data, pr)
			// 	if err != nil {
			// 		return
			// 	}
			// 	log.Info().Msgf("Post heard %s", string(pr.GetContent()))
			// 	sendPost(pr)
		}
	}
}
