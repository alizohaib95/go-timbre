package roles

import (
	"context"
	"encoding/binary"
	"encoding/hex"
	"errors"
	"fmt"
	"math"
	"math/rand"
	"sort"
	"sync"
	"time"

	"github.com/Nik-U/pbc"
	"github.com/golang/protobuf/proto"
	"github.com/guyu96/go-timbre/core"
	"github.com/guyu96/go-timbre/crypto"
	"github.com/guyu96/go-timbre/crypto/pos"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	"github.com/guyu96/go-timbre/protobuf"
	"github.com/guyu96/go-timbre/storage"
	"github.com/guyu96/go-timbre/wallet"
	"github.com/mbilal92/noise"

	// "github.com/mbilal92/pbc"
	"github.com/oasislabs/ed25519"
)

const (
	minerChanSize              = 32     // default miner channel buffer size
	soCacheSize                = 256    // default storage offer cache size
	prCacheSize                = 2000   // default post request cache size
	name                       = "Test" // TODO: change me plz!!, I am just a file name
	NumberOfDealToVerify       = 5      //NumberOfDealToVerify: default number of deal to Verify
	TimeToWaitForProof         = 5
	MaxBlockSize         int64 = 60000 //60KB
)

var (
	testBlockNonce int64
)

func init() {
	testBlockNonce = 0
}

// Discuss: the storage offers are never removed from the cache, what if a storage provider does not
// want to provide storage anymore

// Miner is responsible for matching posts with storage providers, collecting votes, verifying storage deals and extending the blockchain by minting blocks.
type Miner struct {
	node          *net.Node
	incoming      chan net.Message
	startedMining bool

	soCache              *storage.LRU // storage offer cache
	prCache              *storage.LRU // post request cache
	spKadIdCache         *storage.LRU // storage provider kad ID cache
	postToPrimarySpCache *storage.LRU // temp store primary storage provider of post based on ParentHash

	localspReqListCacheMtx sync.Mutex                                            // ActiveDeal mutex
	localspReqListCache    map[string]map[string]*protobuf.ThreadPostRequestList // Map: (Spid) -> (thread) -> requestList
	msgMapMtx              sync.Mutex                                            // ActiveDeal mutex
	msgMap                 map[string][]byte
	storageLeftMtx         sync.Mutex        // ActiveDeal mutex
	storageLeft            map[string]uint32 // To keep track of pledged storage for a post
	threadPostingTimeMtx   sync.Mutex        // ActiveDeal mutex
	threadPostingTime      map[string]int64

	ReceivedChPrPairMtx sync.Mutex                    // ActiveDeal mutex
	ReceivedChPrPair    map[string]*protobuf.ChPrPair // Active Deals from blockchain
	ChPrPair            []*protobuf.ChPrPair
	deals               []*protobuf.Deal // New Deals

	transactions *storage.TransactionPool // Transactions
	votes        *storage.TransactionPool
	podfs        *storage.TransactionPool

	staleSoCache *storage.LRU
	stalePrCache *storage.LRU
	// This keeps all the storage offer of sps who have either indicated they are full or
	// stopProcessingSignal chan struct{}
	// startProcessingSignal            chan struct{}
	// startProcessingSignalSent1stTime bool
	ExitBlockLoop              chan struct{}
	ReceivedChPrPairForTesting map[string]*protobuf.ChPrPair
	StoreDataforTesting        bool
}

// NewMiner creates a new miner.
func NewMiner(node *net.Node) *Miner {
	miner := &Miner{
		node:          node,
		incoming:      make(chan net.Message, minerChanSize),
		startedMining: false,
		// stopProcessingSignal: make(chan struct{}),
		// startProcessingSignalSent1stTime: false,
		// startProcessingSignal:            make(chan struct{}),
	}

	miner.soCache, _ = storage.NewLRU(soCacheSize)
	miner.prCache, _ = storage.NewLRU(prCacheSize)
	miner.staleSoCache, _ = storage.NewLRU(prCacheSize)
	miner.stalePrCache, _ = storage.NewLRU(prCacheSize)

	// TODO: change cache size for followings
	miner.spKadIdCache, _ = storage.NewLRU(prCacheSize)
	miner.postToPrimarySpCache, _ = storage.NewLRU(prCacheSize)

	miner.localspReqListCache = make(map[string]map[string]*protobuf.ThreadPostRequestList)
	miner.msgMap = make(map[string][]byte)
	miner.storageLeft = make(map[string]uint32)
	// miner.challengeCache = make(map[string]*pos.Chall)
	// miner.Activedeals = make(map[string]*protobuf.Activedeals)
	miner.threadPostingTime = make(map[string]int64)
	miner.ReceivedChPrPair = make(map[string]*protobuf.ChPrPair)
	// miner.challengeIssuedDeal = make(map[string]*protobuf.Deal)
	miner.transactions = storage.NewTransactionPool(prCacheSize)
	miner.votes = storage.NewTransactionPool(prCacheSize)
	miner.podfs = storage.NewTransactionPool(prCacheSize)
	node.AddOutgoingChan(miner.incoming)
	miner.ReceivedChPrPairForTesting = make(map[string]*protobuf.ChPrPair)
	miner.StoreDataforTesting = false

	return miner
}

//EmptyCaches empties all the caches on the miner side
func (miner *Miner) EmptyCaches() {
	miner.soCache, _ = storage.NewLRU(soCacheSize)
	miner.prCache, _ = storage.NewLRU(prCacheSize)
	miner.stalePrCache, _ = storage.NewLRU(prCacheSize)
	miner.staleSoCache, _ = storage.NewLRU(prCacheSize)
	miner.spKadIdCache, _ = storage.NewLRU(prCacheSize)
	miner.postToPrimarySpCache, _ = storage.NewLRU(prCacheSize)

	miner.localspReqListCache = make(map[string]map[string]*protobuf.ThreadPostRequestList)
	miner.msgMap = make(map[string][]byte)
	miner.storageLeft = make(map[string]uint32)
	// miner.challengeCache = make(map[string]*pos.Chall)
	// miner.Activedeals = make(map[string]*protobuf.Activedeals)
	miner.threadPostingTime = make(map[string]int64)
	// miner.challengeIssuedDeal = make(map[string]*protobuf.Deal)
	miner.ReceivedChPrPair = make(map[string]*protobuf.ChPrPair)
	miner.transactions.EmptyPool()
	miner.votes.EmptyPool()
	miner.podfs.EmptyPool()

}

//StartListening starts listening once the committee starts with the node as miner
func (miner *Miner) StartListening(ctx context.Context) {
	//TODO stop these processes
	go miner.Process(ctx)
}

//Node returns the node type
func (miner *Miner) Node() *net.Node {
	return miner.node
}

//StartMining starts the mining processes which only need to run in miner's slot
func (miner *Miner) StartMining() {
	go miner.StartProcessingStoredPostRequests()
	// miner.startProcessingSignal <- struct{}{}
	// log.Info().Msgf("Miner: Sent startProcessingSignal 1")
	// miner.startProcessingSignalSent1stTime = false
	// go miner.CheckExpiredDeals()
}

//PushVote pushes the protobif type vote in the vote cache
func (miner *Miner) PushVote(pbVote *protobuf.Vote) {
	miner.votes.Push(pbVote)
}

//PushRetTrans pushes retrieval transaction in the cache
func (miner *Miner) PushRetTrans(pbTrans *protobuf.Transaction) {
	miner.transactions.Push(pbTrans)
}

//PushPodf push the podf to the miner cache
func (miner *Miner) PushPodf(pbPodf *protobuf.PoDF) {
	miner.podfs.Push(pbPodf)
}

//Process processes the incoming messages
func (miner *Miner) Process(ctx context.Context) {
	if miner.startedMining == true {
		log.Info().Msgf("RETURNING from Miner process. Its already processing")
		return
	}
	miner.startedMining = true

	for {
		select {
		case <-ctx.Done():
			log.Info().Msgf("Stopping miner process")
			miner.startedMining = false
			// miner.ResetCache()
			// miner.stopProcessingSignal <- struct{}{}
			return
		case msg := <-miner.incoming:
			switch msg.Code {
			case net.MsgCodeBroadcastTest:
				log.Info().Msgf("BroadCastMsg from %v msg %v", msg.From, string(msg.Data))
			case net.MsgCodeRelayTest:
				log.Info().Msgf("BroadCastMsg from %v msg %v", msg.From, string(msg.Data))
			case net.MsgCodeStorageOfferNew:
				go miner.handleStorageOffer(msg)
			case net.MsgCodePostNew:
				miner.handlePostRequest(msg)
			case net.MsgCodeStorageProviderPostStored:
				miner.handleSpPostRequestStoreReply(msg)
			case net.MsgCodeStorageProviderPostNotStored:
				go miner.handleSpSaysPostNotStored(msg)
			case net.MsgVoteTx:
				go miner.handleRelayedVote(msg)
			case net.MsgAmountTx:
				go miner.handleRelayedAmountTx(msg)
			case net.MsgCodePodf:
				// fmt.Println("Got podf transaction")
				miner.handleRelayedPodf(msg)
			// case net.MsgCodeSpChallengeProf:
			// 	miner.HandleSpChallengeProf(msg)
			case net.MsgCodeSpChallengeProf2:
				// log.Info().Msgf("BroadCastMsg MsgCodeSpChallengeProf2\n")
				go miner.HandleSpChallengeProf(msg)
			}
		}
	}
}

// onStorageOffer stores the storage offer to cache
func (miner *Miner) handleStorageOffer(msg net.Message) {

	ssto := new(protobuf.SignedStorageOffer)
	if err := proto.Unmarshal(msg.Data, ssto); err != nil {
		log.Info().Msgf("Miner: strogeOffer request Unmarshal Error:%v", err)
		return
	}

	if err := miner.verifyStorageOffer(ssto); err != nil {
		log.Error().Msgf("Miner: Failed to verify StorageOffer: %s", err)
		return
	}

	// Only the latest storage offer by any sp is kept
	miner.soCache.Add(string(ssto.Offer.Spid), ssto.Offer)
	miner.spKadIdCache.Add(string(ssto.Offer.Spid), msg.From)
	// miner.storageLeftMtx.Lock()
	// miner.storageLeft[string(ssto.Offer.Spid)] = ssto.Offer.GetSize()
	// miner.storageLeftMtx.Unlock()
	log.Info().Msgf("Miner: StorageOffer Saved")
}

// verifyStorageOffer verifies the offer based on hash and sign
func (miner *Miner) verifyStorageOffer(st *protobuf.SignedStorageOffer) error {
	offer := st.Offer
	if offer.MinPrice <= 0 { // QUESTION: set an upper bound of price?
		return fmt.Errorf("Miner: StorageOffer Price < 0")
	}

	if offer.Size <= 0 { // QUESTION: set lower bound of space?
		return fmt.Errorf("Miner: StorageOffer MaxStorage < 0")
	}

	offerBytes, _ := proto.Marshal(offer)
	if !ed25519.Verify(offer.Spid, offerBytes, st.Sig) {
		return fmt.Errorf("Miner: Invalid StorageOffer signature")
	}

	return nil
}

// onPostRequest verify post, find storage provider and send
func (miner *Miner) handlePostRequest(msg net.Message) { //queryID uint64
	// log.Info().Msgf("Miner: handlePostRequest Enter")
	pr := new(protobuf.PostRequest)
	if err := proto.Unmarshal(msg.Data, pr); err != nil {
		log.Error().Msgf("Miner: PostRequest Unmarshal Error:%v", err)
		return
	}

	if err := miner.verifyPostRequest(pr); err != nil {
		panic(fmt.Sprintf("Miner: Failed to verify PostRequest: %s", err))
	}

	// PosterAdd is the public key
	posterAdd := hex.EncodeToString(pr.SignedInfo.Info.Metadata.GetPid())
	chargedAmount := pr.SignedInfo.Info.GetParam().GetMaxCost()

	miner.node.Wm.PutWalletByAddress(posterAdd, wallet.NewWallet(posterAdd)) //Putting miner in map if not present
	pWallet, err := miner.node.Wm.GetWalletByAddress(posterAdd)              //for poster
	if err != nil {
		panic(fmt.Sprintf("Miner: Wallet error for :%v, %s", posterAdd, err))
		return
	}

	if pWallet.GetBalance()-1000 < int64(chargedAmount) {
		log.Error().Msgf("Miner: Insufficient balance for poster :%v", posterAdd)
		newMsg := net.NewMessage(net.MsgCodeInsufficientBalanceToPost, msg.Data)
		miner.node.Relay(msg.From, newMsg)
		return
	}

	hashbytes, _ := proto.Marshal(pr.SignedInfo.Info)
	hash := crypto.Sha256(hashbytes)
	threadheadPostHash := pr.SignedInfo.Info.Metadata.ThreadRootPostHash
	if threadheadPostHash == nil { // if it is a new post then set threadheadPostHash to Hash of metadata
		threadheadPostHash = hash
	}

	miner.threadPostingTimeMtx.Lock()
	if tpt, ok := miner.threadPostingTime[string(threadheadPostHash)]; ok {
		threadExpiryTime := time.Unix(tpt, 0)
		if time.Now().Truncate(time.Second).After(threadExpiryTime) {
			log.Info().Msgf("Miner: Skipping Post because of thread expired already, %v, thread hash %v, Current Time %v, Expiry Time %v",
				hex.EncodeToString(hash), hex.EncodeToString(threadheadPostHash), time.Now(), threadExpiryTime)
			newMsg := net.NewMessage(net.MsgCodePostThreadExpired, msg.Data)
			miner.node.Relay(msg.From, newMsg)
			miner.threadPostingTimeMtx.Unlock()
			return
		}
	}
	miner.threadPostingTimeMtx.Unlock()

	miner.prCache.Add(string(hash), pr)
	log.Info().Msgf("handlePostRequest: miner.prCache.Size(): %v", miner.prCache.Size())
	// tmpID, _ := noise.UnmarshalID(pr.SignedInfo.Info.Metadata.KadID)
	// log.Info().Msgf("Post has been cached PR DAta: %v, miner.prCache size %v", string(pr.Content), miner.prCache.Size())
	// log.Info().Msgf("Miner: handlePostRequest Exit")
}

//TODO: add method which handles reply in case of provider rejects the post request
// remove post request from cache and create a deal
func (miner *Miner) handleSpPostRequestStoreReply(msg net.Message) {
	// log.Info().Msgf("Miner: handleSpPostRequestStoreReply")

	sInfolist := new(protobuf.SignedInfoList)
	if err := proto.Unmarshal(msg.Data, sInfolist); err != nil {
		log.Error().Msgf("Miner: SignedInfoList Unmarshal Error:%v", err)
		return
	}

	for _, threadInfolist := range sInfolist.List {
		spIDstring := string(threadInfolist.Spid)
		if !ed25519.Verify(threadInfolist.Spid, crypto.HashStructs(threadInfolist.InfoList), threadInfolist.Sig) {
			log.Error().Msgf("Miner: Invalid SignedInfoList signature")
			return
		}

		if threadInfolist.InfoList != nil {

			threadheadPostHash := threadInfolist.InfoList[0].Info.Metadata.ThreadRootPostHash
			if threadheadPostHash == nil { // if it is a new post then set threadheadPostHash to Hash of metadata
				threadheadPostHashbytes, _ := proto.Marshal(threadInfolist.InfoList[0].Info)
				threadheadPostHash = crypto.Sha256(threadheadPostHashbytes)
			}

			storageUsed := uint32(0)
			// Remove post request from cache
			for _, pr_item := range threadInfolist.InfoList {
				hashbytes, _ := proto.Marshal(pr_item.Info)
				hash := crypto.Sha256(hashbytes)
				miner.prCache.Remove(string(hash))
				miner.stalePrCache.Remove(string(hash))
				storageUsed += pr_item.Info.Metadata.GetContentSize()
				// log.Info().Msgf("Making Deal: miner.prCache removed post hash %v", hex.EncodeToString(hash))
			}

			// miner.storageLeftMtx.Lock()
			// miner.storageLeft[spIDstring] = miner.storageLeft[spIDstring] - (storageUsed + 1000) // TODO : 1000 represents chunk in SP
			// miner.storageLeftMtx.Unlock()

			miner.threadPostingTimeMtx.Lock()
			if _, ok := miner.threadPostingTime[string(threadheadPostHash)]; !ok {
				for _, infopr := range threadInfolist.InfoList {
					if infopr.Info.Metadata.ThreadRootPostHash == nil {
						threadheadPostHashbytes, _ := proto.Marshal(infopr.Info)
						threadheadPostHash = crypto.Sha256(threadheadPostHashbytes)
						postingTime := time.Now()
						ExpiryTime := postingTime.Add(time.Second * time.Duration(int(infopr.Info.Param.MinDuration)))
						miner.threadPostingTime[string(threadheadPostHash)] = ExpiryTime.Unix()
						// log.Info().Msgf("Miner: threadPostingTime ExpiryTime %v, MinDuration %v, Posting Time %v", ExpiryTime, int(threadInfolist.InfoList[0].Info.Param.MinDuration), postingTime)
						break
					}
				}
			}
			miner.threadPostingTimeMtx.Unlock()

			// log.Info().Msgf("Miner: threadPostingTime%v ", miner.threadPostingTime)
			miner.msgMapMtx.Lock()
			data := miner.msgMap[string(threadheadPostHash)]
			miner.msgMapMtx.Unlock()
			if len(data) < 150 {
				j := len(data)
				for i := 0; i < 150-j; i++ {
					data = append(data, []byte("0")...)
				}
			}

			// log.Info().Msgf("msgMap Data %v %v")
			tags := pos.Setup(data, []byte(name), miner.node.PosPairing, miner.node.PosPk, miner.node.PosSk, miner.node.PosConfig)
			miner.threadPostingTimeMtx.Lock()
			threadPostingTimeL := miner.threadPostingTime[string(threadheadPostHash)]
			miner.threadPostingTimeMtx.Unlock()

			// threadSInfoMetaDatalist := make([]proto.PostMetadata, len(threadInfolist.InfoList))
			// i := 0
			// for _, pr_item := range threadInfolist.InfoList {
			// 	threadSInfoMetaDatalist[i] = pr_item.metadata
			// 	i++
			// }

			deal := &protobuf.Deal{
				Spid: threadInfolist.Spid,
				// KadID:      threadInfolist.KadID,
				SpSig: threadInfolist.Sig,
				List:  threadInfolist.InfoList,
				// List:      threadSInfoMetaDatalist //threadInfolist.InfoList,
				PublicKey: miner.node.PosPk.ToProto(),
				// Name:       []byte(name),
				// PosParam:   miner.node.PosParam.String(),
				Timestamp:  time.Now().UnixNano(),
				ExpiryTime: threadPostingTimeL,
			}

			Dealbytes, _ := proto.Marshal(deal)
			dealHash := crypto.Sha256(Dealbytes)
			// deal2 := &protobuf.Deal2{
			// 	Spid:       threadInfolist.Spid,
			// 	SpSig:      threadInfolist.Sig,
			// 	List:       threadInfolist.InfoList,
			// 	PublicKey:  miner.node.PosPk.ToProto(),
			// 	Timestamp:  time.Now().UnixNano(),
			// 	ExpiryTime: threadPostingTimeL,
			// }

			// lst := threadInfolist.InfoList
			// lst1 := *lst[0]
			// lst1Bytes, _ := proto.Marshal(lst1.Info)
			// publiketBytes, _ := proto.Marshal(deal.PublicKey)
			// Dealbytes2, _ := proto.Marshal(deal2)
			// log.Info().Msgf("new Deal Size: %v Deal Bytes %v deal2 Bytes %v, spid %v,SpSig %v list %v listBytesSize %v publickey %v %v Name %v %v posparam %v %v timestamp %v  expirtTime %v",
			// 	unsafe.Sizeof(*deal), len(Dealbytes), len(Dealbytes2), len((deal.Spid)), len((deal.SpSig)), unsafe.Sizeof((lst1)), len(lst1Bytes), unsafe.Sizeof(*(deal.PublicKey)),
			// 	len(publiketBytes), unsafe.Sizeof(deal.Name), len(deal.Name), unsafe.Sizeof(deal.PosParam), len(deal.PosParam), unsafe.Sizeof(deal.Timestamp), unsafe.Sizeof(deal.ExpiryTime))

			// dealBytes, _ := proto.Marshal(deal)
			// localExpiryTime := time.Unix(0, int64(deal.ExpiryTime))
			// log.Info().Msgf("Miner --- Deal: %v Tags: %v, Data length %v, data %v", hex.EncodeToString(crypto.Sha256(dealBytes)), tags.String(), len(data), string(data))
			log.Info().Msgf("Miner --- Deal Made! -- DealHash: %v ", hex.EncodeToString(dealHash))

			dealTagPair := &protobuf.DealTagPair{
				DealHash: dealHash,
				Tags:     tags.ToByteArray(),
			}
			// pairing, _ := pbc.NewPairingFromString(deal.PosParam)
			pbKey := new(pos.PublicKey)
			pbKey.FromProto(deal.PublicKey, miner.node.PosPairing)
			// fmt.Println("miner.node.pos: ", miner.node.PosPk, "deal.pk: ", deal.PublicKey, "pbKey", pbKey)

			dealTagPairBytes, _ := proto.Marshal(dealTagPair)
			newMsg := net.NewMessage(net.MsgCodeReceiveVarificationTags, dealTagPairBytes)
			// log.Info().Msgf("MINER: --- dealTagPair msg len %v", len(newMsg.Data))
			miner.node.Relay(msg.From, newMsg)

			miner.msgMapMtx.Lock()
			delete(miner.msgMap, string(threadheadPostHash))
			miner.msgMapMtx.Unlock()

			miner.localspReqListCacheMtx.Lock()
			delete(miner.localspReqListCache[spIDstring], string(threadheadPostHash))
			miner.localspReqListCacheMtx.Unlock()

			miner.deals = append(miner.deals, deal)

			// dealWeightPair := &protobuf.Activedeals{
			// 	Deal:   deal,
			// 	Weight: 1,
			// }

			// miner.Activedeals[string(crypto.Sha256(dealBytes))] = dealWeightPair
		}

		if threadInfolist.InfoListNotStoredPost != nil {
			threadheadPostHash := threadInfolist.InfoListNotStoredPost[0].Info.Metadata.ThreadRootPostHash
			if threadheadPostHash == nil { // if it is a new post then set threadheadPostHash to Hash of metadata
				threadheadPostHashbytes, _ := proto.Marshal(threadInfolist.InfoListNotStoredPost[0].Info)
				threadheadPostHash = crypto.Sha256(threadheadPostHashbytes)
			}

			for _, pr_item := range threadInfolist.InfoList {
				postbytes, _ := proto.Marshal(pr_item)
				var posterID noise.PublicKey
				copy(posterID[:], pr_item.Info.Metadata.Pid)
				// posterID := pr_item.Info.Metadata.Pid
				// posterKadID, _ := noise.UnmarshalID(pr_item.Info.Metadata.KadID)
				// var pbKey noise.PublicKey
				// copy(pbKey[:], pr_item.Info.Metadata.Pid)

				newMsg := net.NewMessage(net.MsgCodePostNotStored, postbytes) // TODO: find another storage provider foe these posts
				miner.node.RelayToPB(posterID, newMsg)
			}

			miner.msgMapMtx.Lock()
			delete(miner.msgMap, string(threadheadPostHash))
			miner.msgMapMtx.Unlock()

			miner.localspReqListCacheMtx.Lock()
			delete(miner.localspReqListCache[spIDstring], string(threadheadPostHash))
			miner.localspReqListCacheMtx.Unlock()
		}

		// if len(miner.localspReqListCache[spIDstring]) > 0 {
		// 	log.Warn().Msgf("Local SP Reqlist Not Empty %v", len(miner.localspReqListCache[spIDstring]))
		// }
	}
	log.Info().Msgf("handleSpPostRequestStoreReply: After Deal made miner.prCache.Size(): %v", miner.prCache.Size())
	// if len(miner.msgMap) > 0 {
	// 	log.Info().Msgf("msgMap Remaining %v", len(miner.msgMap))
	// }

}

func (miner *Miner) handleSpSaysPostNotStored(msg net.Message) {
	log.Info().Msgf("Post not stored at %v , find new provider %v ", msg.From.String(), msg.From.ID.String())
	log.Info().Msgf("SPID handleSpSaysPostNotStored %v %v ", msg.From.ID, msg.From.ID.String())
	// Spid id of the sp goes into the staleCache and is only removed on deal expiration
	spid, _ := hex.DecodeString(msg.From.ID.String())
	val, exist := miner.soCache.Get(string(spid))
	if !exist {
		log.Info().Msgf("Storage Provider doesnt exist in the storage offers cache")
		return
	}
	miner.staleSoCache.Add(string(spid), val)
}

func (miner *Miner) getStorageOfferValues() []*protobuf.StorageOffer {
	// Filter the ones which are in staleSoCache -> they are kept on KadID though
	var values []*protobuf.StorageOffer
	for _, key := range miner.soCache.Keys() {
		val, keyExist := miner.soCache.Get(key)
		if keyExist && !miner.staleSoCache.Contains(key) {
			values = append(values, val.(*protobuf.StorageOffer))
		}
	}
	return values
}

func (miner *Miner) verifyPostRequest(pr *protobuf.PostRequest) error {

	posterPubKey := pr.SignedInfo.Info.Metadata.Pid
	if pr.SignedInfo.Info.Param.MaxCost < 0 {
		return fmt.Errorf("Miner: Invalid MaxCost from poster: %s", posterPubKey)
	}

	if bytes, err := proto.Marshal(pr.SignedInfo.Info); err != nil {
		panic(fmt.Sprintf("Failed to marshal SimpleRequest: %s", err))
	} else {
		if !crypto.Verify(posterPubKey, bytes, pr.SignedInfo.Sig) {
			return fmt.Errorf("Invalid PostRequest signature")
		}
	}

	return nil
}

//handleRelayedVote handles the vote being relayed by the user
func (miner *Miner) handleRelayedVote(msg net.Message) {
	bVote, err := core.VoteFromBytes(msg.Data) //Broadcasted votes
	if err != nil {
		log.Error().Err(err)
		return
	}
	// key := bVote.GetID()
	pbVote := bVote.GetProtoVote()
	miner.votes.Push(pbVote)
	// fmt.Println("Vote added in Miner cache")
	// miner.node.Dpos.BVotes = append(miner.node.Dpos.BVotes, bVote)
	// fmt.Println("relayed vote added in dpos")
}

func (miner *Miner) handleRelayedAmountTx(msg net.Message) {
	aTrans, err := core.TransactionFromBytes(msg.Data)
	if err != nil {
		log.Error().Err(err)
		return
	}
	pbTransaction := aTrans.GetProtoTransaction()
	miner.transactions.Push(pbTransaction)
	log.Info().Msgf("transaction added to the cache")
}

//handleRelayedPodf handles the relayed podf-> Miner puts its in the  cache and wait for the transaction to be added
func (miner *Miner) handleRelayedPodf(msg net.Message) {
	podf, err := core.PodfFromBytes(msg.Data)
	if err != nil {
		fmt.Println(err.Error())
		return
	}
	pbPodf := podf.GetProtoPodf()
	miner.podfs.Push(pbPodf)
	log.Info().Msgf("Podf added to the cache")
}

//UpdateCache updates the cache by removing the redundent transactions(already added in block) from the cache
func (miner *Miner) UpdateCache(block *core.Block) {
	var err error

	blockVotes := block.GetVotes()
	interfaceVotes := make([]interface{}, len(blockVotes))
	for x := range blockVotes {
		interfaceVotes[x] = blockVotes[x]
	}

	err = miner.votes.RemoveRedundantTx(interfaceVotes)
	if err != nil {
		// REDO Waller,Votes  changes
		panic(err)
	}

	blockTransactions := block.GetTransactions()
	interfaceTransactions := make([]interface{}, len(blockTransactions))
	for x := range blockTransactions {
		interfaceTransactions[x] = blockTransactions[x]
	}
	err = miner.transactions.RemoveRedundantTx(interfaceTransactions)
	if err != nil {
		panic(err)
	}

	bPodf := block.GetPodf()
	interfacePodf := make([]interface{}, len(bPodf))
	for x := range bPodf {
		interfacePodf[x] = bPodf[x]
	}

	err = miner.podfs.RemoveRedundantTx(interfacePodf)
	if err != nil {
		panic(err)
	}

	// log.Info().Msgf("Update Stats: miner.prCache.Size() Before: %v", miner.prCache.Size())
	for _, deal := range block.GetDeals() {
		// just in case
		// dealBytes, _ := proto.Marshal(deal)
		// if _, ok := miner.Activedeals[string(crypto.Sha256(dealBytes))]; ok {
		// 	continue
		// } else {
		// 	dealWeightPair := &protobuf.Activedeals{
		// 		Deal:   deal,
		// 		Weight: 1,
		// 	}
		// 	miner.Activedeals[string(crypto.Sha256(dealBytes))] = dealWeightPair
		// }

		// spIDstring := string(deal.Spid)
		// storageUsed := uint32(0)
		for _, infopr := range deal.List {
			hashbytes, _ := proto.Marshal(infopr.Info)
			hash := crypto.Sha256(hashbytes)
			// log.Info().Msgf("Update Stats: miner.prCache removed post hash %v", hex.EncodeToString(hash))
			miner.prCache.Remove(string(hash))
			miner.stalePrCache.Remove(string(hash))
			// storageUsed += infopr.Info.Metadata.GetContentSize()
		}

		// miner.storageLeftMtx.Lock()
		// miner.storageLeft[spIDstring] = miner.storageLeft[spIDstring] - (storageUsed + 1000) // TODO : 1000 represents chunk think
		// miner.storageLeftMtx.Unlock()

		threadheadPostHash := deal.List[0].Info.Metadata.ThreadRootPostHash
		if threadheadPostHash == nil {
			threadheadPostHashbytes, _ := proto.Marshal(deal.List[0].Info)
			threadheadPostHash = crypto.Sha256(threadheadPostHashbytes)
		}

		miner.threadPostingTimeMtx.Lock()
		if _, ok := miner.threadPostingTime[string(threadheadPostHash)]; !ok {
			miner.threadPostingTime[string(threadheadPostHash)] = deal.ExpiryTime
		}
		miner.threadPostingTimeMtx.Unlock()
	}

	// log.Info().Msgf("Update Stats: miner.prCache.Size() after %v", miner.prCache.Size())
	miner.ReceivedChPrPairMtx.Lock()
	for _, cProof := range block.GetChPrPair() {
		dealHash := string(cProof.Dealhash)
		delete(miner.ReceivedChPrPair, dealHash)
		// log.Info().Msgf("Update Stats: miner.ReceivedChPrPair removed for Deal %v len %v", hex.EncodeToString([]byte(dealHash)), len(miner.ReceivedChPrPair))
	}
	miner.ReceivedChPrPairMtx.Unlock()
	// log.Info().Msgf("Update Stats: miner.ReceivedChPrPair len %v", len(miner.ReceivedChPrPair))

}

//PrintPostStats prints the post stats
func (miner *Miner) PrintPostStats() {

	fmt.Println("Miner: Found Offers ,", miner.soCache.Size(), " PostRequest Cache: ", miner.prCache.Size())
	fmt.Println("Active deals: ", len(miner.node.Activedeals))
	// fmt.Println("Active deals: ", miner.node.Activedeals)
	fmt.Println("Miner: localspReqListCache ", len(miner.localspReqListCache), " msgMap ", len(miner.msgMap))
	// fmt.Println("Miner: localspReqListCache ", miner.localspReqListCache, " msgMap ", miner.msgMap)
	fmt.Println("Miner: threadPostingTime ", len(miner.threadPostingTime))
	// fmt.Println("Miner: threadPostingTime ", miner.threadPostingTime)
	fmt.Println("Miner: ReceivedChPrPair ", len(miner.ReceivedChPrPair), ", ChPrPair: ", len(miner.ChPrPair), ", Deal ", len(miner.deals))
	// fmt.Println("Miner: ReceivedChPrPair ", miner.ReceivedChPrPair, ", ChPrPair: ", miner.ChPrPair, ", Deal", miner.deals)
	fmt.Println("Miner: staleSoCache : ", miner.staleSoCache.Size(), ", stalePrCache Cache: ", miner.stalePrCache.Size())
	// fmt.Println("Miner: staleSoCache : ", miner.staleSoCache, ", stalePrCache Cache: ", miner.stalePrCache)
	miner.node.Wm.PrintWalletBalances()

}

// StartProcessingStoredPostRequests process postRequest that are stored temporarily in cache
func (miner *Miner) StartProcessingStoredPostRequests() {
	log.Info().Msgf("Miner: StartProcessingStoredPostRequests Enter %v", miner.node.Dpos.NumberOfToMineBlockPerSlot)
	time.Sleep(3 * time.Second) // to process incoming block // should be based on signal/channel

	for i := 0; i < miner.node.Dpos.NumberOfToMineBlockPerSlot; i++ {

		breakingTime := miner.node.Dpos.TimeLeftToTick(time.Now().Unix()) - 2*time.Second //here 2 sec before which it should stop
		for breakingTime < 0 {                                                            //In case it is less than 0 then simply wait until new slot comes
			time.Sleep(time.Second)
			breakingTime = miner.node.Dpos.TimeLeftToTick(time.Now().Unix()) - 2*time.Second //here 2 sec before which it should stop
		}

		if miner.node.Dpos.GetCurrentMinerAdd() != miner.node.GetHexID() {
			break //If this is not my slot then simply break(could be possible)
		}

		// fmt.Println("Breaking time: ", breakingTime, "i", i)
		processingTimer := time.After(breakingTime)
		// log.Info().Msgf("Miner: Waiting for startProcessingSignal")
		// <-miner.startProcessingSignal
		// log.Info().Msgf("Miner: Got startProcessingSignal")
	L:
		for {
			if miner.node.Syncer.GetIsSyncing() {
				time.Sleep(2 * time.Second)
				continue
			} else {
				select {

				// case <-miner.stopProcessingSignal:
				// 	break L //Should return once the mining time is over
				case <-processingTimer:
					fmt.Println("BREAKING PROCESSING ---------------------")
					break L

				case <-time.After(2 * time.Second): //This is the time-gap given to recheck new posts.
					log.Info().Msgf("Miner: miner.prCache.Size %v", miner.prCache.Size())
					if miner.prCache.Size() > 0 {

						offers := miner.getStorageOfferValues()
						if len(offers) == 0 {
							log.Info().Msgf("Miner: No more storage request !!")
							continue
						}

						sort.Slice(offers, func(i, j int) bool {
							return offers[i].MinPrice < offers[j].MinPrice
						})

						log.Info().Msgf("Miner: Found Offers : %v, PostRequest Cache: %v", len(offers), miner.prCache.Size())
						for _, key := range miner.prCache.Keys() {
							val1, keyExist := miner.prCache.Get(key)
							if keyExist {
								pr := val1.(*protobuf.PostRequest)
								hash := []byte(key.(string))

								var spid string
								threadheadPostHash := pr.SignedInfo.Info.Metadata.ThreadRootPostHash
								if threadheadPostHash == nil { // if it is a new post then set threadheadPostHash to Hash of metadata
									threadheadPostHash = hash
								}

								threadheadPostHashString := string(threadheadPostHash)
								val, ok := miner.postToPrimarySpCache.Get(threadheadPostHashString)
								if !ok { // If primary sp doesn't already exist
									// TODO: if no storage offer is found respond to poster
									for _, sto := range offers {
										if sto.MinPrice <= pr.SignedInfo.Info.Param.MaxCost && sto.MaxDuration >= pr.SignedInfo.Info.Param.MinDuration && sto.GetSize() >= pr.SignedInfo.Info.Metadata.GetContentSize() {
											spid = string(sto.Spid)
											miner.postToPrimarySpCache.Add(threadheadPostHashString, spid)
											break
											// miner.soCache.Remove(spid) // Remove storage offer from cache
											// miner.storageLeft[spid] = make(map[string]uint32)
											// miner.storageLeft[string(sto.Spid)][threadheadPostHashString] = sto.GetSize() - pr.SignedInfo.Info.Metadata.GetContentSize()
											// log.Info().Msgf("Miner: Found matching StorageOffer, SimpleRequest sent")
											// TODO: Error handling incase of spid does not exist
											// check against poster's sp whitelist
											// if whiteList := pr.Parameter.SpidWhitelist; len(whiteList) > 0 && !util.BytesSliceContains(whiteList, st.Spid) {
											// 	continue
											// }
										}
									}
								} else { // Primary storage provider exists
									// miner.storageLeftMtx.Lock()
									// storageLeft1 := miner.storageLeft[val.(string)]
									// miner.storageLeftMtx.Unlock()
									// fmt.Println("storageLeft1: ", storageLeft1)
									// if storageLeft1 < pr.SignedInfo.Info.Metadata.GetContentSize() { // No storage left -> find new sp
									spid = val.(string)
									// fmt.Println("spid found for post: ", spid)
									if miner.staleSoCache.Contains(spid) {
										fmt.Println("spid in staleCache, find other: ", val.(string))
										// offers = miner.getStorageOfferValues()
										// if len(offers) == 0 {
										// 	log.Info().Msgf("Miner: No more storage request !! 22")
										// 	spid = ""
										// } else {
										// 	sort.Slice(offers, func(i, j int) bool {
										// 		return offers[i].MinPrice < offers[j].MinPrice
										// 	})
										foundNewSp := false
										for _, sto := range offers { // Find New SP for post // TODO: make it a funciton after testing
											if string(sto.Spid) != spid && sto.MinPrice <= pr.SignedInfo.Info.Param.MaxCost && sto.MaxDuration >= pr.SignedInfo.Info.Param.MinDuration && sto.GetSize() >= pr.SignedInfo.Info.Metadata.GetContentSize() {
												if miner.staleSoCache.Contains(string(sto.Spid)) {
													continue
												}

												miner.postToPrimarySpCache.Add(threadheadPostHashString, string(sto.Spid))
												spid = string(sto.Spid)
												fmt.Println("storage full for Found other: ", spid)
												foundNewSp = true
												break // Matched
											}
										}

										if !foundNewSp {
											log.Info().Msgf("spid not set")
											spid = ""
										}
										// }
									}
								}

								if spid == "" {
									log.Info().Msgf("No Offer Matched for Post Request.")
									// return
									continue // Didnt match continue for other prs
								}
								miner.localspReqListCacheMtx.Lock()
								if requestListMap, ok := miner.localspReqListCache[spid]; ok {

									if threadRequestList, ok2 := requestListMap[threadheadPostHashString]; ok2 {
										threadRequestList.List = append(threadRequestList.List, pr)
										// threadRequestList.Content = append(threadRequestList.Content, pr.Content...)
										miner.msgMapMtx.Lock()
										miner.msgMap[threadheadPostHashString] = append(miner.msgMap[threadheadPostHashString], pr.Content...)
										miner.msgMapMtx.Unlock()
									} else {
										threadRequestListNew := new(protobuf.ThreadPostRequestList)
										threadRequestListNew.List = append(threadRequestListNew.List, pr)
										// threadRequestListNew.Content = pr.Content
										miner.localspReqListCache[spid][threadheadPostHashString] = threadRequestListNew
										miner.msgMap[threadheadPostHashString] = pr.Content
									}
								} else {
									threadRequestList := new(protobuf.ThreadPostRequestList)
									threadRequestList.List = append(threadRequestList.List, pr)
									// threadRequestList.Content = pr.Content
									miner.msgMapMtx.Lock()
									miner.msgMap[threadheadPostHashString] = pr.Content
									miner.msgMapMtx.Unlock()

									// requestListNew := new(protobuf.RequestList)
									// requestListNew.List = append(requestListNew.List, threadRequestList)

									miner.localspReqListCache[spid] = make(map[string]*protobuf.ThreadPostRequestList)
									miner.localspReqListCache[spid][threadheadPostHashString] = threadRequestList
								}
								miner.localspReqListCacheMtx.Unlock()
								miner.prCache.Remove(key)
								miner.stalePrCache.Add(string(hash), pr)
							} else {
								miner.prCache.Remove(key)
							}

						}

						miner.localspReqListCacheMtx.Lock()
						for spid, requestListMap := range miner.localspReqListCache {
							toKadID, ok2 := miner.spKadIdCache.Get(spid)
							if !ok2 {
								panic(fmt.Sprintf("Miner: Failed to KadID for Primary spid for post"))
							}

							requestListNew := new(protobuf.RequestList)
							for _, threadRequestList := range requestListMap {
								_ = threadRequestList
								requestListNew.List = append(requestListNew.List, threadRequestList)
							}

							postBytes, err := proto.Marshal(requestListNew)
							if err != nil {
								log.Info().Msgf("Could not marshal request list. Continuing...")
								continue
							}
							newMsg := net.NewMessage(net.MsgCodeStorageProviderRecievePostRequest, postBytes)
							// log.Info().Msgf("MINER: --- request list post %v", requestListNew)
							// log.Info().Msgf("MINER: --- len: %v, request list post msg len %v", len(requestListNew.List), len(newMsg.Data))
							miner.node.Relay(toKadID.(noise.ID), newMsg)
						}
						miner.localspReqListCacheMtx.Unlock()

					}
				}
			}
		}
		time.Sleep(2 * time.Second)
	}

	log.Info().Msgf("Miner: StartProcessingStoredPostRequests Exit")
}

//TestRelayToSp tests relay msg to the storage provider
func (miner *Miner) TestRelayToSp() {
	fmt.Println("Testing sp relay msg...")
	if miner.spKadIdCache.Size() == 0 {
		fmt.Println("No one to test relay to.")
		return
	}
	// id
	id, got := miner.spKadIdCache.Pop()
	if got != nil {
		fmt.Println("Didn't get id")
		return
	}

	bs := []byte("This is a test msg" + time.Now().String())
	log.Info().Msgf("Relaying test msg %v", id.(noise.ID))
	msg := net.NewMessage(net.TestMsg, bs)
	miner.node.Relay(id.(noise.ID), msg)

}

//GetDelegateRegTx returns the DR tx from the cache
func (miner *Miner) GetDelegateRegTx() []*protobuf.Transaction {
	var delegateTx []*protobuf.Transaction
	delSize := miner.transactions.LengthOfPool()
	for i := 0; i < delSize; i++ {
		val, _ := miner.transactions.Pop()
		aTrans := val.(*protobuf.Transaction)
		if aTrans.GetType() == core.TxBecomeCandidate {
			delegateTx = append(delegateTx, aTrans)
		} else {
			miner.transactions.Push(aTrans)

		}

	}
	fmt.Println("returning size: ", len(delegateTx))
	return delegateTx
}

//CheckCandWithDelRegTx checks the delegate reg tx
func (miner *Miner) CheckCandWithDelRegTx(delTx []*protobuf.Transaction) error {
	if len(delTx) != miner.node.Dpos.State.TotalCandidates() { //Should also include itself's
		fmt.Println("tx: ", len(delTx), "can: ", miner.node.Dpos.State.TotalCandidates())
		panic("Some del-reg tx have been missed. Re-run")
	}
	for _, tx := range delTx {
		fmt.Println("id is: ", hex.EncodeToString(tx.GetBody().GetPayerId()))
		if !miner.node.Dpos.State.CheckCandExistance(hex.EncodeToString(tx.GetBody().GetPayerId())) {
			panic("some cand announcement has been missed")
			// return errors.New("Candidate announcement has been missed")
		}
	}

	return nil
}

// GenerateBlock creats the blocks containing the amount tx,votes, proofs and deals
func (miner *Miner) GenerateBlock(transactions []*protobuf.Transaction, votes []*protobuf.Vote, deals []*protobuf.Deal, chPrPair []*protobuf.ChPrPair, podfs []*protobuf.PoDF, roundNum, bTs int64, broadcast bool) *core.Block {
	// statTime := time.Now()
	var block *core.Block
	var err error
	// if len(deals) == 0 {
	// 	fmt.Println("No Deals in the block!!!!")
	// }
	node := miner.node
	node.BcMtx.Lock()
	defer node.BcMtx.Unlock()
	minerPk := miner.node.PublicKey()
	// minerAdd := miner.node.GetAddressUsingPublicKey(minerPk)
	minerAdd := minerPk.String()

	if node.Bc == nil {
		log.Info().Msgf("Creating genesis block")
		trans := miner.GetDelegateRegTx()
		err := miner.CheckCandWithDelRegTx(trans)
		if err != nil {
			fmt.Println(err.Error())
			return nil
		}

		if broadcast == false {
			block, err = core.NewGenesisBlockWithTs(minerPk, miner.node.TimeOffset, trans, bTs)
		} else {
			block, err = core.NewGenesisBlock(minerPk, miner.node.TimeOffset, trans)
		}

		// if block == nil {
		// 	fmt.Println("Signing nil block")
		// } else {
		// 	if block.PbBlock.GetHeader() == nil {
		// 		fmt.Println("Signing nil block header")
		// 	}
		// }
		block.Sign(node.PrivateKey())
		// fmt.Println("Miner G key!!!: ", block.MinerPublicKey())

		if err != nil {
			log.Error().Err(err)
			return nil
		}

		isValid := block.IsValid()
		if isValid != true {
			log.Info().Msgf("Block is not valid!")
			return nil
			// return errors.New("Block is not valid")
		}

		miner.node.Bc, err = core.InitBlockchain(node.Db, block)
		if err != nil {
			log.Error().Err(err)
			return nil
		}

		node.Wm.ApplyGenesis(block)
		node.Dpos.SetBlockchain(node.Bc)
		// err = miner.node.Wm.GenesisDelegateReg(block)
		// if err != nil {
		// 	log.Info().Err(err)
		// }
		// miner.node.Wm.Bc = miner.node.Bc
	} else {
		parent := node.Bc.GetMainTail()
		// log.Info().Msgf("GOT PARENT FOR NEW BLOCK %s", parent)
		if broadcast == false {
			block = core.CreateNewBlockWithTs(parent, node.PublicKey(), node.PrivateKey(), transactions, votes, deals, chPrPair, podfs, miner.node.TimeOffset, roundNum, bTs) //Extra transaction variable as comp to MakeTestBlock
		} else {
			var tchPrPair []*protobuf.ChPrPair
			for _, chPr := range miner.ReceivedChPrPairForTesting {
				chPr.DealStartTime = chPr.DealExpiryTime
				chPr.DealExpiryTime = time.Now().Unix()
				tchPrPair = append(tchPrPair, chPr)
			}
			block = core.CreateNewBlock(parent, node.PublicKey(), node.PrivateKey(), transactions, votes, deals, tchPrPair, podfs, miner.node.TimeOffset, roundNum) //Extra transaction variable as comp to MakeTestBlock
		}
		// node.Bc.Append([]*core.Block{block})                                                                                                          //If you comment the below states(vote,transaction and wallet) out it won't update
		// fmt.Println("Miner key!!!: ", block.MinerPublicKey())
		isValid := block.IsValid()
		if isValid != true {
			log.Info().Msgf("Block is not valid. Returning")
			return nil
			// return errors.New("Block is not valid")
		}

		// wst := time.Now()
		err = node.Wm.CheckAndUpdateWalletStates(block, minerAdd) // only added validated transaction
		if err != nil {
			log.Error().Msgf("Miner -- Wallet state could not be updated")
			log.Error().Msgf(err.Error())
		}
		// fmt.Printf("\tWallet state update Time: %v\n", time.Now().Sub(wst))

		// node.Wm.ReverseBlockUpdate(block, minerAdd)
		// if err != nil {
		// 	panic(err)
		// }
		// node.Dpos.ApplyVotes(block) //For reflecting changes in the current node
		// wst = time.Now()
		// err = miner.node.Wm.CheckAndApplyVotes(block, minerAdd)
		// if err != nil {
		// 	log.Error().Msgf(err.Error())
		// 	// return err
		// }

		miner.node.AddDeals(deals)
		miner.node.RemoveExpiredDeals(block.GetTimestamp(), block.GetChPrPair())
		miner.node.Bc.UpdateIrreversibleBlockNumber(minerAdd, miner.node.Dpos.GetComitteeSize(), miner.node.Dpos.NumberOfToMineBlockPerSlot)
		if miner.node.StorageProvider != nil {
			miner.node.StorageProvider.ProcessNewBlock(block)
		}
		if miner.node.User != nil {
			miner.node.UpdateThreadBase(block)
		}

		// fmt.Printf("\tApply Vote Time: %v\n", time.Now().Sub(wst))
	}
	if err != nil {
		log.Error().Err(err)
		log.Error().Msgf("Miner -- Block Mined but Not added")
		return nil
	}

	// endTime := time.Now()
	// fmt.Printf("\tBlock createing Time: %v\n", endTime.Sub(statTime))

	blockBytes, err := block.ToBytes()
	// fmt.Println("Block bytes:-", len(blockBytes))
	if err != nil {
		log.Error().Err(err)
		return nil
	}
	if broadcast == false {
		log.Info().Msgf("Prepared %s", block)
		// node.Bc.Append([]*core.Block{block})
		// log.Info().Msgf("New Blockchain: %s", node.Bc)

	} else {
		log.Info().Msgf("CREATED %s", block)
		// node.Bc.Append([]*core.Block{block})
	}
	node.Bc.Append(block)
	node.Bc.UpdateMainTailIndex()
	log.Info().Msgf("New Blockchain: %s", node.Bc)
	miner.PrintPostStats()
	miner.deals = nil
	miner.ChPrPair = nil
	// miner.ReceivedChPrPairMtx.Lock()
	// miner.ReceivedChPrPair = make(map[string]*protobuf.ChPrPair)
	// miner.ReceivedChPrPairMtx.Unlock()

	if broadcast == true {
		msg := net.NewMessage(net.MsgCodeBlockNew, blockBytes)
		node.Broadcast(msg)
	}

	return block
}

//GenerateDigestBlock generates the digest
func (miner *Miner) GenerateDigestBlock(roundNum, bTs int64, broadcast bool) (*core.Block, error) {
	fmt.Println("Creating digest block instead")
	var block = new(core.Block)
	var err error

	node := miner.node
	node.BcMtx.Lock()
	defer node.BcMtx.Unlock()

	if node.Bc == nil {
		return nil, errors.New("Blockchain is nil")
	}
	parent := node.Bc.GetMainTail()

	accEntries := miner.node.Wm.TotalWallets()
	block, err = core.CreateEmptyBlock(parent, node.PublicKey(), miner.node.TimeOffset, roundNum, bTs, accEntries)
	if err != nil {
		return nil, err
	}
	// miner.node.Wm.GetDigestBlockReward()
	curEpochNum := miner.node.GetCurEpoch()
	miner.node.Wm.AddBlockState(block)                                //Adding the balances of the users
	block.PbBlock.State.Miners = miner.node.Dpos.GetMinersAddresses() //Setting the miner addresses
	block.PbBlock.State.Epoch = curEpochNum
	miner.node.SetCurEpoch(curEpochNum + 1) //Updating the current epoch

	err = block.HashAndSign(node.PrivateKey())
	if err != nil {
		return nil, err
	}

	isValid := block.IsValid()
	if isValid != true {
		fmt.Println("Block is not valid. Returning")
		log.Info().Msgf("Block is not valid. Returning")
		return nil, errors.New("Invalid block")
		// return errors.New("Block is not valid")
	}
	blockBytes, err := block.ToBytes()
	fmt.Println("Digest Block bytes:-", len(blockBytes))
	if err != nil {
		log.Error().Err(err)
		return nil, errors.New("Error converting block to bytes")
	}

	// node.Bc.Append([]*core.Block{block})
	node.Bc.Append(block)
	log.Info().Msgf("New Blockchain: %s", node.Bc)
	node.Bc.UpdateMainTailIndex()

	miner.deals = nil
	miner.ChPrPair = nil
	if broadcast == true {
		msg := net.NewMessage(net.MsgCodeBlockNew, blockBytes)
		node.Broadcast(msg)
	}

	// err = node.ApplyDigestBlock(block) //Applying changes locally after broadcasting
	minerPk := block.MinerPublicKey()
	minerAdd := minerPk.String()
	err = node.Wm.CheckAndUpdateWalletStates(block, minerAdd) //Skip/remove transactions from block which are invalid
	if err != nil {
		log.Error().Msgf("Miner -- Wallet state could not be updated")
		log.Error().Msgf(err.Error())
	}
	miner.node.EmptyRoleCaches()

	return block, nil
}

// //StartBlockPrep starts to prepare the blocks
// func (miner *Miner) StartBlockPrep() {

// 	//Check if all blocks from last miner has been received
// 	tickDeadline := miner.node.Dpos.TimeLeftToTick(time.Now().Unix())

// 	for tickDeadline-2*time.Second > 0 { //Wait until two seconds are left in the tick or all blocks from miner has been received

// 		err := miner.node.Bc.CheckLastMinerBlocks(miner.node.GetHexID(), int64(miner.node.Dpos.BlocksPerMiner()))
// 		if err != nil {
// 			break
// 		}
// 		time.Sleep(500 * time.Millisecond)
// 	}

// 	//Start filling block
// 	tickDeadline = miner.node.Dpos.TimeLeftToTick(time.Now().Unix())

// 	// for {

// 	// }

// }

//PrepareBlock prepares the blocks
func (miner *Miner) PrepareBlock(roundNum int64) *core.Block {
	bTs := miner.node.Dpos.NextTickTime(time.Now().Unix())
	// miner.stopProcessingSignal <- struct{}{}
	if miner.node.Bc == nil {
		var transactions []*protobuf.Transaction
		var voteList []*protobuf.Vote
		var deals []*protobuf.Deal

		b := miner.GenerateBlock(transactions, voteList, deals, nil, nil, roundNum, bTs*1e9, false)
		return b
	}
	bcHeight := int64(miner.node.Bc.GetMainForkHeight())
	// fmt.Println("Block chain height: ", bcHeight)
	if bcHeight%core.TestEpoch.Duration == 0 {
		//Time to mine a digest block
		b, err := miner.GenerateDigestBlock(roundNum, bTs*1e9, false)
		if err != nil {
			log.Info().Err(err)
		}
		return b
	}

	miner.GenerateChallenges(int64(binary.BigEndian.Uint64(miner.node.Bc.GetMainTail().Hash()[0:8])))
	deals, proofs, transactions, votes, podfs := miner.FillBlock()
	b := miner.GenerateBlock(transactions, votes, deals, proofs, podfs, roundNum, bTs*1e9, false)
	return b
}

//CreateBlockLoop creates block when the ticker notifies in DPOS
func (miner *Miner) CreateBlockLoop() {
	//TODO:- precheck if transactions are valid
	prepared := false
	// var ts int64 //Keeping the timestamp to maintain when block is produced
	var block *core.Block = nil

	for {
		select {
		case <-miner.ExitBlockLoop: //This signal will be given from dpos
			return
		case roundNum := <-miner.node.Dpos.CreateBlock: //This signal will be given from dpos
			// case <-miner.node.Dpos.CreateBlock: //This signal will be given from dpos

			if prepared == true {
				// for preparing == true { //Should wait while preparing is true
				// 	time.Sleep(100 * time.Millisecond)
				// }
				if block != nil { //Make sure block is not stale

					if block.GetTimestamp() < (time.Now().UnixNano() - 2*int64(miner.node.Dpos.BlockPrepTime().Nanoseconds())) {
						fmt.Println("Got old prepared block. Removing it")
						block = nil //Removing the stale block
						prepared = false
					} else {
						fmt.Println("Broadcasting block now!!!")
						log.Info().Msgf("Block: %v", block)
						blockBytes, err := block.ToBytes()
						if err != nil {
							log.Error().Err(err)
							continue
						}
						msg := net.NewMessage(net.MsgCodeBlockNew, blockBytes)
						miner.node.Broadcast(msg)
						// if !miner.startProcessingSignalSent1stTime {
						// 	miner.startProcessingSignal <- struct{}{}
						// log.Info().Msgf("Miner: Sent startProcessingSignal 2")
						// miner.startProcessingSignalSent1stTime = true
						miner.ResetCache()
						// }
						block = nil //Resetting the block
						prepared = false
						// if miner.node.Dpos.TimeLeftToMine(time.Now().Unix())+2*time.Second > miner.node.Dpos.GetTickInterval() {
						// }
						continue
					}
				}
			}
			fmt.Println("DIdn;t get the prepaed block. creating now")
			if miner.node.Bc == nil {
				var transactions []*protobuf.Transaction
				var voteList []*protobuf.Vote
				var deals []*protobuf.Deal
				miner.GenerateBlock(transactions, voteList, deals, nil, nil, roundNum, time.Now().UnixNano(), true)
				continue
			}
			bcHeight := int64(miner.node.Bc.GetMainForkHeight())
			// curRound := miner.node.Dpos.GetRoundNum()
			// curEpoch := float64(bcHeight) / float64(core.TestEpoch.Duration)
			// fmt.Println("CUR EPOCH: ", curEpoch, miner.node.GetCurEpoch())
			if bcHeight%core.TestEpoch.Duration == 0 {
				//Time to mine a digest block

				_, err := miner.GenerateDigestBlock(roundNum, time.Now().UnixNano(), true)
				if err != nil {
					log.Info().Err(err)
				}
				continue
			}

			log.Info().Msgf("Let's mine")

			deals, proofs, transactions, votes, podfs := miner.FillBlock()
			miner.GenerateBlock(transactions, votes, deals, proofs, podfs, roundNum, time.Now().UnixNano(), true)
			miner.deals = nil    //TODO:- Bilal please check if these should be set to nil
			miner.ChPrPair = nil //TODO:- Bilal please check if these should be set to nil

		case <-miner.node.Dpos.PrepBlockSig:
			// if miner.node.Dpos.TimeLeftToTick(time.Now().Unix())-miner.node.Dpos.BlockPrepTime() >= 0 {
			// fmt.Println("Sleeping for the prep time")
			// time.Sleep(miner.node.Dpos.TimeLeftToTick(time.Now().Unix()) - miner.node.Dpos.BlockPrepTime())
			if miner.node.Dpos.GetCurMinerAdd() == miner.node.GetHexID() { //once again checking for the current miner
				// preparing = true
				if block != nil && block.GetTimestamp() < (time.Now().UnixNano()-miner.node.Dpos.GetTickInterval().Nanoseconds()) {
					fmt.Println("Has already prepared a block. Will use that one")
					continue
				}
				block = miner.PrepareBlock(miner.node.Dpos.GetRoundNum())
				// preparing = false
				if block != nil {
					prepared = true
				}
			}
			// }
		}
	}
}

//FillBlock fills the block in ascending order till the limit is reached
func (miner *Miner) FillBlock() ([]*protobuf.Deal, []*protobuf.ChPrPair, []*protobuf.Transaction, []*protobuf.Vote, []*protobuf.PoDF) {
	//0.1 mb size
	type BlockObject int
	var dealTy, proofTy, txTy, voteTy, podfTy BlockObject = 0, 1, 2, 3, 4
	limit := MaxBlockSize
	if limit > MaxBlockSize {
		limit = MaxBlockSize //Ensuring that the limit is not greater than the block
	}

	curSize := 0
	// votes := miner.votes
	// transactions := miner.transactions
	// deals := miner.deals
	// proofs := miner.ChPrPair
	// fmt.Println(len(deals))
	var bTrans []*protobuf.Transaction
	var bVotes []*protobuf.Vote
	var bDeals []*protobuf.Deal
	var bProofs []*protobuf.ChPrPair
	var bPodfs []*protobuf.PoDF

	for {
		if int64(curSize) >= limit || miner.AllEmptyCaches() == true {
			break
		}
		// fmt.Println("curSize: ", curSize)

		var lowestTs int64 = math.MaxInt64
		var toChoose BlockObject //Var for choosing which object

		if len(miner.deals) > 0 {
			firstDeal := miner.deals[0]
			if lowestTs > firstDeal.GetTimestamp() {
				toChoose = dealTy
				lowestTs = firstDeal.GetTimestamp()
				// fmt.Println("lowest TS0:- ", lowestTs)
			}
		}
		if len(miner.ChPrPair) > 0 {
			firstProof := miner.ChPrPair[0]
			if lowestTs > firstProof.GetTimestamp() {
				toChoose = proofTy
				lowestTs = firstProof.GetTimestamp()
				// fmt.Println("lowest TS1:- ", lowestTs)

			}
		}
		if miner.transactions.LengthOfPool() > 0 {
			txhead, err := miner.transactions.GetHead()
			if err != nil {
				continue
			}
			firstTx := txhead.(*protobuf.Transaction)
			if lowestTs > firstTx.GetTimeStamp() {
				toChoose = txTy
				lowestTs = firstTx.GetTimeStamp()
				// fmt.Println("lowest TS2:- ", lowestTs)

			}
		}
		if miner.podfs.LengthOfPool() > 0 {
			podfHead, err := miner.podfs.GetHead()
			if err != nil {
				continue
			}
			firstPodf := podfHead.(*protobuf.PoDF)
			if lowestTs > firstPodf.GetTimeStamp() {
				toChoose = podfTy
				lowestTs = firstPodf.GetTimeStamp()
			}

		}
		if miner.votes.LengthOfPool() > 0 {
			voteHead, err := miner.votes.GetHead()
			if err != nil {
				log.Info().Err(err)
				continue
			}
			firstVote := voteHead.(*protobuf.Vote)

			if lowestTs > firstVote.GetTimeStamp() {
				toChoose = voteTy
				lowestTs = firstVote.GetTimeStamp()
				// fmt.Println("lowest TS3:- ", lowestTs)

			}
		}

		bRound := miner.node.Dpos.GetRoundNum()
		switch toChoose {
		case dealTy:
			err := miner.node.Wm.ValidateDeal(miner.deals[0], bRound)
			if err != nil {
				log.Info().Err(err)
				miner.deals = miner.deals[1:] //Removing deal
				continue
			}
			bDeals = append(bDeals, miner.deals[0])
			curSize += miner.deals[0].XXX_Size()
			miner.deals = miner.deals[1:] //Removing deal
			// fmt.Println("later deals:", len(miner.deals[1:]))

			// fmt.Println("added")

		case proofTy:
			bProofs = append(bProofs, miner.ChPrPair[0])
			curSize += miner.ChPrPair[0].XXX_Size()
			miner.ChPrPair = miner.ChPrPair[1:]
			// fmt.Println("added1")

		case txTy:
			val, _ := miner.transactions.Pop()
			aTrans := val.(*protobuf.Transaction)

			valid, err := miner.node.Wm.IsExecutable(aTrans, bRound)
			if valid != true || err != nil {
				log.Info().Err(err)
				continue
			}
			bTrans = append(bTrans, aTrans)
			curSize += aTrans.XXX_Size()
			// fmt.Println("ADDED tx type in BLOCK")

		case voteTy:
			val, _ := miner.votes.Pop()
			vote := val.(*protobuf.Vote)
			valid, err := miner.node.Wm.ValidateVote(vote, bRound)
			if err != nil || valid != true {
				log.Info().Err(err)
				continue
			}
			bVotes = append(bVotes, vote)
			curSize += vote.XXX_Size()
			// fmt.Println("ADDED vote type in BLOCK")

		case podfTy:
			val, _ := miner.podfs.Pop()
			podf := val.(*protobuf.PoDF)
			err := miner.node.Wm.ValidatePodf(podf, bRound)
			if err != nil {
				continue
			}
			bPodfs = append(bPodfs, podf)
			curSize += podf.XXX_Size()

		default:
			continue
		}

	}
	return bDeals, bProofs, bTrans, bVotes, bPodfs
}

//AllEmptyCaches returns true in case all caches are emptu
func (miner *Miner) AllEmptyCaches() bool {
	if miner.votes.IsEmpty() == true && miner.transactions.IsEmpty() == true && miner.podfs.IsEmpty() && len(miner.deals) == 0 && len(miner.ChPrPair) == 0 {
		return true
	}
	return false
}

// //TODO: sort deals based on last verification time or pick deal randomely
// func (miner *Miner) IssueChallenges() {
// 	// ctx, ctxcancle := context.WithTimeout(context.Background(), (miner.node.Dpos.GetMinerSlotTime() - miner.node.Dpos.GetTickInterval())) //The time should be equivalent to the miner timeslot
// 	// defer ctxcancle()
// 	// for {
// 	// 	select {
// 	// 	case <-ctx.Done(): //This will be signalled once the time is reached
// 	// 		log.Info().Msgf("IssueChallenges finished")
// 	// 		return //Should return once the mining time is over
// 	// 	default:

// 	rand.Seed(time.Now().UTC().UnixNano())
// 	var dealhashKeys []string
// 	for dealhash, _ := range miner.Activedeals {
// 		if _, ok := miner.challengeIssuedDeal[dealhash]; !ok {
// 			dealhashKeys = append(dealhashKeys, dealhash)
// 		}
// 	}

// 	log.Info().Msgf("ISSUE CHAllenge active deals size: %v, %v", len(miner.Activedeals), len(dealhashKeys))

// 	for i := 0; i < NumberOfDealToVerify && i < len(dealhashKeys); i++ {
// 		dealhash := dealhashKeys[rand.Intn(len(dealhashKeys))]
// 		for {
// 			if _, ok := miner.challengeIssuedDeal[dealhash]; !ok {
// 				break
// 			}
// 			dealhash = dealhashKeys[rand.Intn(len(dealhashKeys))]
// 		}
// 		miner.issueChallenge(dealhash, miner.Activedeals[dealhash].Deal)
// 	}

// 	// time.Sleep(2 * time.Second)
// 	// 	}
// 	// }
// }

// func (miner *Miner) issueChallenge(dealHash string, deal *protobuf.Deal) {

// 	miner.challengeIssuedDeal[dealHash] = deal
// 	log.Info().Msgf("Issuing challenge for DealHash: %v", hex.EncodeToString([]byte(dealHash)))
// 	contentSize := int(0)
// 	spKadID, _ := noise.UnmarshalID(deal.GetKadID())
// 	// spKadID := kad.FromString(string(deal.GetKadID()))

// 	//TODO: move it to Deal struct
// 	for _, infoPr := range deal.List.InfoList {
// 		contentSize += int(infoPr.Info.Metadata.ContentSize)
// 	}

// 	if contentSize < 150 {
// 		contentSize = 150
// 	}

// 	//TODO: keep challenge and challengeHash pairs for later verifaction
// 	randNum := rand.New(rand.NewSource(0))
// 	pairing, _ := pbc.NewPairingFromString(deal.PosParam)
// 	ch := pos.Challenge(randNum, pos.GetNumberofBlocksforTag(contentSize, miner.node.PosConfig), 3, pairing)
// 	// log.Info().Msgf("Challenge: %v", ch.String())
// 	protoCh := ch.ToProto()
// 	chBytes, _ := proto.Marshal(protoCh)
// 	miner.challengeCache[string(crypto.Sha256(chBytes))] = ch
// 	// Dealbytes, _ := proto.Marshal(deal)
// 	// DealHash := crypto.Sha256(Dealbytes)
// 	dealChPair := &protobuf.DealChallPair{
// 		DealHash:       []byte(dealHash),
// 		Challenge:      protoCh,
// 		DealStartTime:  deal.Timestamp,
// 		DealExpiryTime: deal.ExpiryTime,
// 	}

// 	dealChPairBytes, _ := proto.Marshal(dealChPair)
// 	msg := net.NewMessage(net.MsgCodeSpChallengeCall, dealChPairBytes)
// 	// log.Info().Msgf("MINER: --- dealchpair msg len %v", len(msg.Data))
// 	miner.node.Relay(spKadID, msg)
// 	go func(dealhash, chHash []byte, spid []byte, DealExpiryTime, DealStartTime int64) {
// 		ctxB, cancelB := context.WithTimeout(context.Background(), TimeToWaitForProof*time.Second)
// 		defer cancelB()
// 		<-ctxB.Done()
// 		challengeProofPair := &protobuf.ChallengeProofPair{
// 			ChallengeHash:  chHash,
// 			Proof:          nil,
// 			Sig:            nil,
// 			Spid:           spid,
// 			Dealhash:       dealhash,
// 			DealExpiryTime: DealExpiryTime,
// 			DealStartTime:  DealStartTime,
// 		}

// 		challengeProofPairBytes, _ := proto.Marshal(challengeProofPair)
// 		newMsg := net.NewMessage(net.MsgCodeSpChallengeProf, challengeProofPairBytes)
// 		miner.incoming <- newMsg
// 	}([]byte(dealHash), crypto.Sha256(chBytes), deal.Spid, deal.ExpiryTime, deal.Timestamp)
// }

// func (miner *Miner) HandleSpChallengeProf(msg net.Message) {
// 	log.Info().Msgf("Miner: HandleSpChallengeProf\n ")
// 	challengeProofPair := new(protobuf.ChallengeProofPair)
// 	if err := proto.Unmarshal(msg.Data, challengeProofPair); err != nil {
// 		log.Info().Msgf("Miner: challengeProofPair Unmarshal Error:%v", err)
// 		return
// 	}

// 	ch, hasKey := miner.challengeCache[string(challengeProofPair.ChallengeHash)]
// 	if !hasKey {
// 		// log.Info().Msgf("Miner: ChallengeHash not found or time expired")
// 		return
// 	}

// 	// deal := miner.Activedeals[string(challengeProofPair.Dealhash)].Deal
// 	if challengeProofPair.Proof == nil { //time out occured
// 		chPrPair := &protobuf.ChPrPair{
// 			Challenge:      ch.ToProto(),
// 			Proof:          nil,
// 			Sig:            nil,
// 			Spid:           challengeProofPair.Spid,
// 			Dealhash:       challengeProofPair.Dealhash,
// 			DealExpiryTime: challengeProofPair.DealExpiryTime,
// 			Timestamp:      time.Now().UnixNano(),
// 			DealStartTime:  challengeProofPair.DealStartTime,
// 		}

// 		miner.ChPrPair = append(miner.ChPrPair, chPrPair)
// 		log.Info().Msgf("Miner ---TimeOUT chPrPair %v", hex.EncodeToString(chPrPair.Dealhash))
// 		delete(miner.challengeCache, string(challengeProofPair.ChallengeHash))
// 		delete(miner.challengeIssuedDeal, string(challengeProofPair.Dealhash))
// 		return
// 	}

// 	// log.Info().Msgf("challengeProofPair: %v", challengeProofPair.String())
// 	proofBytes, _ := proto.Marshal(challengeProofPair.Proof)
// 	if !ed25519.Verify(challengeProofPair.Spid, proofBytes, challengeProofPair.Sig) {
// 		log.Info().Msgf("Miner: Invalid challengeProofPair Proof signature")
// 		return
// 	}

// 	deal := miner.challengeIssuedDeal[string(challengeProofPair.Dealhash)]
// 	pairing, err := pbc.NewPairingFromString(deal.PosParam)
// 	if err != nil {
// 		log.Info().Msgf("Could not generate pairing from string")
// 		return
// 	}
// 	proof := new(pos.Proof)
// 	proof.FromProto(challengeProofPair.Proof, pairing)
// 	// log.Info().Msgf("challengeProofPair: %v", proof.String())

// 	pbKey := new(pos.PublicKey)
// 	pbKey.FromProto(deal.PublicKey, pairing)

// 	valid := pos.Verify([]byte(name), pairing, pbKey, ch, proof)
// 	if valid {
// 		log.Info().Msgf("Miner: SP valideted for deal ")
// 	} else {
// 		log.Info().Msgf("Miner: sp validation failed for deal")
// 	}

// 	chPrPair := &protobuf.ChPrPair{
// 		Challenge:      ch.ToProto(),
// 		Proof:          challengeProofPair.Proof,
// 		Sig:            challengeProofPair.Sig,
// 		Spid:           challengeProofPair.Spid,
// 		Dealhash:       challengeProofPair.Dealhash,
// 		DealExpiryTime: challengeProofPair.DealExpiryTime,
// 		Timestamp:      time.Now().UnixNano(),
// 		DealStartTime:  challengeProofPair.DealStartTime,
// 	}

// 	// protoCh, _ := proto.Marshal(chPrPair)
// 	// fmt.Println("Length of chprpair: ", len(protoCh))

// 	miner.ChPrPair = append(miner.ChPrPair, chPrPair)
// 	delete(miner.challengeCache, string(challengeProofPair.ChallengeHash))
// 	delete(miner.challengeIssuedDeal, string(challengeProofPair.Dealhash))
// 	// log.Info().Msgf("Miner: ChPrPar %v", chPrPair)
// }

func (miner *Miner) HandleSpChallengeProf(msg net.Message) {
	miner.ReceivedChPrPairMtx.Lock()
	defer miner.ReceivedChPrPairMtx.Unlock()

	challengeProofPair := new(protobuf.ChPrPair)
	if err := proto.Unmarshal(msg.Data, challengeProofPair); err != nil {
		log.Info().Msgf("Miner: challengeProofPair Unmarshal Error:%v", err)
		return
	}
	// pairing, _ := pbc.NewPairingFromString(miner.node.PosParam.String())
	// chl := new(pos.Chall)
	// chl.FromProto(challengeProofPair.Challenge, pairing)
	// p := new(pos.Proof)
	// p.FromProto(challengeProofPair.Proof, pairing)
	// log.Info().Msgf("HandleSpChallengeProf2 ReceivedChPrPair -- %v, ch %v", hex.EncodeToString(challengeProofPair.Dealhash), chl.String(), p)

	log.Info().Msgf("Miner: Received chprpair for deal :%v", hex.EncodeToString(challengeProofPair.Dealhash))
	miner.ReceivedChPrPair[string(challengeProofPair.Dealhash)] = challengeProofPair
	if miner.StoreDataforTesting {
		miner.ReceivedChPrPairForTesting[string(challengeProofPair.Dealhash)] = challengeProofPair
	}
}

// func (miner *Miner) CheckExpiredDeals() {
// 	// ctx, ctxcancle := context.WithTimeout(context.Background(), (miner.node.Dpos.GetMinerSlotTime() - miner.node.Dpos.GetTickInterval())) //The time should be equivalent to the miner timeslot
// 	// defer ctxcancle()
// 	// for {
// 	// 	select {
// 	// 	case <-ctx.Done():
// 	// 		log.Info().Msgf("CheckExpiredDeals finished")
// 	// 		return //Should return once the mining time is over
// 	// 	default:
// 	currentTime := time.Now()
// 	//TODO: sort deals based on last verification time or pick deal randomely
// 	for dealhash, dealWeightPair := range miner.Activedeals {
// 		ExpiryTime := time.Unix(0, int64(dealWeightPair.Deal.ExpiryTime))
// 		if currentTime.After(ExpiryTime) {
// 			miner.issueChallenge(dealhash, dealWeightPair.Deal)
// 			log.Info().Msgf("Miner -- Expired DealHahs %v Current Time: %v - ExpiryTime %v ", hex.EncodeToString([]byte(dealhash)), currentTime, ExpiryTime)
// 			// delete(miner.threadPostingTime,)
// 			delete(miner.Activedeals, dealhash)

// 			// Remove from staleCache implying the sp now has space
// 			// And the getStorageOffers func would now include his offer as well
// 			val, exist := miner.staleSoCache.Get(string(dealWeightPair.GetDeal().GetSpid()))
// 			if !exist {
// 				log.Info().Msgf("Storage offer doesn't exist in stale cache")
// 				continue
// 			}
// 			miner.soCache.Add(string(dealWeightPair.GetDeal().GetSpid()), val)
// 		}
// 	}

// 	time.Sleep(1 * time.Second)
// 	go miner.IssueChallenges()
// 	// time.Sleep(2 * time.Second)
// 	// 	}
// 	// }
// }

func (miner *Miner) GenerateChallenges(seed int64) {
	// fmt.Printf("seed %v for Miner Challenge\n", seed)
	challengeIssuedDeal2 := make(map[string]bool)

	// miner.node.DealMtx.RLock()
	// defer miner.node.DealMtx.RUnlock()

	miner.ReceivedChPrPairMtx.Lock()
	defer miner.ReceivedChPrPairMtx.Unlock()

	dealhashKeys, expiredDeal := miner.node.GetActiveAndExpiredDealsHash()
	if dealhashKeys == nil {
		return
	}
	// for index, dealhash := range dealhashKeys {
	// 	log.Info().Msgf("\t GenerateChallenges before deal: %v %v", index, hex.EncodeToString([]byte(dealhash)))
	// }

	sort.Strings(dealhashKeys)
	sort.Strings(expiredDeal)

	// for index, dealhash := range dealhashKeys {
	// 	log.Info().Msgf("\t GenerateChallenges sorted deal: %v %v", index, hex.EncodeToString([]byte(dealhash)))
	// }

	NumberOfDealToVerifyL := NumberOfDealToVerify
	if NumberOfDealToVerifyL > len(dealhashKeys) {
		NumberOfDealToVerifyL = len(dealhashKeys)
	}

	randNum := rand.New(rand.NewSource(seed))
	pbc.SetRandRandom(randNum)

	currentTime := time.Now().Add(miner.node.Dpos.TimeLeftToMine(time.Now().Unix()))
	log.Info().Msgf("Miner -- ISSUE CHAllenge active deals size: len %v, cap %v, NumberOfDealToVerifyL %v", len(dealhashKeys), cap(dealhashKeys), NumberOfDealToVerifyL)

	for j := 0; j < NumberOfDealToVerifyL; j++ {
		r := randNum.Intn(len(dealhashKeys))
		log.Info().Msgf("\t Seed %v  Random Number: %v", seed, r)
		dealhash := dealhashKeys[r]
		for {
			if _, ok := challengeIssuedDeal2[dealhash]; !ok {
				break
			}
			r = randNum.Intn(len(dealhashKeys))
			log.Info().Msgf("\t Seed %v  Random Number: %v", seed, r)
			dealhash = dealhashKeys[r]
		}

		challengeIssuedDeal2[dealhash] = true
		if dealhash != "" {
			log.Info().Msgf("Miner %v -- Issued Challenge DealHash %v", miner.node.Port, hex.EncodeToString([]byte(dealhash)))
			deal := miner.node.GetDealbyHash(dealhash)
			if deal != nil {
				// miner.generateChPrPair(randNum, dealhash, deal, currentTime, len(dealhashKeys))
				miner.generateChPrPair(randNum, dealhash, deal, currentTime)
			} else {
				log.Error().Msgf("\t Deal Not found for Hash %v for deals callenge %v ", hex.EncodeToString([]byte(dealhash)), dealhash)
			}
		} else {
			log.Error().Msgf("GenerateChallenges DealHash empty %v", dealhash)
		}
	}

	for _, dealhash := range expiredDeal {
		r := randNum.Intn(len(dealhashKeys))
		log.Info().Msgf("\t Seed %v  Random Number: %v", seed, r)
		if dealhash != "" {
			deal := miner.node.GetDealbyHash(dealhash)
			if deal != nil {
				if _, ok := challengeIssuedDeal2[dealhash]; !ok {
					log.Info().Msgf("miner %v -- Issued Challenge Expired DealHash %v ", miner.node.Port, hex.EncodeToString([]byte(dealhash)))
					// miner.generateChPrPair(randNum, dealhash, deal, currentTime, len(dealhashKeys))
					miner.generateChPrPair(randNum, dealhash, deal, currentTime)
				}
			} else {
				log.Error().Msgf("GetDealbyHash deal not found in generateHcallenges: %v %v", hex.EncodeToString([]byte(dealhash)), dealhash)
				// panic("GetDealbyHash deal not found in generateHcallenges")
			}
		} else {
			log.Error().Msgf("GenerateChallenges Expired DealHash empty %v", dealhash)
		}
	}
}

// func (miner *Miner) generateChPrPair(randNum *rand.Rand, dealhash string, deal *protobuf.Deal, currentTime time.Time, lendeal int) {
func (miner *Miner) generateChPrPair(randNum *rand.Rand, dealhash string, deal *protobuf.Deal, currentTime time.Time) {
	// log.Info().Msgf("\t Ajeeb Seed 1 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	contentSize := int(0)
	for _, infoPr := range deal.List {
		contentSize += int(infoPr.Info.Metadata.ContentSize)
	}

	// log.Info().Msgf("\t Ajeeb Seed 2 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	if contentSize < 150 {
		contentSize = 150
	}

	// pairing, _ := pbc.NewPairingFromString(deal.PosParam)
	blocksTag := pos.GetNumberofBlocksforTag(contentSize, miner.node.PosConfig)
	ch := pos.Challenge(randNum, blocksTag, 3, miner.node.PosPairing)
	// log.Info().Msgf("\t Ajeeb Seed 3 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	// log.Info().Msgf("%v for Deal %v", ch.String(), hex.EncodeToString([]byte(dealhash)))
	var chPrPair *protobuf.ChPrPair
	var ok bool
	if chPrPair, ok = miner.ReceivedChPrPair[dealhash]; !ok {
		log.Error().Msgf("Proof not found for for Deal %v", hex.EncodeToString([]byte(dealhash)))
		// panic("Proof not found for for Deal")
		chPrPair = &protobuf.ChPrPair{
			Challenge:      ch.ToProto(),
			Proof:          nil,
			Sig:            nil,
			Spid:           deal.Spid,
			Dealhash:       []byte(dealhash),
			DealExpiryTime: deal.ExpiryTime,
			Timestamp:      time.Now().UnixNano(),
			DealStartTime:  deal.Timestamp,
		}
	}

	ExpiryTime := time.Unix(0, int64(deal.ExpiryTime))
	if currentTime.After(ExpiryTime) {
		chPrPair.Timestamp = deal.ExpiryTime
	} else {
		chPrPair.Timestamp = time.Now().UnixNano()

	}

	miner.ChPrPair = append(miner.ChPrPair, chPrPair)
	delete(miner.ReceivedChPrPair, dealhash)
	// log.Info().Msgf("ReceivedChPrPair removed while generating Proof for Deal %v len %v", hex.EncodeToString([]byte(dealhash)), len(miner.ReceivedChPrPair))

	// log.Info().Msgf("\t Ajeeb Seed 4 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	// log.Info().Msgf("\t Ajeeb Seed 5 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	// log.Info().Msgf("\t Ajeeb Seed 6 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	// log.Info().Msgf("\t Ajeeb Seed 7 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	// log.Info().Msgf("\t Ajeeb Seed 8 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	// log.Info().Msgf("\t Ajeeb Seed 9 %v  Random Number: %v", 0, randNum.Intn(lendeal))
	// log.Info().Msgf("\t Ajeeb Seed 10 %v  Random Number: %v", 0, randNum.Intn(lendeal))

	pbKey := new(pos.PublicKey)
	pbKey.FromProto(deal.PublicKey, miner.node.PosPairing)
	chl := new(pos.Chall)
	chl.FromProto(chPrPair.Challenge, miner.node.PosPairing)

	valid := false
	valid2 := false
	if chPrPair.Proof != nil {
		prf := new(pos.Proof)
		prf.FromProto(chPrPair.Proof, miner.node.PosPairing)
		valid = pos.Verify([]byte(name), miner.node.PosPairing, pbKey, ch, prf)
		valid2 = pos.Verify([]byte(name), miner.node.PosPairing, pbKey, chl, prf)
		if !(valid2 && valid) {
			log.Error().Msgf("Miner  -- %v **Failed: %v** - Challenge: %v Proof %v \n for Deal %v", miner.node.Port, valid, ch, prf, hex.EncodeToString([]byte(dealhash)))
			log.Error().Msgf("Miner2 -- %v **Failed: %v** - Challenge: %v Proof %v \n for Deal %v", miner.node.Port, valid2, chl, prf, hex.EncodeToString([]byte(dealhash)))
			// panic("Miner: sp validation failed for deal")
		}
	}

	// if ch.Equals(chl) {
	// 	log.Info().Msgf("Miner -- Both Ch are equal")
	// } else {
	// 	log.Info().Msgf("Miner -- Both Ch are Not equal")
	// }

}

func (miner *Miner) ResetCache() {
	// miner.prCache, _ = storage.NewLRU(prCacheSize)
	for _, key := range miner.stalePrCache.Keys() {
		val1, _ := miner.stalePrCache.Get(key)
		pr := val1.(*protobuf.PostRequest)
		miner.prCache.Add(key, pr)
		miner.stalePrCache.Remove(pr)
	}

	miner.localspReqListCache = make(map[string]map[string]*protobuf.ThreadPostRequestList)
	miner.msgMap = make(map[string][]byte)
	miner.ReceivedChPrPair = make(map[string]*protobuf.ChPrPair)
	miner.transactions.EmptyPool()
	miner.votes.EmptyPool()
}

func (miner *Miner) RemoveExpiredDealDataFromCahce(deal *protobuf.Deal) {

	threadheadPostHash := deal.List[0].Info.Metadata.ThreadRootPostHash
	if threadheadPostHash == nil {
		threadheadPostHashbytes, _ := proto.Marshal(deal.List[0].Info)
		threadheadPostHash = crypto.Sha256(threadheadPostHashbytes)
	}

	miner.postToPrimarySpCache.Remove(string(threadheadPostHash))
	miner.threadPostingTimeMtx.Lock()
	delete(miner.threadPostingTime, (string(threadheadPostHash)))
	miner.threadPostingTimeMtx.Unlock()

	val, exist := miner.staleSoCache.Get(string(deal.Spid))
	if exist {
		miner.soCache.Add(string(deal.Spid), val)
	}
}
