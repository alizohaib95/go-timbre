package roles

import (
	"encoding/hex"
	"fmt"

	"github.com/golang/protobuf/proto"
	"github.com/guyu96/go-timbre/crypto"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	"github.com/guyu96/go-timbre/protobuf"
	"github.com/guyu96/go-timbre/storage"
)

const (
	bpChanSize    = 32  // default BandwidthProvider channel buffer size
	postCacheSize = 128 // default post cache size
)

// All post retrievals go here
// BandwidthProvider
type BandwidthProvider struct {
	node     *net.Node
	incoming chan net.Message

	postCache  *storage.LRU      // Post Cache
	postToSp   map[string]string // posthash -> sp_kad_id
	threadBase map[string]([]string)
}

// NewBandwidthProvider creates a new BandwidthProvider.
func NewBandwidthProvider(node *net.Node) *BandwidthProvider {
	bp := &BandwidthProvider{
		node:       node,
		incoming:   make(chan net.Message, bpChanSize),
		postToSp:   make(map[string]string),
		threadBase: make(map[string]([]string)),
	}
	bp.postCache, _ = storage.NewLRU(prCacheSize)
	node.AddOutgoingChan(bp.incoming)
	return bp
}

func (bp *BandwidthProvider) Setup() {
	node := bp.node
	if node.Bc != nil {
		var allDeals []*protobuf.Deal
		var err error
		walk := node.Bc.GetMainTail()
		for {
			deals := walk.GetDeals()
			allDeals = append(allDeals, deals...)
			walk, err = node.Bc.GetBlockByHash(walk.ParentHash())
			if err != nil {
				break
			}
		}
		for _, deal := range allDeals {

			toRetrieveFrom := string(deal.Spid)
			//			toRetrieveFrom := string(deal.GetKadID())
			for _, post := range deal.List {

				rootHash := hex.EncodeToString(post.Info.GetMetadata().GetThreadRootPostHash())
				postInfobytes, err := proto.Marshal(post.Info)
				if err != nil {
					return
				}
				postMetaDataHash := crypto.Sha256(postInfobytes)
				postMetaDataHashStr := hex.EncodeToString(postMetaDataHash)
				bp.postToSp[postMetaDataHashStr] = toRetrieveFrom
				if rootHash == "" { // Root Post
					bp.threadBase[postMetaDataHashStr] = append(bp.threadBase[postMetaDataHashStr], postMetaDataHashStr)
				} else {
					bp.threadBase[rootHash] = append(bp.threadBase[rootHash], postMetaDataHashStr)
				}
			}
		}
	}
}

// Process starts processing incoming messages for BandwidthProvider.
func (bp *BandwidthProvider) Process() {
	for msg := range bp.incoming {
		switch msg.Code {
		case net.MsgCodeGetPost:
			log.Info().Msgf("Bp getting post")
			bp.sendPost(msg)
			// case net.MsgCodePostContent:
			// bp.onGetPostContent(msg)
		}
	}
}

func (bp *BandwidthProvider) GetThread(threadHeadHash string) {
	// Find in threadbase
	posts, ok := bp.threadBase[threadHeadHash]
	if !ok {
		log.Info().Msgf("Thread does not exist in local base")
		return
	}
	for _, post := range posts {
		// Find sp for each
		// storedWith := kad.FromString(bp.postToSp[post]) // This would be the same for reply posts but incase a thread is stored with 2 or more sp
		postHashInBytes, _ := hex.DecodeString(post)
		_ = postHashInBytes
		msg := net.NewMessage(net.MsgCodeGetPost, postHashInBytes)
		// bp.node.Relay(storedWith, msg)
		// _ = storedWith
		bp.node.RequestAndResponse(msg, bp.onGetPostContent, net.MsgCodePostContent)

	}
}

// GetThreads gets all threads
func (bp *BandwidthProvider) GetThreads() {
	for headHash := range bp.threadBase {
		bp.GetThread(headHash)
	}
}

func (bp *BandwidthProvider) onGetPostContent(msg net.Message) error {
	// Add to cache if not the primary storage provider
	post := &protobuf.StoredPost{}
	err := proto.Unmarshal(msg.Data, post)
	if err != nil {
		return err
	}
	//TODO:ChangeToPK->revisit if done correctly
	if bp.postToSp[hex.EncodeToString(post.Hash)] == bp.node.PublicKey().String() {
		return nil
	}
	bp.postCache.Add(hex.EncodeToString(post.Hash), msg.Data)
	return nil
}

// UpdataThreadBase update post map and store posts in cache
func (bp *BandwidthProvider) UpdateThreadBase() {
	node := bp.node
	if node.Bc != nil {

		tail := node.Bc.GetMainTail()
		deals := tail.GetDeals()

		for _, deal := range deals {
			toRetrieveFrom := string(deal.Spid)
			// toRetrieveFrom := string(deal.GetKadID())
			for _, post := range deal.List {

				rootHash := hex.EncodeToString(post.Info.GetMetadata().GetThreadRootPostHash())
				postInfobytes, err := proto.Marshal(post.Info)
				if err != nil {
					return
				}
				postMetaDataHash := crypto.Sha256(postInfobytes)
				postMetaDataHashStr := hex.EncodeToString(postMetaDataHash)
				bp.postToSp[postMetaDataHashStr] = toRetrieveFrom
				if rootHash == "" { // Root Post
					bp.threadBase[postMetaDataHashStr] = append(bp.threadBase[postMetaDataHashStr], postMetaDataHashStr)
				} else {
					bp.threadBase[rootHash] = append(bp.threadBase[rootHash], postMetaDataHashStr)
				}
				bp.GetThread(postMetaDataHashStr)
			}
		}
	}
}

// populateCache gets posts from storage providers to serve from cache
func (bp *BandwidthProvider) populateCache() {
	bp.GetThreads() // TODO: discuss should the bandwidth provider cache all posts?
}

// sendPost -> if primary storage provider -> retrieve from db
func (bp *BandwidthProvider) sendPost(msg net.Message) {

	contentHash := msg.Data
	content, ok := bp.postCache.Get(hex.EncodeToString(contentHash))

	// If independent bandwidth provider
	if ok {
		// log.Info().Msgf("BP sending Data %v for hash %v from Cache", hex.EncodeToString(content.([]byte)), hex.EncodeToString(contentHash))
		msgToSend := net.NewMessage(net.MsgCodePostContent, content.([]byte))
		bp.node.Relay(msg.From, msgToSend)
		return
	}

	// If storage provider
	pb, err := bp.node.Db.Get(PostBucket, contentHash)
	if err != nil {
		// log.Info().Msgf("Unable to retrieve post")
		return
	}

	post := new(protobuf.StoredPost)
	if err2 := proto.Unmarshal(pb, post); err2 != nil {
		log.Info().Msgf("postBytes unmarshal error")
		return
	}
	// log.Info().Msgf("BP sending Data %v for hash %v from DB", string(post.Content), hex.EncodeToString(contentHash))

	postBytes, err := proto.Marshal(post)
	if err != nil {
		panic(fmt.Sprintf("StorageProvider: Failed to marshal post in storePosttoDB: %s", err))
	}

	msgToSend := net.NewMessage(net.MsgCodePostContent, postBytes)
	bp.node.Relay(msg.From, msgToSend)
}
