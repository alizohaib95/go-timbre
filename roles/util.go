package roles

import (
	"github.com/guyu96/go-timbre/core"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	"github.com/guyu96/go-timbre/protobuf"
)

// GenerateTestBlock creats a test block at the given height
func (miner *Miner) GenerateTestBlock(parent *core.Block, transactions []*protobuf.Transaction, votes []*protobuf.Vote, deals []*protobuf.Deal, chPrPair []*protobuf.ChPrPair, podfs []*protobuf.PoDF, roundNum, bTs, height int64, broadcast bool) *core.Block {
	// statTime := time.Now()
	var block *core.Block
	var err error
	// if len(deals) == 0 {
	// 	fmt.Println("No Deals in the block!!!!")
	// }
	node := miner.node
	node.BcMtx.Lock()
	defer node.BcMtx.Unlock()

	if node.Bc == nil {
		log.Info().Msgf("Creating genesis block")
		if broadcast == false {
			block, err = core.NewGenesisBlockWithTs(miner.node.PublicKey(), miner.node.TimeOffset, transactions, bTs)
		} else {
			block, err = core.NewGenesisBlock(miner.node.PublicKey(), miner.node.TimeOffset, transactions)
		}

		// if block == nil {
		// 	fmt.Println("Signing nil block")
		// } else {
		// 	if block.PbBlock.GetHeader() == nil {
		// 		fmt.Println("Signing nil block header")
		// 	}
		// }
		block.Sign(node.PrivateKey())
		// fmt.Println("Miner G key!!!: ", block.MinerPublicKey())

		if err != nil {
			log.Error().Err(err)
			return nil
		}

		isValid := block.IsValid()
		if isValid != true {
			log.Info().Msgf("Block is not valid!")
			return nil
			// return errors.New("Block is not valid")
		}

		miner.node.Bc, err = core.InitBlockchain(node.Db, block)
		if err != nil {
			log.Error().Err(err)
			return nil
		}
		// err = miner.node.Wm.GenesisDelegateReg(block)
		// if err != nil {
		// 	log.Info().Err(err)
		// }
		// miner.node.Wm.Bc = miner.node.Bc
	} else {
		// parent := node.Bc.GetMainTail()

		// log.Info().Msgf("GOT PARENT FOR NEW BLOCK %s", parent)

		block = core.CreateNewBlockAtHeight(parent, node.PublicKey(), node.PrivateKey(), transactions, votes, deals, chPrPair, podfs, miner.node.TimeOffset, roundNum, height) //Extra transaction variable as comp to MakeTestBlock
		// node.Bc.Append([]*core.Block{block})                                                                                                          //If you comment the below states(vote,transaction and wallet) out it won't update
		// fmt.Println("Miner key!!!: ", block.MinerPublicKey())
		isValid := block.IsValid()
		if isValid != true {
			log.Info().Msgf("Block is not valid. Returning")
			return nil
			// return errors.New("Block is not valid")
		}

		minerPk := block.MinerPublicKey()
		// minerAdd := miner.node.GetAddressUsingPublicKey(minerPk)
		minerAdd := minerPk.String()

		// wst := time.Now()
		err = node.Wm.CheckAndUpdateWalletStates(block, minerAdd) //Skip/remove transactions from block which are invalid
		if err != nil {
			log.Error().Msgf("Miner -- Wallet state could not be updated")
			log.Error().Msgf(err.Error())
		}
		// fmt.Printf("\tWallet state update Time: %v\n", time.Now().Sub(wst))

		// node.Wm.ReverseBlockUpdate(block, minerAdd)
		// if err != nil {
		// 	panic(err)
		// }
		// node.Dpos.ApplyVotes(block) //For reflecting changes in the current node
		// wst = time.Now()
		// err = miner.node.Wm.CheckAndApplyVotes(block, minerAdd)
		// if err != nil {
		// 	log.Error().Msgf(err.Error())
		// 	// return err
		// }

		miner.node.AddDeals(deals)
		// fmt.Printf("\tApply Vote Time: %v\n", time.Now().Sub(wst))
	}
	if err != nil {
		log.Error().Err(err)
		log.Error().Msgf("Miner -- Block Mined but Not added")
		return nil
	}

	// endTime := time.Now()
	// fmt.Printf("\tBlock createing Time: %v\n", endTime.Sub(statTime))

	blockBytes, err := block.ToBytes()
	// fmt.Println("Block bytes:-", len(blockBytes))
	if err != nil {
		log.Error().Err(err)
		return nil
	}
	if broadcast == false {
		log.Info().Msgf("Prepared %s", block)
		// node.Bc.Append([]*core.Block{block})
		// log.Info().Msgf("New Blockchain: %s", node.Bc)

	} else {
		log.Info().Msgf("CREATED %s", block)
		// node.Bc.Append([]*core.Block{block})
		log.Info().Msgf("New Blockchain: %s", node.Bc)

	}
	node.Bc.Append(block)
	node.Bc.UpdateMainTailIndex()

	miner.deals = nil
	miner.ChPrPair = nil
	miner.ReceivedChPrPairMtx.Lock()
	miner.ReceivedChPrPair = make(map[string]*protobuf.ChPrPair)
	miner.ReceivedChPrPairMtx.Unlock()

	if broadcast == true {
		msg := net.NewMessage(net.MsgCodeBlockNew, blockBytes)
		node.Broadcast(msg)
	}

	return block
}
