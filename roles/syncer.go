package roles

import (
	"bytes"
	"context"
	"encoding/hex"
	"errors"
	"fmt"
	"math"
	"math/rand"
	"strconv"
	"sync"
	"time"

	"github.com/guyu96/go-timbre/core"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	"github.com/mbilal92/noise"
)

const (
	syncerChanSize                    = 64 // default syncer channel buffer size
	noOfNodesToQuery                  = 3  // Number of nodes to ask for the blocks
	maxResponseTime                   = 5  // Time limit for a response in seconds
	RequestCurrentTailMaxResponseTime = 5  // Time limit for a response in seconds

)

var (
	testNo       = 10
	errDposIsOff = errors.New("Cannot validate blocks without running DPos. RUN DPOS plz")
	// IrreversibleBlock     = int64(0)
	// MinedCommitteemembers = list.New() //[]string{}
	// MinedCommitteemembersMap = make(map[string]struct{})
	GenesisBlocktoSetWhileSync *core.Block
	ChangeGenesisWhileSyncing  bool
	errFailedValidation        = errors.New("Failed block validation")
)

// Syncer is responsible for synchronizing the blockchain.
type Syncer struct {
	node             *net.Node
	incoming         chan net.Message
	download         chan net.Message
	targetBlock      *core.Block
	curBlock         *core.Block //[]byte
	mu               sync.RWMutex
	isSyncing        bool
	chainSwitch      bool
	Running          bool
	InvalidBlockpool *core.Blockpool
	reSync           bool
}

// NewSyncer creates a new syncer.
func NewSyncer(node *net.Node) *Syncer {
	bp, _ := core.NewBlockpool(core.DefaultBlockpoolCapacity)
	syncer := &Syncer{
		node:             node,
		incoming:         make(chan net.Message, syncerChanSize),
		mu:               sync.RWMutex{},
		isSyncing:        false,
		chainSwitch:      false,
		Running:          false,
		InvalidBlockpool: bp,
		reSync:           false,
	}
	node.AddOutgoingChan(syncer.incoming)
	return syncer
}

//GetIsSyncing returns the syncing bool state
func (syncer *Syncer) GetIsSyncing() bool {
	return syncer.isSyncing
}

//SetIsSyncing sets the isSyncing bool
func (syncer *Syncer) SetIsSyncing(flag bool) {
	syncer.isSyncing = flag
}

// Process starts processing incoming messages for syncer.
func (syncer *Syncer) Process() {
	syncer.Running = true
	for msg := range syncer.incoming {
		switch msg.Code {
		case net.MsgCodeBlockNew:
			block, err := getBlockFromMsg(msg)
			if err == nil {
				go syncer.handleBlockNew(block)
			} else {
				log.Error().Msgf("Could not read block from message")
			}
		case net.TestMsg:
			log.Info().Msgf("resceived in the testmsg in syncer %v", msg.Data) //
		case net.MsgCodeBlockAsk:
			go syncer.handleBlockAsk(msg)
		case net.MsgCodeBlockAskByHeight:
			go syncer.handleBlockByAskByHeight(msg)
		case net.MsgCodeBlockSyncByHeight:
			go syncer.handleBlockSyncByHeight(msg)
		case net.MsgCodeGetCurrentTail:
			go syncer.handleCurrentTailReq(msg)
		case net.MsgCodeCurrentTail:
			go syncer.handleWriteToDownload(msg)
		case net.MsgCodeBlockChainLongSyncResquest:
			go syncer.handlBlockChainLongSyncRequest(msg)
		case net.MsgCodeBlockChainLongSyncResponse:
			syncer.handlBlockChainLongSyncResponse(msg)
		}
	}
	syncer.Running = false
}

func getBlockFromMsg(msg net.Message) (*core.Block, error) {
	return core.NewBlockFromBytes(msg.Data)
}

//handleBlockNew handles the newly broadcasted vote
func (syncer *Syncer) handleBlockNew(block *core.Block) {

	syncer.mu.Lock()
	defer syncer.mu.Unlock()

	isValid := block.IsValid()
	if isValid != true {
		fmt.Println("Got invalid block in syncerHandle block new")
		log.Error().Msgf("Block is invalid")
		return
	}

	log.Info().Msgf("Received %s ", block)

	var err error
	//TODO: exclude DPOS has started bool while isSyncing is perfectly placed. Currently it is called multiple time and if a block is received
	// meanwhile it gets triggered. No new blocks should be recieved while isSyncing is true

	minerPk := block.MinerPublicKey()
	minerAdd := minerPk.String()

	//handling the bug in which block is added to the chain twice
	if minerAdd == syncer.node.Dpos.GetCurrentMinerAdd() && minerAdd == syncer.node.PublicKey().String() { //Current miner shouldn't receive its own block in syncer
		log.Info().Msgf("Received my own block! Not adding")
		return
	}

	node := syncer.node

	//checking if it is the digest block
	// if block.IsDigestBlock() == true { //If state is appended then it is the digest block
	// 	fmt.Println("Got the digest block!!!")
	// 	err = node.ApplyDigestBlock(block)
	// 	if err != nil {
	// 		log.Info().Err(err)
	// 	}
	// 	return
	// }

	if node.Bc == nil {
		if block.IsGenesis() {

			log.Info().Msgf("Received Genesis Block %s", block)
			if syncer.isSyncing {
				if !syncer.curBlock.IsSameAs(block) {
					// if !bytes.Equal(syncer.curBlock, block.Hash()) {
					return
				}
			}

			node.Bc, err = core.InitBlockchain(node.Db, block)
			if err != nil {
				log.Error().Err(err)
				return
			}
			log.Info().Msgf("Blockchain got initiated", node.Bc.Genesis())
			if syncer.isSyncing && syncer.node.Dpos.HasStarted == false { //Meaning the node is downloading old blocks already produced
				syncer.node.Services.UpdateDpos(block)
				// syncer.node.Wm.GenesisDelegateReg(block)  //TODO:-Reintroduce it later
			}

			_, _, _, err = syncer.node.Bc.UpdateMainTailIndex() //TODO:- Ali confirm
			if err != nil {
				log.Info().Err(err)
			}

			// syncer.node.Bc.UpdateIrreversibleBlockNumber(minerAdd, syncer.node.Dpos.GetComitteeSize(), syncer.node.Dpos.NumberOfToMineBlockPerSlot)
			err := syncer.node.Wm.ApplyGenesis(block)
			if err != nil {
				log.Info().Err(err)
			}

			node.Dpos.SetBlockchain(node.Bc)

		} else {
			log.Info().Msgf("Not Genesis block adding to BP")
			node.Bp.Add(block) // This will at after the checkBPforPending function runs
		}

		// syncer.node.Wm.Bc = syncer.node.Bc

	} else {

		if !node.Bc.Contains(block) && !node.Bp.Contains(block) {

			node.Bp.Add(block) // Add to blockpool and then check blockpool for tails that can be added

			tails := syncer.node.Bp.GetTails()
			// TODO: Optimize blockpool so that 1) it dynamically maintains a tail slice  and 2) When a new block is added, mark its chain as modified to avoid going through every possible chain.
			for _, tail := range tails {
				chain := syncer.node.Bp.GetChainByTail(tail)
				fmt.Println("Chain to add %v for tail %v", chain, tail)
				if chain[0].Height() < syncer.node.Bc.IrreversibleBlock { // remove chain
					fmt.Println("removing Chain...", syncer.node.Bc.IrreversibleBlock)
					for _, b := range chain {
						syncer.node.Bp.Remove(b.Hash())
					}
				} else {
					fmt.Println("Applying iterate check and add chain...")
					syncer.IterateCheckAndAddChain(chain)
				}
			}
			InvalidTails := syncer.InvalidBlockpool.GetTails()
			for _, tail := range InvalidTails {
				chain := syncer.InvalidBlockpool.GetChainByTail(tail)
				fmt.Println("Invalid Chain to Remove %v for tail %v", chain, tail)
				if chain[0].Height() < syncer.node.Bc.IrreversibleBlock { // remove chain
					fmt.Println("removing Chain...", syncer.node.Bc.IrreversibleBlock)
					for _, b := range chain {
						syncer.InvalidBlockpool.Remove(b.Hash())
					}
				}
			}

			log.Error().Msgf("Bp Size %d", syncer.node.Bp.Size())

			//Conditioning is not right. Flags are not set. And syning cannot be started without shutting off the DPOS.
			size2by3 := int((2 * syncer.node.Dpos.GetComitteeSize()) / 3)
			if syncer.node.Bp.Size() > size2by3 && !syncer.isSyncing {
				// doneSync := make(chan struct{}) // temp
				log.Error().Msgf("Asking for tail+1 block because Bp has more than 5 blocks. Tail: %v", syncer.node.Bc.GetMainTail().String())

				go syncer.SyncBlockchain()
				// // 	syncer.SyncBlockchain(10, doneSync)
				// syncer.Ask(syncer.node.Bc.GetMainTail().Hash(), 1)
				// syncer.node.Dpos.Stop()
				// syncer.node.Services.RevertDposTill(syncer.node.Bc.GetMainTail())

				// syncer.isSyncing = true
				// fromBlockHeight := int(syncer.node.Bc.GetMainTail().Height() + 1)
				// msg := net.NewMessage(net.MsgCodeBlockAskByHeight, []byte(strconv.Itoa(fromBlockHeight)))
				// node.RequestAndResponse(msg, syncer.handleBlockSyncByHeight, net.MsgCodeBlockSyncByHeight)
				// syncer.node.Services.StartDpos(int(syncer.node.Dpos.GetCommitteeSize()), false)
				// syncer.isSyncing = false
			} else {
				if syncer.isSyncing == false {
					syncer.node.Bp.PrintBlockPool()
				}
			}

		} else {
			if syncer.isSyncing && syncer.curBlock.Height() < block.Height() {
				syncer.curBlock = block //.Hash()
			}
			log.Info().Msgf("Block Already exists in chain. Block:%v", block)
		}
	}
}

// //ProcessVotes processes the vote
// func (syncer *Syncer) ProcessVotes(block *core.Block, minerAdd string) error {
// 	err := syncer.node.Wm.CheckAndApplyVotes(block, minerAdd)
// 	if err != nil {
// 		return err
// 	}
// 	// syncer.node.Dpos.ApplyVotes(block)
// 	return nil
// }

//SendTestMsg sends the test transaction
func (syncer *Syncer) SendTestMsg() {

	bs := []byte(strconv.Itoa(testNo))
	testNo++
	log.Info().Msgf("Came in testMsg")
	msg := net.NewMessage(net.TestMsg, bs)
	syncer.node.Broadcast(msg)
	if testNo == 15 {
		testNo = 10
	}
	log.Info().Msgf("Test msg is sent")
}

func (syncer *Syncer) clearDownloadVars() {
	syncer.targetBlock = nil
	syncer.curBlock = nil
}

func (syncer *Syncer) handleWriteToDownload(msg net.Message) {
	syncer.download <- msg
}

func (syncer *Syncer) Setup() {
	if syncer.node.Bc != nil {
		log.Info().Msgf("Applying local blocks already in the chain")

		// syncer.PrintBlockChain()
		// mainTail, err := syncer.node.Bc.GetChainByTail(syncer.node.Bc.GetMainTail())
		// if err != nil {
		// log.Error().Err(err).Msgf("Error while applying blocks from local chain")
		// } else {
		length := int64(syncer.node.Bc.Length())
		fmt.Println("bc length: ", length, " ", syncer.node.Bc.GetMainTail().Height())

		for i := int64(0); i <= length; i++ {
			block, _ := syncer.node.Bc.GetBlockByHeight(i)
			fmt.Println("bround: bHeight: ", block.GetRoundNum(), block.Height())
			syncer.node.Services.UpdateDpos(block)
			if block.IsGenesis() {
				syncer.node.Wm.ApplyGenesis(block)
			} else {
				// syncer.UpdateStates(block) //Todo review this line.
				syncer.node.Wm.CheckAndUpdateWalletStates(block, block.MinerPublicKey().String())
				// syncer.node.UpdateThreadBase(block)
			}
			// syncer.handleBlockNew(block) //Simply using handleBlockNew doesn't work becase blocks are already loaded. HanldeBlockNew doesn't reapply already included blocks
		}
		// for _, block := range mainTail {
		// if syncer.node.Dpos.HasStarted == false { //while node is downloading old blocks already produced
		// syncer.node.Services.UpdateDpos(block)
		// syncer.UpdateStates(block)
		// }
		// }
		// }

	} else {
		log.Info().Msgf("Blockchain has not been initiated yet.")
	}
}

// SyncBlockchain gets blockchain for a new node and updates for any previously added node
// func (syncer *Syncer) SyncBlockchainAtStart(z int, done chan struct{}) {

// 	if syncer.isSyncing == true {
// 		log.Info().Msgf("Sync is already running..")
// 		return
// 	}
// 	syncer.SyncBlockchain()
// 	// log.Info().Msgf("Sync Started with value of Z %v", z)

// 	// syncer.isSyncing = true

// 	// defer syncer.clearDownloadVars()
// 	// err := syncer.RequestCurrentTail()

// 	// for i := 1; i <= 2; i++ {
// 	// 	if err != nil {
// 	// 		log.Info().Msgf("Could not find current blockchain tail, %v", err)
// 	// 		err = syncer.RequestCurrentTail()
// 	// 	} else {
// 	// 		break
// 	// 	}
// 	// }
// 	// if err != nil {
// 	// 	syncer.isSyncing = false
// 	// 	done <- struct{}{}
// 	// 	return
// 	// }
// 	// // If local blockchain is already up-to-date return
// 	// if syncer.node.Bc != nil && syncer.node.Bc.GetMainTail().Height() >= syncer.targetBlock.Height() {
// 	// 	log.Info().Msgf("Blockchain already up-to-date")
// 	// 	done <- struct{}{}
// 	// 	syncer.isSyncing = false
// 	// 	return
// 	// }

// 	// if syncer.node.Bc != nil {
// 	// 	// Assuming six blocks before would be fixed
// 	// 	syncer.curBlock, err = syncer.node.Bc.GetBlockByHeight(syncer.node.Bc.GetMainTail().Height() - 6)
// 	// 	if err != nil {
// 	// 		log.Error().Msgf("Could not get block via height")
// 	// 		return
// 	// 	}
// 	// }

// 	// syncer.SyncBlockchainCore(z)

// 	log.Info().Msgf("Sync Finished")
// 	done <- struct{}{}
// }

// RequestCurrentTail locally broadcasts and waits for response to update targetBlock
// Update: Request Current Tail would send the node's current tail and get back the commonParent as well
func (syncer *Syncer) RequestCurrentTail() error {

	var blockBytes []byte
	var err error
	if syncer.node.Bc != nil {
		blockBytes, err = syncer.node.Bc.GetMainTail().ToBytes()
		if err != nil {
			log.Error().Msgf("Could not read main tail")
			return errors.New("Cant convert block to bytes")
		}
	}

	msg := net.NewMessage(net.MsgCodeGetCurrentTail, blockBytes)
	syncer.download = make(chan net.Message, 32)

	for _, item := range syncer.chooseNPeers(noOfNodesToQuery) {
		syncer.node.Relay(item, msg)
	}

	shouldRespondIn := RequestCurrentTailMaxResponseTime * time.Second // Maximum time limit to respond
	ctx, cancel := context.WithTimeout(context.Background(), shouldRespondIn)
	defer cancel()

	func() {
		for i := 0; i < noOfNodesToQuery; i++ {
			select {
			case <-ctx.Done():
				return
			case msg := <-syncer.download:
				err := syncer.handleCurrentTailRes(msg)
				if err != nil {
					log.Error().Err(err).Msgf("Error finding the tail")
				}
			}
		}
	}()
	if syncer.targetBlock == nil {
		return errors.New("Could not find tail block")
	}
	return nil
}

func (syncer *Syncer) handleCurrentTailRes(msg net.Message) error {

	data := bytes.Split(msg.Data, []byte(" , "))
	blockBytes := data[0]
	// commonParentBytes := data[1]

	block, err := core.NewBlockFromBytes(blockBytes)
	if err != nil || len(msg.Data) == 0 {
		return errors.New(fmt.Sprintf("Could not read sent tail Errors %v || msg.Data length %v", err, len(msg.Data)))
	}

	// commonParent, err := core.NewBlockFromBytes(commonParentBytes)

	log.Info().Msgf("handleCurrentTailRes request from %v, block %v", msg.From.String(), block)

	if syncer.targetBlock == nil {
		log.Info().Msgf("Target block set to %s", block.String())
		syncer.targetBlock = block
		// syncer.commonParent = commonParent
	} else {
		if block.Height() > syncer.targetBlock.Height() {
			log.Info().Msgf("Target block set to %s", block.String())
			syncer.targetBlock = block
			// syncer.commonParent = commonParent
		} else {
			log.Info().Msgf("Target block is already set.")
		}
	}
	return nil
}

// handleCurrentTailReq sends current tail block if exists otherwise nil along with the common parent of the asking node's tail
func (syncer *Syncer) handleCurrentTailReq(msg net.Message) {

	otherBlock, err := core.NewBlockFromBytes(msg.Data)
	if err != nil || len(msg.Data) == 0 {
		log.Error().Msgf("Could not read sent tail Error %v || msg.Data length %v", err, len(msg.Data))
	}

	var msgToSend []byte
	var blockBytes []byte
	var commonParentBytes []byte
	var commonParent *core.Block
	if syncer.node.Bc != nil {
		blockBytes, err = syncer.node.Bc.GetMainTail().ToBytes()
		if err != nil {
			log.Error().Msgf("Could not read main tail")
			return
		}
		if otherBlock != nil {
			commonParent, _ = syncer.node.Bc.FindCommonAncestor(otherBlock)
		}

		if commonParent != nil {
			commonParentBytes, err = commonParent.ToBytes()
			log.Error().Msgf("Could not convert common parent to bytes")
		}

	}
	msgToSend = append(append(blockBytes, []byte(" , ")...), commonParentBytes...)
	res := net.NewMessage(net.MsgCodeCurrentTail, msgToSend)
	syncer.node.Relay(msg.From, res)
}

func (syncer *Syncer) AskByHeight(fromBlockHeight int) {

	msg := net.NewMessage(net.MsgCodeBlockAskByHeight, []byte(strconv.Itoa(fromBlockHeight)))
	for _, item := range syncer.chooseNPeers(noOfNodesToQuery) {
		syncer.node.Relay(item, msg)
	}
}

// Ask sends message to peers requesting z number of nodes
func (syncer *Syncer) Ask(blockHash []byte, n int) {
	var hashAndZ []byte
	hashAndZ = append(append(append(hashAndZ, blockHash...), []byte(" , ")...), []byte(strconv.Itoa(n))...)
	msg := net.NewMessage(net.MsgCodeBlockAsk, hashAndZ)
	for _, item := range syncer.chooseNPeers(noOfNodesToQuery) {
		syncer.node.Relay(item, msg)
	}
}

func (syncer *Syncer) createAskMsg(blockHash []byte, n int) net.Message {
	var hashAndZ []byte
	hashAndZ = append(append(append(hashAndZ, blockHash...), []byte(" , ")...), []byte(strconv.Itoa(n))...)
	msg := net.NewMessage(net.MsgCodeBlockAsk, hashAndZ)
	return msg
}

// ChooseNPeers returns n randomly selected peers from the peers list
func (syncer *Syncer) chooseNPeers(n int) []noise.ID {
	rand.Seed(time.Now().UnixNano())
	rl := syncer.node.GetPeerAddress() //TODO:ChangeToPK
	rand.Shuffle(len(rl), func(i, j int) { rl[i], rl[j] = rl[j], rl[i] })

	if n > syncer.node.NumPeers() {
		return rl
	}
	return rl[:n]
}

//ValidateForMainTail validates the main-tail block
func (syncer *Syncer) ValidateForMainTail(b *core.Block) error {

	if syncer.isSyncing == true && syncer.node.Dpos.HasStarted == false { //while node is downloading old blocks already produced
		fmt.Println("Updating Dpos call from syncer...")
		syncer.node.Services.UpdateDpos(b)
	} else if syncer.isSyncing == false && syncer.node.Dpos.HasStarted == false {
		log.Warn().Msgf("Cannot validate blocks without running DPos. RUN DPOS plz")
		return errDposIsOff
	}

	//Validating for the digest block
	bcHeight := int64(syncer.node.Bc.GetMainForkHeight())
	if bcHeight%core.TestEpoch.Duration == 0 { //If state is appended in the block then it is the digest block

		curEpochNum := float64(bcHeight) / float64(core.TestEpoch.Duration)
		// fmt.Println("cur e[och: ", curEpochNum, syncer.node.GetCurEpoch())

		if b.IsDigestBlock() == true {
			validWallets := syncer.node.Wm.ValidateDigestBlock(b)
			// validMiners := syncer.node.Dpos.ValidateDigestMiners(b.PbBlock.GetState().GetMiners())
			if validWallets == false {
				return errors.New("Invalid wallets in digest block")
			}
		} else {
			return errors.New("Was expecting Digest block")
		}

		if int32(curEpochNum) != b.GetEpochNum() {
			return errors.New("Epoch numbs in the block doesn't match")
		}
		// syncer.node.SetCurEpoch(syncer.node.GetCurEpoch() + 1)
	}

	// fmt.Println("VALIDATING minor slot")
	errT := syncer.ValidateMinerSlot(b)
	if errT != nil {
		log.Info().Err(errT) //TODO :- Return false in case of error
		fmt.Println(errT.Error())
		return errT // THIS SHOULD CHANGE
	}

	err2 := syncer.node.Wm.CheckAndUpdateWalletStates(b, b.MinerPublicKey().String())
	if err2 != nil {
		fmt.Println("Error updating the states: ", err2.Error())
		log.Info().Err(err2)
		return err2
	}
	// validTx, err2 := syncer.node.Wm.ValidateBlockTx(b)
	// if err2 != nil || validTx == false {
	// 	fmt.Println("valid1: ", validTx, err2.Error())
	// 	log.Error().Msgf("Block Not Added in BC (IterateCheckAndAddChain) %s", b)
	// 	log.Info().Err(err2)
	// 	return err2
	// }

	return nil
}

//IterateCheckAndAddChain ...
func (syncer *Syncer) IterateCheckAndAddChain(chain []*core.Block) bool {
	syncer.node.BcMtx.Lock()
	defer syncer.node.BcMtx.Unlock()
	for _, b := range chain {
		log.Info().Msgf("IterateCheckAndAddChain processing block %v", b)

		// err := syncer.node.Bc.Validate([]*core.Block{b})
		err := syncer.node.Bc.Validate(b)
		if err != nil {
			log.Error().Msgf("Block Not Added in BC, Validation Failed (IterateCheckAndAddChain) %s %v", b, err)
			// syncer.node.Bp.Add(b) //Add to the blockchain if parent is not in the chain // All bp additions in handle block new
			if syncer.InvalidBlockpool.ContainsBlockByHash(b.ParentHash()) {
				fmt.Println("Invalid block Added to InvalidBlockpool ", hex.EncodeToString(b.Hash()))
				syncer.InvalidBlockpool.Add(b)
				syncer.node.Bp.Remove(b.Hash())
			}
			return false
		}

		// if syncer.chainSwitch == true { //If chainSwitch is in process then even block adding on the maintail should not be applied
		// 	fmt.Println("chain switch in process. Adding to bp")
		// 	syncer.node.Bp.Add(b)
		// 	// err = syncer.node.Bc.Append([]*core.Block{b})
		// 	// if err != nil {
		// 	// 	log.Info().Err(err)
		// 	// }
		// 	// continue
		// 	return false
		// }

		inMainTail := bytes.Equal(syncer.node.Bc.GetMainTail().Hash(), b.ParentHash())
		if inMainTail {
			err := syncer.ValidateForMainTail(b)
			if err != nil {

				if syncer.isSyncing == true && syncer.node.Dpos.HasStarted == false { //while node is downloading old blocks already produced
					syncer.node.Services.RevertDposTill(syncer.node.Bc.GetMainTail())
				}

				if err == errDposIsOff { //Don't remove block from pool in that case
					return false
				} else if err.Error() == "Block is from future" {
					//Do something
					return false
				}
				//remove block from blockpool
				fmt.Println("Err is: ", err.Error())
				fmt.Println("Invalid block removing from bp")
				syncer.InvalidBlockpool.Add(b)
				syncer.node.Bp.Remove(b.Hash())
				return false
			}
		}

		// err = syncer.node.Bc.Append([]*core.Block{b})
		err = syncer.node.Bc.Append(b)
		if err != nil { //Append unsuccessful
			if syncer.isSyncing == true && syncer.node.Dpos.HasStarted == false { //while node is downloading old blocks already produced
				syncer.node.Services.RevertDposTill(syncer.node.Bc.GetMainTail())
			}
			fmt.Println("Invalid block parent hahs ", hex.EncodeToString(b.ParentHash()))
			// TODO : rething about htis for now A hack
			if syncer.InvalidBlockpool.ContainsBlockByHash(b.ParentHash()) {
				fmt.Println("Invalid block Added to InvalidBlockpool ", hex.EncodeToString(b.Hash()))
				syncer.InvalidBlockpool.Add(b)
				syncer.node.Bp.Remove(b.Hash())
			}
			log.Info().Err(err)
		} else { // Append successful
			blocksToRevert, blocksToProcess, _, err := syncer.node.Bc.UpdateMainTailIndex()

			if err != nil {
				log.Error().Err(err)
			}

			if inMainTail {
				if len(blocksToProcess) == 0 { //If blocks is from past apply this and restart Dpos to compute committee accordingly
					blocksToRevert, blocksToProcess = syncer.CheckDposRounds(b)
				}
			}

			if len(blocksToProcess) > 0 { // Implying there is a fork
				if len(blocksToRevert) > 0 && len(blocksToProcess) > 1 {
					log.Info().Msgf("A fork occured. Rollback/Process blocks here !!!")
				}

				err := syncer.SwitchFork(blocksToRevert, blocksToProcess)
				if err != nil && err == errFailedValidation {
					//Initiating the resync protocol

					//TODO:- Update maintail index. Coz its way ahead

					// go syncer.ResyncChain() //recheck this
					return false
				}
				// log.Info().Msgf("Stopping the DPOS... %v", syncer.node.GetNodeID().Address)
				// syncer.node.Dpos.Stop()
				// syncer.chainSwitch = true
				// syncer.isSyncing = true

				// // //TODO :- stop dpos. Revert.
				// // _, _ = blocksToRevert, blocksToProcess
				// for _, revertBlock := range blocksToRevert {
				// 	syncer.RevertState(revertBlock)
				// }
				// last := len(blocksToProcess) - 1
				// fmt.Println("Applying blocks")
				// for i := range blocksToProcess { // To run in ascending order
				// 	if !syncer.ValidateForMainTail(blocksToProcess[last-i]) {
				// 		return false
				// 	}
				// 	syncer.UpdateStates(blocksToProcess[last-i])
				// 	syncer.node.Bp.Remove(b.Hash())
				// }
				// log.Info().Msgf("Blocks reverted and applied")
				// log.Info().Msgf("Starting DPOS again")
				// log.Info().Msgf("Please do miner registration yourself self")
				// fmt.Println("Syncer running", syncer.Running)
				// syncer.node.Services.StartDpos2(int(syncer.node.Dpos.GetCommitteeSize()), false)
				// syncer.chainSwitch = false
				// syncer.isSyncing = false
				//TODO:- Test to see if this works
			} else { // Same main fork extended or added to a smaller fork
				if syncer.node.Bc.IsInMainFork(b) {
					syncer.UpdateStates(b)
					if syncer.isSyncing && syncer.curBlock.Height() < b.Height() {
						syncer.curBlock = b //.Hash()
					}
					log.Info().Msgf("Block Added in BC (IterateCheckAndAddChain) %s", b)
				} else {
					log.Info().Msgf("Block Added in BC But not in Main Tail (IterateCheckAndAddChain) %s", b)
					//Check if the miner has double forged
					// fmt.Println("tails: ", syncer.node.Bc.NumForks())
					//Getting the maintail block at the height of the current block
					go func() {
						syncer.CheckAndReportMisbehaviour(b)
					}()

				}
				log.Info().Msgf("New Blockchain: %s", syncer.node.Bc.String())
			}

			syncer.node.Bp.Remove(b.Hash())
		}
		// syncer.PrintBlockChain()
	}

	return true
}

//CheckAndReportMisbehaviour checks if miner has misbehaved and create automatic podf
func (syncer *Syncer) CheckAndReportMisbehaviour(b *core.Block) {

	mainForkBlock, _ := syncer.node.Bc.GetBlockByHeight(b.Height())
	fmt.Println("mainFOrk block: ", mainForkBlock, " this: ", b)
	if mainForkBlock != nil { //Same height block
		if bytes.Equal(mainForkBlock.PublickeyBytes(), b.PublickeyBytes()) { //Same public key
			// if mainForkBlock.GetRoundNum() == b.GetRoundNum() { //Same round
			//Creating a podf transaction
			fmt.Println("Found Double forged block. Reporting...")
			ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
			defer cancel()
			syncer.node.Services.CreatePodf(ctx, mainForkBlock.GetHeader(), b.GetHeader())
			// }
		}

	}
}

//CheckDposRounds validates if block rounds are the same as the DPos
func (syncer *Syncer) CheckDposRounds(b *core.Block) ([]*core.Block, []*core.Block) {
	//This block is child of the main tail which is in another round

	if b.GetRoundNum() != syncer.node.Dpos.GetRoundNum() {

		fmt.Println("B round: ", b.GetRoundNum(), " DPos round: ", syncer.node.Dpos.GetRoundNum())
		// tail := syncer.node.Bc.GetMainTail()
		if syncer.node.Dpos.GetRoundNum() > b.GetRoundNum() {

			if syncer.node.Dpos.GetRoundNum() == b.GetRoundNum()+1 {
				log.Warn().Msgf("Got block from last round!!!!. Will simply add now. Recomputing committee from last round")
				syncer.node.Dpos.RecomputeLastRoundCommittee() //will recompute the committee
				return []*core.Block{}, []*core.Block{}
			} else {
				panic("Block is older than last round. Check this")
			}
			//Check if any block has been produced in this round
			// if tail.GetRoundNum() == b.GetRoundNum() {
			// 	//Restarting Dpos with updated state to this current block
			// 	return []*core.Block{}, []*core.Block{b}
			// } else {
			// 	log.Warn().Msgf("Should have rejected this block %v", b)
			// }
			// return []*core.Block{}, []*core.Block{b}
			//Check if main tail round is the same
		} else { //This condition will never hit since error will already be thrown in previous checks in miner validation
			if syncer.node.Dpos.GetRoundNum() == 0 {
				panic("Restart plz. I started in different slot then my fellows :)")
			}
			// if !syncer.isSyncing {
			panic("Block is from future. Find a way to handle this")
			// }
		}
	}

	return []*core.Block{}, []*core.Block{}
}

//SwitchFork switches to the fork given in blocks to process. Alos stops and restarts the DPos
func (syncer *Syncer) SwitchFork(blocksToRevert, blocksToProcess []*core.Block) error {
	log.Info().Msgf("Stopping the DPOS... %v", syncer.node.GetNodeID().Address)
	wasRunning := false //For DPos. DPos should only be restarted in case it was running beforehand

	if syncer.node.Dpos.HasStarted == true {
		wasRunning = true
	}

	syncer.node.Dpos.Stop()
	// syncer.chainSwitch = true

	if !syncer.isSyncing { //Check this condition
		syncer.isSyncing = true
		defer func(syncer *Syncer) {
			syncer.isSyncing = false
		}(syncer)
	}

	// //TODO :- stop dpos. Revert.
	// _, _ = blocksToRevert, blocksToProcess
	// if len(blocksToRevert) == 0 {
	// syncer.node.Services.RevertDposTill(syncer.node.Bc.GetMainTail())
	// }

	for _, revertBlock := range blocksToRevert {
		syncer.RevertState(revertBlock)
	}

	lastblockToRevert := blocksToRevert[len(blocksToRevert)-1]
	goBackblock := syncer.node.Bc.GetLastMinedCommitteeMemberLength()
	for i := 0; i < goBackblock; i++ {
		lastblockToRevert, _ = syncer.node.Bc.GetBlockByHash(lastblockToRevert.ParentHash())
		if lastblockToRevert == nil {
			break
		}
	}

	counter := int((2*syncer.node.Dpos.GetComitteeSize())/3)*syncer.node.Dpos.NumberOfToMineBlockPerSlot - goBackblock
	log.Info().Msgf("counter : %v, goBackblock %v", counter, goBackblock)
	for i := 0; i < counter; i++ {
		lastblockToRevert, _ = syncer.node.Bc.GetBlockByHash(lastblockToRevert.ParentHash())
		if lastblockToRevert == nil {
			break
		}
		minerAdd := lastblockToRevert.MinerPublicKey().String()
		syncer.node.Bc.UpdateIrreversibleBlockNumber(minerAdd, syncer.node.Dpos.GetComitteeSize(), syncer.node.Dpos.NumberOfToMineBlockPerSlot)
	}

	last := len(blocksToProcess) - 1
	fmt.Println("Applying blocks")
	for i := range blocksToProcess { // To run in ascending order
		err := syncer.ValidateForMainTail(blocksToProcess[last-i])
		if err != nil {
			return errFailedValidation
		}
		syncer.UpdateStates(blocksToProcess[last-i])
		syncer.node.Bp.Remove(blocksToProcess[last-i].Hash())
	}
	if wasRunning == true {
		log.Info().Msgf("Blocks reverted and applied")
		log.Info().Msgf("Starting DPOS again")
		// log.Info().Msgf("Please do miner registration yourself")
		err := syncer.node.Services.StartDpos()
		if err != nil {
			if err.Error() == "Stale Main-Tail" {
				syncer.isSyncing = false
				go syncer.SyncBlockchain()
			}
			return err
		}
	}
	// syncer.chainSwitch = false

	return nil
}

//UpdateStates updates all the state associated with the node
func (syncer *Syncer) UpdateStates(b *core.Block) bool {
	// log.Info().Msgf("checking time left in the round")
	// if syncer.node.NodeType != "Bootstrapper" { //Just for the sake of printing
	// 	sameMiner, timeLeft := syncer.TimeLeftInRound(b) // TODO: Rizwan look in this. U should eventually to comment this
	// 	fmt.Println("Same Miner turn: ", sameMiner, "time left: ", timeLeft)
	// }
	// fmt.Println("New time left in Round:- ", syncer.node.Services.TimeLeftInRoundBySlots(b))

	log.Info().Msgf("Updating States for %v\n", b)
	minerPk := b.MinerPublicKey()
	// minerAdd := syncer.node.GetAddressUsingPublicKey(minerPk)
	minerAdd := minerPk.String()

	// if syncer.isSyncing == true && syncer.node.Dpos.HasStarted == false { //Meaning the node is downloading old blocks already produced
	// 	syncer.node.Services.UpdateDpos(b)
	// }

	syncer.node.AddDeals(b.GetDeals())
	// if syncer.isSyncing {
	// 	syncer.node.RemoveExpiredDealsWhileSync(b.GetTimestamp())
	// }

	// err := syncer.node.Wm.CheckAndUpdateWalletStates(b, minerAdd) //Updates the balances of all the nodes
	// if err != nil {
	// 	log.Error().Msgf(err.Error()) //TODO: Bilal plz verify the changes
	// 	// Remove the block from the chain
	// 	syncer.node.RemoveDeals(b.GetDeals())
	// 	err := syncer.node.Bc.RemoveBlock(b)
	// 	if err != nil {
	// 		log.Info().Msgf("Could not remove block")
	// 	} else {
	// 		log.Info().Msgf("Block Removed because of WalletState Error %s", b)
	// 	}
	// 	return false
	// }

	// err = syncer.ProcessVotes(b, minerAdd)
	// if err != nil {
	// 	log.Info().Err(err)
	// 	syncer.node.RemoveDeals(b.GetDeals())
	// 	err := syncer.node.Bc.RemoveBlock(b)
	// 	if err != nil {
	// 		log.Info().Msgf("Block Could not be Removed because of ProcessVotes Error %s", b)
	// 	} else {
	// 		log.Info().Msgf("Block Removed because of ProcessVotes Error %s", b)
	// 	}
	// 	return false
	// }

	if b.IsDigestBlock() == true { //If state is appended then it is the digest block
		syncer.node.EmptyRoleCaches()
		syncer.node.SetCurEpoch(syncer.node.GetCurEpoch() + 1) //Updating the epoch number
		syncer.node.DeleteDealsFromDB()
	}

	// Adding the deals to the actice deals
	if syncer.node.Miner != nil { //Node having miner object shall have updated active deals
		syncer.node.Miner.UpdateCache(b)
		syncer.node.Miner.PrintPostStats()
	}

	syncer.node.RemoveExpiredDeals(b.GetTimestamp(), b.GetChPrPair())

	if syncer.node.StorageProvider != nil && syncer.isSyncing == false {
		syncer.node.StorageProvider.ProcessNewBlock(b)
	}

	syncer.node.UpdateThreadBase(b)

	syncer.node.Bc.UpdateIrreversibleBlockNumber(minerAdd, syncer.node.Dpos.GetComitteeSize(), syncer.node.Dpos.NumberOfToMineBlockPerSlot)
	return true
}

//RevertState reverts the state using the block- SHould be passed in descending order
func (syncer *Syncer) RevertState(revertBlock *core.Block) {

	log.Info().Msgf("RevertState for bloc %v", revertBlock)
	minerPk := revertBlock.MinerPublicKey()
	minerAdd := minerPk.String()
	syncer.node.Wm.RevertWalletStates(revertBlock, minerAdd) //Should revert in descending order

	// syncer.node.Wm.RevertVotes(revertBlock.GetVotes(), minerAdd, []*protobuf.Vote{}, revertBlock.GetRoundNum())

	//TODO revert deals in hash, posts in cache,
	syncer.node.Services.RevertDposState2(revertBlock)
	syncer.node.Bc.UpdateIrreversibleBlockNumberWhileReverting(minerAdd)
	syncer.node.RemoveDeals(revertBlock.GetDeals())
	syncer.node.AddExpiredDealsWhileReverting(revertBlock.GetChPrPair())

	if revertBlock.IsDigestBlock() == true { //If state is appended then it is the digest block
		syncer.node.SetCurEpoch(syncer.node.GetCurEpoch() - 1) //Updating the epoch number
	}
}

//HandleDigestBlock handles the digest block
func (syncer *Syncer) HandleDigestBlock(b *core.Block) {

	// log.Info().Msgf("Validating the digest block!!!")
	// validWallets := syncer.node.Wm.ValidateDigestBlock(b)
	// validMiners := syncer.node.Dpos.ValidateDigestMiners(b.PbBlock.GetState().GetMiners())
	// fmt.Println("valid miners: ", validMiners, "valid wallets: ", validWallets)
	// if validMiners == true && validWallets == true {
	// fmt.Println("Digest has been validated")
	// _, timeLeft := syncer.TimeLeftInRound(b)
	// if timeLeft == syncer.node.Dpos.RoundToleranceTime() { //Meaning this is the last block in the round
	syncer.node.ApplyDigestBlock(b)
	// }
	// }
	// err = node.ApplyDigestBlock(block)
	// if err != nil {
	// 	log.Info().Err(err)
	// }
}

func (syncer *Syncer) handleBlockByAskByHeight(msg net.Message) {

	heightFrom, err := strconv.Atoi(string(msg.Data))
	if err != nil || err != nil {
		log.Error().Msgf("Could not convert string to integer")
		return
	}

	if syncer.node.Bc == nil {
		log.Error().Msgf("Can't send blocks, blockchain is empty")
		return
	}

	block, err := syncer.node.Bc.GetBlockByHeight(int64(heightFrom))
	if err != nil {
		log.Error().Msgf("Can't send block, block of height: %v does not exist", heightFrom)
		return
	}

	blockBytes, err := block.ToBytes()
	if err != nil {
		log.Error().Msgf("Can't conver block to bytes")
		return
	}
	msgToSend := net.NewMessage(net.MsgCodeBlockSyncByHeight, blockBytes)
	syncer.node.Relay(msg.From, msgToSend)

}

func (syncer *Syncer) handleBlockAsk(msg net.Message) {
	if syncer.isSyncing {
		return
	}
	HashAndZ := bytes.Split(msg.Data, []byte(" , "))
	blockHash := HashAndZ[0]
	n := HashAndZ[1]

	z, err := strconv.Atoi(string(n))
	if err != nil {
		log.Error().Msgf("Could not convert string to integer")
		return
	}

	if syncer.node.Bc == nil {
		log.Info().Msgf("Can't send blocks, blockchain is empty")
		return
	}

	var tempb *core.Block
	if len(blockHash) != 0 { //
		tempb, err = syncer.node.Bc.GetBlockByHash(blockHash)
		if err != nil {
			// log.Error().Msgf("Do not have starting block of hash: %v", hex.EncodeToString(blockHash))
			return
		}
	}

	log.Info().Msgf("Sending %v blocks to From block %v", z, tempb)

	node := syncer.node
	var allBlocks [][]byte
	var toSend []byte
	if node.Bc != nil {
		TargetHeight := int64(z)
		if tempb != nil {
			TargetHeight = tempb.Height() + int64(z)
		}
		if TargetHeight > int64(node.Bc.Length()) {
			TargetHeight = int64(node.Bc.Length())
		}

		TargetHeightHahs, err2 := node.Bc.BlockHashByHeight(TargetHeight)
		if err2 != nil {
			log.Error().Err(err2)
			return
		}

		walk, err3 := node.Bc.GetBlockByHash(TargetHeightHahs)
		if err3 != nil {
			log.Error().Err(err3)
			return
		}

		log.Info().Msgf("starting walk %v", walk)
		for {
			blockBytes, err := walk.ToBytes()
			if err != nil {
				log.Error().Err(err)
				return
			}
			allBlocks = append([][]byte{blockBytes}, allBlocks...)

			walk, err = node.Bc.GetBlockByHash(walk.ParentHash())
			if err != nil || bytes.Equal(walk.Hash(), blockHash) {
				break
			}
		}

		log.Info().Msgf("Sending %v blocks to %v", z, msg.From.Address)
		if z > len(allBlocks) {
			toSend = bytes.Join(allBlocks, []byte(" , "))
			// for _, blockBytes := range allBlocks {
			// 	b, _ := core.NewBlockFromBytes(blockBytes)
			// log.Info().Msgf("Sending block %v to %v", b, msg.From.Address)
			// }
		} else {
			toSend = bytes.Join(allBlocks[:z], []byte(" , "))
			// for _, blockBytes := range allBlocks[:z] {
			// 	b, _ := core.NewBlockFromBytes(blockBytes)
			// log.Info().Msgf("Sending block %v to %v", b, msg.From.Address)
			// }
		}
	}
	msgToSend := net.NewMessage(net.MsgCodeBlockSync, toSend)
	syncer.node.Relay(msg.From, msgToSend)

}

func (syncer *Syncer) handleBlockSyncByHeight(msg net.Message) error {
	log.Info().Msgf("In HandleBlockSyncByHeight")
	block, err := core.NewBlockFromBytes(msg.Data)
	if err != nil {
		log.Error().Msgf("Could not read block from bytes")
		return err
	}
	syncer.handleBlockNew(block)
	return nil
}

func (syncer *Syncer) handlBlockChainLongSyncRequest(msg net.Message) {
	// log.Info().Msgf("handlBlockChainLongSyncRequest Enter")
	if syncer.isSyncing {
		return
	}

	BlockHashs := bytes.Split(msg.Data, []byte("timbre"))

	if syncer.node.Bc == nil {
		log.Info().Msgf("Can't send blocks, blockchain is empty")
		return
	}

	var ByteTosend []byte
	blockBytes, err := syncer.node.Bc.GetMainTail().ToBytes()
	if err != nil {
		log.Info().Msgf("handlBlockChainLongSyncRequest Error GetMainTail().ToBytes() Error %v", err)
		return
	}
	ByteTosend = append(blockBytes, []byte("timbre")...)
	if len(BlockHashs) > 1 {
		var tempb *core.Block
		var err error
		for _, blockHash := range BlockHashs {
			tempb, err = syncer.node.Bc.GetBlockByHash(blockHash)
			if err == nil {
				break
			}
		}
		if tempb == nil {
			log.Info().Msgf(" handlBlockChainLongSyncRequest No hash found in current chain")
			return
		}

		ByteTosend = append(ByteTosend, tempb.Hash()...)
		ByteTosend = append(ByteTosend, []byte("timbre")...)
		ByteTosend = append(ByteTosend, syncer.node.Bc.Genesis().Hash()...)
	} else {
		genesisBlockBytes, _ := syncer.node.Bc.Genesis().ToBytes()
		ByteTosend = append(ByteTosend, genesisBlockBytes...)
		// ByteTosend = append(ByteTosend, syncer.node.Bc.Genesis().Hash()...)
	}

	// fmt.Printf("sending bytes: %v\n", hex.EncodeToString(ByteTosend))
	msgToSend := net.NewMessage(net.MsgCodeBlockChainLongSyncResponse, ByteTosend)

	syncer.node.Relay(msg.From, msgToSend)
}

func (syncer *Syncer) handlBlockChainLongSyncResponse(msg net.Message) {
	log.Info().Msgf("handlBlockChainLongSyncResponse Enter")
	// fmt.Printf("Received bytes: %v\n", hex.EncodeToString(msg.Data))
	bytesArray := bytes.Split(msg.Data, []byte("timbre"))
	mainTailBlockByte := bytesArray[0]
	startBlockByte := bytesArray[1]

	// fmt.Printf("handlBlockChainLongSyncResponse 1 \n")
	block, err := core.NewBlockFromBytes(mainTailBlockByte)
	if err != nil {
		fmt.Printf("Error: Could not read sent tail Error %v || msg.Data length %v", err, len(msg.Data))
		return
	}
	// fmt.Printf("handlBlockChainLongSyncResponse 2 \n")
	var startBlock *core.Block
	if len(bytesArray) > 2 {
		// fmt.Printf("handlBlockChainLongSyncResponse 3 \n")
		startBlock, err = syncer.node.Bc.GetBlockByHash(startBlockByte)
		if err != nil {
			fmt.Printf("Error: Could not find startBlockHash Error %v || msg.Data length %v", err)
			return
		}
	} else {
		startBlock, err = core.NewBlockFromBytes(startBlockByte)
		if err != nil {
			fmt.Printf("Error: Could not read sent Start Block Error %v", err)
			return
		}
	}
	//  else {
	// 	GenesisBlocktoSetWhileSync, err := core.NewBlockFromBytes(startBlockHash)
	// 	if err != nil {
	// 		fmt.Printf("Error: Could not read sent tail Error %v || msg.Data length %v", err, len(msg.Data))
	// 		return
	// 	}
	// 	//TODO: Change Genesis Block
	// 	ChangeGenesisWhileSyncing = true
	// 	startBlockHash = GenesisBlocktoSetWhileSync.Hash()
	// }

	// fmt.Printf("handlBlockChainLongSyncResponse 5 \n")

	// log.Info().Msgf("handlBlockChainLongSyncResponse request from %v, block %v", msg.From.String(), block)

	if syncer.targetBlock == nil {
		log.Info().Msgf("Target block set to %s", block.String())
		// log.Info().Msgf("startingBlock block set to %s", hex.EncodeToString(startBlockHash))
		log.Info().Msgf("startingBlock block set to %s", startBlock.String())
		syncer.targetBlock = block
		syncer.curBlock = startBlock
	} else {
		if block.Height() > syncer.targetBlock.Height() {
			log.Info().Msgf("Target block set to %s", block.String())
			// log.Info().Msgf("startingBlock block set to %s", hex.EncodeToString(startBlockHash))
			log.Info().Msgf("startingBlock block set to %s", startBlock.String())
			syncer.targetBlock = block
			syncer.curBlock = startBlock
			// if ChangeGenesisWhileSyncing {
			// syncer.node.Bc = core.ResetBlockChain(syncer.node.Db, GenesisBlocktoSetWhileSync)
			// Reset All Cache of miner/SP
			// }
		} else {
			log.Info().Msgf("Target block is already set.")
		}
	}
}

func (syncer *Syncer) handleBlockSync(msg net.Message) error {
	log.Info().Msgf("In HandleBlockSync")

	allBlocks := msg.Data
	if len(allBlocks) == 0 {
		return errors.New("Empty block list received")
	}
	leftOver := bytes.Split(allBlocks, []byte(" , "))

	lastsentBlock, err := core.NewBlockFromBytes(leftOver[len(leftOver)-1])

	if err != nil {
		return errors.New("Unable to read latest block")
	}

	log.Info().Msgf("In HandleBlockSync lastsentBlock parent %v lastsentBlock %v", hex.EncodeToString(lastsentBlock.ParentHash()), lastsentBlock)
	// if len(syncer.curBlock) > 0 && bytes.Equal(lastsentBlock.ParentHash(), syncer.curBlock) {
	if syncer.curBlock != nil && lastsentBlock.Height() <= syncer.curBlock.Height() { //IsSameAs(lastsentBlock) {
		return errors.New("Received blocks have already been received")
	}

	for _, blockBytes := range leftOver {
		block, err := core.NewBlockFromBytes(blockBytes)
		if err != nil {
			continue
		}
		syncer.handleBlockNew(block)
	}

	return nil
}

//PrintBlockChain prints the blocks in blockchain
func (syncer *Syncer) PrintBlockChain() {
	log.Info().Msgf("Syncer: BlockChain:\n %s", syncer.node.BlockChainString())
}

//How about the case in which block from the last miner comes in the next miner slot

//ValidateMinerSlot validates the slot of the miner
func (syncer *Syncer) ValidateMinerSlot(block *core.Block) error { //TODO check per miner tolerance
	// fmt.Println("**********")
	// fmt.Println("bts: ", block.GetTimestamp()/1e9, " b-Round: ", block.GetRoundNum(), " Miner: ", block.MinerPublicKey())
	// fmt.Println("DPOS:- ", syncer.node.Dpos.GetCurMinerAdd(), " ROund: ", syncer.node.Dpos.GetRoundNum())
	// fmt.Println("--------------")

	bPk := block.MinerPublicKey().String()
	// bTs := block.GetTimestamp()
	bRound := block.GetRoundNum()
	if bRound == 1 { //Cannot really verify the first round
		return nil
	}
	// blockInterval := syncer.node.Dpos.GetTickInterval().Nanoseconds()
	// blockIntervalSec := syncer.node.Dpos.GetTickInterval().Seconds()
	bHeight := block.Height()
	blocksPerMiner := syncer.node.Dpos.BlocksPerMiner()

	// pBlock, err := syncer.node.Bc.GetBlockByHash(block.ParentHash())
	// if err != nil {
	// 	log.Info().Err(err)
	// }
	// pTs := pBlock.GetTimestamp()
	// pRound := pBlock.GetRoundNum()
	// ppk := pBlock.MinerPublicKey().String()

	if syncer.node.Dpos.HasStarted == true && bRound == syncer.node.Dpos.GetRoundNum() {
		// if syncer.node.Dpos.GetCurrentMinerAdd() != bPk {
		// 	//Allowing block to be acceptable in case it came in the next miner slot due to delays. Assuming new miner has not produced block as yet.
		// 	if syncer.node.Dpos.GetCurrentMinerAdd() == ppk { //If miner mismatch occur in the middle of the next miner slots

		// 		return errors.New("Mismatch miner slot") //In this case parent is the miner of current round and somebody else mined in between
		// 	}
		// }
		err := syncer.VerifyLastMiner(block) //In this case this is the block from last miner which got delayed then just check
		if err != nil {
			return err
		}

	} else {
		err := syncer.VerifyLastMiner(block)
		if err != nil {
			return err
		}
	}
	//FOR now commenting out the timestamp check
	//TODO:- Subtract tolerance time when the miner changes not only when round changes
	// bTimeDiff := bTs - pTs //Check if the timedifference between blocks is equal to the allowed time
	// bTimeDiffSec := float64(bTimeDiff) / 1e9

	// if bRound == pRound { //TODO: Incorporate the block miss
	// 	remSec := int64(bTimeDiffSec) % int64(blockIntervalSec) //Remainder seconds
	// 	if remSec > 1 && remSec < int64(blockIntervalSec)-1 {
	// 		log.Info().Err(errors.New("Block is not produced on correct timestamp"))
	// 	}

	// 	if bTimeDiff > blockInterval+1*1e9 { //giving 1 secconds of buffer
	// 		return errors.New("Miner is delaying block production")
	// 	}
	// 	if bTimeDiff < blockInterval-1*1e9 { //giving 1 secconds of buffer
	// 		log.Info().Err(errors.New("Miner is producing block too often"))
	// 		return errors.New("Miner is producing block too often")
	// 	}
	// } else { //If rounds are different
	// 	if bTimeDiff > blockInterval+(int64(syncer.node.Dpos.RoundToleranceTime()+1)*1e9) { //giving 1 secconds of buffer
	// 		return errors.New("Miner is delaying block production")
	// 	}
	// 	if bTimeDiff < blockInterval-(int64(syncer.node.Dpos.RoundToleranceTime()-1)*1e9) { //giving 1 secconds of buffer
	// 		log.Info().Err(errors.New("Miner is producing block too often"))
	// 		return errors.New("Miner is producing block too often")
	// 	}
	// }

	// checking if miner has produced more blocks then allowed (spamming)
	bHeightCheck := bHeight - int64(blocksPerMiner)
	if bHeightCheck >= 0 {
		// fmt.Println("CHecking height")
		refBlock, err := syncer.node.Bc.GetBlockByHeight(bHeightCheck)
		refRound := refBlock.GetRoundNum()
		if err != nil {
			log.Info().Err(err)
			return err
		}
		//Checking the miner of the reference block
		refMinerPk := refBlock.MinerPublicKey().String()

		if bPk == refMinerPk && refRound == bRound {
			log.Info().Err(errors.New("More blocks produced than allowed"))
			//TODO:- Put punishment protocol here
			return errors.New("More blocks produced than allowed")
		}
	}
	return nil
}

//VerifyLastMiner verfies the last miner in the round turn
func (syncer *Syncer) VerifyLastMiner(block *core.Block) error {
	// bHeight := block.Height()
	// fmt.Println("Verifying last miner: ", bHeight, block.GetTimestamp()/1e9)
	bPk := block.MinerPublicKey().String()
	bRound := block.GetRoundNum()
	bTs := block.GetTimestamp()
	phash := block.ParentHash()
	pBlock, err := syncer.node.Bc.GetBlockByHash(phash)
	if err != nil {
		log.Info().Err(err)
		return err
	}
	if pBlock == nil {
		// fmt.Println("Nil pmblock!!")
		return nil
	}
	pPk := pBlock.MinerPublicKey().String()
	pRound := pBlock.GetRoundNum()
	pTs := pBlock.GetTimestamp()

	if bPk == pPk && bRound == pRound { //if last block was also mine no need to check.
		timeDif := bTs - pTs
		// fmt.Println("time is: ", int64(syncer.node.Dpos.GetTickInterval().Nanoseconds()-1e9))
		if timeDif < int64(syncer.node.Dpos.GetTickInterval().Nanoseconds()-1e9) {
			log.Warn().Err(errors.New("block - parent TS is less then tick interval"))
		}

		// if
		return nil
	}

	// pmBlock, parentMiner, err := syncer.node.Bc.GetLastMiner(bHeight, bRound, bPk)
	// if err != nil {
	// 	return err
	// }
	// if pmBlock == nil {
	// 	fmt.Println("Nil pmblock!!")
	// 	return nil
	// }
	// pmTs := pmBlock.GetTimestamp()
	// pmRound := pmBlock.GetRoundNum()
	// if parentMiner == "" { //In case verification is done for the first miner
	// 	return nil
	// }

	// firstB, err := syncer.node.Bc.GetFirstBlockOfMiner(block, bHeight, bPk) //THis gives the first block produced
	// if err != nil {
	// 	return err
	// }
	// if firstB == nil {
	// 	fmt.Println("First blcok is nil")
	// 	return nil
	// }
	// valid := syncer.node.Dpos.ValidateMiningOrder(pPk, bPk, pRound, bRound, firstB.GetTimestamp()-pmTs)

	miningErr := syncer.node.Dpos.ValidateMiningOrder(pPk, bPk, pRound, bRound, bTs-pTs) //This validation only happens accors the miners
	if miningErr != nil {
		// fmt.Println(err.Error())
		return miningErr
	}
	return nil
}

//TimeLeftInRound returns the time left in the round in seconds
func (syncer *Syncer) TimeLeftInRound(block *core.Block) (bool, float64) {
	sameMiner := false
	bTs := block.GetTimestamp()
	bRound := block.GetRoundNum()
	bPk := block.MinerPublicKey()
	blockHeight := block.Height() //for printing purpose
	printRound := bRound

	fmt.Println("***************", syncer.node.GetNodeID().Address)
	minerInd, exists := syncer.node.Dpos.GetMinerIndex(bRound, bPk.String())
	if exists == false {
		log.Info().Err(errors.New("Miner wasn't allowed to mine in this round"))
	}

	minerSlotsPassed := 1                               //Counting this block as the one block produced by this miner
	blocksPerMiner := syncer.node.Dpos.BlocksPerMiner() //The maximum number of blocks that a miner can produce
	// fmt.Println("BlocksPerMiner: ", blocksPerMiner)
	// totalBlocksTime :=
	minerSlotTime := syncer.node.Dpos.GetMinerSlotTime()
	blockInterval := syncer.node.Dpos.GetTickInterval().Nanoseconds()

	for i := 0; i < blocksPerMiner-1; i++ { //Looping over blocksPerMiner+1 just to ensure that a miner hasn't produced block more than it was allowed
		phash := block.ParentHash()
		pBlock, err := syncer.node.Bc.GetBlockByHash(phash)
		if err != nil {
			log.Info().Err(err)
			break
		}
		pPk := pBlock.MinerPublicKey()
		pRound := pBlock.GetRoundNum()
		pTs := pBlock.GetTimestamp()
		// fmt.Println("ppk:-", hex.EncodeToString(pPk), "pRound: ", pRound)
		bTimeDiff := bTs - pTs //Time difference between the last block and the current block

		slotDiff := int(math.Round(float64(bTimeDiff) / float64(syncer.node.Dpos.GetTickInterval().Nanoseconds()))) //TODO:- Check if round is the right func to use
		fmt.Println("b time diff: ", bTimeDiff, "slot diff: ", slotDiff)
		fmt.Println("bRound: ", bRound, "pRound:", pRound, "p height: ", pBlock.HeightInt())
		// fmt.Println("pts: ", pTs, "parent key:", pPk)
		if pPk.String() != bPk.String() || pRound != bRound {
			//This is the only block produced by the miner
			if pRound != bRound {
				_, pRoundTimeLeft := syncer.TimeLeftInRound(pBlock)
				slotDiff = int(math.Round((float64(bTimeDiff) - pRoundTimeLeft*1e9) / float64(syncer.node.Dpos.GetTickInterval().Nanoseconds())))
			}
			if pRound == bRound && pPk.String() != bPk.String() {
				slotDiff = int(math.Round((float64(bTimeDiff) - syncer.node.Dpos.RoundToleranceTime()*1e9) / float64(syncer.node.Dpos.GetTickInterval().Nanoseconds())))
			}
			fmt.Println("renewed:- ", slotDiff)
			minerSlotsPassed += slotDiff - 1 //TODO:- check this addition
			break
		}
		if minerSlotsPassed > blocksPerMiner {
			log.Info().Err(errors.New("Miner prodecued more blocks than allowed"))
			break
		}

		// bTimeDiff := math.Round(float64(bTs-pTs) / float64(1e9))

		fmt.Println("block Time Diff: ", bTimeDiff)
		//Handles the case if a miner produce block then doesn't produce block and finally produce one
		if bTimeDiff > blockInterval+1*1e9 { //giving 1 secconds of buffer
			i += slotDiff - 1 //TODO:Incorporate missing round
			// bTimeDiff /
			fmt.Println("Time diff was big")
			minerSlotsPassed += slotDiff - 1
		}
		if bTimeDiff < blockInterval-1*1e9 { //giving 1 secconds of buffer
			log.Info().Err(errors.New("miner is producing blocks before time/spammming"))
		}
		minerSlotsPassed++

		bPk = pPk
		bRound = pRound
		bTs = pTs
		block = pBlock

		// if bTimeDiff == syncer.node.Dpos.GetTickInterval().Seconds() {
		// 	fmt.Println("Time difference matches exactly")
		// }
	}
	fmt.Println("Slots passed by the miner:-", minerSlotsPassed)
	if minerSlotsPassed > blocksPerMiner {
		minerSlotsPassed = blocksPerMiner
	}

	minersToCome := syncer.node.Dpos.TotalMinersInRound(bRound) - minerInd - 1
	if minersToCome < 0 && syncer.node.NodeType != "Bootstrapper" {
		fmt.Println("Miner to come:- ", minersToCome, "miner ind: ", minerInd, "total miners", syncer.node.Dpos.TotalMinersInRound(bRound))
		syncer.node.Dpos.PrintMinersByRound()
		fmt.Println(syncer.node.Dpos.GetCurrentMinerAdd(), "Cur round:- ", syncer.node.Dpos.GetRoundNum())
		fmt.Println("Block height: ", blockHeight, "bround: ", printRound, "timeLeftinROund: ", syncer.node.Dpos.TimeLeftInRound())
		panic("Miners to come going in negative...")
	}
	fmt.Println("Miner to come:- ", minersToCome, "miner ind: ", minerInd, "total miners", syncer.node.Dpos.TotalMinersInRound(bRound))

	if len(syncer.node.Dpos.MinersByRound[bRound]) != 0 {
		// fmt.Println("Miners: ", syncer.node.Dpos.MinersByRound[bRound])
		fmt.Println("Miners left: ", (syncer.node.Dpos.MinersByRound[bRound])[minerInd+1:])
	} else {
		fmt.Println("Warn:- Miners are 0", syncer.node.NodeType)
	}
	nextMinersTime := minerSlotTime.Seconds() * float64(minersToCome)
	if minerSlotsPassed >= blocksPerMiner {
		// if minersToCome == 0 { //if this is the last block in the round then there is the buffer time at the end of the round
		// 	return sameMiner, syncer.node.Dpos.RoundToleranceTime()
		// }
		fmt.Println("^^^^^^^^^^^^^^^^^^^^^^^^^^")
		return sameMiner, nextMinersTime + syncer.node.Dpos.RoundToleranceTime()
	}

	sameMiner = true
	curMinerTime := (float64(blocksPerMiner-minerSlotsPassed))*syncer.node.Dpos.GetTickInterval().Seconds() + syncer.node.Dpos.RoundToleranceTime()
	timeTillRoundEnd := nextMinersTime + curMinerTime
	fmt.Println("^^^^^^^^^^^^^^^^")

	return sameMiner, timeTillRoundEnd
}

//RoundStartTime gives the unix value of round start time with respect to current block timestamp
func (syncer *Syncer) RoundStartTime(block *core.Block) int64 {
	bRound := block.GetRoundNum()
	bTs := block.GetTimestamp() //in nanoseconds
	bTs = bTs / (1 * 1e9)       //in seconds
	totalRoundTime := syncer.node.Dpos.TotalRoundTime(bRound)
	// tolTime := syncer.node.Dpos.RoundToleranceTime() //Round tolerance time towards the end. In seconds
	_, timeLeftInRound := syncer.TimeLeftInRound(block)
	// if timeLeftInRound == 0 {
	// 	return bTs - int64(totalRoundTime) + int64(tolTime)
	// }
	timeTillRoundStart := totalRoundTime - timeLeftInRound
	return bTs - int64(timeTillRoundStart)
}

//ResyncChain will resync the chain
func (syncer *Syncer) ResyncChainOld(tailHeight int) {
	//Trim chain till tail height in case it is higher

	if syncer.reSync == true {
		return
	}
	fmt.Println("Running resync chain protocol...")

	syncer.reSync = true

	tail := syncer.node.Bc.GetMainTail()
	// fmt.Println("Running resync chain protocol 1.5")
	tailTs := tail.GetTimestamp()
	curTime := time.Now().UnixNano()
	if (curTime - tailTs) < (2 * syncer.node.Dpos.GetTickInterval().Nanoseconds()) {
		fmt.Println("Running resync chain protocol -- returning in if condition ...")
		syncer.reSync = false
		return
	}

	// wasRunning := false //For DPos. DPos should only be restarted in case it was running beforehand
	// if syncer.node.Dpos.HasStarted == true {
	// 	wasRunning = true
	// }

	syncer.node.Dpos.Stop() //Ensure that DPOS has stopped
	syncer.isSyncing = true
	syncer.node.Services.RevertDposTill(syncer.node.Bc.GetMainTail())

	// fmt.Println("Running resync chain protocol 1")

	// count := 0
	for (curTime - tailTs) > (2 * syncer.node.Dpos.GetTickInterval().Nanoseconds()) {
		fromBlockHeight := int(syncer.node.Bc.GetMainTail().Height() + 1)
		msg := net.NewMessage(net.MsgCodeBlockAskByHeight, []byte(strconv.Itoa(fromBlockHeight)))
		// fmt.Println("Running resync chain protocol 2")
		syncer.node.RequestAndResponse(msg, syncer.handleBlockSyncByHeight, net.MsgCodeBlockSyncByHeight)
		time.Sleep(3 * time.Second)
		// fmt.Println("Running resync chain protocol 3")
		// fmt.Println("count is: ", count)
		// count++
		tail = syncer.node.Bc.GetMainTail()
		// fmt.Println("Running resync chain protocol 4")
		tailTs = tail.GetTimestamp()
		curTime = time.Now().UnixNano()
	}

	// _ = wasRunning

	syncer.reSync = false
	err := syncer.node.Services.StartDpos()
	if err == nil {
		syncer.isSyncing = false
	}

}

//SyncBlockchain asks for the new block in case main tail is stale
func (syncer *Syncer) SyncBlockchain() {
	fmt.Println("Running sync chain protocol new...")

	if syncer.isSyncing == true {
		fmt.Println("Already syncing...")
		return
	}

	syncer.isSyncing = true
	defer func(syncer *Syncer) {
		syncer.isSyncing = false
	}(syncer)

	syncer.targetBlock = nil
	syncer.curBlock = nil
	blockheight := int64(0)
	counterToGetMainTail := 0

	if syncer.node.Bc != nil {
		// tail := syncer.node.Bc.GetMainTail()
		fmt.Println("Running resync chain protocol 1.5 new")
		// tailTs := tail.GetTimestamp()
		// curTime := time.Now().UnixNano()
		// if (curTime - tailTs) < (2 * syncer.node.Dpos.GetTickInterval().Nanoseconds()) {
		// 	// syncer.isSyncing = false
		// 	return
		// }
		syncer.node.Dpos.Stop()
		syncer.node.Services.RevertDposTill(syncer.node.Bc.GetMainTail())
		// count := 0
		blockheight = syncer.node.Bc.GetMainTail().Height()
	}
L:
	for {
	L1:
		for {
			var lastNblocksData []byte
			fmt.Println("ResyncChain: Restart", counterToGetMainTail)
			for i := 0; i < 10 && blockheight > 0; i++ {
				b, _ := syncer.node.Bc.GetBlockByHeight(blockheight)
				lastNblocksData = append(lastNblocksData, b.Hash()...)
				lastNblocksData = append(lastNblocksData, []byte("timbre")...)
				blockheight = blockheight - 1
			}

			msg := net.NewMessage(net.MsgCodeBlockChainLongSyncResquest, lastNblocksData)

			ctx, cancel := context.WithTimeout(context.TODO(), 5*time.Second)
			for _, item := range syncer.chooseNPeers(noOfNodesToQuery) {
				// fmt.Println("ResyncChain: send request to ", item)
				syncer.node.Relay(item, msg)
			}

		L2:
			for {
				select {
				case <-time.After(1 * time.Second):
					fmt.Println("ResyncChain: wait till targetBlock is set")
					// time.Sleep(1 * time.Second)
					// if syncer.targetBlock != nil {
					// 	cancel()
					// 	break L1
					// }
				case <-ctx.Done():
					fmt.Println("ResyncChain: ctx.Done case")
					if syncer.targetBlock != nil {
						cancel()
						break L1
					}
					// blockheight -= 1
					// if blockheight <= 0 {
					// 	blockheight = 0
					// }
					if syncer.node.Bc == nil {
						counterToGetMainTail += 1
						if counterToGetMainTail >= 2 {
							fmt.Println("ResyncChain: Blockchain in nil, could not find any Tail, start own chain, returning counterToGetMainTail", counterToGetMainTail)
							// syncer.isSyncing = false
							break L
						}
					}
					cancel()
					break L2

				}
			}
		}

		if syncer.node.Bc == nil || syncer.targetBlock.Height() > syncer.node.Bc.GetMainTail().Height() {
			syncer.SyncBlockchainCore(10)
		} else {
			break L
		}

		// previousTargetHight := syncer.node.Bc.GetMainTail().Height()
		// err := syncer.RequestCurrentTail()
		// if err != nil {
		// 	log.Error().Msgf("Error while getting CurrentTail")
		// 	continue
		// }
		// if syncer.targetBlock.Height() >= previousTargetHight+10 {
		// 	log.Info().Msgf("Target Block Updated sync again")
		// 	// break // break because in case of a missing block, say block 5, the maintail remians at 4 whereas
		// 	//the targetblockheight is 20 and this keeps on looping forever.
		// }
	}

	// syncer.isSyncing = false
	// syncer.node.Dpos.Start()
	log.Info().Msgf("Sync done. Startig DPos now...")
	err := syncer.node.Services.StartDpos()
	if err != nil {
		if err.Error() == "Stale Main-Tail" {
			syncer.isSyncing = false
			go syncer.SyncBlockchain()
		}
		// return err
	}
	syncer.node.Bp, _ = core.NewBlockpool(core.DefaultBlockpoolCapacity)
	fmt.Println("ResyncChain: syncer.isSyncing ", syncer.isSyncing)
}

func (syncer *Syncer) SyncBlockchainCore(z int) {
	var lastBlockHash []byte
	for {
		b1 := (syncer.node.Bc != nil && syncer.targetBlock.Height() <= syncer.node.Bc.GetMainTail().Height())
		// b2 := syncer.node.Bc != nil && len(syncer.curBlock) > 0 && bytes.Equal(syncer.curBlock, syncer.targetBlock.Hash())
		b2 := syncer.curBlock != nil && syncer.curBlock.IsSameAs(syncer.targetBlock)
		if b1 || b2 {
			log.Info().Msgf("Sync Complete. b1 %v b2 %v", b1, b2)
			log.Info().Msgf("Sync Complete. Target Height: %d ||| Current Block Height:  %d ||| Main Height:  %d", syncer.targetBlock.Height(), syncer.curBlock.Height(), syncer.node.Bc.GetMainTail().Height())
			break
		}

		if syncer.node.Bc != nil && syncer.node.Bc.GetMainTail() != nil {
			lastBlockHash = syncer.curBlock.Hash()
			// lastBlockHash = syncer.node.Bc.GetMainTail().Hash()
			log.Info().Msgf("syncer.curBlock height %v", syncer.curBlock.Height())
		}

		askMsg := syncer.createAskMsg(lastBlockHash, z)
		respChan := make(chan net.Message, 128)
		syncer.node.AddOutgoingChan(respChan)
		for _, item := range syncer.chooseNPeers(noOfNodesToQuery) {
			syncer.node.Relay(item, askMsg)
		}
		// syncer.node.Broadcast(askMsg)                                           // Ask bilal bhai
		ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second) // 3 second timeout
	L:
		for {
			select {
			case <-ctx.Done():
				log.Error().Msgf("Context timed out before blocks received")
				break L
			case msg := <-respChan:
				if msg.Code == net.MsgCodeBlockSync {
					err := syncer.handleBlockSync(msg) // Add error handling
					if err != nil {
						log.Error().Msgf("HandleBlockSync err %v", err)
					}
					break L
				}
				// case <-time.After(1 * time.Second):
				// 	log.Error().Msgf("Wait for MsgCodeBlockSync")
			}
		}
		cancel()

		syncer.node.RemoveOutgoingChan(respChan)
	}

	log.Info().Msgf("Network Sync Done, Iterate Blockpool")
	tails := syncer.node.Bp.GetTails()
	for _, tail := range tails {
		chain := syncer.node.Bp.GetChainByTail(tail)
		log.Info().Msgf("BP tails: %v", tails)
		syncer.IterateCheckAndAddChain(chain)
	}
}
