package roles

import (
	"bytes"
	"strings"
	"time"

	"github.com/guyu96/go-timbre/net"
	"github.com/guyu96/go-timbre/protobuf"
)

// Moderator submits new posts to a Timbre forum.
type Moderator struct {
	node     *net.Node
	incoming chan net.Message

	keywords []string
}

// NewModerator creates a new User.
func NewModerator(node *net.Node) *Moderator {
	mod := &Moderator{
		node:     node,
		incoming: make(chan net.Message, userChanSize),
	}
	node.AddOutgoingChan(mod.incoming)
	return mod
}

//Process for the User where it will be receiving messages
func (mod *Moderator) Process() {
	for msg := range mod.incoming {
		// log.Info().Msgf("User Received something %v", msg.Code)
		switch msg.Code {
		case net.MsgCodeGetModList:
			mod.onGetModList(msg)
		}
	}
}

// AddKeyword adds keyword to the keywords array
func (mod *Moderator) AddKeyword(keyword string) {
	mod.keywords = append(mod.keywords, keyword)
}

type Filter func(*protobuf.StoredPost) bool

func (mod *Moderator) filterByKeyWord(record *protobuf.StoredPost) bool {
	for _, keyword := range mod.keywords {
		if strings.Contains(string(record.Content), keyword) {
			return true
		}
	}
	return false
}

func (mod *Moderator) filterDuplicates(records []string) []string {
	mapper := map[string]bool{}
	filtered := []string{}

	for _, record := range records {
		if ok := mapper[record]; ok {
			continue
		}
		mapper[record] = true
		filtered = append(filtered, record)
	}

	return filtered
}

func (mod *Moderator) applyFilters(records []*protobuf.StoredPost, filters ...Filter) []*protobuf.StoredPost {

	if len(filters) == 0 {
		return records
	}

	filteredRecords := make([]*protobuf.StoredPost, 0, len(records))
	for _, r := range records {
		keep := true

		for _, f := range filters {
			if !f(r) {
				keep = false
				break
			}
		}
		if keep {
			filteredRecords = append(filteredRecords, r)
		}
	}

	return filteredRecords
}

// onGetModList returns list of filtered posts
func (mod *Moderator) onGetModList(msg net.Message) {

	mod.node.User.GetThreads()
	time.Sleep(1 * time.Second)
	ts := mod.node.User.GetStoredP()

	filteredPosts := mod.applyFilters(ts, mod.filterByKeyWord)
	toSend := mod.getHashes(filteredPosts)
	newMsg := net.NewMessage(net.MsgCodeModPosts, toSend)
	mod.node.Relay(msg.From, newMsg)
}

func (mod *Moderator) getHashes(posts []*protobuf.StoredPost) []byte {

	var postHashes [][]byte
	for _, item := range posts {
		postHashes = append(postHashes, item.Hash)
	}
	toSend := bytes.Join(postHashes, []byte(" ;,; "))
	return toSend
}
