package services

import (

	// "math"

	"errors"
	"fmt"
	"math"
	"time"

	"github.com/guyu96/go-timbre/core"
	dposstate "github.com/guyu96/go-timbre/dpos/state"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	"github.com/guyu96/go-timbre/wallet"
	// pb "github.com/guyu96/go-timbre/protobuf"
)

var (
	// pbRound int64 = 0
	nextTs       int64 = 0
	ErrStaleTail       = errors.New("Stale Main-Tail")
)

//DposSyncer updates the dpos
type DposSyncer struct {
	pbRound   int64
	nextTs    int64
	lastMiner string
	genesisTs int64
}

//DposUpdate syncs the dpos
var DposUpdate *DposSyncer = &DposSyncer{
	pbRound:   0,
	nextTs:    0,
	lastMiner: "",
	genesisTs: 0,
}

//StartDpos starts the DPOS consensus protocol.
func (s *Services) StartDpos() error {
	node := s.node

	if node.Dpos.HasStarted == true {
		log.Info().Msgf("Dpos has already started can't start now")
		return nil
	}

	log.Info().Msgf("Starting DPos...")
	// nodeID := node.GetNodeID()
	// peerAdd := node.GetPeerAddress()[0] //Only taking first address for the testing purpose

	if s.node.Bc != nil { //That means that this node has synced before
		log.Info().Msgf("starting DPOS after the state catch-up")

		//Getting the latest block
		s.node.Dpos.SetRoundTimeOffset(0)
		tailBlock := s.node.Bc.GetMainTail()
		tailBlockTs := tailBlock.GetTimestamp() / (1e9) //in seconds
		tailRound := tailBlock.GetRoundNum()
		fmt.Println("height: ", tailBlock.Height(), "tail round: ", tailRound, "ts: ", tailBlock.GetTimestamp()/1e9)

		// fmt.Println("Curtime - tailBlock: ", curTime-(tailBlock.GetTimestamp()/(1e9)))
		curTime := time.Now().Unix()           //Assuming ur UNIX time is correct.
		timeSinceTail := curTime - tailBlockTs //in seconds
		fmt.Println("TimeSinceTail: ", timeSinceTail)
		if timeSinceTail > 2*int64(s.node.Dpos.GetTickInterval().Seconds()) {
			// fmt.Println("Check")
			log.Warn().Err(errors.New("Curtime and last block time has big difference. SYNC again"))
			go s.node.Syncer.SyncBlockchain() //Should initiate resync chain protocol
			//review this change
			log.Info().Msgf("Current-time is way ahead tail. Need to sync again")
			return ErrStaleTail

		}
		// _, timeLeft := s.node.Syncer.TimeLeftInRound(tailBlock) //return the time left in seconds in the round
		timeLeft := s.TimeLeftInRoundBySlots(tailBlock) //Gives the timeLeft form time.Now()
		fmt.Println("TimeLeft: ", timeLeft)
		// if timeLeft < 0 {
		// 	fmt.Println(timeLeft)
		// 	s.node.Dpos.PrintMinersByRound()
		// 	panic("time left is below zero")
		// }
		// timeToSync := int64(timeLeft) - timeSinceTail

		// fmt.Println("TIME LEFT: ", timeLeft, " sync time ", timeToSync)
		// fmt.Println("value: ", -(int64(s.node.Dpos.GetTickInterval().Seconds() / 2)))
		if timeLeft <= 2 { //To increase tolerance
			if timeLeft > 0 {
				time.Sleep(2 * time.Second)
				// time.Sleep(s.node.Dpos.GetTickInterval() / 2) //For round tolerance time
			}
			// else if timeLeft > -(int64(s.node.Dpos.GetTickInterval().Seconds() / 2)) {
			// 	time.Sleep(s.node.Dpos.GetTickInterval() / 2) //For round tolerance time
			// }

			fmt.Println("SHould start next round")
			node.Dpos.AdvanceRound(time.Now().UnixNano())
			s.node.Dpos.SetRoundStartTime(s.RoundStartTime(s.node.Dpos.GetRoundNum())) //in seconds

			// s.node.Dpos.AdvanceRoundInSec(float64(timeSinceTail) - timeLeft)
			fmt.Println("ROUND START:- ", s.node.Dpos.GetRoundStartTime())

			if s.node.Dpos.GetRoundNum() != tailRound+1 {
				fmt.Println("DPos round- Expected: ", s.node.Dpos.GetRoundNum(), tailRound+1)
				negComTime := -int64(s.node.Dpos.GetCommitteeTime().Seconds())
				fmt.Println("negComTime", negComTime)
				if timeLeft < negComTime {
					return errors.New("Need to sync again. Sync time was bigger than round")
				} else {
					panic("Tail round isn't +1. Weird situation")
				}
				// s.node.Dpos.PrintMinersByRound()
				// panic("Tail round isn't +1")
				// node.Dpos.SetRoundNum(tailBlock.GetRoundNum() + 1)
			}
		} else {
			fmt.Println("Continuing the current round for ", timeLeft)
			// fmt.Println("Dpos time left: ", s.node.Dpos.TimeLeftToMine(time.Now().Unix()))
			// s.node.Dpos.SetTimeLeftInRound(float64(timeToSync))
			// s.node.Dpos.SetRoundStartTime(s.node.Syncer.RoundStartTime(tailBlock))
			// s.node.Dpos.SetRoundStartTime(tailBlockTs - int64(timeLeft)) //in seconds
			s.node.Dpos.SetRoundStartTime(s.RoundStartTime(s.node.Dpos.GetRoundNum())) //in seconds
			fmt.Println("ROUND START:- ", s.node.Dpos.GetRoundStartTime(), "RN: ", s.node.Dpos.GetRoundNum())

			if s.node.Dpos.GetRoundNum() != tailRound {
				//THis condition shouldn't hit
				panic("Not in correct round")
				// fmt.Println("Resetting the round num")
				// node.Dpos.SetRoundNum(tailBlock.GetRoundNum())
			}
		}

		fmt.Println(".")
		fmt.Println("TIME HAS BEE SET. ")
		fmt.Println(".")

		if tailRound != s.node.Dpos.GetRoundNum() {
			log.Info().Err(errors.New("Round nums of tail block and DPOS doesn't match"))
			//TODO:- Fix this -> ALready fixed above.
		}

		node.Dpos.Start()
		log.Info().Msgf("Node started and running")
		// log.Info().Msgf("Requesting to register me as candidate")

		// err := s.registerCand()
		// if err != nil {
		// 	return err
		// }

		return nil
	}

	err := node.Dpos.Setup()
	if err != nil {
		return err
	}

	// timeInSlot := s.node.Dpos.TimeLeftToMine(time.Now().Unix()).Seconds()

	// fmt.Println("TIME IN SLOT", timeInSlot)
	// if timeInSlot < 3 {
	// 	time.Sleep(s.node.Dpos.TimeLeftToMine(time.Now().Unix()))
	// }

	fmt.Println("STARTING DPOS AT: ", time.Now().Unix())
	node.Dpos.Start()
	log.Info().Msgf("Node started and running")
	// time.Sleep(200 * time.Millisecond)
	// log.Info().Msgf("Announcing reg-Cand !")

	//move out
	// if s.node.Bc == nil && minerRole == true { //Only register without transaction if it hasn't synced up
	// 	go func() {
	// 		for i := 0; i < 3; i++ { //Announcing candidacy 4 times
	// 			s.AnnounceCandidacy()

	// 			time.Sleep(1 * time.Second)
	// 		}
	// 		s.DoDelegateRegisteration() //Also adding the transaction to establish as a ground truth
	// 		// err = s.DoDelegateRegisteration() //Also adding the transaction- To establish as a ground truth
	// 	}()
	// 	// go func() {

	// 	// 	tx, err := s.DoDelegateRegisteration() //Does the automatic delegate registration if the falg is provided
	// 	// 	if tx == nil || err != nil {
	// 	// 		return
	// 	// 	}

	// 	// 	for i := 0; i < 3; i++ { //Announcing candidacy 4 times
	// 	// 		time.Sleep(1 * time.Second)

	// 	// 		transBytes, err := tx.ToBytes()
	// 	// 		if err != nil {
	// 	// 			return
	// 	// 		}
	// 	// 		msg := net.NewMessage(net.MsgAmountTx, transBytes)
	// 	// 		s.RelayMsgToMiners(msg) //Relaying the same transaction 4 times so that nodes joining late may not miss it
	// 	// 	}
	// 	// }()
	// }
	return nil
}

// //ResyncMainChain will resync the main tail until it has the updated state
// func (s *Services) ResyncMainChain(tailHeight int) {
// 	//Trim chain till tail height in case it is higher

// 	//

// 	tail := s.node.Bc.GetMainTail()
// 	tailTs := tail.GetTimestamp()
// 	curTime := time.Now().UnixNano()
// 	// if (curTime - tailTs) < (2 * s.node.Dpos.GetTickInterval().Nanoseconds()) {
// 	// 	fmt.Println("Running resync chain protocol -- returning in if condition ...")
// 	// 	return
// 	// }

// 	s.RevertDposTill(s.node.Bc.GetMainTail())

// 	// count := 0
// 	for (curTime - tailTs) > (2 * s.node.Dpos.GetTickInterval().Nanoseconds()) {
// 		fromBlockHeight := int(s.node.Bc.GetMainTail().Height() + 1)
// 		msg := net.NewMessage(net.MsgCodeBlockAskByHeight, []byte(strconv.Itoa(fromBlockHeight)))
// 		fmt.Println("Running resync chain protocol 2")
// 		syncer.node.RequestAndResponse(msg, syncer.handleBlockSyncByHeight, net.MsgCodeBlockSyncByHeight)
// 		time.Sleep(3 * time.Second)
// 		fmt.Println("Running resync chain protocol 3")
// 		// fmt.Println("count is: ", count)
// 		// count++
// 		tail = syncer.node.Bc.GetMainTail()
// 		fmt.Println("Running resync chain protocol 4")
// 		tailTs = tail.GetTimestamp()
// 		curTime = time.Now().UnixNano()
// 	}

// 	_ = wasRunning

// 	syncer.node.Services.StartDpos(int(syncer.node.Dpos.GetCommitteeSize()), false)
// 	syncer.isSyncing = false
// }

//StartDpos2 starts the DPOS consensus protocol.
func (s *Services) StartDpos2(comSize int, minerRole bool) error {
	node := s.node
	// nodeID := node.GetNodeID()
	// peerAdd := node.GetPeerAddress()[0] //Only taking first address for the testing purpose

	if s.node.Bc != nil { //That means that this node has synced before
		log.Info().Msgf("starting DPOS after the state catch-up")

		//Getting the latest block
		s.node.Dpos.SetRoundTimeOffset(0)
		tailBlock := s.node.Bc.GetMainTail()
		tailBlockTs := tailBlock.GetTimestamp() / (1e9) //in seconds
		tailRound := tailBlock.GetRoundNum()
		fmt.Println("Tail block height: ", tailBlock.Height(), "tail round: ", tailRound)

		// fmt.Println("Curtime - tailBlock: ", curTime-(tailBlock.GetTimestamp()/(1e9)))
		curTime := time.Now().Unix()           //Assuming ur UNIX time is correct.
		timeSinceTail := curTime - tailBlockTs //in seconds
		if timeSinceTail > 2*int64(s.node.Dpos.GetMinerSlotTime().Seconds()) {
			log.Info().Err(errors.New("Curtime and last block time has big difference. SYNC again"))
			return errors.New("Current-time is way ahead tail. Need to sync again")

		}
		// _, timeLeft := s.node.Syncer.TimeLeftInRound(tailBlock) //return the time left in seconds in the round

		timeLeft := s.TimeLeftInRoundBySlots(tailBlock) //Gives the timeLeft form time.Now()

		// fmt.Println("value: ", -(int64(s.node.Dpos.GetTickInterval().Seconds() / 2)))
		if timeLeft <= int64(s.node.Dpos.GetTickInterval().Seconds()-1) { //To increase tolerance (-2)
			count := 0
			for timeLeft < int64(s.node.Dpos.GetTickInterval().Seconds()-1) {
				fmt.Println("Wait for the next round... ", timeLeft)
				time.Sleep(3 * time.Second) //Sleeping for some time
				tailBlock = s.node.Bc.GetMainTail()
				tailBlockTs = tailBlock.GetTimestamp() / (1e9) //in seconds
				tailRound = tailBlock.GetRoundNum()
				timeLeft = s.TimeLeftInRoundBySlots(tailBlock) //Gives the timeLeft form time.Now()
				count++
				if count > 5 && timeLeft > 2 { //In rare condition when only one miner is mining and second one tries to join but gets first slot to mine
					// fmt.Println("Breaking condition") //In that case loop shall break because no block will produce with timeLeft greater than 4
					break
				}
			}

			// if timeLeft > 0 {
			// 	time.Sleep(2 * time.Second)
			// 	time.Sleep(s.node.Dpos.GetTickInterval() / 2) //For round tolerance time
			// } else if timeLeft > -(int64(s.node.Dpos.GetTickInterval().Seconds() / 2)) {
			// 	time.Sleep(s.node.Dpos.GetTickInterval() / 2) //For round tolerance time
			// }

			// fmt.Println("SHould start next round")
			// node.Dpos.AdvanceRound(tailBlock.GetTimestamp() + int64(timeLeft)*1e9)
			// s.node.Dpos.SetRoundStartTime(s.RoundStartTime(s.node.Dpos.GetRoundNum())) //in seconds

			// // s.node.Dpos.AdvanceRoundInSec(float64(timeSinceTail) - timeLeft)
			// fmt.Println("ROUND START:- ", s.node.Dpos.GetRoundStartTime())

			// if s.node.Dpos.GetRoundNum() != tailRound+1 {
			// 	fmt.Println("DPos round- Expected: ", s.node.Dpos.GetRoundNum(), tailRound+1)
			// 	negComTime := -int64(s.node.Dpos.GetCommitteeTime().Seconds())
			// 	fmt.Println("negComTime", negComTime)
			// 	if timeLeft < negComTime {
			// 		return errors.New("Need to sync again. Sync time was bigger than round")
			// 	} else {
			// 		panic("Tail round isn't +1. Weird situation")
			// 	}
			// }

		}
		// else {
		fmt.Println("Continuing the round for ", timeLeft)
		// fmt.Println("Dpos time left: ", s.node.Dpos.TimeLeftToMine(time.Now().Unix()))
		// s.node.Dpos.SetTimeLeftInRound(float64(timeToSync))
		// s.node.Dpos.SetRoundStartTime(s.node.Syncer.RoundStartTime(tailBlock))
		// s.node.Dpos.SetRoundStartTime(tailBlockTs - int64(timeLeft)) //in seconds
		s.node.Dpos.SetRoundStartTime(s.RoundStartTime(s.node.Dpos.GetRoundNum())) //in seconds
		fmt.Println("ROUND START:- ", s.node.Dpos.GetRoundStartTime(), "RN: ", s.node.Dpos.GetRoundNum())

		if s.node.Dpos.GetRoundNum() != tailRound {
			//THis condition shouldn't hit
			panic("Not in correct round")
			fmt.Println("Resetting the round num")
			node.Dpos.SetRoundNum(tailBlock.GetRoundNum())
		}
		// }

		fmt.Println(".")
		fmt.Println("TIME HAS BEE SET.2 ")
		fmt.Println(".")

		if tailRound != s.node.Dpos.GetRoundNum() {
			log.Info().Err(errors.New("Round nums of tail block and DPOS doesn't match"))
		}

		node.Dpos.Start()

		log.Info().Msgf("Node started and running")
		// log.Info().Msgf("Requesting to register me as candidate")

		return nil
	}

	err := node.Dpos.Setup()
	if err != nil {
		return err
	}

	// timeInSlot := s.node.Dpos.TimeLeftToMine(time.Now().Unix()).Seconds()

	// fmt.Println("TIME IN SLOT", timeInSlot)
	// if timeInSlot < 3 {
	// 	time.Sleep(s.node.Dpos.TimeLeftToMine(time.Now().Unix()))
	// }

	fmt.Println("STARTING DPOS AT: ", time.Now().Unix())
	node.Dpos.Start()
	log.Info().Msgf("Node started and running")
	time.Sleep(200 * time.Millisecond)
	log.Info().Msgf("Announcing reg-Cand !")

	if s.node.Bc == nil && minerRole == true { //Only register without transaction if it hasn't synced up
		go func() {
			for i := 0; i < 5; i++ { //Announcing candidacy 3 times
				s.AnnounceCandidacy()
				time.Sleep(1 * time.Second)
			}
			// err = s.DoDelegateRegisteration() //Also adding the transaction- To establish as a ground truth
		}()
	}
	return nil
}

//TimeLeftInRoundBySlots returns the time left in the round from time.Now() using the sum of all the slots passed
func (s *Services) TimeLeftInRoundBySlots(b *core.Block) int64 {
	bRn := b.GetRoundNum()
	gts := s.node.Bc.Genesis().GetTimestamp() / (1e9) //Genesis timestamp in seconds
	chainStartTime := gts - int64(s.node.Dpos.GetTickInterval().Seconds())
	fmt.Println("Chain start time: ", chainStartTime)
	curTime := time.Now().Unix()
	slotsPassed := s.node.Dpos.SlotsPassedByRound[bRn]

	//Round end time in which this block is mined
	endTime := chainStartTime + slotsPassed*int64(s.node.Dpos.GetMinerSlotTime().Seconds())
	return endTime - curTime
}

//RoundStartTime returns the round start time
func (s *Services) RoundStartTime(round int64) int64 {
	gts := s.node.Bc.Genesis().GetTimestamp() / (1e9) //Genesis timestamp in seconds
	chainStartTime := gts - int64(s.node.Dpos.GetTickInterval().Seconds())
	slotsPassed := s.node.Dpos.SlotsPassedByRound[round-1]
	roundStart := chainStartTime + slotsPassed*int64(s.node.Dpos.GetMinerSlotTime().Seconds())

	return roundStart
}

//RoundNum used to round numbers
func RoundNum(x, unit float64) float64 {
	return math.Round(x/unit) * unit
}

//Only called when a node is catching up to the DPOS state
// func (s *Services) registerCand() error {
// 	node := s.node
// 	nodeID := node.GetNodeID()
// myCandidate, err := node.Dpos.State.RegisterCandidate(nodeID.Address()) //Treating address as ID for testing
// 	if err != nil {
// 		return err
// 	}
// 	//Registering myself to the dposstate
// 	s.node.Dpos.State.PutCandidate(nodeID.Address(), myCandidate)
// 	if err != nil {
// 		return err
// 	}
// 	s.AnnounceCandidacy()

// 	return nil
// }

//timeLeftInRound returns the time left(in seconds) in the round based on the blocks on the blockchain
//TimeLeftInRound returns the time left in the round
func (s *Services) timeLeftInRound(block *core.Block) (bool, float64) {
	sameMiner := false
	bTs := block.GetTimestamp()
	bRound := block.GetRoundNum()
	bPk := block.MinerPublicKey()

	minerInd, exists := s.node.Dpos.GetMinerIndex(bRound, bPk.String())
	if exists == false {
		log.Info().Err(errors.New("Miner wasn't allowed to mine in this round"))
	}

	minerSlotsPassed := 1 //Counting this block as the first produced by this miner
	blocksPerMiner := s.node.Dpos.BlocksPerMiner()
	// fmt.Println("BlocksPerMiner: ", blocksPerMiner)
	// totalBlocksTime :=
	minerSlotTime := s.node.Dpos.GetMinerSlotTime()
	blockInterval := s.node.Dpos.GetTickInterval().Nanoseconds()

	for i := 0; i < blocksPerMiner; i++ { //Looping over blocksPerMiner+1 just to ensure that a miner hasn't produced block more than it was allowed
		phash := block.ParentHash()
		pBlock, err := s.node.Bc.GetBlockByHash(phash)
		if err != nil {
			log.Info().Err(err)
			break
		}
		pPk := pBlock.MinerPublicKey()
		pRound := block.GetRoundNum()
		pTs := pBlock.GetTimestamp()
		// fmt.Println("ppk:-", hex.EncodeToString(pPk), "pRound: ", pRound)
		bTimeDiff := bTs - pTs

		slotDiff := int(math.Round(float64(bTimeDiff) / float64(s.node.Dpos.GetTickInterval().Nanoseconds()))) //TODO:- Check if round is the right func to use
		fmt.Println("b time diff: ", bTimeDiff, "slot diff: ", slotDiff)
		fmt.Println("bRound: ", bRound, "pRound:", pRound)
		if pPk.String() != bPk.String() || pRound != bRound {
			//This is the only block produced by the miner
			if pRound != bRound {
				_, pRoundTimeLeft := s.timeLeftInRound(pBlock)
				slotDiff = int(math.Round((float64(bTimeDiff) - pRoundTimeLeft*1e9) / float64(s.node.Dpos.GetTickInterval().Nanoseconds())))
			}
			if pRound == bRound && pPk.String() != bPk.String() {
				slotDiff = int(math.Round((float64(bTimeDiff) - s.node.Dpos.RoundToleranceTime()*1e9) / float64(s.node.Dpos.GetTickInterval().Nanoseconds())))
			}
			fmt.Println("renewed:- ", slotDiff)
			minerSlotsPassed += slotDiff - 1 //TODO:- check this addition
			break
		}
		if minerSlotsPassed > blocksPerMiner {
			log.Info().Err(errors.New("Miner prodecued more blocks than allowed"))
			break
		}

		// bTimeDiff := math.Round(float64(bTs-pTs) / float64(1e9))

		fmt.Println("block Time Diff: ", bTimeDiff)
		//Handles the case if a miner produce block then doesn't produce block and finally produce one
		if bTimeDiff > blockInterval+1*1e9 { //giving 1 secconds of buffer
			i += slotDiff - 1 //TODO:Incorporate missing round
			// bTimeDiff /
			fmt.Println("Time diff was big")
			minerSlotsPassed += slotDiff - 1
		}
		if bTimeDiff < blockInterval-1*1e9 { //giving 1 secconds of buffer
			log.Info().Err(errors.New("miner is producing blocks before time/spammming"))
		}
		minerSlotsPassed++

		bPk = pPk
		bRound = pRound
		bTs = pTs
		block = pBlock

		// if bTimeDiff == s.node.Dpos.GetTickInterval().Seconds() {
		// 	fmt.Println("Time difference matches exactly")
		// }
	}
	if minerSlotsPassed > blocksPerMiner {
		minerSlotsPassed = blocksPerMiner
	}

	fmt.Println("Slots passed by the miner:-", minerSlotsPassed)
	minersToCome := s.node.Dpos.TotalMinersInRound(bRound) - minerInd - 1
	fmt.Println("Miner to come:- ", minersToCome)
	nextMinersTime := minerSlotTime.Seconds() * float64(minersToCome)
	if minerSlotsPassed >= blocksPerMiner {
		// if minersToCome == 0 { //if this is the last block in the round then there is the buffer time at the end of the round
		// 	return sameMiner, s.node.Dpos.RoundToleranceTime()
		// }
		return sameMiner, nextMinersTime + s.node.Dpos.RoundToleranceTime()
	}

	sameMiner = true
	curMinerTime := (float64(blocksPerMiner-minerSlotsPassed))*s.node.Dpos.GetTickInterval().Seconds() + s.node.Dpos.RoundToleranceTime()
	timeTillRoundEnd := nextMinersTime + curMinerTime
	return sameMiner, timeTillRoundEnd
}

//AnnounceCandidacy broadcasts the candidate registration announcement
func (s *Services) AnnounceCandidacy() error {
	node := s.node
	// nodeAdd := node.GetNodeID().Address()
	nodeAdd := node.PublicKey().String()

	//Creates new candidate
	// myCand := dposstate.NewCandidate(nodeAdd, nodeAdd, 0, time.Now()) //TODO:- fix time according to zone use NTP protocol
	myCand, err := node.Dpos.State.RegisterCand(nodeAdd)
	// s.node.Wm.GetWalletByAddress(nodeAdd)
	if err != nil {
		// return err
		myCand, _ = s.node.Dpos.State.GetCandidate(nodeAdd)
	}

	err = s.node.Wm.SetCandidacy(nodeAdd)
	if err != nil {
		fmt.Println(err.Error())
	}

	// nodeID := node.GetNodeID()
	// myCand, err := s.node.Dpos.State.GetCandidate(nodeID.Address())
	// if err != nil {
	// 	return err
	// }
	candBytes, err := myCand.ToBytes()
	if err != nil {
		return err
	}
	msg := net.NewMessage(net.MsgCandidateRegisteration, candBytes)
	node.Broadcast(msg)
	return nil
}

//RegisterCandidate registers the candidate
func (s *Services) RegisterCandidate(msg net.Message) {
	if s.node.Bc != nil { //Check if the blockchain has already started
		log.Info().Err(errors.New("Cannot register without Tx"))
		return
	}
	// log.Info().Msgf("Registering candidate: ", msg.From)
	newCand, err := dposstate.FromBytes(msg.Data)
	if err != nil {
		log.Info().Msgf(err.Error())
	}
	err = s.node.Dpos.State.PutCandidate(newCand.ID, newCand)
	if err != nil {
		// log.Info().Msgf(err.Error())
		return
	}
	s.node.Wm.PutWalletByAddress(newCand.Address, wallet.NewWallet(newCand.Address))
	cWallet, err := s.node.Wm.GetWalletByAddress(newCand.Address)
	if err != nil {
		fmt.Errorf("Cand wallet doesn't exists")
		return
	}
	cWallet.SetCandidate(true) //Setting the isCandidate field of wallet as true

	log.Info().Msgf("A candidate has been successfully added to the state!!")
}

// // RevertDposState reverts the DPOS state( stale function)
// func (s *Services) RevertDposState(block *core.Block) {
// 	if block.IsGenesis() == true {
// 		return
// 	}
// 	bRound := block.GetRoundNum()

// 	parentHash := block.ParentHash()
// 	pBlock, err := s.node.Bc.GetBlockByHash(parentHash)
// 	if err != nil {
// 		log.Info().Err(err)
// 		log.Info().Err(errors.New("No parent to check revert state to"))
// 	}
// 	pbRound := pBlock.GetRoundNum()

// 	if pbRound != bRound {
// 		s.node.Dpos.SetRoundNum(pbRound) //Decreasing the round number-> putting it equal to the parent with diff round
// 		s.node.Dpos.DeleteMinerRoundEntry(bRound)
// 		s.node.Dpos.InitializeComMembers(s.node.Dpos.GetMinersInRound(pbRound)) //Committee should be emptied. It will be filled up with the previous round committee
// 	} else {
// 		if bRound != s.node.Dpos.GetRoundNum() { //Rizwan review this. This ensures that DPOS is at the
// 			fmt.Println("WARN--- Bround-dposROund", bRound, s.node.Dpos.GetRoundNum())
// 			s.node.Dpos.SetRoundNum(pbRound)
// 			s.node.Dpos.InitializeComMembers(s.node.Dpos.GetMinersInRound(pbRound))
// 		}
// 	}
// }

//RevertDposState2 reverts the state of the DPOS
func (s *Services) RevertDposState2(block *core.Block) {
	if block.IsGenesis() == true {
		return
	}
	bRound := block.GetRoundNum()

	parentHash := block.ParentHash()
	pBlock, err := s.node.Bc.GetBlockByHash(parentHash)
	if err != nil {
		log.Info().Err(err)
		log.Info().Err(errors.New("No parent to check revert state to"))
	}
	pbRound := pBlock.GetRoundNum()

	//Ensure that dpos round is the same as this block round
	if s.node.Dpos.GetRoundNum() > bRound { //Handle the case where current block is in n-1 round and new round hasn't produced a block as yet

		nextRoundMiners := s.node.Dpos.GetMinersInRound(bRound + 1)

		for i := bRound + 1; i <= s.node.Dpos.GetRoundNum(); i++ {
			s.node.Dpos.DeleteMinerRoundEntry(i) //Just deleting entry to be on safe side
			s.node.Dpos.DeleteSlotsPassedEntry(i)
		}

		s.node.Dpos.SetRoundNum(bRound) //Decreasing the round number-> putting it equal to the parent with diff round
		s.node.Dpos.InitializeComMembers(s.node.Dpos.GetMinersInRound(bRound), nextRoundMiners)

	} else if s.node.Dpos.GetRoundNum() < bRound {
		panic("DPOS round lower then current blockRound. Handle this")
	}

	for i := bRound; i > pbRound; i-- {
		fmt.Println("WARN: Del entry: ", bRound, pbRound, "DPOS round: ", s.node.Dpos.GetRoundNum())
		nextRoundMiners := s.node.Dpos.GetMinersInRound(i)
		s.node.Dpos.SetRoundNum(i - 1) //Decreasing the round number-> putting it equal to the parent with diff round
		s.node.Dpos.DeleteMinerRoundEntry(i)
		s.node.Dpos.DeleteSlotsPassedEntry(i)
		s.node.Dpos.InitializeComMembers(s.node.Dpos.GetMinersInRound(i-1), nextRoundMiners)
	}

}

//RevertDposTill reverts the Dpos state to the given block. Dpos state does include this block
func (s *Services) RevertDposTill(block *core.Block) {
	log.Info().Msgf("Reverting DPOS till: %v %v", block.GetRoundNum(), block.Height())
	fmt.Println("Dpos round number: ", s.node.Dpos.GetRoundNum())

	if block.IsGenesis() == true || block == nil {
		return
	}
	bRound := block.GetRoundNum()

	if s.node.Dpos.GetRoundNum() > bRound { //Handle the case where current block is in n-1 round and new round hasn't produced a block as yet
		s.node.Dpos.SetRoundNum(bRound) //Decreasing the round number-> putting it equal to the parent with diff round
		nextRoundMiners := s.node.Dpos.GetMinersInRound(bRound + 1)
		s.node.Dpos.InitializeComMembers(s.node.Dpos.GetMinersInRound(bRound), nextRoundMiners)

		for i := bRound + 1; i <= s.node.Dpos.GetRoundNum(); i++ {
			s.node.Dpos.DeleteMinerRoundEntry(i) //Just deleting entry to be on safe side
			s.node.Dpos.DeleteSlotsPassedEntry(i)
		}
	} else if s.node.Dpos.GetRoundNum() < bRound {
		panic("DPOS round lower then current blockRound. Block from future??")
	}
}

//UpdateDpos updates the dpos
func (s *Services) UpdateDpos(block *core.Block) { //Take round by round jump

	//Every block shouldn't update the state
	n := s.node
	nrTs := new(int64) //next Round timestamp(start)
	var prevMiner string = ""
	var pbRound int64 = 0
	// bts := block.GetTimestamp()
	// pBlock := new(core.Block)

	if block.IsGenesis() == false { //For the non-genesis block
		parentHash := block.ParentHash()
		pBlock, err := s.node.Bc.GetBlockByHash(parentHash)
		if err != nil {
			log.Info().Err(err)
			log.Info().Err(errors.New("Not syncing in order!!!!!"))
		}
		pbRound = pBlock.GetRoundNum()
		prevMiner = pBlock.MinerPublicKey().String()
	}

	// cbTs := block.GetTimestamp()   //Current block timestamp
	cbRound := block.GetRoundNum() //Current block round
	// fmt.Println("cbround:", cbRound, "pbRound:", pbRound)

	if cbRound > pbRound+2 {
		log.Info().Err(errors.New("Empty rounds or out of sync blocks are being applied"))
	}
	// fmt.Println(" cbRound: ", cbRound, " pbRound: ", pbRound)

	//Since it is updating so it might not receive the register candidate announcement thus for every vote it registers the candidates in the store
	minerAdd := block.MinerPublicKey().String()
	// log.Info().Msgf("DPOS services -- minerAdd %v, prevMiner %v", minerAdd, prevMiner)

	if cbRound == 1 && minerAdd != prevMiner { //THis is corrosponding to the first round where genesis block along with other blocks will be created
		if block.IsGenesis() == true {
			// DposUpdate.genesisTs = block.GetTimestamp()
			n.Dpos.SetCommitteeSize(0) //Should start with zero
		}
		// minerWallet := wallet.NewWallet(minerAdd)
		// n.Wm.PutWalletByAddress(minerAdd, minerWallet)
		// n.Wm.SetCandidacy(minerAdd)

		n.Dpos.State.AddGenesisRoundCand(minerAdd, minerAdd) //For now using add and ID as the same TODO: change second argument to the ID

		n.Dpos.SetRoundNum(0)                                                                        //Advance round will increment the number since it is called in the start of the round
		nextTs = n.Bc.Genesis().GetTimestamp() - s.node.Dpos.GetTickInterval().Nanoseconds() + 2*1e9 //DPOS start time with 2 second of the gap
		// fmt.Println("GENESIS ts: ", nextTs)
		// if DposUpdate.lastMiner == "" || DposUpdate.lastMiner != minerAdd {

		// n.Dpos.SetCommitteeSize(n.Dpos.GetCommitteeSize() + 1) //Required initially for setting the size before advancing the round
		fmt.Println("Advancing GENESIS ROUND")
		n.Dpos.ResetRoundTimeOffset() //Time offset should be set to zero coz if might have been called before
		*nrTs = n.Dpos.AdvanceRound(nextTs)
		if *nrTs == 0 {
			log.Info().Err(errors.New("Invalid round. Committee size is 0"))
		}
		pbRound++
		nextTs = *nrTs
		return
	}

	//Check if DPOS roundnumber is same as the block parent, if not then reset it
	if pbRound != 0 && s.node.Dpos.GetRoundNum() != pbRound {
		parentHash := block.ParentHash()
		pBlock, err := s.node.Bc.GetBlockByHash(parentHash)
		if err != nil {
			log.Error().Msgf(err.Error())
			return
		}
		s.RevertDposTill(pBlock)
	}

	if pbRound == cbRound { //Assuming DPOS is in previous block state
		return
	}

	//nextTs is basically when does the parent round ends

	// if nextTs < bts-s.node.Dpos.GetCommitteeTime().Nanoseconds() { //This function can be invoked any time so nextTs might not be set for the forks occuring during the rounds
	// 	_, timeLeft := s.node.Syncer.TimeLeftInRound(pBlock)
	// 	nextTs = pBlock.GetTimestamp() + (int64(timeLeft+1) * 1e9)
	// }

	for ; cbRound > pbRound+1; pbRound++ { //Should only advance round if block roundNum is greater than the previous block round numround
		//There are no blocks produced in this round
		// fmt.Println("No blocks were prodeced in: ", pbRound+1)
		*nrTs = n.Dpos.AdvanceRound(nextTs)
		// fmt.Println("next round ts: ", (*nrTs / (int64(time.Second) / int64(time.Nanosecond))))
		nextTs = *nrTs
	}

	*nrTs = n.Dpos.AdvanceRound(nextTs)
	if *nrTs == 0 {
		log.Info().Err(errors.New("Invalid round. Committee size is 0"))
	}
	// fmt.Println("next round ts: ", (*nrTs / (int64(time.Second) / int64(time.Nanosecond))))
	pbRound = cbRound
	nextTs = *nrTs
}

//Note:- These functions are not used currently. But may be used later for optimization in catch-up state
//CatchUpState updates to the current dpos state of the network
// func (s *Services) CatchUpState() error {

// 	n := s.node
// 	// n.Dpos.State.ResetState() //SO that changes made from block syncing is nullified

// 	nDpos := dpos.New(0, n.Wm.MyWallet.GetAddress(), 0, true, n.Wm, s.node.Bc) //Used to compute the committee size in one particular(last) round without casting any effect on running dpos
// 	//If node's dpos is used then the votes effect may be altered by ongoing voting

// 	fmt.Println("length of bc: ", n.Bc.Length())
// 	tail := n.Bc.GetMainTail()

// 	// fmt.Println(tails[0].GetRoundNum()

// 	fmt.Println("Tail round number is: ", tail.GetRoundNum())
// 	cRound := tail.GetRoundNum() //CurrentRoundNumber form blocks
// 	lRound := cRound - 1
// 	pHash := tail.ParentHash()

// 		pBlock, err := n.Bc.GetBlockByHash(pHash)
// 		if err != nil {
// 			return err
// 		}

// 		if pBlock.IsGenesis() || pBlock.GetRoundNum() < lRound {
// 			fmt.Println("Break conditionn in default1")
// 			break
// 		}

// 		if pBlock.GetRoundNum() == lRound {
// 			//Apply changes to the dpos state to compute the committee number of current round

// 			// nDpos.ApplyVotes(pBlock)
// 		}

// 		pHash = pBlock.ParentHash()
// 	}

// 	cComSize := nDpos.ComputeNextCommitteeSize(n.Dpos.State.SortByVotes(s.node.Bc))
// 	nDpos.SetCommitteeSize(cComSize)
// 	n.Dpos.SetCommitteeSize(cComSize) //Setting the nodes comSize to again compute the time unitl next round

// 	comDur := nDpos.GetComDuration()
// 	fmt.Println("Committe duration is : ", comDur)
// 	s.PrepareForNextRound(comDur, cRound)

// 	return nil
// }

//PrepareForNextRound prepares the node for the comming round of the dpos
func (s *Services) PrepareForNextRound(deadline time.Duration, round int64) error {
	fmt.Println("Preparing for next round: ", deadline)
	n := s.node
	n.Dpos.State.ResetState()

	var err error
	tail := n.Bc.GetMainTail()
	tailTs := tail.GetTimestamp() //Timestamp in seconds
	cRound := tail.GetRoundNum()

	if round != cRound { //If tail block is of different round than the last block from sync
		err = errors.New("Failed to prepare before the next round starts")
		return err
	}
	pHash := tail.ParentHash()

Loop:
	for timeout := time.After(deadline); ; {
		fmt.Println("Starting loop")
		select {
		case <-timeout:
			//if this happens it should start over again
			err = errors.New("Failed to prepare before the next round starts")
			break Loop

		default:
			for {
				fmt.Println("Check")
				pBlock, err := n.Bc.GetBlockByHash(pHash)
				if err != nil {
					return err
				}
				if pBlock.IsGenesis() || pBlock.GetRoundNum() < cRound {
					fmt.Println("Break conditionn in default2")
					break
				}
				if pBlock.GetRoundNum() == cRound {
					//Apply changes to the dpos state to compute the committee number of current round
					// n.Dpos.ApplyVotes(pBlock)
				}
				pHash = pBlock.ParentHash()
			}
			break Loop
		}

	}
	fmt.Println("Loop done")

	timeLeft := n.Dpos.GetComDuration() //timeLeft in next round
	if timeLeft < 0 {
		return errors.New("Failed to prepare before the next round starts")
	}

	n.Dpos.TransitionRound()

	//In case there is round/s with no block then round number will not be the immediate next one
	timeGap := time.Now().Add(n.TimeOffset).UnixNano() - tailTs
	roundOffset := timeGap / int64(n.Dpos.GetMinerSlotTime())
	fmt.Println("Miners slot", int64(n.Dpos.GetMinerSlotTime()), "time now: ", time.Now().Add(n.TimeOffset).UnixNano(), "tailts: ", tailTs)
	fmt.Println("Round offset is: ", roundOffset, "current roundNum: ", n.Dpos.GetRoundNum(), " timeGap: ", timeGap, int64(n.Dpos.GetMinerSlotTime()))
	n.Dpos.SetRoundNum(cRound + roundOffset)

	fmt.Println("time left: ", timeLeft)
	time.Sleep(timeLeft)

	return err
}
