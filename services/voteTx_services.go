package services

import (
	"context"
	"errors"

	"github.com/guyu96/go-timbre/core"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/net"
	"github.com/guyu96/go-timbre/roles"
)

var (
	voteToSend = 100
)

// //DoVoteTrans creates a vote transaction in DPOS
// func (s *Services) DoVoteTrans(ctx context.Context, port string, miner *roles.Miner) error {
// 	log.Info().Msgf("Sending transaction to other peer if candidate")
// 	log.Info().Msgf("Check:- if peer has already registered as a candidate")

// 	node := s.node
// 	nodeID := node.GetNodeID()
// 	peerAdd := node.GetPeerAddress()[0] //Only taking first address for the testing purpose
// 	peerAdd = "127.0.0.1:" + port

// 	log.Info().Msgf("Recepient of vote tx: ", peerAdd)
// 	_, err := node.Dpos.State.GetCandidate(peerAdd) //To check if the address is already a candidate
// 	if err != nil {
// 		return err
// 	}
// 	votes := map[string]int64{peerAdd: int64(voteToSend)}
// 	voteToSend++
// 	myVote, err := core.NewVote(nodeID.Address(), votes, node.PrivateKey(), []string{})
// 	if err != nil {
// 		return err
// 	}

// 	protoVote := myVote.GetProtoVote()
// 	// log.Info().Msgf("proto type!: ", reflect.TypeOf(protoVote))

// 	voteBytes, err := myVote.ToBytes()

// 	if err != nil {
// 		return err
// 	}
// 	// myNodeAdd := node.GetNodeID().Address()
// 	msg := net.NewMessage(net.MsgVoteTx, voteBytes)
// 	// minerAdd := s.node.Dpos.GetMinersAddresses()

// 	var minerAdd []string

// 	if miner != nil {
// 		// miner.votes.Push(protoVote)
// 		miner.PushVote(protoVote)
// 		log.Info().Msgf("My vote added in miner cache")
// 	}
// 	if node.Dpos.HasStarted == true { //if it is running dpos then it already know the addresses
// 		minerAdd = s.node.Dpos.GetMinersAddresses()
// 		err := s.RelayMsgToAddresses(minerAdd, msg)
// 		return err
// 	}

// 	s.AskMinersAdd()
// 	select {
// 	case miners := <-s.minersInfo:
// 		currentMiners, err := s.node.Dpos.MinersFromBytes(miners)
// 		if err != nil {
// 			return err
// 		}
// 		minerAdd = currentMiners.GetMinerAddresses()
// 	case <-ctx.Done():
// 		return errors.New("Unable to fetch the miner addresses within the time limit")
// 	}
// 	err = s.RelayMsgToAddresses(minerAdd, msg)
// 	if err != nil {
// 		return err
// 	}
// 	return nil
// }

//DoVote votes the candidate
func (s *Services) DoVote(ctx context.Context, pk string, miner *roles.Miner) error {
	node := s.node
	// nodeID := node.GetNodeID()
	// peerAdd := node.GetPeerAddress()[0] //Only taking first address for the testing purpose
	// peerAdd = "127.0.0.1:" + port
	peerAdd := pk

	myVote := "+20|" + peerAdd //TODO:ChangeToPK

	log.Info().Msgf("Recepient of vote tx: ", peerAdd)
	_, err := node.Dpos.State.GetCandidate(peerAdd) //To check if the address is already a candidate
	if err != nil {
		// return err
		log.Info().Msgf(err.Error())
	}

	var votes []string
	votes = append(votes, myVote)
	// samVotes := map[string]int64{peerAdd: int64(voteToSend)}

	voteTrans, _ := core.NewVote(node.PublicKey().String(), node.PrivateKey(), votes)
	// if err != nil {
	// 	return err
	// }

	protoVote := voteTrans.GetProtoVote()
	// // log.Info().Msgf("proto type!: ", reflect.TypeOf(protoVote))
	voteBytes, err := voteTrans.ToBytes()
	if err != nil {
		return err
	}
	// myNodeAdd := node.GetNodeID().Address()
	msg := net.NewMessage(net.MsgVoteTx, voteBytes)
	var minerAdd []string

	if miner != nil {
		// miner.votes.Push(protoVote)
		miner.PushVote(protoVote)
		log.Info().Msgf("My vote added in miner cache")
	}
	if node.Dpos.HasStarted == true { //if it is running dpos then it already know the addresses
		minerAdd = s.node.Dpos.GetMinersAddresses()
		err := s.RelayMsgToAddresses(minerAdd, msg)
		return err
	}

	s.AskMinersAdd()
	select {
	case miners := <-s.minersInfo:
		currentMiners, err := s.node.Dpos.MinersFromBytes(miners)
		if err != nil {
			return err
		}
		minerAdd = currentMiners.GetMinerAddresses()
	case <-ctx.Done():
		return errors.New("Unable to fetch the miner addresses within the time limit")
	}
	err = s.RelayMsgToAddresses(minerAdd, msg)
	if err != nil {
		return err
	}
	return nil
}

//DoUnVote unvotes the guy previously voted
func (s *Services) DoUnVote(ctx context.Context, pk string, miner *roles.Miner) error {
	node := s.node
	// nodeID := node.GetNodeID()
	// peerAdd := node.GetPeerAddress()[0] //Only taking first address for the testing purpose
	// peerAdd = "127.0.0.1:" + port       //TODO:ChangeToPK
	peerAdd := pk

	myVote := "-20|" + peerAdd //Fixed 20 percent unvote

	log.Info().Msgf("Recepient of vote tx: ", peerAdd)
	_, err := node.Dpos.State.GetCandidate(peerAdd) //To check if the address is already a candidate
	if err != nil {
		// return err
		log.Info().Msgf(err.Error())
	}

	var votes []string
	votes = append(votes, myVote)
	// samVotes := map[string]int64{peerAdd: int64(voteToSend)}

	voteTrans, _ := core.NewVote(node.PublicKey().String(), node.PrivateKey(), votes)
	// if err != nil {
	// 	return err
	// }

	protoVote := voteTrans.GetProtoVote()
	// // log.Info().Msgf("proto type!: ", reflect.TypeOf(protoVote))
	voteBytes, err := voteTrans.ToBytes()
	if err != nil {
		return err
	}
	// myNodeAdd := node.GetNodeID().Address()
	msg := net.NewMessage(net.MsgVoteTx, voteBytes)
	var minerAdd []string

	if miner != nil {
		// miner.votes.Push(protoVote)
		miner.PushVote(protoVote)
		log.Info().Msgf("My vote added in miner cache")
	}
	if node.Dpos.HasStarted == true { //if it is running dpos then it already know the addresses
		minerAdd = s.node.Dpos.GetMinersAddresses()
		err := s.RelayMsgToAddresses(minerAdd, msg)
		return err
	}

	s.AskMinersAdd()
	select {
	case miners := <-s.minersInfo:
		currentMiners, err := s.node.Dpos.MinersFromBytes(miners)
		if err != nil {
			return err
		}
		minerAdd = currentMiners.GetMinerAddresses()
	case <-ctx.Done():
		return errors.New("Unable to fetch the miner addresses within the time limit")
	}
	err = s.RelayMsgToAddresses(minerAdd, msg)
	if err != nil {
		return err
	}
	return nil
}

//DoPercentVote votes a candidate with the given percentage
func (s *Services) DoPercentVoteDuplicate(ctx context.Context, pk string, percent string) error {
	node := s.node
	// nodeID := node.GetNodeID()
	// peerAdd := node.GetPeerAddress()[0] //Only taking first address for the testing purpose
	// peerAdd = "127.0.0.1:" + port       //TODO:ChangeToPK
	peerAdd := pk
	myVote := "+" + percent + "|" + peerAdd

	log.Info().Msgf("Recepient of vote tx: ", peerAdd)
	_, err := node.Dpos.State.GetCandidate(peerAdd) //To check if the address is already a candidate
	if err != nil {
		// return err
		log.Info().Msgf(err.Error())
	}

	var votes []string
	votes = append(votes, myVote)
	// samVotes := map[string]int64{peerAdd: int64(voteToSend)}

	voteTrans, _ := core.NewVote(node.PublicKey().String(), node.PrivateKey(), votes)
	// if err != nil {
	// 	return err
	// }

	// protoVote := voteTrans.GetProtoVote()
	// // log.Info().Msgf("proto type!: ", reflect.TypeOf(protoVote))
	voteBytes, err := voteTrans.ToBytes()
	if err != nil {
		return err
	}
	// myNodeAdd := node.GetNodeID().Address()
	msg := net.NewMessage(net.MsgVoteTx, voteBytes)
	var minerAdd []string

	// if miner != nil {
	// 	// miner.votes.Push(protoVote)
	// 	miner.PushVote(protoVote)
	// 	log.Info().Msgf("My vote added in miner cache")
	// }
	if node.Dpos.HasStarted == true { //if it is running dpos then it already know the addresses
		minerAdd = s.node.Dpos.GetMinersAddresses()
		err := s.RelayMsgToAddresses(minerAdd, msg)
		return err
	}

	s.AskMinersAdd()
	select {
	case miners := <-s.minersInfo:
		currentMiners, err := s.node.Dpos.MinersFromBytes(miners)
		if err != nil {
			return err
		}
		minerAdd = currentMiners.GetMinerAddresses()
	case <-ctx.Done():
		return errors.New("Unable to fetch the miner addresses within the time limit")
	}
	err = s.RelayMsgToAddresses(minerAdd, msg)
	if err != nil {
		return err
	}
	return nil
}

//DoPercentVote votes a candidate with the given percentage
func (s *Services) DoPercentVote(ctx context.Context, pk string, miner *roles.Miner, percent string) error {
	node := s.node
	// nodeID := node.GetNodeID()
	// peerAdd := node.GetPeerAddress()[0] //Only taking first address for the testing purpose
	// peerAdd = "127.0.0.1:" + port       //TODO:ChangeToPK
	peerAdd := pk
	myVote := "+" + percent + "|" + peerAdd

	log.Info().Msgf("Recepient of vote tx: ", peerAdd)
	_, err := node.Dpos.State.GetCandidate(peerAdd) //To check if the address is already a candidate
	if err != nil {
		// return err
		log.Info().Msgf(err.Error())
	}

	var votes []string
	votes = append(votes, myVote)
	// samVotes := map[string]int64{peerAdd: int64(voteToSend)}

	voteTrans, _ := core.NewVote(node.PublicKey().String(), node.PrivateKey(), votes)
	// if err != nil {
	// 	return err
	// }

	protoVote := voteTrans.GetProtoVote()
	// // log.Info().Msgf("proto type!: ", reflect.TypeOf(protoVote))
	voteBytes, err := voteTrans.ToBytes()
	if err != nil {
		return err
	}
	// myNodeAdd := node.GetNodeID().Address()
	msg := net.NewMessage(net.MsgVoteTx, voteBytes)
	var minerAdd []string

	if miner != nil {
		// miner.votes.Push(protoVote)
		miner.PushVote(protoVote)
		log.Info().Msgf("My vote added in miner cache")
	}
	if node.Dpos.HasStarted == true { //if it is running dpos then it already know the addresses
		minerAdd = s.node.Dpos.GetMinersAddresses()
		err := s.RelayMsgToAddresses(minerAdd, msg)
		return err
	}

	s.AskMinersAdd()
	select {
	case miners := <-s.minersInfo:
		currentMiners, err := s.node.Dpos.MinersFromBytes(miners)
		if err != nil {
			return err
		}
		minerAdd = currentMiners.GetMinerAddresses()
	case <-ctx.Done():
		return errors.New("Unable to fetch the miner addresses within the time limit")
	}
	err = s.RelayMsgToAddresses(minerAdd, msg)
	if err != nil {
		return err
	}
	return nil
}

// DoPercentUnVote unvotes by given percentage
func (s *Services) DoPercentUnVote(ctx context.Context, pk string, miner *roles.Miner, percent string) error {
	node := s.node
	// nodeID := node.GetNodeID()
	// peerAdd := node.GetPeerAddress()[0] //Only taking first address for the testing purpose
	// peerAdd = "127.0.0.1:" + port           //TODO:ChangeToPK
	peerAdd := pk

	myVote := "-" + percent + "|" + peerAdd //Fixed 20 percent unvote

	log.Info().Msgf("Recepient of vote tx: %v", peerAdd)
	_, err := node.Dpos.State.GetCandidate(peerAdd) //To check if the address is already a candidate
	if err != nil {
		// return err
		log.Info().Msgf(err.Error())
	}

	var votes []string
	votes = append(votes, myVote)
	// samVotes := map[string]int64{peerAdd: int64(voteToSend)}

	voteTrans, _ := core.NewVote(node.PublicKey().String(), node.PrivateKey(), votes)
	// if err != nil {
	// 	return err
	// }

	protoVote := voteTrans.GetProtoVote()
	// // log.Info().Msgf("proto type!: ", reflect.TypeOf(protoVote))
	voteBytes, err := voteTrans.ToBytes()
	if err != nil {
		return err
	}
	// myNodeAdd := node.GetNodeID().Address()
	msg := net.NewMessage(net.MsgVoteTx, voteBytes)
	var minerAdd []string

	if miner != nil {
		// miner.votes.Push(protoVote)
		miner.PushVote(protoVote)
		log.Info().Msgf("My vote added in miner cache")
	}
	if node.Dpos.HasStarted == true { //if it is running dpos then it already know the addresses
		minerAdd = s.node.Dpos.GetMinersAddresses()
		err := s.RelayMsgToAddresses(minerAdd, msg)
		return err
	}

	s.AskMinersAdd()
	select {
	case miners := <-s.minersInfo:
		currentMiners, err := s.node.Dpos.MinersFromBytes(miners)
		if err != nil {
			return err
		}
		minerAdd = currentMiners.GetMinerAddresses()
	case <-ctx.Done():
		return errors.New("Unable to fetch the miner addresses within the time limit")
	}
	err = s.RelayMsgToAddresses(minerAdd, msg)
	if err != nil {
		return err
	}
	return nil
}
