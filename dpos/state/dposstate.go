package dposstate

import (
	"errors"
	"fmt"
	"sort"
	"time"

	"github.com/guyu96/go-timbre/log"
	pb "github.com/guyu96/go-timbre/protobuf"
	"github.com/guyu96/go-timbre/wallet"
)

const (
	defaultCommitteeSize = 3
)

var (
	voteReq = true
)

//BlockchainAccess to access the hight of the chain
type BlockchainAccess interface {
	Length() int //To get the length of the blockchain
}

//State maintains the states of the candidates to be elected eventually for the next round
type State struct {
	childCommitteeSize uint32
	candidateState     map[string]*Candidate //For Candidate voting in current round to elect committee for next round
	totalVoteStaked    uint64
	// committeeState []*Candidate
}

//NewDposState returns the state object
func NewDposState(vtReq bool) *State {
	// Todo : connect with storage for persistence
	voteReq = vtReq
	return &State{
		candidateState:     make(map[string]*Candidate),
		childCommitteeSize: defaultCommitteeSize,
		totalVoteStaked:    0,
	}
}

//CloneState clone the current state
func (s *State) CloneState() *State {

	copyState := &State{
		candidateState:     make(map[string]*Candidate),
		childCommitteeSize: s.childCommitteeSize,
		totalVoteStaked:    s.totalVoteStaked,
	}

	for k, v := range s.candidateState {
		copyState.candidateState[k] = v
	}

	return copyState

}

//CloneStateToPb clone the current state
func (s *State) CloneStateToPb() *pb.CommitteeState {
	copyState := &pb.CommitteeState{
		ChildCommitteeSize: s.childCommitteeSize,
		TotalVoteStaked:    s.totalVoteStaked,
		CandidatesState:    make([]*pb.Candidate, len(s.candidateState)),
	}
	for _, v := range s.candidateState {
		copyState.CandidatesState = append(copyState.CandidatesState, v.ToProto())
	}
	return copyState
}

//ResetState resets the votepower of candidates in map to zero
func (s *State) ResetState() {

	if voteReq == false { //Vote requirement is set to 100 so that atmost 10 nodes can be miners without voting
		for key, candidate := range s.candidateState {
			candidate.VotePower = 100
			s.candidateState[key] = candidate
		}
		s.totalVoteStaked = uint64(100 * len(s.candidateState))
		return
	}
	for key, candidate := range s.candidateState {
		candidate.VotePower = 0
		s.candidateState[key] = candidate
	}
	s.totalVoteStaked = 0

	// fmt.Println("Dpos child states has been resetted")
}

//InitializeStateWithMembers put the members in the state
func (s *State) InitializeStateWithMembers(candidates []*Candidate) {
	//Emptying the state map
	s.candidateState = make(map[string]*Candidate)

	for _, cand := range candidates {
		s.candidateState[cand.Address] = cand
	}
	s.totalVoteStaked = 0

}

//TotalCandidates returns the length of the candidate state
func (s *State) TotalCandidates() int {
	return len(s.candidateState)
}

//CheckCandExistance checks if candidate exists in the candidate state
func (s *State) CheckCandExistance(id string) bool {
	if _, ok := s.candidateState[id]; ok {
		return true
	}
	return false
}

//GetTotalVoteStaked returns the total vote power being casted in a round
func (s *State) GetTotalVoteStaked() uint64 {
	return s.totalVoteStaked
}

//GetChildCommitteeSize returns the ChildCommitteeSize
func (s *State) GetChildCommitteeSize() uint32 {
	return s.childCommitteeSize
}

//SetChildCommitteeSize sets the committee size
func (s *State) SetChildCommitteeSize(size uint32) {
	s.childCommitteeSize = size
}

// GetCandidates returns candidate list from candidate state.
func (s *State) GetCandidates() []*Candidate {

	candidates := make([]*Candidate, 0, len(s.candidateState))
	for _, value := range s.candidateState {
		candidates = append(candidates, value)
	}
	return candidates
}

// PutCandidate add candidate to candidate array
func (s *State) PutCandidate(id string, candidate *Candidate) error {
	if _, ok := s.candidateState[id]; ok {
		return errors.New("Candidate with id already exists")
	}
	s.candidateState[id] = candidate
	return nil
}

//GetCandidate returns the candidate by id
func (s *State) GetCandidate(id string) (*Candidate, error) {
	if _, ok := s.candidateState[id]; ok {
		return s.candidateState[id], nil
	}
	return nil, errors.New("Candidate is not in store")
}

// AddVotePowerToCandidate adds vote to the candidate
func (s *State) AddVotePowerToCandidate(id string, amount int64) error {

	candidate, err := s.GetCandidate(id)
	if err != nil {
		return err
	}
	candidate.VotePower += amount
	s.totalVoteStaked += uint64(amount)
	log.Info().Msgf(id, "Gained vote power by: %d", amount)
	return nil
}

//SubVotePowerToCandidate subtracts the vote power from candidates
func (s *State) SubVotePowerToCandidate(id string, amount int64) error {

	candidate, err := s.GetCandidate(id)
	if err != nil {
		return err
	}
	candidate.VotePower -= amount
	return nil
}

// SortByVotes Sort candidates for committee for votes
func (s *State) SortByVotes(bc BlockchainAccess) []*Candidate {

	candidates := s.GetCandidates()

	if bc != nil {
		bcH := bc.Length() //Returns the height of the blockchain
		// fmt.Println("BC IS not NILLLL: ", bcH)

		for i, cand := range candidates { //Removing the banned candidates from the sorting list
			if cand.bannedTill >= int64(bcH) {
				candidates = append(candidates[0:i], candidates[i+1:]...)
				fmt.Println("BANNED: ", cand)
			}
		}
	} else {
		// fmt.Println("BC IS NILLLL")
	}

	sort.SliceStable(candidates, func(i, j int) bool {
		return candidates[i].Address < (candidates[j].Address)
	})

	sort.SliceStable(candidates, func(i, j int) bool {
		return candidates[i].VotePower > (candidates[j].VotePower)
	})

	return append([]*Candidate(nil), candidates...)
}

//DelCandidate deletes the candidate id
func (s *State) DelCandidate(id string) error {
	if _, ok := s.candidateState[id]; ok {
		delete(s.candidateState, id)
		return nil
	}
	return errors.New("No entry against this id")
}

//RegisterCand registers the candidate if wallet balance is positive
func (s *State) RegisterCand(id string) (*Candidate, error) {
	//Set of checks to ensure eligibility of the node to become candidate

	// if nodeWallet.GetBalance() > 0 {
	newCandidate := NewCandidate(id, id, 0, time.Now().Unix())

	if _, alreadyPresent := s.candidateState[id]; alreadyPresent {
		return nil, errors.New("Entry already in record. Can't register again")
	}
	s.candidateState[id] = newCandidate
	return newCandidate, nil
	// }

	// return nil, errors.New("Not eligible to become candidate")
}

//QuitCandidate remove candidate from the state
func (s *State) QuitCandidate(id string) error {
	if _, present := s.candidateState[id]; !present {
		return errors.New("Entry not in record. Can't unregister")
	}
	delete(s.candidateState, id)
	return nil
}

// //CheckAndApplyVotes alter the candidate based on the vote casted
// func (s *State) CheckAndApplyVotes(block *core.Block) error {
// 	isValid := block.IsValid()
// 	if isValid != true {
// 		// fmt.Println("the block is not valid")
// 		return errors.New("Block is not valid")
// 	}
// 	votes := block.GetVotes()
// 	for _, vote := range votes {
// 		for key, value := range vote.VotedCand {
// 			if value > 0 {
// 				log.Info().Msgf("Adding vote power and updating state")
// 				err := s.AddVotePowerToCandidate(key, value)
// 				if err != nil {
// 					return err
// 				}
// 			} else {
// 				log.Info().Msgf("subtracting vote power and updating state")
// 				err := s.SubVotePowerToCandidate(key, value)
// 				if err != nil {
// 					return err
// 				}
// 			}
// 		}
// 	}
// 	return nil
// }

//Outdated code
//PutCandidatesFromVote put the candidate into the state by seeing the votes-->Used for catching up to the updated state
// func (s *State) PutCandidatesFromVote(block *core.Block) error {

// 	isValid := block.IsValid()
// 	if isValid != true {
// 		return errors.New("Block is not valid")
// 	}
// 	votes := block.GetVotes()
// 	// d.BVotes = []*core.Vote{}
// 	for _, vote := range votes {
// 		for key, value := range vote.VotedCand {
// 			if value > 0 {
// 				// log.Info().Msgf("Adding vote power and updating state")
// 				//TODO:- Pass ID-- And time offset too
// 				newCandidate := NewCandidate(key, key, 0, time.Now()) //ID and address is essentially the same so same key at two places for now
// 				s.PutCandidate(key, newCandidate)
// 			}
// 		}
// 	}

// 	return nil
// }

//AddGenesisRoundCand adds the cand in the first round
func (s *State) AddGenesisRoundCand(address string, ID string) {
	newCandidate := NewCandidate(address, address, 0, time.Now().Unix())
	s.PutCandidate(address, newCandidate)
}

//UpdateVotePower updates the votepower in the state
func (s *State) UpdateVotePower(wallets wallet.WalletAccess) {
	for ID, cand := range s.candidateState {
		newVP, bannedHeight, err := wallets.GetCandVotePower(ID) //Gives the votepower VP
		if err != nil {
			continue
		}

		oldVP := cand.VotePower
		//TODO:= Check if this subtraction creates any issue
		var DeltaVP int64 = int64(newVP) - int64(oldVP)
		cand.SetVotePower(int64(newVP))
		cand.SetBannedHeight(bannedHeight)
		s.totalVoteStaked += uint64(DeltaVP)
	}
}

//PrintCandidates print candidates in the state
func (s *State) PrintCandidates() {
	for _, cand := range s.candidateState {
		fmt.Println(cand.Address, " banned till: ", cand.bannedTill)
	}
}

//CheckCandByAdd checks if a candidate with address exists in the dpos state
func (s *State) CheckCandByAdd(add string) bool {
	if _, ok := s.candidateState[add]; ok {
		return true
	}
	return false
}
