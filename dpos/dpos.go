package dpos

// package dpos
import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"math"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/golang/protobuf/proto"
	dposstate "github.com/guyu96/go-timbre/dpos/state"
	"github.com/guyu96/go-timbre/log"
	pb "github.com/guyu96/go-timbre/protobuf"
	"github.com/guyu96/go-timbre/wallet"
)

// This is global for testing only
var (
	stateForDPOS *dposstate.State
	VoteRequired = true
)

// DPOS constants
const (
	miningTickInterval          = 10 * time.Second
	candidatesFilePath          = "bootstrap_committee.txt"
	minerSlotTime               = 20 * time.Second
	blockPrepTime               = 2 * time.Second //Time before tick when miner should start to prepare block
	defaultCommitteeSize        = 2
	minComSize           uint32 = 1
	maxComSize                  = 15
	InitComSize                 = 2
)

// Dpos structure
type Dpos struct {
	committeeSize      uint32
	HasStarted         bool
	committee          []*dposstate.Candidate //Committee from second last round
	nextRoundCommittee []*dposstate.Candidate //Committee members of the next round from the last round
	quitChT            chan int
	quitChL            chan int
	roundNumber        int64            //Current round number
	State              *dposstate.State //Current state which keeps updated based on vote tx in the current round
	// slotLength	 time.Time  //Time duration of every size
	startRound      chan uint32
	roundEnded      chan struct{}
	MiningSignal    chan struct{}
	ListeningSignal chan struct{}
	PrepBlockSig    chan struct{}

	CreateBlock chan int64
	// NodeBlockTick      chan struct{}
	curMinerAdd                string
	nodeAddress                string //Only required for testing purpose
	TimeOffset                 time.Duration
	roundTimeOffset            float64
	firstBlockProd             bool
	MinersByRound              map[int64][]string
	SlotsPassedByRound         map[int64]int64
	wallets                    wallet.WalletAccess
	bcHeight                   int64
	roundStartTime             int64 //Unix value of the round start time
	NumberOfToMineBlockPerSlot int
	bc                         BlockchainAccess
}

// New returns dpos consensus.
func New(comSize uint32, address string, timeOffset time.Duration, voteReq bool, walletsAccess wallet.WalletAccess, bcAccess BlockchainAccess) *Dpos {
	// log.Info().Msgf("Creating a dpos state to track candidate votes")
	VoteRequired = voteReq
	newState := dposstate.NewDposState(voteReq)
	return &Dpos{
		committeeSize:      comSize,
		HasStarted:         false,
		quitChT:            make(chan int),
		quitChL:            make(chan int),
		roundNumber:        0, //Round number starts with zero
		State:              newState,
		startRound:         make(chan uint32),
		roundEnded:         make(chan struct{}),
		MiningSignal:       make(chan struct{}),
		ListeningSignal:    make(chan struct{}),
		PrepBlockSig:       make(chan struct{}),
		MinersByRound:      make(map[int64][]string),
		SlotsPassedByRound: make(map[int64]int64),
		CreateBlock:        make(chan int64),
		// NodeBlockTick:      make(chan struct{}, 256),
		nodeAddress:                address,
		TimeOffset:                 timeOffset,
		roundTimeOffset:            0,
		bcHeight:                   0,
		roundStartTime:             0,
		wallets:                    walletsAccess,
		NumberOfToMineBlockPerSlot: int(minerSlotTime / miningTickInterval),
		bc:                         bcAccess,
	}

}

//SetBlockchain sets the blockchain in the DPOS
func (d *Dpos) SetBlockchain(bcAccess BlockchainAccess) {
	d.bc = bcAccess
}

//DeleteMinerRoundEntry deletes the entry in the miner by round
func (d *Dpos) DeleteMinerRoundEntry(round int64) {
	if _, ok := d.MinersByRound[round]; ok {
		delete(d.MinersByRound, round)
	}
}

//BlockPrepTime returns time before tick when miner should start preparing the block
func (d *Dpos) BlockPrepTime() time.Duration {
	return blockPrepTime
}

//DeleteSlotsPassedEntry deletes the miner round num
func (d *Dpos) DeleteSlotsPassedEntry(round int64) {
	if _, ok := d.SlotsPassedByRound[round]; ok {
		delete(d.SlotsPassedByRound, round)
	}
}

//TotalRoundTime returns the total time of the round
func (d *Dpos) TotalRoundTime(round int64) float64 {
	if miners, ok := d.MinersByRound[round]; ok {
		roundMiners := len(miners)
		return minerSlotTime.Seconds() * float64(roundMiners)
	}

	return 0
}

//TotalCurRoundDuration returns the total CurRoundDuration
func (d *Dpos) TotalCurRoundDuration() float64 {
	return minerSlotTime.Seconds() * float64(len(d.committee))
}

//SetRoundStartTime sets the current round start time
func (d *Dpos) SetRoundStartTime(ts int64) {
	d.roundStartTime = ts
}

//GetRoundStartTime returns the round start time
func (d *Dpos) GetRoundStartTime() int64 {
	return d.roundStartTime
}

//GetCurMinerAdd returns the miner at the moment
func (d *Dpos) GetCurMinerAdd() string {
	return d.curMinerAdd
}

func (d *Dpos) GetComitteeSize() uint32 {
	return d.committeeSize
}

//SetBcHeight the height of the blockchain in dpos
func (d *Dpos) SetBcHeight(height int64) {
	d.bcHeight = height
}

//GetBcHeight return the height of the blockchain in dpos
func (d *Dpos) GetBcHeight() int64 {
	return d.bcHeight
}

//GetRoundTimeOffset returns the time offset in round
func (d *Dpos) GetRoundTimeOffset() float64 {
	return d.roundTimeOffset
}

//SetRoundTimeOffset sets the roundTimeoffset
func (d *Dpos) SetRoundTimeOffset(val float64) {
	d.roundTimeOffset = val
}

//ResetRoundTimeOffset resets the roundTimeOffset to zero
func (d *Dpos) ResetRoundTimeOffset() {
	d.roundTimeOffset = 0
}

//GetMinComSize returns the minimum committe size as set in the const
func (d *Dpos) GetMinComSize() uint32 {
	return minComSize
}

//GetMinerSlotTime returns the slot time of the miners
func (d *Dpos) GetMinerSlotTime() time.Duration {
	return minerSlotTime
}

//GetCommitteeTime returns the committe duration
func (d *Dpos) GetCommitteeTime() time.Duration {
	return minerSlotTime * time.Duration(d.committeeSize)
}

//GetRoundNum returns the current round number
func (d *Dpos) GetRoundNum() int64 {
	return d.roundNumber
}

//SetRoundNum sets the round number
func (d *Dpos) SetRoundNum(num int64) {
	d.roundNumber = num
}

//GetTickInterval returns the tick interval b/w blocks
func (d *Dpos) GetTickInterval() time.Duration {
	return miningTickInterval
}

//SetCommitteeSize sets the size of the committee size
func (d *Dpos) SetCommitteeSize(size uint32) {
	d.committeeSize = size
}

//GetCommitteeSize returns the committee size of the committee
func (d *Dpos) GetCommitteeSize() uint32 {
	return d.committeeSize
}

// getCandidatesFromFile read candidates for bootstrapping
func (d *Dpos) getCandidatesFromFile(filePath string) error {
	f, err := os.Open(filePath)
	if err != nil {
		return err
	}
	defer f.Close()

	id := 1
	r := bufio.NewReader(f)

	for {

		l, err := r.ReadString('\n')
		l = strings.TrimSuffix(l, "\n")

		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		d.committee = append(d.committee, &dposstate.Candidate{
			ID:        strconv.Itoa(id),
			Address:   l,
			VotePower: 0,
			TimeStamp: time.Now().Unix(),
		})
		id++
	}
	return nil
}

//GetMinerIndex returns the position of miner in the round commitee
func (d *Dpos) GetMinerIndex(round int64, pk string) (int, bool) {
	if miners, present := d.MinersByRound[round]; present {
		for i, miner := range miners {
			if miner == pk {
				return i, true
			}
		}
	}

	return 0, false
}

//TotalMinersInRound returns the number of miners in the round
func (d *Dpos) TotalMinersInRound(round int64) int {
	if miners, ok := d.MinersByRound[round]; ok {
		return len(miners)
	}
	return 0
}

//GetMinersInRound returns miners in a particular round. If no then returns the empty list
func (d *Dpos) GetMinersInRound(round int64) []string {
	if miners, ok := d.MinersByRound[round]; ok {
		return miners
	}
	return []string{}
}

//StartDummyCommittee just put dummy candidates with dummy addresses to the current committee
func (d *Dpos) StartDummyCommittee(memCount int) {

	log.Info().Msgf("Creating dummy bootstrap committee")
	startPort := 7000
	l := "127.0.0.1:" //TODO:ChangeToPK
	for i := 0; i < memCount; i++ {
		add := l + strconv.Itoa(startPort)

		d.committee = append(d.committee, &dposstate.Candidate{
			ID:        strconv.Itoa(i),
			Address:   add,
			VotePower: 0,
			TimeStamp: time.Now().Unix(),
		})
		startPort++
	}

}

//BootstrapCommittee empties the committee and then bootstraps the committee with the set of miners given
func (d *Dpos) BootstrapCommittee(members []string) {
	newCommittee := []*dposstate.Candidate{} //Empties the committee

	for _, member := range members {
		d.committee = append(d.committee, &dposstate.Candidate{
			ID:        member,
			Address:   member,
			VotePower: 0,
			TimeStamp: time.Now().Unix(),
		})
	}
	d.State.InitializeStateWithMembers(newCommittee)
}

// Start DPOS
func (d *Dpos) Start() {
	if d.HasStarted == true {
		log.Info().Msgf("Dpos has already started")
		return
	}
	d.HasStarted = true
	go d.roundTracker()
	go d.loop()
	// log.Info().Msgf("DPOS go-routines has started->Signaling round start")
	d.startRound <- uint32(len(d.committee)) //First light weight signal to start the round
	// fmt.Println("Round start signaled")
}

// Stop DPOS
func (d *Dpos) Stop() {
	if !d.HasStarted {
		return
	}
	d.quitChL <- 1
	d.quitChT <- 1
	d.HasStarted = false
}

// Setup sets up up dpos from file
func (d *Dpos) Setup() error {

	comSize := InitComSize
	d.StartDummyCommittee(comSize)

	return nil
}

//roundTracker ensures the
func (d *Dpos) roundTracker() {

	for {
		select {
		// case <-d.startRound:
		// fmt.Println("starting round")
		case <-d.roundEnded:
			fmt.Printf("round no: %v has successfully ended \n", d.roundNumber)
			d.TransitionRound()
			// time.Sleep(1 * time.Second)

			d.roundNumber++
			log.Info().Msgf("Starting next round %v", d.roundNumber)
			d.startRound <- d.committeeSize

		case <-d.quitChT:
			fmt.Println("Stopped Dpos RT.")
			// log.Info().Msgf("Stopped Dpos roundTracker.")
			return

		}
	}

}

// func (d *Dpos)

//InitializeComMembers resets the current committe members to the given miners
func (d *Dpos) InitializeComMembers(miners, nextRoundMiners []string) {
	//First check if the member is in the DPOSState
	d.committee = []*dposstate.Candidate{}
	for _, miner := range miners {
		cand, err := d.State.GetCandidate(miner)
		if err != nil {
			log.Info().Err(errors.New("Com cannot be initialized with the said member"))
			continue
		}
		candidate := *cand
		d.committee = append(d.committee, &candidate)
	}

	d.nextRoundCommittee = []*dposstate.Candidate{}

	for _, miner := range nextRoundMiners {
		cand, err := d.State.GetCandidate(miner)
		if err != nil {
			log.Info().Err(errors.New("Next round com cannot be initialized with the said member"))
			continue
		}
		candidate := *cand
		d.nextRoundCommittee = append(d.nextRoundCommittee, &candidate)
	}

}

//EmptyCurrentCommittee empties the current miner committee
func (d *Dpos) EmptyCurrentCommittee() {
	d.committee = []*dposstate.Candidate{}
}

//RecomputeLastRoundCommittee reomputes the last round committee
func (d *Dpos) RecomputeLastRoundCommittee() {
	d.State.UpdateVotePower(d.wallets)
	committeeCand := d.State.SortByVotes(d.bc)
	nextComSize := d.ComputeNextCommitteeSize(committeeCand)
	d.copyCandToNextRoundCommittee(committeeCand, nextComSize) //Copies cand to last round committe
}

//TransitionRound makes preparation for the transition to the next round
func (d *Dpos) TransitionRound() {
	// time.Sleep(time.Second)

	d.State.UpdateVotePower(d.wallets)
	committeeCand := d.State.SortByVotes(d.bc)
	nextComSize := d.ComputeNextCommitteeSize(committeeCand)
	// curComSize := d.committeeSize

	// log.Info().Msgf("Transitioning round %v %v", curComSize, nextComSize)
	if d.roundNumber == 0 {
		d.copyCandToNextRoundCommittee(committeeCand, nextComSize) //Copies cand to last round committe
	}

	// d.committeeSize = nextComSize
	d.populateFromNextRoundCom() //will copy the members of next round committee to current round committee

	d.copyCandToNextRoundCommittee(committeeCand, nextComSize) //Copies cand to last round committe
	d.committeeSize = uint32(len(d.committee))
	if d.committeeTime() == 0 {
		panic("No committee member to support network!!")
	}

	for _, cand := range d.committee {
		log.Info().Msgf("Cand: %v , votePower: %v", cand.Address, cand.VotePower)
	}

	if d.MiningEligibility(d.nodeAddress) == true {
		// log.Info().Msgf("Starting node's miner to listen to all the transactions and offers")
		d.ListeningSignal <- struct{}{}

	}
	//Instead of completely renewing state(requiring candidate registration) just resetting the votepower to zero
	// d.State.ResetState()

	// if nextComSize != curComSize {
	//-------------
	// comDur := d.GetComDuration()
	// var sleepTime float64 = 2

	// if comDur.Seconds() < (d.committeeTime().Seconds() - sleepTime) {

	// 	log.Info().Msgf("IT SHOULD CATCH UP TO NEXT ROUND")
	// 	d.roundTimeOffset += comDur.Seconds() //Time offset in seconds which happens only when committee size changes
	// }
	//------------------------------------

	// comDur := d.GetComDuration()
	// curComTime := time.Now().Add(d.TimeOffset + time.Duration(d.roundTimeOffset)*time.Second)
	// nextRoundTs := curComTime.UnixNano() + int64(comDur)
	// fmt.Println("NEXT ROUND TS:- ", (nextRoundTs / (int64(time.Second) / int64(time.Nanosecond))))
	d.roundStartTime = lastMiningSlot(time.Now().Unix() + 1).Unix()                                      //adding two to be on safe side
	d.MinersByRound[d.roundNumber+1] = d.GetMinersAddresses()                                            //Putting the committee address . TODO:- see if u need check if entry already exists
	d.SlotsPassedByRound[d.roundNumber+1] = d.SlotsPassedByRound[d.roundNumber] + int64(d.committeeSize) //it wil return the sum of slots passed so far
	// time.Sleep(1 * time.Second)
}

//AdvanceRound advances to next round for catching up fast to most updated Dpos state
func (d *Dpos) AdvanceRound(startTime int64) int64 {
	d.roundNumber++

	d.State.UpdateVotePower(d.wallets)
	committeeCand := d.State.SortByVotes(d.bc)
	// committeeCand := d.State.GetCandidates()

	nextComSize := d.ComputeNextCommitteeSize(committeeCand)
	// curComSize := d.committeeSize

	if d.roundNumber == 1 { //In Genesis initialize the last round committee with the current one
		d.copyCandToNextRoundCommittee(committeeCand, nextComSize) //Copies cand to last round committe
	}

	// d.committeeSize = nextComSize
	d.populateFromNextRoundCom() //will copy the members of last round to the second last round

	d.copyCandToNextRoundCommittee(committeeCand, nextComSize)
	d.committeeSize = uint32(len(d.committee))
	if d.committeeSize == 0 {
		log.Info().Err(errors.New("Zero committee size"))
		return 0
	}
	// d.State.ResetState()

	// comDur := d.GetComDurationByTime(startTime)
	comDur := d.committeeTime()
	fmt.Println("committee duration: ", comDur)

	// if nextComSize != curComSize {
	// var sleepTime float64 = 2
	// fmt.Println("if comDur: ", comDur.Seconds(), "comTime: ", (d.committeeTime().Seconds() - sleepTime))
	// // log.Info().Msgf("TIME in transition is: ", comDur.Seconds(), (d.committeeTime().Seconds() - sleepTime))
	// var oldOffsetVal float64

	// if comDur.Seconds() < d.committeeTime().Seconds() {
	// 	oldOffsetVal = d.roundTimeOffset
	// 	d.roundTimeOffset += float64(comDur.Seconds()) //Time offset in seconds which happens only when committee size changes

	// 	comDur = d.GetComDurationByTime(startTime+int64((d.roundTimeOffset-oldOffsetVal)*1e9)) + time.Duration((d.roundTimeOffset-oldOffsetVal)*1e9)
	// }
	// }

	// fmt.Println("updated com duaration is: ", comDur, "next comSize: ", nextComSize, "offset: ", int64(d.roundTimeOffset*1e9), "roundNum: ", d.roundNumber)
	// // nextRoundTs := startTime + int64(comDur) + int64(d.roundTimeOffset*1e9) + int64(d.TimeOffset*1e9)
	nextRoundTs := startTime + comDur.Nanoseconds()

	d.MinersByRound[d.roundNumber] = d.GetMinersAddresses()                                              //Putting miners addresses. RoundNumer has already been incremented
	d.SlotsPassedByRound[d.roundNumber] = d.SlotsPassedByRound[d.roundNumber-1] + int64(d.committeeSize) //When entry doesn't exist it returns 0

	d.roundStartTime = startTime / 1e9
	fmt.Println("NEXT ROUND TS:- ", (nextRoundTs / (int64(time.Second) / int64(time.Nanosecond))), "roundStart", d.roundStartTime)

	return nextRoundTs
}

//SetTimeLeftInRound sets the time left in the current round
func (d *Dpos) SetTimeLeftInRound(timeLeft float64) {

	comDur := d.GetComDuration()
	// var sleepTime float64 = 1
	// log.Info().Msgf("TIME in transition is: ", comDur.Seconds(), (d.committeeTime().Seconds() - sleepTime))
	// fmt.Println("if comDur: ", comDur.Seconds(), "comTime: ", (d.committeeTime().Seconds() - sleepTime))

	if comDur.Seconds() < (timeLeft) {
		log.Info().Msgf("Catching down!!!")
		d.roundTimeOffset -= timeLeft - comDur.Seconds() //Time offset in seconds which happens only when committee size changes
	} else if comDur.Seconds() > (timeLeft) {
		log.Info().Msgf("Catching UP!!!")
		d.roundTimeOffset += comDur.Seconds() - timeLeft
	}
	fmt.Println("After time set:- ", d.GetComDuration())
}

//AdvanceRoundInSec advances the round
func (d *Dpos) AdvanceRoundInSec(sec float64) {
	comDur := d.GetComDuration()
	if comDur.Seconds() > (sec) {
		d.roundTimeOffset += sec //Time offset in seconds which happens only when committee size changes
	} else {
		log.Info().Err(errors.New("Can't increment. Time left in round is less"))
	}
}

//Outdated func...

//GetComDurationByTime returns the duration of the committe using the starting time of the round
func (d *Dpos) GetComDurationByTime(sTime int64) time.Duration {
	//Convert time to seconds
	// fmt.Println("sTime is: ", sTime)
	curComTime := sTime / (int64(time.Second) / int64(time.Nanosecond))
	// fmt.Println("curComTime: ", curComTime, "timeDivision: ", (int64(time.Second) / int64(time.Nanosecond)))

	curComInd := d.currentCommitteeIndex(curComTime)
	nextComTime := d.nextCommitteeTime(curComInd + 1)
	// fmt.Println("curTime: ", sTime, "curCom ind: ", curComInd)
	comDur := time.Duration(nextComTime-curComTime) * time.Second
	return comDur
	// time.Sleep(1 * time.Second)
}

//Outdated func...

//GetComDuration returns the timeDuration in seconds for the current active committee(set of miners)
func (d *Dpos) GetComDuration() time.Duration {
	// curComTime := time.Now().Add(d.TimeOffset + time.Duration(d.roundTimeOffset)*time.Second)
	curComTime := time.Now().Unix()

	// fmt.Println("curComTime: ", curComTime.Unix())

	curComInd := d.currentCommitteeIndex(curComTime)
	nextComTime := d.nextCommitteeTime(curComInd + 1)
	// fmt.Println("curComTime: ", curComTime.Unix(), " curCom ind: ", curComInd)
	comDur := time.Duration(nextComTime-curComTime) * time.Second
	return comDur
}

//ComputeNextCommitteeSize computes the committe of the sorted child candidates
func (d *Dpos) ComputeNextCommitteeSize(sortedCand []*dposstate.Candidate) uint32 {

	if d.State.GetTotalVoteStaked() == 0 || len(sortedCand) == 0 { //If no vote is casted then default size is 2
		comLen := uint32(len(sortedCand))
		d.State.SetChildCommitteeSize(comLen)

		if comLen <= maxComSize {
			if comLen%2 == 0 && comLen > 1 {
				comLen--
			}
			return comLen
		} else if comLen > maxComSize {
			return maxComSize
		}

		return minComSize
	}

	var size uint32 = 0
	//Candidate must get atleast 10% of the total votepower to get entry in the committee
	for _, cand := range sortedCand {
		if float64(cand.VotePower) >= (float64(0.1) * float64(d.State.GetTotalVoteStaked())) {
			size++
		}
		if cand.VotePower == 0 { //Since slice is sorted by votepower no need to look further
			break
		}
	}

	if size > maxComSize {
		d.State.SetChildCommitteeSize(size)
		return maxComSize
	}

	if size%2 == 0 && size > 1 {
		size--
	}

	d.State.SetChildCommitteeSize(size)
	return size
}

//MiningEligibility checks the eligibility of the candidate to mine by comparing current committee members
func (d *Dpos) MiningEligibility(add string) bool {
	for _, cand := range d.committee {
		if cand.Address == add {
			return true
		}
	}
	return false
}

//copyCandToNextRoundCommittee copy the pointer candidates into the committee from state candidates passed
func (d *Dpos) copyCandToNextRoundCommittee(committeeCand []*dposstate.Candidate, nextComSize uint32) {
	d.nextRoundCommittee = []*dposstate.Candidate{} //Emptying the current slice
	// var candidate dposstate.Candidate
	// log.Info().Msgf("nextRoundCommittee size now is: ", d.committeeSize)
	for index, cand := range committeeCand {
		candidate := *cand
		d.nextRoundCommittee = append(d.nextRoundCommittee, &candidate)
		if uint32(index) >= nextComSize-1 { //Only copy candidates equivalent to committe size
			break
		}
	}
}

//populateFromNextRoundCom copies the committee next round to the current round
func (d *Dpos) populateFromNextRoundCom() {
	d.committee = []*dposstate.Candidate{}

	for _, cand := range d.nextRoundCommittee {
		candidate := *cand
		d.committee = append(d.committee, &candidate)
	}

}

//loop loops over all the miner slots and then transition to next round
func (d *Dpos) loop() {
	log.Info().Msgf("Started Dpos loop")
	nextMinerTurn := false
	if d.roundStartTime == 0 { //For the first time only
		d.roundStartTime = lastMiningSlot(time.Now().Unix()).Unix()
		fmt.Println("Ensure same round-START: ", d.roundStartTime)
	}
	// nextCommitteeTurn := false
	// defer ticker.Stop()

	for {
		totalMiners := <-d.startRound //waits for the round start signal
		fmt.Println("round start: ", d.roundStartTime, "slot passed", d.SlotsPassedByRound[d.roundNumber])
		// log.Info().Msgf("Total miner in this round are: ", totalMiners)
		if totalMiners == 0 {
			// time.Sleep(2 * time.Second)
			panic("No miners")
		}

		// comDur := d.GetComDuration()
		// log.Info().Msgf("committee duration: ", comDur, time.Duration(d.roundTimeOffset)*time.Second)
		// comTimer := time.After(comDur)
		// fmt.Println("committee length: ", len(d.committee), d.committeeSize)

		mIndex := 0 //Setting the miner index
		// for range d.committee {
	ComLoop:
		for mIndex < int(d.committeeSize) {

			blocksMined := 0
			// ticker := time.NewTicker(miningTickInterval)
			// curTime := (time.Now().Add(d.TimeOffset + time.Duration(d.roundTimeOffset)*time.Second)).Unix()
			curTime := (time.Now().Unix())

			// log.Info().Msgf("Time left to mine: ", d.TimeLeftToMine(curTime))

			tickTimer := time.After(d.TimeLeftToTick(curTime))
			blockPrepTimer := time.After(time.Duration(math.MaxInt64)) //Settin it to the infinity
			// if d.TimeLeftToTick(time.Now().Unix()) >= blockPrepTime {
			// 	BlockPrepTimer := time.After(d.TimeLeftToTick(time.Now().Unix()) - 2*time.Second)
			// }else{
			// 	BlockPrepTimer := time.After(d.TimeLeftToTick(time.Now().Unix()) - 2*time.Second)
			// }
			for d.TimeLeftToMine(curTime) < 0 { //For safety in case time is negative minerTimer will signal right away
				time.Sleep(100 * time.Millisecond)
				// curTime = (time.Now().Add(d.TimeOffset + time.Duration(d.roundTimeOffset)*time.Second)).Unix()
				curTime = (time.Now().Unix())

			}

			log.Info().Msgf("Miner time left: %v", d.TimeLeftToMine(curTime)+time.Duration(200)*time.Millisecond)
			minerTimer := time.After(d.TimeLeftToMine(curTime) + time.Duration(200)*time.Millisecond)
			log.Info().Msgf("commitee size: %v", len(d.committee))
			mIndex = d.calcMinerIndUsingRoundTs(time.Now().Unix())
			fmt.Println("new IND: ", mIndex)
			miner := d.committee[mIndex]
			d.curMinerAdd = miner.Address
			if d.curMinerAdd == d.nodeAddress {
				d.MiningSignal <- struct{}{} //Current node just has started to mine
				// d.PrepBlockSig <- struct{}{}
				if d.TimeLeftToTick(time.Now().Unix()) >= blockPrepTime {
					blockPrepTimer = time.After(d.TimeLeftToTick(time.Now().Unix()) - 2*time.Second)
				}
			}

			for {
				select {
				// case now := <-ticker.C:
				// 	log.Info().Msgf("%s to mine now. VotePower: %v", miner.Address, miner.VotePower)
				// 	d.mineBlock(now.Add(d.TimeOffset), miner, d.roundNumber) //Should be called on miner
				// 	d.bcHeight++                                             //TODO:- make increment in the syncer
				// 	blocksMined++
				// 	if blocksMined < d.BlocksPerMiner(){
				// 	}

				case <-tickTimer:
					fmt.Println("TS is: ", time.Now().Unix())
					log.Info().Msgf("%s to mine now. VotePower: %v", miner.Address, miner.VotePower)
					d.mineBlock(time.Now(), miner, d.roundNumber) //Should be called on miner
					d.bcHeight++                                  //TODO:- make increment in the syncer
					blocksMined++

					// d.NodeBlockTick <- struct{}{}

					if blocksMined < d.BlocksPerMiner() {
						// curTime = (time.Now().Add(d.TimeOffset + time.Duration(d.roundTimeOffset)*time.Second)).Unix()
						curTime = (time.Now().Unix())
						tickTimer = time.After(d.TimeLeftToTick(curTime))
						if d.curMinerAdd == d.nodeAddress {
							blockPrepTimer = time.After(d.TimeLeftToTick(time.Now().Unix()) - blockPrepTime)
						}
					} else {
						nextMinerTurn = true
						fmt.Println("Ticker time up!")
					}

				case <-blockPrepTimer:
					fmt.Println("Start preparing block...")
					d.PrepBlockSig <- struct{}{}

				case <-minerTimer:
					// log.Info().Msgf("Next miner turn!!!: ", now.Add(d.TimeOffset))
					// log.Info().Msgf("Next miner turn!!")
					fmt.Println("Miner time up!")
					nextMinerTurn = true

				// case <-comTimer:
				// 	log.Info().Msgf("Next committee time!!!")
				// 	nextMinerTurn = true
				// 	nextCommitteeTurn = true

				case <-d.quitChL:
					fmt.Println("Stopped Dpos loop.")
					// log.Info().Msgf("Stopped Dpos loop.")
					return
				}

				if nextMinerTurn == true {
					nextMinerTurn = false
					break
				}
			}

			if mIndex == len(d.committee)-1 { //In case you are syncing up all nodes might not get the turn. So check index
				nextMinerTurn = false

				log.Info().Msgf("Across round tolerance time:- %v", time.Second)
				// tolTicker := time.After(miningTickInterval / 2)
				tolTicker := time.After(time.Second)

				// timer := time.Now().UnixNano()
				d.curMinerAdd = ""
				for {
					// break ComLoop
					select {
					case <-tolTicker:
						// fmt.Println("slept for: ", time.Now().UnixNano()-timer)
						break ComLoop
					case <-d.quitChL:
						fmt.Println("Stopped Dpos loop2.")
						// log.Info().Msgf("Stopped Dpos loop signaling.")
						return
					}
				}

				// time.Sleep(miningTickInterval / 2)
				// break
			}
			// if nextCommitteeTurn == true {
			// 	nextCommitteeTurn = false
			// 	nextMinerTurn = false
			// 	break
			// }
			// time.Sleep(1 * time.Second)
		}
		// fmt.Println("stuck-1")
		d.roundEnded <- struct{}{}
		// fmt.Println("stuck-2")

	}
}

//GetCurrentMinerAdd returns the address of the current miner
func (d *Dpos) GetCurrentMinerAdd() string {
	return d.curMinerAdd
}

func (d *Dpos) signalMinertoProcessPr(miner *dposstate.Candidate) error {
	if miner.Address == d.nodeAddress {
		d.CreateBlock <- -1
		return nil
	}

	return errors.New("Not my turn")
}

func (d *Dpos) mineBlock(now time.Time, miner *dposstate.Candidate, roundNum int64) error {

	if miner.Address == d.nodeAddress {
		// log.Info().Msgf("Miner to mine block now:- ", miner.Address, "votePower: ", miner.VotePower)
		// log.Info().Msgf("Only I am elligible to mine in this round")

		d.CreateBlock <- roundNum //Block creation will be triggered in miner.go
		return nil
	}
	return errors.New("Not my turn")
}

//TimeLeftToMine returns the time left in next mining slot
func (d *Dpos) TimeLeftToMine(ts int64) time.Duration {
	nextTime := nextMiningSlot(ts)
	timeLeft := nextTime - ts
	return time.Duration(timeLeft) * time.Second
}

//TimeLeftToTick returns the time left in the mining tick in seconds
func (d *Dpos) TimeLeftToTick(ts int64) time.Duration {
	nextTime := nextTickSlot(ts)
	timeLeft := nextTime - ts
	return time.Duration(timeLeft) * time.Second
}

//NextTickTime return returns the time of next tixk
func (d *Dpos) NextTickTime(ts int64) int64 {
	nextTime := nextTickSlot(ts)
	return nextTime
}

//NextMiningSlotTime return returns the time of next mining slot
func (d *Dpos) NextMiningSlotTime(ts int64) time.Time {
	return time.Unix(nextMiningSlot(ts), 0)
}

//Basically it rounds down to the start of the round and gives the timestamp
func lastMiningSlot(ts int64) time.Time {
	now := time.Duration(ts) * time.Second
	last := ((now - time.Second) / minerSlotTime) * minerSlotTime
	return time.Unix(int64(last/time.Second), 0)
}

func nextTickSlot(ts int64) int64 {
	now := time.Duration(ts) * time.Second
	next := ((now + miningTickInterval) / miningTickInterval) * miningTickInterval
	return int64(next / time.Second)
}

func nextMiningSlot(ts int64) int64 {
	now := time.Duration(ts) * time.Second
	// next := ((now + minerSlotTime - time.Second) / minerSlotTime) * minerSlotTime
	// fmt.Println("now is: ", now, "next is: ", next)
	next := ((now + minerSlotTime) / minerSlotTime) * minerSlotTime
	// fmt.Println("now is: ", now, "next is: ", next)

	// log.Info().Msgf("Next slot time: ", next)
	return int64(next / time.Second)
}

func (d *Dpos) calcMinerIndex(ts int64) int {
	return (int(ts) / int(minerSlotTime.Seconds())) % int(d.committeeSize)
}

//Uses the rouding. (Not true)Because this function is only called when signalling based on absolute slot happens.(Will not be called in the middle of the slot time; could be when syncing)
func (d *Dpos) calcMinerIndUsingRoundTs(ts int64) int {
	timeDif := ts - d.roundStartTime
	// rtimeDif := RoundNum(float64(timeDif), minerSlotTime.Seconds()) //Rounding because RoundStartTime may not be exact
	return int(timeDif) / int(minerSlotTime.Seconds()) % int(d.committeeSize)
}

//RoundNum rounds the number to the nearest unit
func RoundNum(x, unit float64) float64 {
	return math.Round(x/unit) * unit
}

func (d *Dpos) nextCommitteeTime(curComInd int) int64 {
	return int64(curComInd) * int64(d.committeeTime().Seconds())
}

func (d *Dpos) currentCommitteeIndex(ts int64) int {
	return int(ts / int64(d.committeeTime().Seconds()))
}

func (d *Dpos) committeeTime() time.Duration {
	return minerSlotTime * time.Duration(d.committeeSize)
}

//BlocksPerMiner returns the number of block a miner can produce
func (d *Dpos) BlocksPerMiner() int {
	numOfBlocks := minerSlotTime.Seconds() / miningTickInterval.Seconds()
	return int(numOfBlocks)
}

//RoundBlockTime returns the time in seconds for creating the blocks in a round
func (d *Dpos) RoundBlockTime() float64 {
	return float64(d.BlocksPerMiner()) * miningTickInterval.Seconds()
}

//RoundToleranceTime returns the time buffer(in seconds) at the end of the round
func (d *Dpos) RoundToleranceTime() float64 {
	return d.GetMinerSlotTime().Seconds() - d.RoundBlockTime()
}

//GetMinersAddresses returns the addresses of the miners currently mining in this round
func (d *Dpos) GetMinersAddresses() []string {
	var minerAddresses []string
	for _, cand := range d.committee {
		minerAddresses = append(minerAddresses, cand.Address)
	}
	return minerAddresses
}

//GetMinersForNextRound returns the miners computed using last round -> stored in nextRoundCommittee. It may not be final and could change in case there are delayed blocks from last round
func (d *Dpos) GetMinersForNextRound() []string {
	var minerAddresses []string
	for _, cand := range d.nextRoundCommittee {
		minerAddresses = append(minerAddresses, cand.Address)
	}
	return minerAddresses
}

//GetMinersByID get the miners ID
func (d *Dpos) GetMinersByID() []string {
	var minerIDs []string
	for _, cand := range d.committee {
		minerIDs = append(minerIDs, cand.ID)
	}
	return minerIDs
}

//PrintMiners print miners in the current round of the miners
func (d *Dpos) PrintMiners() {
	miners := d.GetMinersAddresses()
	for _, miner := range miners {
		fmt.Println(miner)
	}
}

//MinersToBytes makes a proto message of set of miners if current node is running dpos
func (d *Dpos) MinersToBytes() ([]byte, error) {

	if d.HasStarted == true {
		minerAdds := d.GetMinersAddresses()
		miners := &pb.CurrentMiners{
			MinerAddresses: minerAdds,
			NumMiners:      uint32(len(minerAdds)),
		}
		return proto.Marshal(miners)
	}

	return []byte{}, errors.New("Node doesn't runs dpos")
}

//MinersFromBytes returns the CurrentMiner instance form the bytes
func (d *Dpos) MinersFromBytes(minerBytes []byte) (*pb.CurrentMiners, error) {
	miners := new(pb.CurrentMiners)
	if err := proto.Unmarshal(minerBytes, miners); err != nil {
		return nil, err
	}
	return miners, nil
}

//ValidateMiningOrder validates the order of miner is the round map
func (d *Dpos) ValidateMiningOrder(parentMiner, childMiner string, pmRound, CRound int64, timeDiff int64) error {
	fmt.Println("Validating mining order...", parentMiner, childMiner, pmRound, CRound)
	slotDif := 0
	pmPosition := 0
	parentPresent, childPresent := false, false

	for i := pmRound; i < CRound+1; i++ {
		// fmt.Println("check")
		if i == pmRound {
			if pMiners, ok := d.MinersByRound[pmRound]; ok {
				// fmt.Println("pminers: ", pMiners)
				for j, pMiner := range pMiners {
					if pMiner == parentMiner {
						parentPresent = true
						slotDif += len(pMiners) - j - 1
						pmPosition = j
						fmt.Println("lower slot dif: ", slotDif)
						break
					}
				}
			} else {
				return errors.New("Blocks parent round doesn't exist")
			}
		}
		if i == CRound {
			if CMiners, ok := d.MinersByRound[CRound]; ok {
				// fmt.Println("cminers: ", CMiners)
				for k, CMiner := range CMiners {
					if CMiner == childMiner {
						childPresent = true
						if pmRound == CRound {
							slotDif = k - pmPosition
							fmt.Println("Upper slot dif: ", slotDif)

							if k < pmPosition {
								log.Info().Err(errors.New(("Child mining position is less than parent")))
								return errors.New("Child mining position is less than parent")
							}
							break
						}

						slotDif += k + 1
						fmt.Println("Upper slot dif: ", slotDif)
						break
					}
				}
			} else {
				return errors.New("Block is from future")
			}
		}
		if i != pmRound && i != CRound {
			slotDif += len(d.MinersByRound[i])
		}
	}
	// actualSlotDif := math.Ceil(float64(timeDiff) / float64(minerSlotTime.Nanoseconds()))
	// actualSlotDif := int(math.Round(float64(timeDiff) / float64(minerSlotTime.Nanoseconds()))) //TODO:- Check if round is the right func to use
	fmt.Println("time diff: ", timeDiff, "slotDiff:= ", slotDif)
	safeTimeDif := int64(slotDif-1)*minerSlotTime.Nanoseconds() + miningTickInterval.Nanoseconds()
	fmt.Println("safe time Diff: ", safeTimeDif)

	if parentPresent == false {
		log.Warn().Msgf("Parent miner not found")
		return errors.New("Parent miner not found")
	} else if childPresent == false {
		log.Warn().Msgf("Child miner not found")
		return errors.New("Child miner not found")
	}
	safeTimeDif = safeTimeDif - (miningTickInterval.Nanoseconds() - 1e9)
	fmt.Println("compTime: ", safeTimeDif)
	if timeDiff >= safeTimeDif { //Giving miningTickInterval-1 second of buffer time
		return nil
	}
	fmt.Println("ActualTime: ", timeDiff, " safeTime: ", safeTimeDif)

	log.Info().Msgf("Safe time not maintained")

	return errors.New("Safe time diff not maintained")
}

//ValidateDigestMiners validates the digest block miner presence and their positions
func (d *Dpos) ValidateDigestMiners(dMiners []string) bool {
	curMiners := d.GetMinersAddresses()
	if len(curMiners) != len(dMiners) {
		return false
	}

	for i, miner := range dMiners {
		if miner != curMiners[i] {
			return false
		}
	}
	return true
}

//PrintMinersByRound print all the miners by the round
func (d *Dpos) PrintMinersByRound() {
	// for key, val := range d.MinersByRound {
	// 	fmt.Println("Round: ", key, " ", val)
	// }
	var i, roundStart int64 = 0, 0
	for i = roundStart; i <= d.roundNumber; i++ {
		if val, ok := d.MinersByRound[i]; ok {
			fmt.Println("Round: ", i, " ", val)
		}
	}

}

//PrintSlotsPassedByRound prints the slots passed by round
func (d *Dpos) PrintSlotsPassedByRound() {
	// for key, val := range d.SlotsPassedByRound {
	// 	fmt.Println("Round: ", key, " ", val)
	// }
	var i, roundStart int64 = 0, 0
	for i = roundStart; i <= d.roundNumber; i++ {
		if val, ok := d.SlotsPassedByRound[i]; ok {
			fmt.Println("Round: ", i, " ", val)
		}
	}
}

//TimeLeftInRound returns the timeleft in round according to the DPOS
func (d *Dpos) TimeLeftInRound() int64 {
	curTime := time.Now().Unix()
	timeLeft := int64(d.committeeTime().Seconds()) - (curTime - d.roundStartTime)
	return timeLeft
}

//TakeDposSnapshot takes the snapshot of the dpos
func (d *Dpos) TakeDposSnapshot() *StateSnapshot {

	copyState := d.State.CloneState()

	snapShot := &StateSnapshot{
		roundNumber:   d.roundNumber,
		MinersByRound: make([][]string, len(d.MinersByRound)+1),
		CandState:     copyState,
	}

	snapShot.AddNextRoundCom(d.nextRoundCommittee) //Coping the nextRoundCOmmittee

	for k, v := range d.MinersByRound { //adding the miners by round in the array
		snapShot.MinersByRound[k] = v
	}

	return snapShot
}

//DposSnapshotUsingPb takes the snapshot of the Dpos state in pb struct
func (d *Dpos) DposSnapshotUsingPb() *PbStateSnapshot {
	copyCandState := d.State.CloneStateToPb()

	snap := &PbStateSnapshot{
		SnapShot: pb.DposSnapShot{
			RoundNumber:   d.roundNumber,
			CandState:     copyCandState,
			MinersByRound: make([]*pb.StringArray, len(d.MinersByRound)+1),
		},
	}
	snap.AddNextRoundComPb(d.nextRoundCommittee)

	for k, v := range d.MinersByRound { //adding the miners by round in the array
		miners := &pb.StringArray{
			StringArray: v,
		}
		snap.SnapShot.MinersByRound[k] = miners
	}

	return snap
}
