package dpos

import (
	"fmt"
	"testing"
	"time"

	dposstate "github.com/guyu96/go-timbre/dpos/state"
	"github.com/guyu96/go-timbre/wallet"
)

func TestDpos_getCandidatesFromFile(t *testing.T) {

	type fields struct {
		committeeSize int
		HasStarted    bool
		committee     []*dposstate.Candidate
		quitCh        chan int
		roundNumber   int64
		State         *dposstate.State
	}
	type args struct {
		filePath string
	}
	// testCom:=make(map[string]*dposstate.Candidate)
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		// TODO: Add test cases.
		{
			name: "Testing to check if parsing from txt file is correctly happening",
			fields: fields{
				committeeSize: 3,
				HasStarted:    true,
				roundNumber:   0,
			},
			args: args{
				filePath: "bootstrap_committee.txt",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Dpos{
				committeeSize: uint32(tt.fields.committeeSize),
				HasStarted:    tt.fields.HasStarted,
				committee:     tt.fields.committee,
				quitCh:        tt.fields.quitCh,
				roundNumber:   tt.fields.roundNumber,
				State:         tt.fields.State,
			}
			if err := d.getCandidatesFromFile(tt.args.filePath); (err != nil) != tt.wantErr {
				t.Errorf("Dpos.getCandidatesFromFile() error = %v, wantErr %v", err, tt.wantErr)
			}
			for key, value := range d.committee {
				fmt.Println("Key is:", key, "Value:", value)
			}

		})
	}
}

func TestDpos_calcMinerIndUsingRoundTs(t *testing.T) {
	type fields struct {
		committeeSize      uint32
		HasStarted         bool
		committee          []*dposstate.Candidate
		quitCh             chan int
		roundNumber        int64
		State              *dposstate.State
		startRound         chan uint32
		roundEnded         chan struct{}
		MiningSignal       chan struct{}
		ListeningSignal    chan struct{}
		CreateBlock        chan int64
		curMinerAdd        string
		nodeAddress        string
		TimeOffset         time.Duration
		roundTimeOffset    float64
		firstBlockProd     bool
		MinersByRound      map[int64][]string
		SlotsPassedByRound map[int64]int64
		wallets            wallet.WalletAccess
		bcHeight           int64
		roundStartTime     int64
	}
	type args struct {
		ts int64
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   int
	}{
		// TODO: Add test cases.
		{
			name: "Test-1",
			fields: fields{
				roundStartTime: 11,
				committeeSize:  3,
			},
			args: args{
				ts: 20,
			},
			want: 0,
		},
		{
			name: "Test-2",
			fields: fields{
				roundStartTime: 10,
				committeeSize:  3,
			},
			args: args{
				ts: 20,
			},
			want: 1,
		},
		{
			name: "Test-3",
			fields: fields{
				roundStartTime: 5,
				committeeSize:  3,
			},
			args: args{
				ts: 15,
			},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &Dpos{
				committeeSize:      tt.fields.committeeSize,
				HasStarted:         tt.fields.HasStarted,
				committee:          tt.fields.committee,
				quitCh:             tt.fields.quitCh,
				roundNumber:        tt.fields.roundNumber,
				State:              tt.fields.State,
				startRound:         tt.fields.startRound,
				roundEnded:         tt.fields.roundEnded,
				MiningSignal:       tt.fields.MiningSignal,
				ListeningSignal:    tt.fields.ListeningSignal,
				CreateBlock:        tt.fields.CreateBlock,
				curMinerAdd:        tt.fields.curMinerAdd,
				nodeAddress:        tt.fields.nodeAddress,
				TimeOffset:         tt.fields.TimeOffset,
				roundTimeOffset:    tt.fields.roundTimeOffset,
				firstBlockProd:     tt.fields.firstBlockProd,
				MinersByRound:      tt.fields.MinersByRound,
				SlotsPassedByRound: tt.fields.SlotsPassedByRound,
				wallets:            tt.fields.wallets,
				bcHeight:           tt.fields.bcHeight,
				roundStartTime:     tt.fields.roundStartTime,
			}
			if got := d.calcMinerIndUsingRoundTs(tt.args.ts); got != tt.want {
				t.Errorf("Dpos.calcMinerIndUsingRoundTs() = %v, want %v", got, tt.want)
			}
		})
	}
}
