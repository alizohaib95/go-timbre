package net

import (
	"bufio"
	"context"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"os"
	"path"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/Nik-U/pbc"
	gproto "github.com/gogo/protobuf/proto"
	"github.com/golang/protobuf/proto"
	"github.com/guyu96/go-timbre/core"
	"github.com/guyu96/go-timbre/crypto"
	"github.com/guyu96/go-timbre/crypto/pos"
	"github.com/guyu96/go-timbre/dpos"
	"github.com/guyu96/go-timbre/log"
	"github.com/guyu96/go-timbre/protobuf"
	"github.com/guyu96/go-timbre/storage"
	"github.com/guyu96/go-timbre/wallet"
	"github.com/mbilal92/noise"
	"github.com/mbilal92/noise/network"
	// "github.com/mbilal92/pbc"
)

const (
	incomingChanSize = 128 // Default incoming channel buffer size
	blocksToAskFor   = 10  // value for z
	comSize          = 5
	keysPath         = "../keys/"
)

var (
	nodeBucket          []byte // name for node info storage bucket
	privateKeyKey       []byte // key for node private key
	fullNodeStateBucket []byte // Full node state bucket
	dealBucket          []byte // Full node state bucket
)

func init() {
	nodeBucket = []byte("node-bucket")
	privateKeyKey = []byte("private-key")
	fullNodeStateBucket = []byte("full-state")
	dealBucket = []byte("deal-bucket")
}

type Keypair struct {
	PrivateKey noise.PrivateKey
	PublicKey  noise.PublicKey
}

// Node encapsulates a runnable node in the Timbre p2p network.
type Node struct {
	Db    *storage.Database // database connection
	Bc    *core.Blockchain  // blockchain
	Bp    *core.Blockpool   // blockpool
	BcMtx sync.Mutex        // blockchain mutex

	TimeOffset time.Duration
	Dpos       *dpos.Dpos
	Wm         *wallet.WalletManager //Wallet manager which will also contain my wallet
	keys       Keypair               // public and private keys
	ntw        *network.Network      // network
	incoming   chan Message          // incoming messages from network are redirected here
	outgoing   *sync.Map
	LocalSync  bool
	curEpoch   int32
	Port       uint16
	//TODO: make struct for POS
	PosPairing *pbc.Pairing
	PosConfig  *pos.Config
	PosPk      *pos.PublicKey
	PosSk      *pos.SecretKey
	PosParam   *pbc.Params

	NodeType          string
	User              User // Need to discuss
	StorageProvider   StorageProvider
	Miner             Miner
	BandwidthProvider BandwidthProvider
	Syncer            Syncer
	Services          Services

	DealMtx     sync.RWMutex             // blockchain mutex
	Activedeals map[string]protobuf.Deal // Active Deals from blockchain
	// LastRoundExpiredDeals map[string]bool          // Active Deals from blockchain
	TransactionNonce uint64
	QuitNonceUpChan  chan bool
	PendingTrans     int

	// postToSp   map[string]string     // Maps postHashes with its storage provider (For now broadcasting)
	ThreadBase    map[string]([]string) // Maps threadRootHash -> all posts in the thread
	ThreadBaseMap sync.Map
}

// HandlerFunc Function to be passed to requestAndresponse
type HandlerFunc func(msg Message) error

// type Services interface {
// 	Process()
// 	StartDpos(comSize int, minerRole bool) error
// 	StartDpos2(comSize int, minerRole bool) error
// 	UpdateDpos(*core.Block)
// 	AnnounceCandidacy() error
// 	StartExplorer(pno string)
// 	DoDelegateRegisteration() (*core.Transaction, error)
// 	// RevertDposState(block *core.Block)
// 	RevertDposState2(block *core.Block)
// 	RevertDposTill(block *core.Block)
// 	TimeLeftInRoundBySlots(b *core.Block) int64
// 	DoAmountTrans(ctx context.Context, port string, txAmountFrom uint64) error
// 	DoPercentVoteDuplicate(ctx context.Context, pk string, percent string) error
// 	CreatePodf(ctx context.Context, h1 *protobuf.BlockHeader, h2 *protobuf.BlockHeader) error
// }

// // Interfaces for roles inorder to give mutual access to roles
// // There is a seperate role for each inorder to avoid empty functions in other roles
// type BaseRole interface {
// 	Setup()
// 	Process()
// }

// type User interface {
// 	BaseRole
// 	SendPostRequest(content, parentPostHash, threadheadPostHash []byte, maxCost, minDuration uint32, postAuhtorPk []byte) string
// 	GetStoredP() []*protobuf.StoredPost
// 	GetThread(a string)
// 	GetThreads()
// 	GetTs() [][]byte
// 	GetThreadRoots() []string
// 	GetThreadFromCache(key string) ([]*rpcpb.Post, error)
// 	UpdateLikes(hash string, votes uint32)
// }

// type StorageProvider interface {
// 	Process()
// 	SendStorageOffer(minPrice, maxDuration, size uint32)
// 	IssueChallenges(seed int64)
// 	EmptyCaches()
// 	// GetSpBlockTickChan() chan struct{}
// 	FilterMyDeals(deals []*protobuf.Deal)
// 	ProcessNewBlock(b *core.Block)
// 	GetStorageProviderStats() *rpcpb.StorageProviderStats
// 	RemoveExpiredDealDataFromCahce(deal *protobuf.Deal)
// }

// type Syncer interface {
// 	Process()
// 	ResyncChain()
// 	GetIsSyncing() bool
// 	// SyncBlockchainAtStart(z int, done chan struct{})
// 	SyncBlockchain()
// 	PrintBlockChain()
// 	TimeLeftInRound(block *core.Block) (bool, float64)
// 	RoundStartTime(block *core.Block) int64
// 	SetIsSyncing(flag bool)
// }

// type BandwidthProvider interface {
// 	BaseRole
// 	UpdateThreadBase()
// 	GetThreads()
// }

// type Miner interface {
// 	// BaseRole
// 	Process(context.Context)
// 	TestRelayToSp()
// 	PrintPostStats()
// 	CreateBlockLoop()
// 	StartListening(ctx context.Context)
// 	StartMining()
// 	PushRetTrans(pbTrans *protobuf.Transaction)
// 	PushPodf(pbPodf *protobuf.PoDF)
// 	UpdateCache(block *core.Block)
// 	EmptyCaches()
// 	RemoveExpiredDealDataFromCahce(deal *protobuf.Deal)
// }

// NewNode creates a new node.
func NewNode(Path string, host string, port uint16, comSize int, vtReq bool) (*Node, error) {
	localSync := false

	keyFile := strconv.Itoa(int(port)) + "_key.txt"      //stores the private key here
	if _, err := os.Stat(keysPath); os.IsNotExist(err) { //Create directory to the for the keys path
		fmt.Println("creating directory")
		os.Mkdir(keysPath, os.ModePerm)
	}
	// Open database connection.
	dbPath := path.Join(Path+"db/", fmt.Sprintf("%d.db", port))
	// os.Remove(dbPath)
	db, err := storage.OpenDatabase(dbPath)
	if err != nil {
		log.Info().Err(err)
		return nil, err
	}

	// db.DeleteBucket(nodeBucket)
	// db.DeleteBucket([]byte("block-bucket"))

	// db.NewBucket([]byte("block-bucket"))
	// db.NewBucket(nodeBucket)

	// if db.HasBucket(fullNodeStateBucket) {
	// 	fmt.Println("Restoring full node")
	// 	n, err := db.Get(nodeBucket, privateKeyKey)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	var nodeOld Node
	// 	err = json.Unmarshal(n, &nodeOld)
	// 	if err != nil {
	// 		return nil, err
	// 	}
	// 	return &nodeOld, nil
	// }

	// Retrieve blockchain if it exists in database.
	var bc *core.Blockchain
	if db.HasBucket(core.BlockBucket) {
		fmt.Println("Started loading bc")
		bc, err = core.LoadBlockchain(db)

		if bc != nil {
			localSync = true
		}
		if err != nil {
			return nil, err
		}
		fmt.Println("Blockchain loaded from db.: ", localSync)
	} else {
		if err := db.NewBucket(core.BlockBucket); err != nil {
			return nil, err
		}
	}
	// Create blockpool.
	bp, err := core.NewBlockpool(core.DefaultBlockpoolCapacity)
	if err != nil {
		return nil, err
	}
	// Retrieve or generate keypair.
	var keys Keypair
	if _, err2 := os.Stat(keysPath + keyFile); err2 == nil {
		//key file exists
		log.Info().Msgf("keysPath + keyFile: %v", keysPath+keyFile)
		keys.PrivateKey = noise.LoadKey(keysPath + keyFile)
		keys.PublicKey = keys.PrivateKey.Public()

	} else if os.IsNotExist(err2) {
		// path/to/whatever does *not* exist
		var err error
		keys.PublicKey, keys.PrivateKey, err = noise.GenerateKeys(nil)
		// fmt.Println("pk:- ", hex.EncodeToString(keys.PublicKey()), "pvk: ", hex.EncodeToString(keys.PrivateKey()))
		// fmt.Println("Nonce: ", keys.Nonce, keys.C1, keys.C2)
		db.DeleteBucket(nodeBucket) //Deleting bucket to make node every time:- TODO:Change it to restore state
		err = db.NewBucket(nodeBucket)
		if err != nil {
			log.Info().Err(err)
			return nil, err
		}

		noise.PersistKey(keysPath+keyFile, keys.PrivateKey)
	}

	if !db.HasBucket(dealBucket) {
		// } else {
		if err := db.NewBucket(dealBucket); err != nil {
			return nil, err
		}
	}

	// Initialize network.
	// logger, err := zap.NewDevelopment(zap.AddStacktrace(zap.PanicLevel))
	// if err != nil {
	// 	panic(err)
	// }
	// defer logger.Sync()
	ntw, err := network.New(host, port, keys.PrivateKey, nil, false)
	if err != nil {
		return nil, err
	}
	//Initializes wallet-> More like creating an account
	// walletMan := wallet.NewWalletManager(keys.PublicKey.String(), []byte("Nagra"), keys, bc)
	walletMan := wallet.NewWalletManager(keys.PublicKey.String())
	//to syncronize to the time central time server
	// log.Info().Msgf("Quering central time server: time.google.com")
	// response, err := ntp.Query("time.google.com")
	// if err != nil {
	// 	return nil, err
	// }
	// Initializing the dpos consensus protocol
	committeeSize := uint32(comSize)
	// newDpos := dpos.New(committeeSize, bc, wallet.GetAddress(), response.ClockOffset)
	newDpos := dpos.New(committeeSize, walletMan.MyWallet.GetAddress(), 0, vtReq, walletMan, bc)

	posConfig := pos.MakeTestConfig()
	posPairing, posPk, posSk, param := pos.KeyGen(8, 16, posConfig) // TODO: make these values variable
	// f, err := os.Create(keysPath + "PosParam.txt")
	// param.WriteTo(f)

	// log.Info().Msgf("param %v pairing %v", param, posPairing)
	newNode := &Node{
		Db:          db,
		Bc:          bc,
		Bp:          bp,
		Wm:          walletMan,
		keys:        keys,
		ntw:         ntw,
		Dpos:        newDpos,
		PosPairing:  posPairing,
		PosConfig:   posConfig,
		PosPk:       posPk,
		PosSk:       posSk,
		PosParam:    param,
		TimeOffset:  0,
		curEpoch:    1,
		incoming:    make(chan Message, incomingChanSize),
		outgoing:    new(sync.Map),
		LocalSync:   localSync,
		Activedeals: make(map[string]protobuf.Deal),
		// LastRoundExpiredDeals: make(map[string]bool),
		// TimeOffset: response.ClockOffset,
		NodeType:         "",
		TransactionNonce: 1,
		QuitNonceUpChan:  make(chan bool, 128),
		PendingTrans:     0,
		Port:             port,
	}

	// newNode.ntw.HandlePeerDisconnection(newNode)
	walletMan.SetTxExecutor(newNode)
	walletMan.SetNode(newNode)

	return newNode, nil
}

// PublicKey returns the node's public key.
func (node *Node) PublicKey() noise.PublicKey {
	return node.keys.PublicKey
}

// PrivateKey returns the node's private key.
func (node *Node) PrivateKey() noise.PrivateKey {
	return node.keys.PrivateKey
}

// // KadIdBytes return the node's comma sperated KadId string in bytes
// func (node *Node) KadIdBytes() []byte {
// 	return node.ntw.GetNodeID().Marshal()
// }

// NumPeers returns the number of peers the node has.
func (node *Node) NumPeers() int {
	return node.ntw.GetNumPeers()
}

// GetPeerAddress returns the address of the peers
func (node *Node) GetPeerAddress() []noise.ID {
	return node.ntw.GetPeerAddrs()
}

//GetNet returns the node network type
func (node *Node) GetNet() *network.Network {
	return node.ntw
}

// //GetPeerKadID returns the peer Kad id by giving the address
// func (node *Node) GetPeerKadID(address string) noise.ID {
// 	return node.ntw.GetPeerKadID(address)
// }

// //GetPeerKadByPK return the id using public key
// func (node *Node) GetPeerKadByPK(pk []byte) noise.ID {
// 	return node.ntw.GetPeerKadID(node.ntw.AddressFromPK(pk)) //node.ntw.GetPeerKadIDUsingPK(pk)
// }

// //GetAddressUsingPublicKey returns the address associated with the public key
// func (node *Node) GetAddressUsingPublicKey(publicKey noise.PublicKey) string {
// 	return node.ntw.AddressFromPK(publicKey)
// }
// //DisconnectWithPeers disconnects with the peers
// func (node *Node) DisconnectWithPeers(ids []noise.ID) {
// 	node.ntw.DisconWithPeers(ids)
// }

// GetNodeID returns the ID of the node
func (node *Node) GetNodeID() noise.ID {
	return node.ntw.GetNodeID()
}

func (node *Node) GetNodeIDBytes() []byte {
	return node.ntw.GetNodeID().Marshal()
}

//GetCurEpoch returns the current epoch number
func (node *Node) GetCurEpoch() int32 {
	return node.curEpoch
}

//SetCurEpoch sets the value of the current epoch
func (node *Node) SetCurEpoch(num int32) {
	node.curEpoch = num
}

// AddOutgoingChan adds an outgoing channel to node. All incoming messages will be redirected to all outoging channels.
func (node *Node) AddOutgoingChan(out chan Message) {
	// node.outgoing = append(node.outgoing, out)
	node.outgoing.Store(out, out)
}

// RemoveOutgoingChan deletes an outgoing chan
func (node *Node) RemoveOutgoingChan(out chan Message) {
	// c, _ := node.outgoing.Load(out)
	// if c == nil {
	// 	return
	// }
	node.outgoing.Delete(out)
}

// Broadcast broadcasts message to the entire network.
func (node *Node) Broadcast(msg Message) {
	// log.Warn().Msgf("Broadcast msg code-length:- %v %v", msg.Code, len(msg.Data))
	node.ntw.Broadcast(msg.Code, msg.Data)
}

// Relay relays message to peer with given ID.
// func (node *Node) Relay(peerID noise.PublicKey, msg Message) { //TODO:ChangeToPK
func (node *Node) Relay(peerID noise.ID, msg Message) { //TODO:ChangeToPK
	// log.Info().Msgf("From %v, TO %v, DATA %v", node.GetNodeID().Address(), peerID.Address(), len(msg.Data))
	// log.Warn().Msgf("Relay msg code-length:- %v %v", msg.Code, len(msg.Data))

	node.ntw.Relay(peerID, msg.Code, msg.Data)
}

func (node *Node) RelayToPB(peerID noise.PublicKey, msg Message) { //TODO:ChangeToPK
	// log.Info().Msgf("From %v, TO %v, DATA %v", node.GetNodeID().Address(), peerID.Address(), len(msg.Data))
	node.ntw.RelayToPB(peerID, msg.Code, msg.Data)
}

// RequestAndResponse broadcast message and exit if receive response from one
func (node *Node) RequestAndResponse(msg Message, f HandlerFunc, filterMsgCode byte) {
	// Temporary response channel
	respChan := make(chan Message, 128)
	node.AddOutgoingChan(respChan)
	defer node.RemoveOutgoingChan(respChan)

	node.Broadcast(msg)
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second) // 3 second timeout
	defer cancel()

	for {
		select {
		case <-ctx.Done():
			return
		case msg := <-respChan:
			if msg.Code == filterMsgCode {
				f(msg) // Add error handling
				return
			}
		}
	}

}

// BootstrapFromFile bootstraps the network from peers stored in the peers file.
func (node *Node) BootstrapFromFile(filepath string) error {
	f, err := os.Open(filepath)
	if err != nil {
		return err
	}
	defer f.Close()

	peerAddrs := []string{}
	r := bufio.NewReader(f)
	for {
		l, err := r.ReadString('\n')
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		peerAddrs = append(peerAddrs, strings.TrimSpace(l))
	}

	log.Info().Msgf("Bootstrap File Peers %v", peerAddrs)
	node.ntw.BootstrapDefault(peerAddrs)
	return nil
}

// PersistPeerAddrs persists the node's peers to file.
func (node *Node) PersistPeerAddrs(filepath string) error {
	f, err := os.Create(filepath)
	if err != nil {
		return err
	}
	defer f.Close()

	for _, addr := range node.ntw.GetPeerAddrs() {
		_, err := f.WriteString(addr.Address + "\n")
		if err != nil {
			return err
		}
	}
	return nil
}

// Listen directs incoming relay and broadcast messages to incoming chan.
func (node *Node) Listen() {
	relayChan := node.ntw.GetRelayChan()
	broadcastChan := node.ntw.GetBroadcastChan()
	for {
		select {
		case msg := <-relayChan:
			node.incoming <- NewMessageFromRelay(msg)
		case msg := <-broadcastChan:
			node.incoming <- NewMessageFromBroadcast(msg)
		}
	}
}

//CloseInc closes the incoming channel
func (node *Node) CloseInc() {
	close(node.incoming)
}

// Distribute distributes messages from the incomming channel to all outgoing channels.
func (node *Node) Distribute() {
	for msg := range node.incoming {
		node.outgoing.Range(func(key, value interface{}) bool {
			c, _ := node.outgoing.Load(key)
			c.(chan Message) <- msg
			return true
		})
	}
}

//BlockChainString converts it into string
func (node *Node) BlockChainString() string {
	return node.Bc.String()
}

//OnDisconnectCleanup cleans up on peer disconnection
func (node *Node) OnDisconnectCleanup(id noise.ID) {
	//Donot remove the commeted code!
	// pAdd := id.Address()
	// pPk := id.PublicKey()

	// node.Dpos.State.DelCandidate(pAdd)
	// node.Dpos.State.DelCandidate(string(pPk))
	// fmt.Println("DPOS cleaned up after disconnection")
}

// SetUser sets poster for node
func (node *Node) SetUser(u User) {
	node.User = u
	node.NodeType += "| User |"
}

//SetBootstrapper sets the nodetype tag to bootstrapper
func (node *Node) SetBootstrapper() {
	node.NodeType = "Bootstrapper"
}

//GetHexID return the HexId of the node
func (node *Node) GetHexID() string {
	return node.PublicKey().String()
}

// SetStorageProvider sets storageProvider for node
func (node *Node) SetStorageProvider(sp StorageProvider) {
	node.StorageProvider = sp
	node.NodeType += "| Storage Provider |"
}

// SetMiner sets miner for node
func (node *Node) SetMiner(m Miner) {
	node.Miner = m
	node.NodeType += "| Miner |"
}

func (node *Node) SetAndStartMiner(m Miner) {
	node.Miner = m
	node.NodeType += "| Miner |"
	go node.Miner.CreateBlockLoop()
	// <-doneSync //If syncronization is done then only start dpos
	// err := node.Services.StartDpos(comSize)
	// if err != nil {
	// 	log.Error().Err(err)
	// }
	// if node.Bc != nil && node.Dpos.State.CheckCandByAdd(hex.EncodeToString(node.GetNodeID().PublicKey())) == false { //If first block has been created than only register with the explicit delegateRegister transaction
	if node.Bc != nil && node.Dpos.State.CheckCandByAdd(node.keys.PublicKey.String()) == false { //If first block has been created than only register with the explicit delegateRegister transaction
		_, err := node.Services.DoDelegateRegisteration() //Does the automatic delegate registration if the falg is provided
		if err != nil {
			log.Info().Msgf(err.Error())
		}
	}
	// if (*indexerFlag) != false {
	// 	indexer = roles.NewIndexer(node)
	// 	go indexer.Process()
	// 	log.Info().Msgf("Indexer process has started")
	// 	indexer.StartServer(syncer, indexerPort)
	// }

	go func() {
		for {
			select {
			case <-node.Dpos.ListeningSignal:
				ctx, _ := context.WithTimeout(context.Background(), (node.Dpos.GetCommitteeTime() - 2*1e9)) //The time should be equivalent to the miner timeslot
				fmt.Println("Starting listening", node.Dpos.GetCommitteeTime()-2*1e9)
				node.Miner.StartListening(ctx)
			case <-node.Dpos.MiningSignal:
				// ctx, _ := context.WithTimeout(context.Background(), (node.Dpos.GetMinerSlotTime() - node.Dpos.GetTickInterval())) //The time should be equivalent to the miner timeslot
				// defer cancel()
				node.Miner.StartMining()
			}
		}
	}()
	log.Info().Msgf("Miner started")
}

func (node *Node) SetAndStartBandwidthProvider(bp BandwidthProvider) {
	node.BandwidthProvider = bp
	node.NodeType += "| Bandwidth Provider |"
	go node.BandwidthProvider.Setup()
	go node.BandwidthProvider.Process()
	// go node.BandwidthProvider.GetThreads()
	log.Info().Msgf("Bandwidth started")
}

func (node *Node) SetAndStartStorageProvider(sp StorageProvider) {
	node.StorageProvider = sp
	go node.StorageProvider.Process()
	node.NodeType += "| Storage Provider |"
	log.Info().Msgf("Storage Provider started")
}

func (node *Node) SetAndStartPoster(u User) {
	node.User = u
	node.NodeType += "| User |"
	go node.User.Setup()
	go node.User.Process()
	log.Info().Msgf("Poster started")
}

// SettBandwidthProvider sets bandwidth Provider for node
func (node *Node) SetBandwidthProvider(bp BandwidthProvider) {
	node.BandwidthProvider = bp
	node.NodeType += "| Bandwidth Provider |"
}

func (node *Node) SetService(s Services) {
	node.Services = s
}

//SetSyncer sets the syncer interface in the node
func (node *Node) SetSyncer(syncer Syncer) {
	node.Syncer = syncer
}

func (node *Node) GetNodeType() string {
	return node.NodeType
}

//FindCandByAdd checks if the candidate is present in the state or not
func (node *Node) FindCandByAdd(add string) bool {
	return node.Dpos.State.CheckCandByAdd(add)
}

//EmptyRoleCaches empties the caches of the sp and miner
func (node *Node) EmptyRoleCaches() {
	//EMpting the caches of the miner
	if node.Miner != nil {
		log.Info().Msgf("Emptying the caches of the miner")
		node.Miner.EmptyCaches()
	}
	if node.StorageProvider != nil {
		log.Info().Msgf("Emptying the caches of the SP")
		node.StorageProvider.EmptyCaches()
	}

}

//ApplyDigestBlock applies the dogest block to the node
func (node *Node) ApplyDigestBlock(block *core.Block) error {
	fmt.Println("Applying the digest block")

	isValid := block.IsValid()
	if isValid != true {
		log.Info().Msgf("Block is not valid. Returning")
		return errors.New("Invalid block")
		// return errors.New("Block is not valid")
	}

	//Finding the time left in the current committee
	if node.Dpos == nil || node.Dpos.HasStarted == false {
		node.Wm.ResetWalletManagerState(core.TestEpoch.RollBackPercentage) //If user is not running DPOS. Will be called by the syncer
	} else {
		//For user who is running dpos
		// breakTime := node.Dpos.GetComDuration() + node.Dpos.GetMinerSlotTime() //Break time
		// fmt.Println("Break time: ", breakTime)
		//Stopping the DPOS
		// node.Dpos.Stop()

		// var wg sync.WaitGroup
		// go func() {
		// wg.Add(1)
		// _, timeLeft := node.Syncer.TimeLeftInRound(block)
		// if timeLeft == node.Dpos.RoundToleranceTime() { //Meaning this is the last block in the round
		log.Info().Msgf("Clean the caches and trim blockchain while the node sleeps b/w epochs!!!")
		node.Wm.ResetWalletManagerState(core.TestEpoch.RollBackPercentage) //Resetting the wallet states
		// node.Dpos.BootstrapCommittee(block.GetMinersFromDigestBlock())     //Bootstraps the committee again (after an epoch comes to the end)
		//If u are storage provider clean your caches as well

		// }

		// 	wg.Done()
		// }()
		// wg.Wait()
		// time.Sleep(breakTime)
		// log.Info().Msgf("Restarting dpos with previous committee!!!")
		// node.Dpos.Start()
	}

	return nil
}

func (node *Node) Setup(services Services, syncer Syncer, bootstrapPath string, isMiner bool) {
	node.BootstrapFromFile(bootstrapPath)
	node.Services = services
	node.Syncer = syncer
	// doneSync := make(chan struct{})

	go node.Listen()
	go node.Distribute()
	go node.Syncer.Process()
	go node.Services.Process()
	// go node.Syncer.SyncBlockchainAtStart(blocksToAskFor, doneSync)
	node.Syncer.SyncBlockchain()
	// <-doneSync
	// go node.CheckExpiredDeals()
	// go node.Syncer.CheckBpForPending()
	// node.Services.StartDpos(comSize, isMiner) //Sending minerRole as false //TODO: @Bilal review
}

//SetupWithoutSync does the setup with out the sync
func (node *Node) SetupWithoutSync(services Services, syncer Syncer, bootstrapPath string, isMiner bool) {
	node.BootstrapFromFile(bootstrapPath)
	time.Sleep(1 * time.Second) // wait for boostrap
	node.Services = services
	node.Syncer = syncer
	// doneSync := make(chan struct{})

	go node.Listen()
	go node.Distribute()
	go node.Syncer.Process()
	go node.Services.Process()
	// go node.Syncer.SyncBlockchainAtStart(blocksToAskFor, doneSync)
	// <-doneSync
	// go node.Syncer.CheckBpForPending()
	// node.Services.StartDpos(comSize, isMiner) //Sending minerRole as false //TODO: @Bilal review
}

func (node *Node) SyncTransActionNonceAfterTenSec() {
	log.Info().Msgf("Sleeping for 10 sec to update nonce later..")
	time.Sleep(10 * time.Second)
	confirmedNonce := node.Wm.MyWallet.GetNonce()
	log.Info().Msgf("Temp Nonce: %v, Confirmed Nonce: %v ", node.TransactionNonce, confirmedNonce)

	select {
	case <-node.QuitNonceUpChan:
		log.Info().Msgf("Not updating NONCE")
		return
	default:
		log.Info().Msgf("Updating NONCE")
		node.TransactionNonce = confirmedNonce + 1
		node.PendingTrans = 0
	}

}

func (node *Node) AddDeals(deals []*protobuf.Deal) {
	// fmt.Printf("\t\tNode - AddDeals node.DealMtx.Lock()\n")
	node.DealMtx.Lock()
	// defer node.DealMtx.Unlock()
	for _, deal := range deals {
		dealBytes, _ := proto.Marshal(deal)
		dealhashBytes := crypto.Sha256(dealBytes)
		node.Activedeals[string(dealhashBytes)] = *deal
		node.Db.Put(dealBucket, dealhashBytes, dealBytes)
	}
	node.DealMtx.Unlock()
	// fmt.Printf("\t\tNode - AddDeals node.DealMtx.Unlock()\n")
}

func (node *Node) RemoveDeals(deals []*protobuf.Deal) {
	// fmt.Printf("\t\tNode - RemoveDeals node.DealMtx.Lock()\n")
	node.DealMtx.Lock()
	// defer node.DealMtx.Unlock()
	for _, deal := range deals {
		dealBytes, _ := proto.Marshal(deal)
		dealhashBytes := crypto.Sha256(dealBytes)
		delete(node.Activedeals, string(dealhashBytes))
		node.Db.Delete(dealBucket, dealhashBytes)
	}
	node.DealMtx.Unlock()
	// fmt.Printf("\t\tNode - RemoveDeals node.DealMtx.Unlock()\n")
}

func (node *Node) GetDealbyHash(dealHash string) *protobuf.Deal {
	// fmt.Printf("\t\tNode - GetDealbyHash node.DealMtx.RLock()\n")
	node.DealMtx.RLock()
	defer node.DealMtx.RUnlock()
	if deal, ok := node.Activedeals[dealHash]; ok {
		// node.DealMtx.RUnlock()
		// fmt.Printf("\t\tNode - GetDealbyHash node.DealMtx.RUnlock()\n")
		return &deal
	}
	dealBytes, err := node.Db.Get(dealBucket, []byte(dealHash))
	if err != nil {
		log.Error().Msgf("Unable to find Deal in db %v", hex.EncodeToString([]byte(dealHash)))
		return nil
	}

	deal := new(protobuf.Deal)
	if err := proto.Unmarshal(dealBytes, deal); err != nil {
		log.Error().Msgf(" dealBytes Unmarshal Error:%v", err)
		return nil
	}

	// node.DealMtx.RUnlock()
	// fmt.Printf("\t\tNode - GetDealbyHash node.DealMtx.RUnlock()\n")
	return deal
}

// func (node *Node) CheckExpiredDeals() {
// 	// var spchan chan struct{} //TODO: move it dpos add chan in DPos
// 	// if node.StorageProvider != nil {
// 	// 	spchan = node.StorageProvider.GetSpBlockTickChan()
// 	// }
// 	for {
// 		// log.Info().Msgf("Node CheckExpiredDeals")
// 		// select {
// 		// case _ = <-node.Dpos.NodeBlockTick:
// 		// 	if node.StorageProvider != nil {
// 		// 		log.Info().Msgf("Sending Sp signal CheckExpiredDeals")
// 		// 		spchan <- struct{}{}
// 		// 	}
// 		time.Sleep(node.Dpos.TimeLeftToTick(time.Now().Unix()))
// 		// fmt.Printf("\t\tNode - CheckExpiredDeals node.DealMtx.Lock()\n")
// 		node.DealMtx.Lock()
// 		fmt.Sprintln("\t Node Removing ExpiredDeals")
// 		for dealHash := range node.LastRoundExpiredDeals {
// 			// if node.StorageProvider != nil {
// 			// 	deal := node.Activedeals[dealHash]
// 			// 	node.StorageProvider.RemoveExpiredDealDataFromCahce(&deal)
// 			// }
// 			delete(node.Activedeals, dealHash)
// 			delete(node.LastRoundExpiredDeals, dealHash)
// 			log.Info().Msgf("Node2 -- Removed Expired DealHahs %v Remaining %v", hex.EncodeToString([]byte(dealHash)), len(node.Activedeals))

// 		}
// 		// node.DealMtx.Unlock()
// 		// fmt.Printf("\t\tNode - CheckExpiredDeals node.DealMtx.UnLock()\n")
// 		currentTime := time.Now().Truncate(time.Second) //.Add(-1 * time.Second) // to make sure the deal with expiry time at exact time is not deleted now
// 		// fmt.Printf("\t\tNode - Expired Deal Time %v\n", currentTime)
// 		// fmt.Printf("\t\tNode - CheckExpiredDeals node.DealMtx.RLock()\n")
// 		// node.DealMtx.RLock()
// 		for dealhash, deal := range node.Activedeals {
// 			ExpiryTime := time.Unix(int64(deal.ExpiryTime), 0) // wait extra for 1 minning round
// 			if currentTime.After(ExpiryTime) {                 // TODO: DPOS time for block window, change it to block window time
// 				// log.Info().Msgf("Node -- Removed Expired DealHahs %v Current Time: %v - ExpiryTime %v ", hex.EncodeToString([]byte(dealhash)), currentTime, ExpiryTime)
// 				// delete(node.Activedeals, dealhash)
// 				node.LastRoundExpiredDeals[dealhash] = true
// 				if node.Miner != nil {
// 					node.Miner.RemoveExpiredDealDataFromCahce(&deal)
// 				}
// 			}
// 		}

// 		node.DealMtx.Unlock()
// 		// node.DealMtx.RUnlock()
// 		// fmt.Printf("\t\tNode - CheckExpiredDeals node.DealMtx.UnLock()\n")
// 		// time.Sleep(1 * time.Second)
// 		// }
// 	}
// }

func (node *Node) GetActiveAndExpiredDealsHash() ([]string, []string) {
	i := 0
	numofdeal := len(node.Activedeals) // - len(node.LastRoundExpiredDeals)
	if numofdeal <= 0 {
		return nil, nil
	}

	dealhashKeys := make([]string, numofdeal)
	// var dealhashKeys1 []string
	var expiredDeal []string
	// fmt.Printf("\t node.Activedeals size %v, numofdeal %v Expired Deals %v\n", len(node.Activedeals), numofdeal, len(node.LastRoundExpiredDeals))
	// fmt.Printf("\t\tNode - GetActiveAndExpiredDealsHash DealMtx.RLock()\n")
	node.DealMtx.RLock()
	// currentTime := time.Now().Add(node.Dpos.TimeLeftToMine(time.Now().Unix()))
	// currentTime := node.Dpos.NextMiningSlotTime(time.Now().Unix()).Truncate(time.Second)
	currentTime := time.Unix(node.Dpos.NextTickTime(time.Now().Unix()), 0).Truncate(time.Second)
	for dealhash, deal := range node.Activedeals {
		if dealhash != "" {
			// if _, ok := node.LastRoundExpiredDeals[dealhash]; !ok {
			dealhashKeys[i] = dealhash
			// dealhashKeys1 = append(dealhashKeys1, dealhash)
			// fmt.Printf("\t GetActiveAndExpiredDealsHash with I %v deal: %v \n", i, hex.EncodeToString([]byte(dealhashKeys[i])))
			i++
			ExpiryTime := time.Unix(int64(deal.ExpiryTime), 0) //.Add(time.Second * 15) // wait extra for 1 minning round
			if currentTime.After(ExpiryTime) {                 // TODO: DPOS time for block window, change it to block window time
				expiredDeal = append(expiredDeal, dealhash)
			}
			// } else {
			// if _, ok := node.LastRoundExpiredDeals[dealhash]; ok {
			// 	log.Info().Msgf("\t Fuck Scene GetActiveAndExpiredDealsHash1 fond in LastRoundExpiredDeals deal: %v len: %v", hex.EncodeToString([]byte(dealhash)), len(node.LastRoundExpiredDeals))
			// }
		} else {
			log.Error().Msgf("GetActiveAndExpiredDealsHash DealHahs empty %v", dealhash)
		}
	}
	node.DealMtx.RUnlock()
	// fmt.Printf("\t\tNode - GetActiveAndExpiredDealsHash DealMtx.RUnLock()\n")
	fmt.Printf("\t\tNode - GetActiveAndExpiredDealsHash node.Activedeals size %v dealhashKeys :%v expire deal: %v I: %v \n", len(dealhashKeys), len(expiredDeal), i)
	// for index, dealhash := range dealhashKeys {
	// 	fmt.Printf("\t GetActiveAndExpiredDealsHash deal: %v %v \n", index, hex.EncodeToString([]byte(dealhash)))
	// }
	// for index, dealhash := range dealhashKeys1 {
	// 	a[i] = a[len(a)-1] // Copy last element to index i.
	// 	a[len(a)-1] = ""   // Erase last element (write zero value).
	// 	a = a[:len(a)-1]   // Truncate slice.
	// 	// fmt.Printf("\t GetActiveAndExpiredDealsHash deal1: %v %v \n", index, hex.EncodeToString([]byte(dealhash)))
	// }
	return dealhashKeys, expiredDeal
}

func (node *Node) RemoveExpiredDealsWhileSync(blocktimestamp int64) {

	currentTime := time.Unix(0, blocktimestamp).Add(-20 * time.Second) // wait extra for 1 minning round
	for dealhash, deal := range node.Activedeals {
		ExpiryTime := time.Unix(int64(deal.ExpiryTime), 0) // wait extra for 1 minning round
		if currentTime.After(ExpiryTime) {                 // TODO: DPOS time for block window, change it to block window time
			delete(node.Activedeals, dealhash)
		}
	}
}

//UpdateThreadBase ThreadBase is a map for all posts on the network
func (node *Node) UpdateThreadBase(b *core.Block) {

	deals := b.GetDeals()
	for _, deal := range deals {

		// toRetrieveFrom := deal.GetSpid() //
		posts := deal.List
		for _, post := range posts {

			rootHash := hex.EncodeToString(post.Info.GetMetadata().GetThreadRootPostHash())
			postInfobytes, err := gproto.Marshal(post.Info)
			if err != nil {
				log.Error().Msgf("Cannot unmarshal post..continuing to next post in deal")
				continue
			}
			postMetaDataHash := crypto.Sha256(postInfobytes)
			postMetaDataHashStr := hex.EncodeToString(postMetaDataHash)

			// node.postToSp[postMetaDataHashStr] = toRetrieveFrom
			if rootHash == "" { // Root Post
				// Initiate a thread
				// node.ThreadBase[postMetaDataHashStr] = make([]string, 0)
				node.ThreadBaseMap.Store(postMetaDataHashStr, make([]string, 0))

				if node.User != nil {
					go node.User.GetThread(postMetaDataHashStr) // To make retrieval faster for now
				}
			} else {
				// Check if the root post of the reply exists
				// _, ok := node.ThreadBase[rootHash]
				val, ok := node.ThreadBaseMap.Load(rootHash)
				if !ok {
					log.Error().Msgf("Root Post for the reply does not exist..continuing to next post in deal")
					continue // move to nex post
				}
				// Add to the threads' posts
				newval := append(val.([]string), postMetaDataHashStr)
				node.ThreadBaseMap.Store(rootHash, newval)

				if node.User != nil {
					go node.User.GetThread(rootHash) // To make retrieval faster for now
				}
			}

		}

	}
}

// func (node *Node) RemoveExpiredDeals(blocktimestamp int64) {
func (node *Node) RemoveExpiredDeals(blocktimestamp int64, chPrP []*protobuf.ChPrPair) { // TODO: change it deals from block only
	log.Info().Msgf("Node -- RemoveExpiredDeals ")
	// fmt.Printf("\t\tNode - RemoveExpiredDeals DealMtx.Lock()\n")
	currentTime := time.Unix(0, blocktimestamp).Truncate(time.Second)
	node.DealMtx.Lock()
	for _, cProof := range chPrP {
		// fmt.Println("Starting the payment of the verification pair")
		dealHash := string(cProof.Dealhash)
		deal, ok := node.Activedeals[dealHash]
		// deal := node.GetDealbyHash(dealHash)
		// if deal == nil {
		if !ok {
			fmt.Sprintf("RemoveExpiredDeals Reference deal cannot be found %v", hex.EncodeToString([]byte(dealHash)))
			continue
		}
		ExpiryTime := time.Unix(int64(deal.ExpiryTime), 0) // wait extra for 1 minning round
		if currentTime.After(ExpiryTime) {                 // TODO: DPOS time for block window, change it to block window time
			log.Info().Msgf("Node -- Removed Expired DealHahs %v Remaining %v", hex.EncodeToString([]byte(dealHash)), len(node.Activedeals))
			if node.StorageProvider != nil {
				node.StorageProvider.RemoveExpiredDealDataFromCahce(&deal)
			}
			if node.Miner != nil {
				node.Miner.RemoveExpiredDealDataFromCahce(&deal)
			}

			delete(node.Activedeals, dealHash)
		}
	}
	node.DealMtx.Unlock()
	// currentTime := time.Unix(0, blocktimestamp).Truncate(time.Second)
	// for dealhash, deal := range node.Activedeals {
	// 	ExpiryTime := time.Unix(int64(deal.ExpiryTime), 0) // wait extra for 1 minning round
	// 	if currentTime.After(ExpiryTime) {                 // TODO: DPOS time for block window, change it to block window time
	// 		log.Info().Msgf("Node -- Removed Expired DealHahs %v Remaining %v", hex.EncodeToString([]byte(dealhash)), len(node.Activedeals))
	// 		if node.StorageProvider != nil {
	// 			deal := node.Activedeals[dealhash]
	// 			node.StorageProvider.RemoveExpiredDealDataFromCahce(&deal)
	// 		}
	// 		if node.Miner != nil {
	// 			node.Miner.RemoveExpiredDealDataFromCahce(&deal)
	// 		}

	// 		delete(node.Activedeals, dealhash)
	// 	}
	// }
	// fmt.Printf("\t\tNode - RemoveExpiredDeals DealMtx.UnLock()\n")
}

func (node *Node) AddExpiredDealsWhileReverting(chPrP []*protobuf.ChPrPair) { // TODO: change it deals from block only
	log.Info().Msgf("Node -- RemoveExpiredDeals ")
	// fmt.Printf("\t\tNode - RemoveExpiredDeals DealMtx.Lock()\n")
	node.DealMtx.Lock()
	for _, cProof := range chPrP {
		// fmt.Println("Starting the payment of the verification pair")
		dealHash := string(cProof.Dealhash)
		_, ok := node.Activedeals[dealHash]
		// deal := node.GetDealbyHash(dealHash)
		// if deal == nil {
		if !ok {
			dealBytes, err := node.Db.Get(dealBucket, cProof.Dealhash)
			if err != nil {
				log.Error().Msgf("AddExpiredDealsWhileReverting Unable to find Deal in db %v", hex.EncodeToString([]byte(dealHash)))
				continue
				// return
			}

			deal := new(protobuf.Deal)
			if err := proto.Unmarshal(dealBytes, deal); err != nil {
				log.Error().Msgf("AddExpiredDealsWhileReverting dealBytes Unmarshal Error:%v", err)
				continue
				// return nil
			}
			node.Activedeals[dealHash] = *deal
		}
	}
	node.DealMtx.Unlock()
}
func (node *Node) GetPosPairing() *pbc.Pairing {
	return node.PosPairing
}

func (node *Node) DeleteDealsFromDB() {
	node.Db.DeleteBucket(dealBucket)
	err := node.Db.NewBucket(dealBucket)
	if err != nil {
		log.Error().Err(err)
	}
}

func (node *Node) Close() {
	node.ntw.RemovePeers()
	node.ntw.Close()
}

func (node *Node) NewNetwork(host string) {
	ntw, err := network.New(host, node.Port, node.keys.PrivateKey, nil, false)
	if err != nil {
		log.Error().Msgf("NewNetwork network Error:%v", err)
	}
	node.ntw = ntw
	go node.Listen()
}
