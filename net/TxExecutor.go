package net

import (
	"encoding/hex"

	"github.com/guyu96/go-timbre/log"
	pb "github.com/guyu96/go-timbre/protobuf"
)

//ExecuteRegisterCandidate executes the regster candidate transaction
func (n *Node) ExecuteRegisterCandidate(tx *pb.Transaction) error {
	//Check if the transaction type matches with delegate registration
	// if tx.Type != core.TxBecomeCandidate {
	// 	return errors.New("Ivalid transaction type. Not a delegate registration tx!")
	// }
	//Check if the node has the valid balance to make tx
	//  Get resources utilized and do some kind of multiplication
	// resEstimate := core.ResourcesRequired(tx) //Resource estimate
	// txCost := resEstimate.CalculateTotalCost(core.TestRP)

	//Update the wallet.-> By makin payment

	//If successful. Insert candidate in the DPOS
	candID := tx.Body.GetPayerId()
	// if n.Dpos == nil {
	// 	panic("DPOS is nil")
	// }
	// if n.Dpos.State == nil {
	// 	panic("State is nil")
	// }
	_, err := n.Dpos.State.RegisterCand(hex.EncodeToString(candID)) //Treating address as ID for testing
	if err != nil {
		return err
	}
	log.Info().Msgf("Candidate has been registered. %s", hex.EncodeToString(candID))
	return nil
}

//ExecuteQuitCandidate executes the quit candidate transaction
func (n *Node) ExecuteQuitCandidate(tx *pb.Transaction) error {

	// if tx.Type != core.TxQuitCandidate {
	// 	return errors.New("Invalid transaction type")
	// }
	//Update the wallet

	//Removing candidate from all linked ds
	candID := tx.Body.GetPayerId()
	err := n.Dpos.State.QuitCandidate(hex.EncodeToString(candID))
	if err != nil {
		return err
	}

	n.Wm.UnSetCandidacy(hex.EncodeToString(candID))

	return nil
}
