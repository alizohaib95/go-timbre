package net

// import (
// 	"encoding/hex"
// 	"sync"

// 	"github.com/guyu96/go-timbre/core"
// 	"github.com/guyu96/go-timbre/crypto/pos"
// 	"github.com/guyu96/go-timbre/dpos"
// 	"github.com/guyu96/go-timbre/wallet"
// 	"github.com/mbilal92/noise/network"
// 	kad "github.com/mbilal92/noise/skademlia"
// )

// //NewTestNode returns a node object for testing purpose
// func NewTestNode(port uint16, comSize int, vtReq bool) (*Node, error) {
// 	host := "127.0.0.1"
// 	newNode := new(Node) //Initializes the node
// 	localSync := false

// 	var bc *core.Blockchain
// 	// Create blockpool.
// 	bp, err := core.NewBlockpool(core.DefaultBlockpoolCapacity)
// 	if err != nil {
// 		return nil, err
// 	}
// 	// Retrieve or generate keypair.
// 	var keys *kad.Keypair
// 	keys = kad.RandomKeys()
// 	// Initialize network.
// 	ntw, err := network.New(host, port, keys)
// 	if err != nil {
// 		return nil, err
// 	}
// 	//Initializes wallet-> More like creating an account
// 	walletMan := wallet.NewWalletManager(hex.EncodeToString(ntw.GetNodeID().PublicKey()), []byte("Nagra"), keys, newNode.Bc)

// 	// Initializing the dpos consensus protocol
// 	committeeSize := uint32(comSize)
// 	newDpos := dpos.New(committeeSize, walletMan.MyWallet.GetAddress(), 0, vtReq, walletMan)

// 	posConfig := pos.MakeTestConfig()
// 	posPairing, posPk, posSk, param := pos.KeyGen(8, 16, posConfig) // TODO: make these values variable

// 	newNode = &Node{
// 		Db:         nil, //Not required for testing
// 		Bc:         bc,
// 		Bp:         bp,
// 		Wm:         walletMan,
// 		keys:       keys,
// 		ntw:        ntw,
// 		Dpos:       newDpos,
// 		PosPairing: posPairing,
// 		PosConfig:  posConfig,
// 		PosPk:      posPk,
// 		PosSk:      posSk,
// 		PosParam:   param,
// 		TimeOffset: 0,
// 		incoming:   make(chan Message, incomingChanSize),
// 		outgoing:   new(sync.Map),
// 		LocalSync:  localSync,
// 		// TimeOffset: response.ClockOffset,
// 	}
// 	newNode.ntw.HandlePeerDisconnection(newNode)
// 	walletMan.SetTxExecutor(newNode)

// 	return newNode, nil
// }
