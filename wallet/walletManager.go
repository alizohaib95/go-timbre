package wallet

import (
	// "sync"
	"bytes"
	"encoding/hex"
	"errors"
	"fmt"

	"github.com/golang/protobuf/proto"
	"github.com/guyu96/go-timbre/core"
	"github.com/guyu96/go-timbre/crypto"
	"github.com/guyu96/go-timbre/crypto/pos"
	"github.com/guyu96/go-timbre/log"
	pb "github.com/guyu96/go-timbre/protobuf"
	// "github.com/mbilal92/pbc"
)

const (
	name                = "Test"
	blockThresholdToBan = 3
	banDuration         = 100 //No of blocks for which the block is banned
	GenesisRounds       = 1   //No of rounds untill free transactions will be entertained
)

//WalletManager keeps track of wallet state of users
type WalletManager struct {
	// wMutex           sync.Mutex
	walletByAddress map[string]*Wallet //Only updating state of this for now
	// walletByUsername map[string]*Wallet //TODO use this if required
	MyWallet *Wallet
	// Bc                    blockChain
	node     Node
	Executor TxExecutor
	// Activedeals           map[string]pb.Deal // Active Deals from blockchain
	LatChVerificationTime map[string]int64
}

//NewWalletManager returns the new WalletManager object
func NewWalletManager(userAddress string) *WalletManager {
	wm := &WalletManager{
		walletByAddress: make(map[string]*Wallet),
		// walletByUsername: make(map[string]*Wallet),
		// Activedeals:           make(map[string]pb.Deal),
		LatChVerificationTime: make(map[string]int64),

		MyWallet: NewWallet(userAddress),
		// Bc:       blkChain,
	}
	wm.walletByAddress[userAddress] = wm.MyWallet //Populate the walletByAddress with current node's wallet
	return wm
}

//SnapShotWallets takes the snapshot of the wallets
func (wm *WalletManager) SnapShotWallets() *WalletsSnapshot {

	var snapShot = new(WalletsSnapshot)
	snapShot.wallets = []*Wallet{}

	for _, w := range wm.walletByAddress {
		snapShot.wallets = append(snapShot.wallets, w.DeepCopy())
	}

	return snapShot
}

//SnapShotWalletsUsingPb takes the snapshot of the wallets
func (wm *WalletManager) SnapShotWalletsUsingPb() *PbWalletsSnapShot {

	var snapShot = new(PbWalletsSnapShot)
	snapShot.PbWallets.Wallets = []*pb.Wallet{}

	for _, w := range wm.walletByAddress {
		snapShot.PbWallets.Wallets = append(snapShot.PbWallets.Wallets, w.DeepCopyToPbWallet())
	}

	return snapShot
}

//TotalWallets returns the total wallet by address
func (wm *WalletManager) TotalWallets() int {
	return len(wm.walletByAddress)
}

//GetWalletByAddress returns the wallet instance by key
func (wm *WalletManager) GetWalletByAddress(sampleAddress string) (*Wallet, error) {

	if wallet, ok := wm.walletByAddress[sampleAddress]; ok {
		return wallet, nil
	}
	str := sampleAddress + " wallet doesn't exist in the record,Can't fetch"
	return nil, errors.New(str)
}

//SetTxExecutor Sets the tx executor
func (wm *WalletManager) SetTxExecutor(executor TxExecutor) {
	wm.Executor = executor
}

//SetNode Sets the node
func (wm *WalletManager) SetNode(node Node) {
	wm.node = node
}

//SetCandidacy sets the candidacy of the wallet
func (wm *WalletManager) SetCandidacy(pk string) error {
	w, err := wm.GetWalletByAddress(pk)
	if err != nil {
		return err
	}
	w.SetCandidate(true)
	return nil
}

//UnSetCandidacy sets the candidacy of the wallet
func (wm *WalletManager) UnSetCandidacy(pk string) error {
	w, err := wm.GetWalletByAddress(pk)
	if err != nil {
		return err
	}
	w.SetCandidate(false)
	return nil
}

// //GetWalletByUsername returns the wallet instacne by username
// func (wm *WalletManager) GetWalletByUsername(sampleUsername string) (*Wallet, error) {

// 	if wallet, ok := wm.walletByUsername[sampleUsername]; ok {
// 		return wallet, nil
// 	}
// 	return nil, errors.New("The given username's wallet doesn't exist in the record. Can't fetch")
// }

//PutWalletByAddress inserts the wallet entry by Address
func (wm *WalletManager) PutWalletByAddress(sampleAddress string, sampleWallet *Wallet) error {

	if _, ok := wm.walletByAddress[sampleAddress]; ok {
		return errors.New("wallet entry already present")
	}
	wm.walletByAddress[sampleAddress] = sampleWallet

	return nil
}

// //PutWalletByUsername inserts the wallet entry by Username
// func (wm *WalletManager) PutWalletByUsername(sampleUsername string, sampleWallet *Wallet) error {

// 	if _, ok := wm.walletByUsername[sampleUsername]; ok {
// 		return errors.New("wallet entry already present")
// 	}
// 	wm.walletByUsername[sampleUsername] = sampleWallet

// 	return nil
// }

// //DelWalletByAddress deletes the entry from map by address
// func (wm *WalletManager) DelWalletByAddress(sampleAddress string) error {

// 	if _, ok := wm.walletByAddress[sampleAddress]; ok {
// 		delete(wm.walletByAddress, sampleAddress)
// 		return nil
// 	}
// 	return errors.New("No entry to delete")
// }

// //DelWalletByUsername deletes the entry from map by username
// func (wm *WalletManager) DelWalletByUsername(sampleUsername string) error {

// 	if _, ok := wm.walletByUsername[sampleUsername]; ok {
// 		delete(wm.walletByUsername, sampleUsername)
// 		return nil
// 	}
// 	return errors.New("No entry to delete")
// }

//CheckEntryByAddress returns true if entry is already present
func (wm *WalletManager) CheckEntryByAddress(sampleAddress string) bool {

	if _, ok := wm.walletByAddress[sampleAddress]; ok {
		return true
	}

	return false
}

//RevertWalletStates reverts the state of all the wallets in WalletByAddress
func (wm *WalletManager) RevertWalletStates(block *core.Block, minerAdd string) error {
	// fmt.Println("Updating wallet states")
	// isValid := block.IsValid()
	// if isValid != true {
	// 	return errors.New("Block is not valid")
	// }
	log.Info().Msgf("RevertWalletStates Reverting block...")
	mWallet, err := wm.GetWalletByAddress(minerAdd) //for miner
	if err != nil {
		return err
	}

	if block.IsEmpty() {
		mWallet.balance -= wm.RewardForEmptyBlock()
		mWallet.blockReward -= wm.RewardForEmptyBlock()
		wm.ReAssignVotePower(mWallet, 0-int64(wm.RewardForEmptyBlock()))
	} else {
		mWallet.balance -= wm.RewardForBlockCreation()
		mWallet.blockReward -= wm.RewardForBlockCreation()
		wm.ReAssignVotePower(mWallet, 0-int64(wm.RewardForBlockCreation()))
	}

	if block.IsDigestBlock() {
		wm.UndoDigestReset(core.TestEpoch.RollBackPercentage)

		// mWallet.balance -= wm.RewardForBlockCreation() //Taking away the reward for the block creation
		// mWallet.blockReward -= wm.RewardForBlockCreation()
		// wm.ReAssignVotePower(mWallet, int64(wm.RewardForBlockCreation()))
		return nil
	}

	transactions := block.GetTransactions()
	err = wm.RevertTransactions(transactions, mWallet, block.GetRoundNum())
	if err != nil {
		return err //Should never hit this
	}

	deals := block.GetDeals()
	err = wm.RevertDeals(deals, mWallet, block.GetRoundNum())
	if err != nil {
		return err //Should never hit this
	}

	podfs := block.GetPodf()
	err = wm.RevertPodf(podfs, mWallet, block.GetRoundNum())
	if err != nil {
		return err //Should never hit this
	}

	votes := block.GetVotes()
	err = wm.RevertVotes(votes, mWallet, block.GetRoundNum())
	if err != nil {
		return err //Should never hit this
	}

	chPrPair := block.GetChPrPair()
	err = wm.RevertChPrPair(chPrPair) //Handle the verifications
	if err != nil {
		return err //Should never hit this as well
	}

	return nil
}

//ValidateBlockTx validate if the transaction can be applied
func (wm *WalletManager) ValidateBlockTx(block *core.Block) (bool, error) {

	// isValid := block.IsValid()
	// if isValid != true {
	// 	return false, errors.New("Block is not valid")
	// }
	transactions := block.GetTransactions()
	for _, tx := range transactions {
		valid, err := wm.IsExecutable(tx, block.GetRoundNum()) //validating retrieval,becomeCand,QuitCandidate transaction
		if valid == false || err != nil {
			fmt.Println("Err is: ", err.Error())
			return false, err
		}
	}

	votes := block.GetVotes() //Validating votes
	for _, vote := range votes {

		valid, err := wm.ValidateVote(vote, block.GetRoundNum())
		if err != nil || valid == false {
			return valid, err
		}
		// voterID := vote.GetId()
		// txFee := core.GetTxFee(vote)
		// vWallet, err := wm.GetWalletByAddress(voterID) //Fetching the voter wallet
		// if err != nil {
		// 	return false, errors.New("Voter wallet doesn't exist")
		// }
		// if vWallet.balance < txFee {
		// 	return false, errors.New("Insufficient balance to pay vote fee")
		// }
		// for _, votedCand := range vote.VoteCand {
		// 	action, percent, candID, err := ParseVote(votedCand)
		// 	if err != nil {
		// 		return false, err
		// 	}
		// 	if action == "-" {
		// 		err = vWallet.CheckUnvoteValidity(percent, candID)
		// 		if err != nil {
		// 			return false, err
		// 		}
		// 	} else {
		// 		err = vWallet.CheckVoteValidity(percent, candID)
		// 		if err != nil {
		// 			return false, err
		// 		}
		// 	}
		// }
	}

	podfs := block.GetPodf()
	for _, podf := range podfs {
		err := wm.ValidatePodf(podf, block.GetRoundNum())
		if err != nil {
			return false, err
		}
	}

	deals := block.GetDeals() //Validate the deals in the block
	for _, deal := range deals {
		err := wm.ValidateDeal(deal, block.GetRoundNum())
		if err != nil {
			return false, err
		}
	}

	return true, nil
}

//ValidatePodf validates the podf
func (wm *WalletManager) ValidatePodf(podf *pb.PoDF, bRound int64) error {
	h1 := podf.Header1
	h2 := podf.Header2

	if h1.Height != h2.Height { //Heights should be same
		return errors.New("Heights doesn't match in podf")
	}
	if bytes.Equal(h1.Hash, h2.Hash) == true { //Headers hash should be different
		return errors.New("Both headers have the same hash")
	}
	err := core.ValidateHeaderSign(h1) //Validating signatures of header 1
	if err != nil {
		return err
	}
	err = core.ValidateHeaderSign(h2) //Validating signatures of header 2
	if err != nil {
		return err
	}

	//Checking if reported miner is already a candidate
	minerAdd := hex.EncodeToString(h1.GetMinerPublicKey())
	mWallet, err := wm.GetWalletByAddress(minerAdd)
	if err != nil {
		return err
	}

	if mWallet.isCandidate == false {
		return errors.New("Reported miner is not a candidate")
	}

	// Checking if the tx-Maker has the balance to pay podf tx
	txFee := core.GetTxFee(podf)
	if bRound <= GenesisRounds {
		txFee = 0 //Free transactions for the genesis round
	}
	makerPk := hex.EncodeToString(podf.GetPublicKey())
	txMaker, err := wm.GetWalletByAddress(makerPk)
	if err != nil {
		wm.PutWalletByAddress(makerPk, NewWallet(makerPk))
		txMaker, err = wm.GetWalletByAddress(makerPk)
	}
	if txMaker.balance < txFee {
		return errors.New("Unable to pay txFee")
	}

	//Checking if the reported miner is already banned till the reported height
	if mWallet.bannedTill >= h1.GetHeight() {
		return errors.New("Reported miner is already banned")
	}

	//check if the user has already been punished at the given height
	for i := len(mWallet.podfHeights) - 1; i >= 0; i-- {
		if mWallet.podfHeights[i] == h1.Height {
			return errors.New("Misbehaving miner is already reported at the given height")
		} else if mWallet.podfHeights[i] < (h1.Height - 21) {
			break //No need to look further
		}
	}
	fmt.Println("PODF has been validated")
	return nil
}

//ValidateVote validates the vote transaction
func (wm *WalletManager) ValidateVote(vote *pb.Vote, bRound int64) (bool, error) {
	voterID := vote.GetId()
	txFee := core.GetTxFee(vote)
	if bRound <= GenesisRounds {
		txFee = 0 //No transaction fee for the genesis rounds
	}
	vWallet, err := wm.GetWalletByAddress(voterID) //Fetching the voter wallet
	if err != nil {
		wm.PutWalletByAddress(voterID, NewWallet(voterID))
		vWallet, _ = wm.GetWalletByAddress(voterID) // creating new wallet with new vote
		// return false, errors.New("Voter wallet doesn't exist")
	}
	if vWallet.balance < txFee {
		return false, errors.New("Insufficient balance to pay vote fee")
	}
	for _, votedCand := range vote.VoteCand {
		action, percent, candID, err := ParseVote(votedCand)
		if err != nil {
			return false, err
		}
		if action == "-" {
			err = vWallet.CheckUnvoteValidity(percent, candID)
			if err != nil {
				return false, err
			}
		} else {
			err = vWallet.CheckVoteValidity(percent, candID)
			if err != nil {
				return false, err
			}
		}
	}
	return true, nil
}

// //GenesisDelegateReg handles the registration made in genesis block
// func (wm *WalletManager) GenesisDelegateReg(block *core.Block) error {
// 	transactions := block.GetTransactions()

// 	for _, tx := range transactions {

// 		if txType := tx.GetType(); txType == core.TxBecomeCandidate {
// 			candAdd := hex.EncodeToString(tx.GetBody().GetPayerId())
// 			log.Info().Msgf("Adding G-reg:- %v", candAdd)
// 			//For payer
// 			if wm.CheckEntryByAddress(candAdd) == false { //Not present in map
// 				wm.PutWalletByAddress(candAdd, NewWallet(candAdd))
// 			}
// 			_ = wm.Executor.ExecuteRegisterCandidate(tx) //Ignoring already present error becasue some nodes will register by announcement

// 			// if err != nil {
// 			// 	log.Info().Err(err)
// 			// }
// 		}
// 	}
// 	return nil
// }

//ApplyGenesis update the wallet with Genesis block- For now it only entertains delegate reg transaction however other can be added easily
func (wm *WalletManager) ApplyGenesis(b *core.Block) error {
	fmt.Println("Applying Genesis...")
	minerAdd := b.MinerPublicKey().String()
	wm.PutWalletByAddress(minerAdd, NewWallet(minerAdd)) //Putting miner in map if not present
	wm.SetCandidacy(minerAdd)
	mWallet, err := wm.GetWalletByAddress(minerAdd) //for miner
	if err != nil {
		return err
	}

	transactions := b.GetTransactions()
	for _, tx := range transactions {
		if tx.GetType() == core.TxBecomeCandidate {
			wm.ApplyDelRegTxForGenesis(tx, 0, b.GetRoundNum())
		}
	}
	// wm.GenesisDelegateReg(b)

	if b.IsEmpty() {
		mWallet.balance += wm.RewardForEmptyBlock()
		mWallet.blockReward += wm.RewardForEmptyBlock()
		wm.ReAssignVotePower(mWallet, int64(wm.RewardForEmptyBlock()))

	} else {
		mWallet.balance += wm.RewardForBlockCreation()
		mWallet.blockReward += wm.RewardForBlockCreation()
		wm.ReAssignVotePower(mWallet, int64(wm.RewardForBlockCreation()))
	}

	return nil
}

// //UpdateWalletStates updates the state of all the wallets in WalletByAddress
// func (wm *WalletManager) UpdateWalletStates(block *core.Block, minerAdd string) error {
// 	// log.Info().Msgf("Updating wallet states")
// 	// isValid := block.IsValid()
// 	// if isValid != true {
// 	// 	return errors.New("Block is not valid")
// 	// }

// 	wm.PutWalletByAddress(minerAdd, NewWallet(minerAdd)) //Putting miner in map if not present
// 	wm.SetCandidacy(minerAdd)
// 	mWallet, err := wm.GetWalletByAddress(minerAdd) //for miner
// 	if err != nil {
// 		return err
// 	}

// 	if block.IsDigestBlock() {
// 		log.Info().Msgf("Wallet state has been resetted")
// 		wm.ResetWalletManagerState(core.TestEpoch.RollBackPercentage)
// 		mWallet.balance += wm.RewardForBlockCreation() //Getting the reward for the block creation
// 		mWallet.blockReward += wm.RewardForBlockCreation()
// 		wm.ReAssignVotePower(mWallet, int64(wm.RewardForBlockCreation()))
// 		return nil
// 	}

// 	transactions := block.GetTransactions()
// 	err = wm.ApplyTransactions(transactions, mWallet, block.GetRoundNum())
// 	if err != nil {
// 		return err
// 	}

// 	deals := block.GetDeals()
// 	err = wm.applyDeals(deals, mWallet, block.GetRoundNum())
// 	if err != nil {
// 		return err
// 	}

// 	chPrPair := block.GetChPrPair()
// 	err = wm.ApplyChPrPair(chPrPair) //Handle the verifications
// 	if err != nil {
// 		return err
// 	}

// 	podfs := block.GetPodf()
// 	err = wm.ApplyPodf(podfs, mWallet, block.GetRoundNum())
// 	if err != nil {
// 		return err
// 	}

// 	if minerAdd == wm.MyWallet.address {
// 		// fmt.Println("Received reward for block Creation")
// 	}

// 	if block.IsEmpty() {
// 		mWallet.balance += wm.RewardForEmptyBlock()
// 		mWallet.blockReward += wm.RewardForEmptyBlock()
// 		wm.ReAssignVotePower(mWallet, int64(wm.RewardForEmptyBlock()))

// 	} else {
// 		// if block.IsDigestBlock() {
// 		// 	mWallet.balance += 10 * wm.RewardForBlockCreation() //More reward for the epoch block
// 		// 	mWallet.blockReward += 10 * wm.RewardForBlockCreation()
// 		// 	wm.ReAssignVotePower(mWallet, int64(10*wm.RewardForBlockCreation()))

// 		// } else {
// 		mWallet.balance += wm.RewardForBlockCreation()
// 		mWallet.blockReward += wm.RewardForBlockCreation()
// 		wm.ReAssignVotePower(mWallet, int64(wm.RewardForBlockCreation()))
// 		// }

// 	}
// 	// wm.CheckExpiredDeals()
// 	return nil
// }

//CheckAndUpdateWalletStates updates the state of all the wallets in WalletByAddress
func (wm *WalletManager) CheckAndUpdateWalletStates(block *core.Block, minerAdd string) error {
	log.Info().Msgf("Checking Updating wallet states")

	// isValid := block.IsValid()
	// if isValid != true {
	// 	return errors.New("Block is not valid")
	// }

	wm.PutWalletByAddress(minerAdd, NewWallet(minerAdd)) //Putting miner in map if not present
	wm.SetCandidacy(minerAdd)                            //Check if you need to unset candidacy as well
	mWallet, err := wm.GetWalletByAddress(minerAdd)      //for miner
	if err != nil {
		return err
	}

	if block.IsEmpty() { //Giving reward to the miner for the block creation
		mWallet.balance += wm.RewardForEmptyBlock()
		mWallet.blockReward += wm.RewardForEmptyBlock()
		wm.ReAssignVotePower(mWallet, int64(wm.RewardForEmptyBlock()))

	} else {
		// if block.IsDigestBlock() {
		// 	mWallet.balance += 10 * wm.RewardForBlockCreation() //More reward for the epoch block
		// 	mWallet.blockReward += 10 * wm.RewardForBlockCreation()
		// 	wm.ReAssignVotePower(mWallet, int64(10*wm.RewardForBlockCreation()))

		// } else {
		mWallet.balance += wm.RewardForBlockCreation()
		mWallet.blockReward += wm.RewardForBlockCreation()
		wm.ReAssignVotePower(mWallet, int64(wm.RewardForBlockCreation()))
		// }
	}

	if block.IsDigestBlock() {
		log.Info().Msgf("Wallet state has been resetted")
		wm.ResetWalletManagerState(core.TestEpoch.RollBackPercentage)
		// mWallet.balance += wm.RewardForBlockCreation() //Getting the reward for the block creation
		// mWallet.blockReward += wm.RewardForBlockCreation()
		// wm.ReAssignVotePower(mWallet, int64(wm.RewardForBlockCreation()))
		return nil
	}

	transactions := block.GetTransactions()
	appliedTxs, err := wm.ApplyTransactions(transactions, mWallet, block.GetRoundNum())
	if err != nil {
		fmt.Println("Faulty tx found. Reverting block...")
		err1 := wm.InitiateRevertBlock(appliedTxs, 0, 0, 0, 0, block)
		if err1 != nil {
			log.Error().Msgf(err.Error())
		}
		return err
	}

	deals := block.GetDeals()
	appliedDeals, err := wm.applyDeals(deals, mWallet, block.GetRoundNum())
	if err != nil {
		fmt.Println("Faulty deal found. Reverting block...")
		err1 := wm.InitiateRevertBlock(len(transactions), appliedDeals, 0, 0, 0, block)
		if err1 != nil {
			log.Error().Msgf(err.Error())
		}
		return err
	}

	chPrPair := block.GetChPrPair()                  // no error in case if it fails
	appliedProofs, err := wm.ApplyChPrPair(chPrPair) //Handle the verifications

	if err != nil {
		fmt.Println("Faulty proof found. Reverting block...")
		err1 := wm.InitiateRevertBlock(len(transactions), len(deals), appliedProofs, 0, 0, block)
		if err1 != nil {
			log.Error().Msgf(err.Error())
		}
		return err
	}

	podfs := block.GetPodf()
	appliedPodfs, err := wm.ApplyPodf(podfs, mWallet, block.GetRoundNum())
	if err != nil {
		fmt.Println("Faulty podf found. Reverting block...")
		err1 := wm.InitiateRevertBlock(len(transactions), len(deals), len(chPrPair), appliedPodfs, 0, block)
		if err1 != nil {
			log.Error().Msgf(err.Error())
		}

		return err
	}

	votes := block.GetVotes()
	appliedVotes, err := wm.ApplyVotes(votes, mWallet, block.GetRoundNum())
	if err != nil {
		fmt.Println("Faulty vote found. Reverting block...")
		err1 := wm.InitiateRevertBlock(len(transactions), len(deals), len(chPrPair), len(podfs), appliedVotes, block)
		if err1 != nil {
			log.Error().Msgf(err.Error())
		}
		return err
	}

	if minerAdd == wm.MyWallet.address {
		// fmt.Println("Reward reverted for block Creation")
	}

	return nil
}

//InitiateRevertBlock initiates the revert block protocol. It creates a new block with only applied txs that need to be reverted
func (wm *WalletManager) InitiateRevertBlock(numTy, numDeal, numProof, numPodf, numVotes int, origBlock *core.Block) error {

	newBlock := new(core.Block)

	var rTrans []*pb.Transaction
	var rDeals []*pb.Deal
	var rProofs []*pb.ChPrPair
	var rPodfs []*pb.PoDF
	var rVotes []*pb.Vote

	transactions := origBlock.GetTransactions()
	for i, tx := range transactions {
		if i >= numTy {
			break
		}
		rTrans = append(rTrans, tx)
	}

	deals := origBlock.GetDeals()
	for i, deal := range deals {
		if i >= numDeal {
			break
		}
		rDeals = append(rDeals, deal)
	}

	proofs := origBlock.GetChPrPair()
	for i, proof := range proofs {
		if i >= numProof {
			break
		}
		rProofs = append(rProofs, proof)
	}

	podfs := origBlock.GetPodf()
	for i, podf := range podfs {
		if i >= numPodf {
			break
		}
		rPodfs = append(rPodfs, podf)
	}
	votes := origBlock.GetVotes()
	for i, vote := range votes {
		if i >= numVotes {
			break
		}
		rVotes = append(rVotes, vote)
	}

	newBlock = core.NewBareBlock(origBlock.ParentHash(), origBlock.MinerPublicKey(), rTrans, rDeals, rProofs, rVotes, rPodfs, 0, origBlock.GetRoundNum())

	err := wm.RevertWalletStates(newBlock, origBlock.MinerPublicKey().String())
	if err != nil {
		log.Error().Msgf(err.Error())
		fmt.Println("Failed to revert block", err.Error())
		return err
	}

	return nil
}

//ApplyPodf validates and applies the podf. In case validation is failed it reports back the number of applied podfs
func (wm *WalletManager) ApplyPodf(podfs []*pb.PoDF, mWallet *Wallet, bRound int64) (int, error) {

	for i, podf := range podfs {

		err := wm.ValidatePodf(podf, bRound) //Validateing the Podf
		if err != nil {
			return i, err
		}

		transactionFee := core.GetTxFee(podf) //Transaction fee for the podf

		if bRound <= GenesisRounds {
			transactionFee = 0 //No transaction fee for the Genesis round
		}

		//Penalize block reward from the miner
		h1 := podf.GetHeader1()
		repMiner := hex.EncodeToString(h1.GetMinerPublicKey()) //Address of the reported miner
		rmWallet, err := wm.GetWalletByAddress(repMiner)
		if err != nil {
			log.Error().Msgf(err.Error()) //This should never hit because it is already validated
		}
		// if rmWallet.balance >= wm.RewardForBlockCreation() {
		rmWallet.balance -= wm.RewardForBlockCreation() // He is penalized full block reward
		rmWallet.blockReward -= wm.RewardForBlockCreation()
		wm.ReAssignVotePower(rmWallet, 0-int64(wm.RewardForBlockCreation()))
		fmt.Println("Misbehaving miner has been punished...")
		// } else {
		// 	minerBal := rmWallet.balance
		// 	rmWallet.balance -= minerBal //Setting it equal to zero
		// 	rmWallet.blockReward -= minerBal
		// 	wm.ReAssignVotePower(rmWallet, 0-int64(minerBal))

		// }

		rmWallet.podfHeights = append(rmWallet.podfHeights, h1.Height)
		if len(rmWallet.podfHeights)%blockThresholdToBan == 0 {
			rmWallet.bannedTill = h1.GetHeight() + banDuration
		}

		//Rewarding the node who reported the misbehaving the miner
		senderAdd := hex.EncodeToString(podf.GetPublicKey())
		if wm.CheckEntryByAddress(senderAdd) == false { //Not present in map
			wm.PutWalletByAddress(senderAdd, NewWallet(senderAdd))
		}
		sWallet, err := wm.GetWalletByAddress(senderAdd)
		if err != nil {
			log.Error().Msgf(err.Error()) //This should never hit because it is already validated
		}
		sWallet.balance += (wm.RewardForPodf() - transactionFee)
		wm.ReAssignVotePower(sWallet, int64(wm.RewardForPodf()-transactionFee))

		mWallet.balance += transactionFee
		wm.ReAssignVotePower(mWallet, int64(transactionFee))
		mWallet.transactionFee += transactionFee

	}

	return 0, nil
}

//RevertPodf reverts the podf
func (wm *WalletManager) RevertPodf(podfs []*pb.PoDF, mWallet *Wallet, bRound int64) error {

	for _, podf := range podfs {

		transactionFee := core.GetTxFee(podf) //Transaction fee for the podf
		if bRound <= GenesisRounds {
			transactionFee = 0 //No transaction fee for the Genesis round
		}

		//Penalize block reward from the miner
		h1 := podf.GetHeader1()
		repMiner := hex.EncodeToString(h1.GetMinerPublicKey()) //Address of the reported miner
		rmWallet, err := wm.GetWalletByAddress(repMiner)
		if err != nil {
			return err
		}
		// if rmWallet.balance >= wm.RewardForBlockCreation() {
		rmWallet.balance += wm.RewardForBlockCreation() // He is penalized full block reward
		rmWallet.blockReward += wm.RewardForBlockCreation()
		wm.ReAssignVotePower(rmWallet, int64(wm.RewardForBlockCreation()))

		// } else {
		// 	minerBal := rmWallet.balance
		// 	rmWallet.balance -= minerBal //Setting it equal to zero
		// 	rmWallet.blockReward -= minerBal
		// 	wm.ReAssignVotePower(rmWallet, 0-int64(minerBal))

		// }
		if len(rmWallet.podfHeights)%blockThresholdToBan == 0 {
			rmWallet.bannedTill = -1 //resetting the banned height
		}

		rmWallet.podfHeights = rmWallet.podfHeights[:len(rmWallet.podfHeights)-1] //Removing the last element

		//Rewarding the node who reported the misbehaving the miner
		senderAdd := hex.EncodeToString(podf.GetPublicKey())
		// if wm.CheckEntryByAddress(senderAdd) == false { //Not present in map
		// 	wm.PutWalletByAddress(senderAdd, NewWallet(senderAdd))
		// }
		sWallet, err := wm.GetWalletByAddress(senderAdd)
		if err != nil {
			return err
		}
		sWallet.balance -= (wm.RewardForPodf() - transactionFee)

		wm.ReAssignVotePower(sWallet, 0-int64(wm.RewardForPodf()-transactionFee))

		mWallet.balance -= transactionFee
		wm.ReAssignVotePower(mWallet, 0-int64(transactionFee))
		mWallet.transactionFee -= transactionFee

	}

	return nil
}

//ApplyChPrPair makes payment of the successful verification of the storage deals
func (wm *WalletManager) ApplyChPrPair(chPrP []*pb.ChPrPair) (int, error) {
	//First validate the CHPrPair

	for i, cProof := range chPrP {
		// fmt.Println("Starting the payment of the verification pair")
		dealHash := string(cProof.Dealhash)
		// var deal pb.Deal
		var ok bool
		// Activedeals := wm.node.GetActiveDealMap()
		// dealMutex := wm.node.GetActiveDealMutex()
		// dealMutex.RLock()
		// if deal, ok = Activedeals[dealHash]; !ok {
		// 	return errors.New(fmt.Sprintf("Reference deal cannot be found %v", dealHash))
		// }
		// dealMutex.RUnlock()
		deal := wm.node.GetDealbyHash(dealHash)
		if deal == nil {
			return i, errors.New(fmt.Sprintf("Reference deal cannot be found %v", hex.EncodeToString(cProof.Dealhash)))
		}
		// deal, bHeight, err := wm.Bc.BinaryDealSearch(cProof.GetDealStartTime())
		// if err != nil {
		// 	return err
		// }
		// if deal == nil {
		// 	deal, bHeight, err = wm.Bc.LinearDealSearch(cProof.GetDealStartTime()) //Does a linear search in case deals in not found(may b bcoz it is not sorted.)
		// 	if deal == nil {
		// 		return errors.New("Reference deal cannot be found")
		// 	}
		// }
		// refDeal, err := getDealFromBlock(block, cProof.GetDealhash())
		// refDeal := deal
		// log.Info().Msgf("WM -- DEAL %v", deal)
		//Validating the CHPrPair
		var LastTs int64
		if LastTs, ok = wm.LatChVerificationTime[dealHash]; !ok {
			LastTs = deal.GetTimestamp()
		}
		wm.LatChVerificationTime[dealHash] = cProof.Timestamp

		valid := wm.CheckChPrPair(deal, cProof, dealHash)
		if !valid {
			log.Info().Msgf("WalletManager: SP failed validation for deal,, Didn't pay SP %v", hex.EncodeToString(cProof.Dealhash)) // TODO: penalize SP
			continue
		}

		// LastTs, err := wm.Bc.FindLastVerification(bHeight, cProof.Dealhash)
		// if err != nil {
		// 	log.Info().Err(err)
		// 	return err
		// }
		// fmt.Println("Last veri Timestamp is: ", LastTs)
		// if LastTs == 0 { //This means it is the first verification
		// 	//Then it should take the timestamp of the deal
		// 	// fmt.Println("Ohh this is the first deal. Taking the deal ts!!!!!!!!!!!!!")
		// 	LastTs = deal.GetTimestamp()
		// }
		curTs := cProof.GetTimestamp() //Current proof timestamp
		if curTs > deal.GetExpiryTime() {
			log.Info().Msgf("GOT verification after the expiray time!!!!!!")
			curTs = deal.GetExpiryTime()
			// delete(wm.Activedeals, dealHash)
			delete(wm.LatChVerificationTime, dealHash)
		}

		// fmt.Println("CurTs:- ", curTs, " last Ts:- ", LastTs)
		wm.VerificationPayment(deal, curTs-LastTs)
	}
	return 0, nil
}

//RevertChPrPair makes payment of the successful verification of the storage deals
func (wm *WalletManager) RevertChPrPair(chPrP []*pb.ChPrPair) error {
	//First validate the CHPrPair

	for _, cProof := range chPrP {
		// fmt.Println("Starting the payment of the verification pair")
		dealHash := string(cProof.Dealhash)
		// var deal pb.Deal
		var ok bool
		// Activedeals := wm.node.GetActiveDealMap()
		// dealMutex := wm.node.GetActiveDealMutex()
		// dealMutex.RLock()
		// if deal, ok = Activedeals[dealHash]; !ok {
		// 	return errors.New(fmt.Sprintf("Reference deal cannot be found %v", dealHash))
		// }
		// dealMutex.RUnlock()
		deal := wm.node.GetDealbyHash(dealHash)
		if deal == nil {
			return errors.New(fmt.Sprintf("Reference deal cannot be found %v", hex.EncodeToString(cProof.Dealhash)))
		}
		// deal, bHeight, err := wm.Bc.BinaryDealSearch(cProof.GetDealStartTime())
		// if err != nil {
		// 	return err
		// }
		// if deal == nil {
		// 	deal, bHeight, err = wm.Bc.LinearDealSearch(cProof.GetDealStartTime()) //Does a linear search in case deals in not found(may b bcoz it is not sorted.)
		// 	if deal == nil {
		// 		return errors.New("Reference deal cannot be found")
		// 	}
		// }
		// refDeal, err := getDealFromBlock(block, cProof.GetDealhash())
		// refDeal := deal
		// log.Info().Msgf("WM -- DEAL %v", deal)
		//Validating the CHPrPair
		var LastTs int64
		if LastTs, ok = wm.LatChVerificationTime[dealHash]; !ok {
			LastTs = deal.GetTimestamp()
		}
		wm.LatChVerificationTime[dealHash] = cProof.Timestamp

		valid := wm.CheckChPrPair(deal, cProof, dealHash)
		if !valid {
			log.Info().Msgf("WalletManager: SP failed validation for deal,, Didn't pay SP %v", hex.EncodeToString(cProof.Dealhash)) // TODO: penalize SP
			continue
		}

		// LastTs, err := wm.Bc.FindLastVerification(bHeight, cProof.Dealhash)
		// if err != nil {
		// 	log.Info().Err(err)
		// 	return err
		// }
		// fmt.Println("Last veri Timestamp is: ", LastTs)
		// if LastTs == 0 { //This means it is the first verification
		// 	//Then it should take the timestamp of the deal
		// 	// fmt.Println("Ohh this is the first deal. Taking the deal ts!!!!!!!!!!!!!")
		// 	LastTs = deal.GetTimestamp()
		// }
		curTs := cProof.GetTimestamp() //Current proof timestamp
		if curTs > deal.GetExpiryTime() {
			log.Info().Msgf("GOT verification after the expiray time!!!!!!")
			curTs = deal.GetExpiryTime()
			// delete(wm.Activedeals, dealHash)
			delete(wm.LatChVerificationTime, dealHash)
		}

		// fmt.Println("CurTs:- ", curTs, " last Ts:- ", LastTs)
		wm.RevertVerificationPayment(deal, curTs-LastTs)
	}
	return nil
}

//CheckChPrPair validates the ChPrPair before making changes to the wallet
func (wm *WalletManager) CheckChPrPair(deal *pb.Deal, cProof *pb.ChPrPair, dealHash string) bool {

	if cProof.Proof == nil {
		// panic("WalletManager: Proof Nil")
		return false
	}

	// pairing, _ := pbc.NewPairingFromString(deal.PosParam)
	pairing := wm.node.GetPosPairing()

	chl := new(pos.Chall)
	chl.FromProto(cProof.Challenge, pairing)
	p := new(pos.Proof)
	p.FromProto(cProof.Proof, pairing)

	// randPrf := pos.GetRandProof(pairing)

	pbKey := new(pos.PublicKey)
	pbKey.FromProto(deal.PublicKey, pairing)
	// fmt.Println("chl: ", chl, " p ", p)
	// fmt.Println("chl: ", chl, " p ", p, " pbKey: ", pbKey)
	valid := pos.Verify([]byte(name), pairing, pbKey, chl, p)
	// if valid {
	// 	log.Info().Msgf("WalletManager: SP valideted for deal %v", hex.EncodeToString([]byte(dealHash)))
	// } else {
	// 	log.Info().Msgf("WalletManager: sp validation failed for deal %v", hex.EncodeToString([]byte(dealHash)))
	// 	panic("WalletManager: sp validation failed for deal")
	// }

	return valid
}

//VerificationPayment is the payment against verified storage time
func (wm *WalletManager) VerificationPayment(deal *pb.Deal, storageDuration int64) error {
	// spID, _ := noise.UnmarshalID(deal.GetKadID())
	// spID := kad.FromString(string(deal.GetKadID()))
	// spAdd := spID.String()
	spAdd := hex.EncodeToString(deal.Spid)

	wm.PutWalletByAddress(spAdd, NewWallet(spAdd))
	posts := deal.List

	for _, post := range posts {
		// posterID := kad.FromString(string((post.GetInfo()).GetMetadata().GetKadID()))
		// posterAdd := posterID.String()
		posterID := post.GetInfo().GetMetadata().GetPid()
		posterAdd := hex.EncodeToString(posterID)
		wm.PutWalletByAddress(posterAdd, NewWallet(posterAdd))
		totalDealTime := deal.GetExpiryTime() - deal.GetTimestamp()

		chargedAmount := float64(post.GetInfo().GetParam().GetMaxCost()) * (float64(storageDuration) / float64(totalDealTime))

		// fmt.Println("Storage duration", storageDuration, "totalDeal Time", totalDealTime, "charged am:-", chargedAmount)
		// log.Info().Msgf("Veri charged amount is!!!!!!!!!!!!!: %v", chargedAmount)
		// pWallet, err := wm.GetWalletByAddress(posterAdd) //for poster
		// if err != nil {
		// 	return err
		// }
		// pWallet.balance -= uint64(chargedAmount)

		sWallet, err := wm.GetWalletByAddress(spAdd) //for storage provider
		if err != nil {
			return err
		}
		sWallet.balance += int64(chargedAmount)
		wm.ReAssignVotePower(sWallet, int64(chargedAmount))

		if spAdd == wm.MyWallet.GetAddress() {
			log.Info().Msgf("I Got payment for storing!!")
		} else {
			log.Info().Msgf("Amount paid to the SP")
		}
	}
	return nil
}

//RevertVerificationPayment is the payment against verified storage time
func (wm *WalletManager) RevertVerificationPayment(deal *pb.Deal, storageDuration int64) error {
	// spID, _ := noise.UnmarshalID(deal.GetKadID())
	// spID := kad.FromString(string(deal.GetKadID()))
	// spAdd := spID.String()
	spAdd := hex.EncodeToString(deal.Spid)

	wm.PutWalletByAddress(spAdd, NewWallet(spAdd))
	posts := deal.List

	for _, post := range posts {
		// posterID := kad.FromString(string((post.GetInfo()).GetMetadata().GetKadID()))
		// posterAdd := posterID.String()
		posterID := post.GetInfo().GetMetadata().GetPid()
		posterAdd := hex.EncodeToString(posterID)
		wm.PutWalletByAddress(posterAdd, NewWallet(posterAdd))
		totalDealTime := deal.GetExpiryTime() - deal.GetTimestamp()

		chargedAmount := float64(post.GetInfo().GetParam().GetMaxCost()) * (float64(storageDuration) / float64(totalDealTime))

		// fmt.Println("Storage duration", storageDuration, "totalDeal Time", totalDealTime, "charged am:-", chargedAmount)
		// log.Info().Msgf("Veri charged amount is!!!!!!!!!!!!!: %v", chargedAmount)
		pWallet, err := wm.GetWalletByAddress(posterAdd) //for poster
		if err != nil {
			return err
		}
		pWallet.balance += int64(chargedAmount)
		wm.ReAssignVotePower(pWallet, int64(chargedAmount))

		sWallet, err := wm.GetWalletByAddress(spAdd) //for storage provider
		if err != nil {
			return err
		}
		sWallet.balance -= int64(chargedAmount)
		wm.ReAssignVotePower(sWallet, int64(0-chargedAmount))

		log.Info().Msgf("Storage amount reverted")

		if spAdd == wm.MyWallet.GetAddress() {
			log.Info().Msgf("my storage payment reverted...")
		}
	}
	return nil
}

func getDealHash(deal *pb.Deal) []byte {
	dealBytes, _ := proto.Marshal(deal)
	dealHash := crypto.Sha256(dealBytes)
	return dealHash
}

func getDealFromBlock(b *core.Block, sampleHash []byte) (*pb.Deal, error) {

	deals := b.GetDeals()

	for _, deal := range deals {
		dealHash := getDealHash(deal)
		if string(dealHash) == string(sampleHash) {
			return deal, nil
		}
	}
	return nil, errors.New("Deal not found in the block")
}

//RevertTransactions reverts all the transactions
func (wm *WalletManager) RevertTransactions(transactions []*pb.Transaction, mWallet *Wallet, bRound int64) error {
	for _, tx := range transactions {
		transactionFee := core.GetTxFee(tx)

		if bRound <= GenesisRounds {
			transactionFee = 0 //No transaction fee for the Genesis round/s
		}

		switch txType := tx.GetType(); txType {
		case core.TxTransfer:
			err := wm.RevertRetTx(tx, transactionFee)
			if err != nil {
				return err
			}
		case core.TxBecomeCandidate:
			err := wm.RevertDelegateRegistrationTx(tx, transactionFee)
			if err != nil {
				return err
			}
		case core.TxQuitCandidate:
			err := wm.RevertQuitDelegateTx(tx, transactionFee)
			if err != nil {
				return err
			}
		default:
			log.Info().Msgf("Invalid transaction type!!")
			// return errors.New("Invalid type found in transaction")
		}

		log.Info().Msgf("Tx fee reverting: %d", transactionFee)

		mWallet.balance -= transactionFee
		wm.ReAssignVotePower(mWallet, 0-int64(transactionFee))
		mWallet.transactionFee -= transactionFee
	}
	return nil
}

//ApplyTransactions applies transaction to the wallets. If any transaction is found in valid it returns the error and the number of transactions successfully applied
func (wm *WalletManager) ApplyTransactions(transactions []*pb.Transaction, mWallet *Wallet, bRound int64) (int, error) {

	for i, tx := range transactions {

		// resourceEstimate := core.ResourcesRequired(tx)
		// transactionFee := resourceEstimate.CalculateTotalCost(core.TestRP)
		transactionFee := core.GetTxFee(tx)
		if bRound <= GenesisRounds {
			transactionFee = 0 //No transaction fee for the Genesis rounds
		}

		switch txType := tx.GetType(); txType {
		case core.TxTransfer:
			err := wm.ApplyRetTx(tx, transactionFee, bRound)
			if err != nil {
				return i, err
			}
		case core.TxBecomeCandidate:
			err := wm.ApplyDelegateRegistrationTx(tx, transactionFee, bRound)
			if err != nil {
				return i, err
			}
		case core.TxQuitCandidate:
			err := wm.ApplyQuitDelegateTx(tx, transactionFee, bRound)
			if err != nil {
				return i, err
			}
		default:
			log.Info().Msgf("Invalid transaction type!!")
			return i, errors.New("Invalid type found in transaction")
		}

		// log.Info().Msgf("Tx fee charged: %d", transactionFee)

		mWallet.balance += transactionFee
		wm.ReAssignVotePower(mWallet, int64(transactionFee))
		mWallet.transactionFee += transactionFee

		// if recipientAdd == wm.MyWallet.address { //If transaction's recepient address is in the transaction
		// 	log.Info().Msgf("yay I received some decibels!!!")

		// }
		// if payerAdd == wm.MyWallet.address {
		// 	log.Info().Msgf("Decibels got deducted")
		// }

	}

	return 0, nil
}

//ApplyRetTx validates and applies the retrieval/transfer transaction. If invalid it returns the error
func (wm *WalletManager) ApplyRetTx(tx *pb.Transaction, transactionFee, bRound int64) error {
	//transactionFee will be zero in case bRound is less than the GenesisRound
	payerAdd := hex.EncodeToString(tx.GetBody().GetPayerId())
	recipientAdd := hex.EncodeToString(tx.GetBody().GetRecipientId())
	//Check if the transaction is executable

	//For payer
	if wm.CheckEntryByAddress(payerAdd) == false { //Not present in map
		wm.PutWalletByAddress(payerAdd, NewWallet(payerAdd))
	}
	//For receiver
	if wm.CheckEntryByAddress(recipientAdd) == false { //If recipient is not present in the map
		wm.PutWalletByAddress(recipientAdd, NewWallet(recipientAdd))
	}

	_, err := wm.IsExecutable(tx, bRound) //Validating the transactions
	if err != nil {
		log.Info().Msgf("Transaction is not valid: %s", err.Error())
		return err
	}

	pWallet, err := wm.GetWalletByAddress(payerAdd) //for payer
	if err != nil {
		return err
	}
	pWallet.balance -= (tx.GetBody().GetDecibels() + transactionFee)
	delta := 0 - int64(tx.GetBody().GetDecibels()+transactionFee)
	wm.ReAssignVotePower(pWallet, delta)

	pWallet.SetNonce(pWallet.GetNonce() + 1)

	rWallet, err := wm.GetWalletByAddress(recipientAdd) //For recipient
	if err != nil {
		return err
	}
	rWallet.balance += tx.GetBody().GetDecibels()
	wm.ReAssignVotePower(rWallet, int64(tx.GetBody().GetDecibels()))

	if recipientAdd == wm.MyWallet.address { //If transaction's recepient address is in the transaction
		log.Info().Msgf("yay I received some decibels!!!")
	}
	if payerAdd == wm.MyWallet.address {
		log.Info().Msgf("Decibels got deducted")
	}

	log.Info().Msgf("Updated Wallet with ret/transfer tx")

	return nil
}

//RevertRetTx reverts the retrieval transaction
func (wm *WalletManager) RevertRetTx(tx *pb.Transaction, transactionFee int64) error {
	//transactionFee will be zero in case bRound is less than the GenesisRound

	payerAdd := hex.EncodeToString(tx.GetBody().GetPayerId())
	recipientAdd := hex.EncodeToString(tx.GetBody().GetRecipientId())
	fmt.Println("payerAdd", payerAdd, "reci ", recipientAdd)

	pWallet, err := wm.GetWalletByAddress(payerAdd) //for payer
	if err != nil {
		return err
	}
	pWallet.balance += (tx.GetBody().GetDecibels() + transactionFee)
	delta := int64(tx.GetBody().GetDecibels() + transactionFee)
	wm.ReAssignVotePower(pWallet, delta)

	pWallet.SetNonce(pWallet.GetNonce() - 1)

	rWallet, err := wm.GetWalletByAddress(recipientAdd) //For recipient
	if err != nil {
		return err
	}
	rWallet.balance -= tx.GetBody().GetDecibels()
	wm.ReAssignVotePower(rWallet, 0-int64(tx.GetBody().GetDecibels()))

	if recipientAdd == wm.MyWallet.address { //If transaction's recepient address is in the transaction
		log.Info().Msgf("My Decibels deducted-> Revert tx")
	}
	if payerAdd == wm.MyWallet.address {
		log.Info().Msgf("I got back decibels-> Revert tx")
	}

	log.Info().Msgf("Reverted Wallet with ret/transfer tx")

	return nil
}

//ApplyDelegateRegistrationTx  validates and applies the delegate registration. Return invalid in case error is found
func (wm *WalletManager) ApplyDelegateRegistrationTx(tx *pb.Transaction, transactionFee, bRound int64) error {
	//transactionFee will be zero in case bRound is less than the GenesisRound
	payerAdd := hex.EncodeToString(tx.GetBody().GetPayerId())
	//For payer
	if wm.CheckEntryByAddress(payerAdd) == false { //Not present in map
		wm.PutWalletByAddress(payerAdd, NewWallet(payerAdd))
	}
	valid, err := wm.IsExecutable(tx, bRound)
	if valid != true {
		log.Info().Msgf("Transaction is not valid: %s", err.Error())
		return err
	}
	pWallet, err := wm.GetWalletByAddress(payerAdd) //for payer
	if err != nil {
		return err
	}
	err = wm.Executor.ExecuteRegisterCandidate(tx)
	if err != nil {
		return err
	}
	pWallet.isCandidate = true

	pWallet.balance -= (tx.GetBody().GetDecibels() + transactionFee)
	delta := 0 - int64(tx.GetBody().GetDecibels()+transactionFee)
	wm.ReAssignVotePower(pWallet, delta)

	pWallet.SetNonce(pWallet.GetNonce() + 1)

	if payerAdd == wm.MyWallet.address {
		log.Info().Msgf("Decibels got deducted")
	}
	log.Info().Msgf("Updated Wallet with delegate registration tx")
	return nil
}

//ApplyDelRegTxForGenesis applies the delegate registration
func (wm *WalletManager) ApplyDelRegTxForGenesis(tx *pb.Transaction, transactionFee, bRound int64) error {
	//transactionFee will be zero in case bRound is less than the GenesisRound
	payerAdd := hex.EncodeToString(tx.GetBody().GetPayerId())
	//For payer
	if wm.CheckEntryByAddress(payerAdd) == false { //Not present in map
		wm.PutWalletByAddress(payerAdd, NewWallet(payerAdd))
	}
	_, err := wm.IsExecutable(tx, bRound)
	// if valid != true {
	// 	// log.Info().Msgf("Transaction is not valid: %s", err.Error())
	// }
	pWallet, err := wm.GetWalletByAddress(payerAdd) //for payer
	if err != nil {
		return err
	}
	wm.Executor.ExecuteRegisterCandidate(tx)

	pWallet.isCandidate = true

	// pWallet.balance -= (tx.GetBody().GetDecibels() + transactionFee)
	// delta := 0 - int64(tx.GetBody().GetDecibels()+transactionFee)
	// wm.ReAssignVotePower(pWallet, delta)
	pWallet.SetNonce(pWallet.GetNonce() + 1)

	if payerAdd == wm.MyWallet.address {
		log.Info().Msgf("Got Officially registered through DR-transaction(Ignore errors)")
	}
	log.Info().Msgf("Updated Wallet with delegate registration tx")
	return nil
}

//RevertDelegateRegistrationTx applies the delegate registration
func (wm *WalletManager) RevertDelegateRegistrationTx(tx *pb.Transaction, transactionFee int64) error {
	payerAdd := hex.EncodeToString(tx.GetBody().GetPayerId())

	pWallet, err := wm.GetWalletByAddress(payerAdd) //for payer
	if err != nil {
		return err
	}
	err = wm.Executor.ExecuteQuitCandidate(tx)
	if err != nil {
		return err
	}
	pWallet.isCandidate = false

	pWallet.balance += (tx.GetBody().GetDecibels() + transactionFee)
	delta := int64(tx.GetBody().GetDecibels() + transactionFee)
	wm.ReAssignVotePower(pWallet, delta)

	pWallet.SetNonce(pWallet.GetNonce() - 1)

	if payerAdd == wm.MyWallet.address {
		log.Info().Msgf("Got decibels-> RevertRegisterCand")
	}
	log.Info().Msgf("Revert Wallet with delegate registration tx")
	return nil
}

//ApplyQuitDelegateTx applies the delegate quit transaction
func (wm *WalletManager) ApplyQuitDelegateTx(tx *pb.Transaction, transactionFee, bRound int64) error {
	payerAdd := hex.EncodeToString(tx.GetBody().GetPayerId())
	//For payer
	if wm.CheckEntryByAddress(payerAdd) == false { //Not present in map
		wm.PutWalletByAddress(payerAdd, NewWallet(payerAdd))
	}
	valid, err := wm.IsExecutable(tx, bRound)
	if valid != true {
		log.Info().Msgf("Transaction is not valid: %s", err.Error())
		return err
	}
	pWallet, err := wm.GetWalletByAddress(payerAdd) //for payer
	if err != nil {
		return err
	}
	err = wm.Executor.ExecuteQuitCandidate(tx)
	if err != nil {
		return err
	}
	pWallet.isCandidate = false
	pWallet.balance -= (tx.GetBody().GetDecibels() + transactionFee)
	delta := 0 - int64(tx.GetBody().GetDecibels()+transactionFee)
	wm.ReAssignVotePower(pWallet, delta)

	pWallet.SetNonce(pWallet.GetNonce() + 1)

	if payerAdd == wm.MyWallet.address {
		log.Info().Msgf("Decibels got deducted")
	}
	log.Info().Msgf("Updated Wallet with delegate quit tx")
	return nil
}

//RevertQuitDelegateTx reverts the quit delegate transaction by registering the candidate again
func (wm *WalletManager) RevertQuitDelegateTx(tx *pb.Transaction, transactionFee int64) error {
	payerAdd := hex.EncodeToString(tx.GetBody().GetPayerId())

	pWallet, err := wm.GetWalletByAddress(payerAdd) //for payer
	if err != nil {
		return err
	}
	err = wm.Executor.ExecuteRegisterCandidate(tx)
	if err != nil {
		return err
	}
	pWallet.isCandidate = true
	pWallet.balance += (tx.GetBody().GetDecibels() + transactionFee)
	delta := int64(tx.GetBody().GetDecibels() + transactionFee)
	wm.ReAssignVotePower(pWallet, delta)

	pWallet.SetNonce(pWallet.GetNonce() - 1)

	if payerAdd == wm.MyWallet.address {
		log.Info().Msgf("Got decibels-> RevertQuitDelegate")
	}
	log.Info().Msgf("Reverted Wallet against delegate quit tx")
	return nil
}

//ReAssignVotePower assigns the votepower among those who have been voted. Called when balance is changed(_,+/-)
func (wm *WalletManager) ReAssignVotePower(voter *Wallet, delta int64) {
	for cand, percent := range voter.Voted {
		if candWallet, ok := wm.walletByAddress[cand]; ok {
			if delta > 0 {
				oldAmount := uint64((float64(percent) / float64(100)) * float64(int64(voter.balance)-delta))
				newAmount := uint64((float64(percent) / float64(100)) * float64(voter.balance))
				// toAdd := uint64((float64(percent) / float64(100)) * float64(delta))
				toAdd := newAmount - oldAmount
				candWallet.AddVotePower(toAdd)
			} else { // In case delta is negative
				oldAmount := uint64((float64(percent) / float64(100)) * float64(int64(voter.balance)-delta)) //- delta -(-)=+
				newAmount := uint64((float64(percent) / float64(100)) * float64(voter.balance))
				// toSub := math.Abs((float64(percent) / float64(100)) * float64(delta)) //uint64 results in overflow in case value is negative
				toSub := oldAmount - newAmount
				// fmt.Println("Subtracting: ", candWallet.GetAddress(), toSub, uint64(toSub))
				candWallet.SubVotePower(toSub)
				// fmt.Println("Votep of cand", candWallet.GetVotePower())
			}
		}
	}
}

//IsExecutable checks if the transaction is valid
func (wm *WalletManager) IsExecutable(tx *pb.Transaction, bRound int64) (bool, error) { //Only for the transaction
	payerAdd := hex.EncodeToString(tx.GetBody().GetPayerId())
	txNonce := tx.GetBody().GetNonce()

	pWallet, err := wm.GetWalletByAddress(payerAdd) //for payer
	if err != nil {
		if txNonce == 1 { //Putting wallet only when nonce is first
			wm.PutWalletByAddress(payerAdd, NewWallet(payerAdd))
			pWallet, _ = wm.GetWalletByAddress(payerAdd) //Updating pWallet in case the new wallet has been added to the wallet manager
		} else {
			return false, err
		}
	}
	if wm.CheckNonce(txNonce, pWallet) != true { //1check:- Nonce should be in order //Shouldn't this be pwallet + 1?
		// pWallet.SetNonce(pWallet.GetNonce() + 1) //don't set your nonce here. Coz this func is called multiple times to validate
		log.Info().Msgf("Out of order.Nonce!! TODO:- Fix this")
		// return false, errors.New("out of order nonce")
	}
	txFee := core.GetTxFee(tx)

	if bRound <= GenesisRounds {
		txFee = 0
	}

	if pWallet.GetBalance() < txFee {
		return false, errors.New("Payer can't pay fee")
	}

	switch txType := tx.Type; txType {

	case core.TxBecomeCandidate:
		exist := wm.Executor.FindCandByAdd(pWallet.GetAddress()) //Checking if candidate is already present
		if exist == true {
			return false, errors.New("Cand already exist")
		}
	case core.TxQuitCandidate:
		exist := wm.Executor.FindCandByAdd(pWallet.GetAddress()) //Checking if candidate is present
		if exist == false {
			return false, errors.New("Cand doesn't exist")
		}
	case core.TxTransfer:
		if pWallet.GetBalance() < (tx.GetBody().GetDecibels() + txFee) { //Checking if payer have sufficient balance
			return false, errors.New("Payer doesn't hold enough decibels")
		}

	default:
		return false, errors.New("Invalid type transaction") //Checking if the type is invalid
	}

	//////////////////////////////////////////////////////////////////////////////////////
	// pWallet.SetNonce(txNonce) //Updating the nonce
	return true, nil
}

//CheckNonce checks if the nonce of the transaction is correct, as expected
func (wm *WalletManager) CheckNonce(txNonce uint64, wallet *Wallet) bool {
	return txNonce == (wallet.GetNonce() + 1)
}

//applyDeals validates and applies the changes corrosponding to the deal amount in the wallet
func (wm *WalletManager) applyDeals(deals []*pb.Deal, mWallet *Wallet, bRound int64) (int, error) {

	for i, deal := range deals {

		// spID, _ := noise.UnmarshalID(deal.GetKadID())
		// spID := kad.FromString(string(deal.GetKadID()))
		// spAdd := spID.ID.String()

		err := wm.ValidateDeal(deal, bRound) //validates the deal in case error is found return the successful applied deals
		if err != nil {
			return i, err
		}

		spAdd := hex.EncodeToString(deal.Spid)
		wm.PutWalletByAddress(spAdd, NewWallet(spAdd))
		posts := deal.List

		// dealBytes, _ := proto.Marshal(deal)
		// wm.Activedeals[string(getDealHash(deal))] = *deal
		// TODO get deals from Node, remove active deasl from Wallet
		// resourceEstimate := core.ResourcesRequired(deal)
		// transactionFee := resourceEstimate.CalculateTotalCost(core.TestRP)
		// transactionFee := core.GetTxFee(deal) //This is outdated
		// perPostFee := float64(transactionFee) / float64(len(posts))
		var totalFee float64 = 0
		var perPostFee float64 = 0

		for _, post := range posts {
			perPostFee = float64(core.GetTxFee(post)) //New post fee
			if bRound <= GenesisRounds {
				perPostFee = 0 //No transaction fee for the Genesis rounds
			}

			posterAdd := hex.EncodeToString(post.GetInfo().GetMetadata().GetPid())

			wm.PutWalletByAddress(posterAdd, NewWallet(posterAdd))

			chargedAmount := post.GetInfo().GetParam().GetMaxCost()

			pWallet, err := wm.GetWalletByAddress(posterAdd) //for poster
			if err != nil {
				log.Error().Msgf(err.Error())
				// return err
			}
			if pWallet.balance < (int64(chargedAmount) + int64(perPostFee)) {
				panic("Deal payment not possible-> Balance will go in negative. Handle this")
			}
			pWallet.balance -= (int64(chargedAmount) + int64(perPostFee))
			toSub := int64(0 - chargedAmount - uint32(perPostFee))
			wm.ReAssignVotePower(pWallet, int64(toSub))

			// sWallet, err := wm.GetWalletByAddress(spAdd) //for storage provider
			// if err != nil {
			// 	return err
			// }
			// sWallet.balance += uint64(chargedAmount)
			// fmt.Println("Amount deducted from poster and paid to the SP")

			// if spAdd == wm.MyWallet.GetAddress() {
			// 	fmt.Println("I Got payment for storing!!")
			// 	// wm.MyWallet.balance += uint64(chargedAmount)
			// }
			// if posterAdd == wm.MyWallet.GetAddress() {
			// 	log.Info().Msgf("I have Paid for posting!!")
			// 	// wm.MyWallet.balance -= uint64(chargedAmount)
			// }
			totalFee += perPostFee

		}
		// log.Info().Msgf("Tx fee charged: %d Per Post fee: %f", transactionFee, perPostFee)

		mWallet.balance += int64(totalFee)
		wm.ReAssignVotePower(mWallet, int64(totalFee))
		mWallet.transactionFee += int64(totalFee)
	}

	return 0, nil
}

//ValidateDeal checks if deal can be applied of is valid or not
func (wm *WalletManager) ValidateDeal(deal *pb.Deal, bRound int64) error {
	posts := deal.List
	transactionFee := core.GetTxFee(deal)
	perPostFee := float64(transactionFee) / float64(len(posts))
	for _, post := range posts {
		perPostFee = float64(core.GetTxFee(post)) //New post fee
		if bRound <= GenesisRounds {
			perPostFee = 0 //No transaction fee for the Genesis rounds
		}

		// posterID := kad.FromString(string((post.GetInfo()).GetMetadata().GetKadID()))
		// posterAdd := posterID.String()
		posterID := post.GetInfo().GetMetadata().GetPid()
		posterAdd := hex.EncodeToString(posterID)
		wm.PutWalletByAddress(posterAdd, NewWallet(posterAdd))

		chargedAmount := post.GetInfo().GetParam().GetMaxCost()

		pWallet, err := wm.GetWalletByAddress(posterAdd) //for poster
		if err != nil {
			return err
		}
		if pWallet.balance < (int64(chargedAmount) + int64(perPostFee)) {
			return errors.New("insufficient payer balance")
		}
	}
	return nil
}

//RevertDeals reverse the deals in the block
func (wm *WalletManager) RevertDeals(deals []*pb.Deal, mWallet *Wallet, bRound int64) error {

	for _, deal := range deals {

		// spID, _ := noise.UnmarshalID(deal.GetKadID())
		// spID := kad.FromString(string(deal.GetKadID()))
		// spAdd := spID.String()
		spAdd := hex.EncodeToString(deal.Spid)
		wm.PutWalletByAddress(spAdd, NewWallet(spAdd))
		posts := deal.List

		// transactionFee := core.GetTxFee(deal)
		// perPostFee := float64(transactionFee) / float64(len(posts))

		var totalFee float64 = 0
		var perPostFee float64 = 0

		for _, post := range posts {
			perPostFee = float64(core.GetTxFee(post)) //New post fee
			if bRound <= GenesisRounds {
				perPostFee = 0 //No deal fee for the Genesis rounds
			}

			// posterID := kad.FromString(string((post.GetInfo()).GetMetadata().GetKadID()))
			// posterAdd := posterID.String()
			posterID := post.GetInfo().GetMetadata().GetPid()
			posterAdd := hex.EncodeToString(posterID)

			// wm.PutWalletByAddress(posterAdd, NewWallet(posterAdd))

			chargedAmount := post.GetInfo().GetParam().GetMaxCost()

			pWallet, err := wm.GetWalletByAddress(posterAdd) //for poster
			if err != nil {
				return err
			}
			pWallet.balance += (int64(chargedAmount) + int64(perPostFee))
			toAdd := int64(chargedAmount + uint32(perPostFee))
			wm.ReAssignVotePower(pWallet, int64(toAdd))

			// sWallet, err := wm.GetWalletByAddress(spAdd) //for storage provider
			// if err != nil {
			// 	return err
			// }
			// sWallet.balance -= uint64(chargedAmount)
			// fmt.Println("Amount deducted from poster and paid to the SP")

			// if spAdd == wm.MyWallet.GetAddress() {
			// 	fmt.Println("I Got payment for storing!!")
			// 	// wm.MyWallet.balance += uint64(chargedAmount)
			// }
			if posterAdd == wm.MyWallet.GetAddress() {
				fmt.Println("I am Paid for posting!!")
				// wm.MyWallet.balance -= uint64(chargedAmount)
			}
			totalFee += perPostFee
		}
		// log.Info().Msgf("Tx fee charged: %d Per Post fee: %f", transactionFee, perPostFee)

		mWallet.balance -= int64(totalFee)
		wm.ReAssignVotePower(mWallet, int64(0-totalFee))
		mWallet.transactionFee -= int64(totalFee)
	}
	return nil
}

//PrintWalletBalances prints the balance of all the users node has encountered from blocks
func (wm *WalletManager) PrintWalletBalances() {

	fmt.Println("Printing the states of all the wallets...")
	fmt.Println("Currently MY balance is: ", wm.MyWallet.GetBalance(), ". Block reward: ", wm.MyWallet.blockReward, " Tx fee: ", wm.MyWallet.transactionFee)
	for add, wallet := range wm.walletByAddress {
		fmt.Println(add, " has decibels ", wallet.GetBalance(), ". Block reward: ", wallet.blockReward, " Tx Fee: ", wallet.transactionFee, " VP: ", wallet.votePower, "Roll back: ", wallet.rollBackAmount)
	}
	fmt.Println("**********************")
}

//RewardForBlockCreation returns the amount of the block creation reward
func (wm *WalletManager) RewardForBlockCreation() int64 {
	return 10
}

//RewardForEmptyBlock adds the reward for the empty block
func (wm *WalletManager) RewardForEmptyBlock() int64 {
	return 5
}

//RewardForPodf returns the reward for podf
func (wm *WalletManager) RewardForPodf() int64 {
	return wm.RewardForBlockCreation() / 2
}

//TransactionFeePercentage returns the percentage of the transaction fee
func (wm *WalletManager) TransactionFeePercentage() float64 {
	return 0.1
}

//ApplyVotes checks and applies the votes in the vote transation. If err is found the  it return the num of votes successfully applied
func (wm *WalletManager) ApplyVotes(votes []*pb.Vote, mWallet *Wallet, bRound int64) (int, error) {

	// isValid := block.IsValid()   //This chack is now handled in handleBlockNew() which invokes this function
	// if isValid != true {
	// 	return errors.New("Block is not valid")
	// }
	// originalVotes := core.DeepCopyVotes(votes)
	for i, vote := range votes {

		_, err := wm.ValidateVote(vote, bRound) //validating the vote
		if err != nil {
			return i, err
		}

		voterID := vote.GetId()
		txFee := core.GetTxFee(vote)
		if bRound <= GenesisRounds {
			txFee = 0 //Setting fee to zero in case of Genesis rounds
		}

		// fmt.Println("func txFEE: ", txFee)
		// log.Info().Msgf("vote tx fee:- %d", txFee)

		vWallet, err := wm.GetWalletByAddress(voterID) //Fetching the voter wallet
		if err != nil {
			// toRevert := votes[0:v]
			// wm.RevertVotes(toRevert, minerAdd, originalVotes)
			log.Warn().Msgf("No transaction done before to put wallet entry. For now just adding the wallet")
			wm.PutWalletByAddress(voterID, NewWallet(voterID))
			vWallet, _ = wm.GetWalletByAddress(voterID)
		}

		// if vWallet.balance < txFee {
		// 	log.Info().Msgf("Insufficient balance to pay vote fee")
		// 	// toRevert := votes[0:v]
		// 	// wm.RevertVotes(toRevert, minerAdd, originalVotes)
		// }

		// mWallet, err := wm.GetWalletByAddress(minerAdd)
		// if err != nil {
		// 	// toRevert := votes[0:v]
		// 	// wm.RevertVotes(toRevert, minerAdd, originalVotes)
		// 	return err
		// }

		//////// Make payment for the transaction against the vote
		vWallet.balance -= txFee
		// fmt.Println("Check1")
		// wm.PrintVotePower()
		wm.ReAssignVotePower(vWallet, 0-int64(txFee))
		// fmt.Println("Check2")
		// wm.PrintVotePower()

		mWallet.balance += txFee
		mWallet.transactionFee += txFee
		wm.ReAssignVotePower(mWallet, int64(txFee))
		// if len(vote.VoteCand) > 20 {
		// 	return errors.New("Vote reaches the maximum number") //Cannot vote more than 10| max unvote
		// }
		for _, votedCand := range vote.VoteCand {

			action, percent, candID, err := ParseVote(votedCand)
			// fmt.Println("check-1")
			if err != nil { //Will never hit coz it has already bein verified
				// toRevert := votes[0 : v+1]
				// toRevert[len(toRevert)-1].VoteCand = vote.VoteCand[0:c]
				// wm.RevertVotes(toRevert, minerAdd, originalVotes)
				return i, err
			}
			// fmt.Println("check-2")

			if action == "-" { //Corrosponding to unvote
				// vWallet, err = wm.GetWalletByAddress(voterID)
				// if err != nil {
				// 	toRevert := votes[0 : v+1]
				// 	toRevert[len(toRevert)-1].VoteCand = vote.VoteCand[0:c]
				// 	wm.RevertVotes(toRevert,minerAdd,originalVotes)
				// 	return err
				// }
				err = vWallet.CheckUnvoteValidity(percent, candID)
				if err != nil { //Will never hit coz it has already been verified
					// toRevert := votes[0 : v+1]
					// toRevert[len(toRevert)-1].VoteCand = vote.VoteCand[0:c]
					// // fmt.Println("To Revert: ", toRevert[len(toRevert)-1].VotedCand, " c", c)
					// // fmt.Println("original: ", originalVotes[len(toRevert)-1])
					// wm.RevertVotes(toRevert, minerAdd, originalVotes)
					return i, err
				}
				vWallet.ExecuteUnvote(percent, candID)
				vWallet.AddVoteLeft(percent)
				wm.SubVotePower(candID, percent, vWallet)

			} else { //corrosponding to the vote
				// vWallet, err = wm.GetWalletByAddress(voterID)
				// if err != nil {
				// 	toRevert := votes[0 : v+1]
				// 	toRevert[len(toRevert)-1].VoteCand = vote.VoteCand[0:c]
				// 	wm.RevertVotes(toRevert,minerAdd,originalVotes)
				// 	return err
				// }
				err = vWallet.CheckVoteValidity(percent, candID)
				if err != nil { //Will never hit coz it has already been verified
					// toRevert := votes[0 : v+1]
					// toRevert[len(toRevert)-1].VoteCand = vote.VoteCand[0:c]
					// wm.RevertVotes(toRevert, minerAdd, originalVotes)
					return i, err
				}
				// oldVotePercent, _ := cWallet.GetCandVotePercentage(candID)
				vWallet.ExecuteVote(percent, candID)
				vWallet.SubVoteLeft(percent)
				wm.AddVotePower(candID, percent, vWallet)
			}

		}
		////  Make payment for the voting transaction
	}
	return 0, nil
}

//AddVotePower updates the votepower(+ + +)
func (wm *WalletManager) AddVotePower(candID string, percentChange int, voter *Wallet) error {
	if cand, ok := wm.walletByAddress[candID]; ok {
		totalPercentage := voter.Voted[candID]
		totalAmount := uint64((float64(totalPercentage) / float64(100)) * float64(voter.GetBalance()))
		prevAmount := uint64((float64(totalPercentage-percentChange) / float64(100)) * float64(voter.GetBalance()))
		// toAdd := uint64((float64(percentChange) / float64(100)) * float64(voterBalance))
		toAdd := totalAmount - prevAmount
		cand.AddVotePower(toAdd)
		return nil
	}
	return errors.New("Cand doesn't exist")
}

//SubVotePower subtracts the votePower of the candidate(+ + +)
func (wm *WalletManager) SubVotePower(candID string, percentChange int, voter *Wallet) error {
	if cand, ok := wm.walletByAddress[candID]; ok {
		totalPercentage := voter.Voted[candID] //This contains an updated percentage
		totalAmount := uint64((float64(totalPercentage) / float64(100)) * float64(voter.GetBalance()))
		prevAmount := uint64((float64(totalPercentage+percentChange) / float64(100)) * float64(voter.GetBalance()))
		toSub := prevAmount - totalAmount //Coz previous amount should be greater
		// toSub := uint64((float64(percentChange) / float64(100)) * float64(voterBalance))
		cand.SubVotePower(toSub)
		return nil
	}
	return errors.New("Cand doesn't exist")
}

//RevertVotes undo the votes- Possible in case block is found corrupt
func (wm *WalletManager) RevertVotes(votes []*pb.Vote, mWallet *Wallet, bRound int64) error {
	fmt.Println("Reverting votes...")
	for _, vote := range votes {
		voterID := vote.GetId()
		var txFee int64 = 0
		// if len(originalVotes) == 0 { //If votes are actually the original votes then there is no need to send

		txFee = core.GetTxFee(vote)
		// } else {
		// 	txFee = core.GetTxFee(originalVotes[i]) //Fee should be calculated according to the original votes because it is added the same way. Votes may be modified (see errors)
		// }
		if bRound <= GenesisRounds {
			txFee = 0 //Setting fee to zero in case of Genesis rounds
		}

		vWallet, err := wm.GetWalletByAddress(voterID)
		if err != nil {
			return err //Should never hit this
		}

		vWallet.balance += txFee
		wm.ReAssignVotePower(vWallet, int64(txFee))
		mWallet.balance -= txFee
		mWallet.transactionFee -= txFee
		wm.ReAssignVotePower(mWallet, 0-int64(txFee))

		for _, votedCand := range vote.VoteCand {

			action, percent, candID, _ := ParseVote(votedCand)

			if action == "-" { //Corrosponding to unvote
				// vWallet, _ := wm.GetWalletByAddress(voterID) //for poster

				vWallet.ExecuteVote(percent, candID)
				vWallet.SubVoteLeft(percent)
				wm.AddVotePower(candID, percent, vWallet)

			} else {
				// vWallet, _ := wm.GetWalletByAddress(voterID)
				// cWallet.CheckVoteValidity(percent, candID)
				vWallet.ExecuteUnvote(percent, candID)
				vWallet.AddVoteLeft(percent)
				wm.SubVotePower(candID, percent, vWallet)
			}
		}
	}
	return nil
}

//GetCandVotePower returns the votepower of the candidate
func (wm *WalletManager) GetCandVotePower(candID string) (uint64, int64, error) {
	if cand, ok := wm.walletByAddress[candID]; ok {
		return cand.GetVotePower(), cand.bannedTill, nil
	}

	return 0, -1, errors.New("Candidate doesn't exist")
}

//PrintWalletVotes prints the votes of all the wallets
func (wm *WalletManager) PrintWalletVotes() {
	fmt.Println("Printing the votes of all the wallets...")
	// fmt.Print("I voted : ")
	// wm.MyWallet.PrintVotes()
	for add, wallet := range wm.walletByAddress {
		fmt.Println(add, " voted: ")
		wallet.PrintVotes()
	}
	fmt.Println("**********************")
}

//PrintVotePower prints the votepower of the candidate
func (wm *WalletManager) PrintVotePower() {
	fmt.Println("Printing votepower(VP)...")
	for add, wallet := range wm.walletByAddress {
		fmt.Println(add, " VP: ", wallet.GetVotePower())
	}
	fmt.Println("*************************")
}

//AddBlockState puts the wallet state in the block
func (wm *WalletManager) AddBlockState(block *core.Block) {
	//Appending the block state in the block
	count := 0
	for id, wallet := range wm.walletByAddress { //putting all the balances in the block
		// block.PbBlock.State.AccountState[id] = wallet.GetBalance()
		entry := &pb.AccState{
			Pk:      id,
			Balance: wallet.GetBalance(),
		}
		block.PbBlock.State.AccountStates[count] = entry
		// fmt.Println("Putting: ", id, " bal: ", wallet.GetBalance())
		count++
	}

}

//ResetWalletManagerState resets the amount in the wallet to the rollback amount
func (wm *WalletManager) ResetWalletManagerState(rollBack int) {
	var rollBackMoney int64
	for _, wallet := range wm.walletByAddress {
		initBal := wallet.GetBalance()
		rollBackMoney = int64((float64(rollBack) / float64(100)) * float64(initBal))
		wallet.SetRollBackAmount(rollBackMoney)
		wallet.SetBalance(rollBackMoney)

		delta := int64(rollBackMoney) - int64(initBal) //Will be negative
		wm.ReAssignVotePower(wallet, delta)
		//Setting all components to zero
		wallet.SetBlockReward(0)
		wallet.SetTransactionFee(0)
		wallet.SetProofReward(0)
	}

}

//UndoDigestReset undo the wallet reset done by the digest block
func (wm *WalletManager) UndoDigestReset(rollBack int) {
	var rollBackMoney int64
	for _, wallet := range wm.walletByAddress {
		initBal := wallet.GetBalance()
		rollBackMoney = int64((float64(100) / float64(rollBack)) * float64(initBal))
		wallet.SetBalance(rollBackMoney)
		wallet.SetRollBackAmount(0)

		delta := int64(rollBackMoney) - int64(initBal) //Will be positive (current - previous amount)
		wm.ReAssignVotePower(wallet, delta)
		//Setting all components to zero
		// wallet.SetBlockReward(0)
		// wallet.SetTransactionFee(0)
		// wallet.SetProofReward(0)
	}
}

//GetDigestBlockReward is a function to claim the digest block reward
func (wm *WalletManager) GetDigestBlockReward() {
	myWallet := wm.walletByAddress[wm.MyWallet.GetAddress()]

	myWallet.balance += 10 * wm.RewardForBlockCreation() //More reward for the epoch block
	myWallet.blockReward += 10 * wm.RewardForBlockCreation()
	wm.ReAssignVotePower(myWallet, int64(10*wm.RewardForBlockCreation()))
}

//ValidateDigestBlock validates if the local balances matches with the ones appended in the block
func (wm *WalletManager) ValidateDigestBlock(digest *core.Block) bool {

	bWalletState := digest.PbBlock.GetState().GetAccountStates()
	if len(bWalletState) != len(wm.walletByAddress) {
		log.Info().Err(errors.New("No of entries doesn't match"))
		//TODO:- return false if entries in the DigestBlock are not equal
	}
	for _, entry := range bWalletState {
		id, bal := entry.GetPk(), entry.GetBalance()
		if wal, ok := wm.walletByAddress[id]; ok {
			if wal.balance != bal {
				//Balance mismath
				// log.Info().Msgf("id %v got %v miner %v", id, bal, wal.balance)
				log.Info().Err(errors.New("Entries balance mismath"))
				return false
			}
		} else {
			log.Info().Err(errors.New("Entry doesn't exist"))
			return false
		}
	}
	return true
}
