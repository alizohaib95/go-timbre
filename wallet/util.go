package wallet

import (
	"errors"
	"strconv"
	"strings"

	"github.com/guyu96/go-timbre/log"
)

//ParseVote parses the vote
func ParseVote(vote string) (string, int, string, error) {

	sepExist := strings.Contains(vote, "|")
	if !sepExist {
		return "", 0, "", errors.New("Invalid vote. Separater doesn't exist")
	}
	s := strings.Split(vote, "|")
	action := string(s[0][0])
	// fmt.Println("parsed vote is: ", s, "action: ", action)

	if action != "-" && action != "+" {
		log.Info().Msgf("Invalid action sign found in vote: %s", vote)
		return "", 0, "", errors.New("Invalid action sign found in vote: ")
	}
	var num int = 0
	var percentage string = ""
	percentage = s[0][1:]
	if percentage == "" {
		return "", 0, "", errors.New("Invalid action sign found in vote: ")
	}
	num, _ = strconv.Atoi(percentage)
	if num < 0 || num > 100 {
		return "", 0, "", errors.New("Invalid vote number found")
	}
	// fmt.Println("action: ", action, "amountPer", percentage)

	candidate := s[1]
	if candidate == "" {
		return "", 0, "", errors.New("No address provided. Vote invalid")
	}

	return action, num, candidate, nil
}
