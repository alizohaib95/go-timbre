#!/usr/bin/env python

# Copyright (C) 2003-2007  Robey Pointer <robeypointer@gmail.com>
#
# This file is part of paramiko.
#
# Paramiko is free software; you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation; either version 2.1 of the License, or (at your option)
# any later version.
#
# Paramiko is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
# details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with Paramiko; if not, write to the Free Software Foundation, Inc.,
# 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA.


import base64
import getpass
import os
import socket
import sys
import traceback
from paramiko.py3compat import input
import time
import paramiko
import re
import threading
import select
from paramiko.py3compat import u


def makeConnection(hostname, port, username, password):
    client = paramiko.SSHClient()
    client.load_system_host_keys()
    client.set_missing_host_key_policy(paramiko.WarningPolicy())
    if not UseGSSAPI and not DoGSSAPIKeyExchange:
        client.connect(hostname, port, username, password)
    else:
        try:
            client.connect(
                hostname,
                port,
                username,
                gss_auth=UseGSSAPI,
                gss_kex=DoGSSAPIKeyExchange,
            )
        except Exception:
            # traceback.print_exc()
            password = getpass.getpass(
                "Password for %s@%s: " % (username, hostname)
            )
            client.connect(hostname, port, username, password)

    chan = client.invoke_shell()
    # data = str()
    # while True:
    #     data = ""
    #     if chan.recv_ready():
    #         data += chan.recv(9999)
    #         print data
    #         if "Last login" in data:
    #             print "chlo shabash"
    #             break
    #     else:
    #         continue

    return client, chan


def GetOutPut(chann, msg):
    while True:
        r, w, e = select.select([chann], [], [])
        if chann in r:
            try:
                x = u(chann.recv(1024))
                if len(x) == 0:
                    sys.stdout.write("\r\n*** EOF\r\n")
                    break
                sys.stdout.write("\n" + msg + ":")
                sys.stdout.write(x)
                sys.stdout.flush()
            except socket.timeout:
                pass

    # data = ""
    # while True:        
    #     if chan.recv_ready():
    #         data += chann.recv(9999)
    #         if msg in data:
    #             print(data)
    #             break
    #     else:
    #         continue
    # return data


# setup logging
paramiko.util.log_to_file("demo_simple.log")
# Paramiko client configuration
UseGSSAPI = (
    paramiko.GSS_AUTH_AVAILABLE
)  # enable "gssapi-with-mic" authentication, if supported by your python installation
DoGSSAPIKeyExchange = (
    paramiko.GSS_AUTH_AVAILABLE
)  # enable "gssapi-kex" key exchange, if supported by your python installation
# UseGSSAPI = False
# DoGSSAPIKeyExchange = False
port = 22

# # get hostname
# username = ""
# if len(sys.argv) > 1:
#     hostname = sys.argv[1]
#     if hostname.find("@") >= 0:
#         username, hostname = hostname.split("@")
# else:
#     hostname = input("Hostname: ")
# if len(hostname) == 0:
#     print("*** Hostname required.")
#     sys.exit(1)

# if hostname.find(":") >= 0:
#     hostname, portstr = hostname.split(":")
#     port = int(portstr)


# # get username
# if username == "":
#     default_username = getpass.getuser()
#     username = input("Username [%s]: " % default_username)
#     if len(username) == 0:
#         username = default_username
# if not UseGSSAPI and not DoGSSAPIKeyExchange:
#     password = getpass.getpass("Password for %s@%s: " % (username, hostname))


# now, connect and use paramiko Client to negotiate SSH2 across the connection
try:
    whatToDo = sys.argv[1]
    client, chan = makeConnection("10.230.9.216", '4410', 'timbre', 'm8r093j3')
    time.sleep(1)

    clientSP, chanSP = makeConnection("10.230.9.216", '4410', 'timbre', 'm8r093j3')
    time.sleep(1)

    clientPoster, chanPoster = makeConnection("10.230.9.136", '4410', 'timbre', 'm8r093j3')
    time.sleep(1)

    clientMiner, chanMiner = makeConnection("10.230.9.136", '4410', 'timbre', 'm8r093j3')
    time.sleep(1)

    t1 = threading.Thread(target = GetOutPut, args = (chan, "chan"))
    t1.daemon = True
    t2 = threading.Thread(target = GetOutPut, args = (chanPoster, "chanPoster"))
    t2.daemon = True
    t3 = threading.Thread(target = GetOutPut, args = (chanMiner, "chanMiner"))
    t3.daemon = True
    t4 = threading.Thread(target = GetOutPut, args = (chanSP, "chanSP"))
    t4.daemon = True

    t1.start()
    t2.start()
    t3.start()
    t4.start()
    # GetOutPut(chanPoster, "chanPoster")
    # GetOutPut(chanMiner,  "chanMiner")
    # GetOutPut(chanSP,  "chanSP")

    # trp = client.get_transport()
    # chan = trp.open_session()
    # print(repr(client.get_transport()))
    #  chan.exec_command('ls\n')
    # output = stdout.readlines()
    # print('\n'.join(output))    
    print("*** Here we go!\n")
    # chan.send('Bilal\n')

    # chan.send('echo $tada\n')
    # stdin, stdout, stderr = chan.exec_command('read tada')
    # output = stdout.readlines()
    # chan.send("Bukak\n")
    # stdin.write("Bilal")
    # stdin.write("\n")
    # stdin.flush()
    # time.sleep(4)
    # output = stdout.readlines()
    # print('\n'.join(output))    
    # stdin2, stdout2, stderr2 = client.exec_command('echo $var2')
    # output2 = stdout2.readlines()
    # print('\n'.join(output2))    

    if "start" in whatToDo:
        chan.send('cd /home/timbre/goprojects/go-timbre/simulation/main/\n')
        chanSP.send('cd /home/timbre/goprojects/go-timbre/simulation/main/\n')
        chanMiner.send('cd /home/timbre/goprojects/go-timbre/simulation/main/\n')
        chanPoster.send('cd /home/timbre/goprojects/go-timbre/simulation/main/\n')
        
        # chan.send('yarn dev > output.txt &\n')
        time.sleep(1)
        chan.send('go run main.go\n')
        time.sleep(2)
        chanMiner.send('go run main.go -p 22005 -m \n')
        chanSP.send('go run main.go -p 22006 -s \n')
        chanPoster.send('go run main.go -p 22007 -u \n')
        time.sleep(4)
        print("here")
        # print(GetOutPut(chan, "pw -> print wallet balances"))
        # print(GetOutPut(chanPoster, "pw -> print wallet balances"))
        # print(GetOutPut(chanMiner,  "pw -> print wallet balances"))
        # print(GetOutPut(chanSP,  "pw -> print wallet balances"))

        print("here2")
        time.sleep(20)
        chanSP.send('sso\n')
        time.sleep(1)
        chanPoster.send('se\n')
        time.sleep(1)
        chanPoster.send('sp\n')
        time.sleep(1)
        chanPoster.send('BilalSaleem\n')
        time.sleep(3)

        print("here3")
        chan.send('\n')
        chanMiner.send('\n')
        chanSP.send('\n')
        chanPoster.send('\n')
        time.sleep(1)
        # print(GetOutPut(chan, "pw -> print wallet balances"))
        # print(GetOutPut(chanPoster, "pw -> print wallet balances"))
        # print(GetOutPut(chanMiner,  "pw -> print wallet balances"))
        # print(GetOutPut(chanSP,  "pw -> print wallet balances"))
        input()
        time.sleep(5)
        # chan.send('exit\n')
    else :
        chan.send('ps -aux | grep "main"\n')
        time.sleep(1)
        print(GetOutPut(chan))
        data = GetOutPut(chan)
        print(data)
        pidList = []
        for line in data.split("\n"):
            print(line.split(" "))
            if "grep" not in line and "go" in line and "main" in line:
                pidList.append(re.compile(" +").split(line)[3].strip())
        time.sleep(2)
        print pidList
        for pid in pidList:
            chan.send("kill " + pid + "\n")
            time.sleep(1)

        # time.sleep(2)
        chan.send("ps -aux | grep 'go run main.go'")
        print(GetOutPut(chan))

    # print(data)
    # stdin, stdout, stderr = client.exec_command('go run main.go > output.txt')
    # for line in stdout:
    #     print line.strip('\n')
    # time.sleep(15)
    # interactive.interactive_shell(chan)
    chan.close()
    chanMiner.close()
    chanSP.close()
    chanPoster.close()
    client.close()
    clientMiner.close()
    clientSP.close()
    clientPoster.close()

except Exception as e:
    print("*** Caught exception: %s: %s" % (e.__class__, e))
    traceback.print_exc()
    try:
        client.close()
    except:
        pass
    sys.exit(1)
#import paramiko
#import time

# client = paramiko.SSHClient()
# client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
# client.connect('10.230.9.216', port = '4410', username='timbre', password='m8r093j3')

# # stdin, stdout, stderr = client.exec_command('ls -l')

# # for line in stdout:
# #     print line.strip('\n')

# stdin, stdout, stderr = client.exec_command('cd timbre-frontend')
# for line in stdout:
#     print line.strip('\n')
# stdin, stdout, stderr = client.exec_command('yarn dev')
# for line in stdout:
#     print line.strip('\n')

# time.sleep(60)
# client.close()
