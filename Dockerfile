FROM golang:1.12.1-stretch

# Set necessary environmet variables needed for our image
RUN apt-get update && apt-get install -y unzip
RUN apt-get install -y autoconf automake libtool protobuf-compiler sudo
RUN pwd
ENV SRC_DIR go/src/github.com/go-timbre 
ENV GO111MODULE on

# Install protoc
#ENV PROTOBUF_URL https://github.com/google/protobuf/releases/download/v3.3.0/protobuf-cpp-3.3.0.tar.gz
#RUN curl -L -o /tmp/protobuf.tar.gz $PROTOBUF_URL
#WORKDIR /tmp/
#RUN tar xvzf protobuf.tar.gz
#WORKDIR /tmp/protobuf-3.3.0
RUN mkdir /export
#RUN ./autogen.sh && \
 #   ./configure --prefix=/export && \
  #  make -j 3 && \
   # make check && \
    #make install

# Install protoc-gen-go
RUN go get github.com/golang/protobuf/protoc-gen-go
RUN cp /go/bin/protoc-gen-go /export
#Ending protobuf installation

COPY . $SRC_DIR

# COPY go.mod .
# COPY go.sum .
# RUN go mod download

#ADD . /go-timbre/

#RUN curl https://github.com/protocolbuffers/protobuf/releases/download/v3.11.4/protoc-3.11.4-linux-x86_64.zip
#RUN unzip -o $PROTOC_ZIP -d /usr/local bin/protoc
#RUN unzip -o $PROTOC_ZIP -d /usr/local 'include/*'
#RUN rm -f protoc-3.11.4-linux-x86_64.zip


WORKDIR $SRC_DIR

RUN ls

RUN make proto

RUN go mod download && ls
#RUN cp install_pbc.sh /

#WORKDIR /
RUN sudo ./install_pbc.sh
#WORKDIR $SRC_DIR

# Move to /dist directory as the place for resulting binary folder
WORKDIR simulation/main

RUN ls

# # Build the application
# RUN go build -o main .

# Move to /dist directory as the place for resulting binary folder
# WORKDIR /dist

# Copy binary from build to main folder
# RUN cp /go_timbre/simulation/main

# Export necessary port
EXPOSE 7001

# Command to run when starting the container
CMD ["go","run","main.go"]
