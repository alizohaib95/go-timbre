## Setting up the client

In the client directory, you can first run:

### `npm install`

to install the dependencies and then run

### `npm start`
or
### `yarn start`

to start the application

You may start the testnet first and then start the react application to avoid
port conflicts

To run multiple clients open the folder in multiple terminals and
set the REACT_APP_API variable in the npm script. For example

## `REACT_APP_API=5200 npm start`

By default, the api url points to 7200.

Make sure there is a node serving the api at the given port.
To set the api ports, use the following flags while running the test-net file.

For example:

## `go run TestNet-Server.go -lf c.txt -sp 4000 -mp 4001 -pp 4002 -ep 4003 -ip 4004 -http 5200 -rpc 5211`