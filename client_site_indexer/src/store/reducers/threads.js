// This file contains all reducer logic related to threads
import {
    FETCH_THREADS_REQUEST,
    FETCH_THREADS_SUCCESS,
    FETCH_THREADS_ERROR,
    FETCH_THREAD_REQUEST,
    FETCH_THREAD_SUCCESS,
    FETCH_THREAD_ERROR,
    CREATE_THREAD_REQUEST,
    CREATE_THREAD_SUCCESS,
    CREATE_THREAD_ERROR,
    GETFILTERS_REQUEST,
    GETFILTERS_SUCCESS,
    GETFILTERS_ERROR,
    ADD_TAG_REQUEST,
    ADD_TAG_SUCCESS,
    ADD_TAG_ERROR

} from '../actions/threads';

const initialState = {
    isFetching: false,
    threads: [],
};

const updateItems = (post, items) =>
    items.map(i => (i.id === post.id ? post : i));

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_THREADS_REQUEST:
            return { ...state, isFetching: true,newPost: null, thread: null };
        case FETCH_THREADS_SUCCESS:
            let x = state.threads.concat(action.threads)
            var unique = x.map(ar => JSON.stringify(ar))
                .filter((itm, idx, arr) => arr.indexOf(itm) === idx)
                .map(str => JSON.parse(str));
            return { ...state, isFetching: false, threads: unique };
        case FETCH_THREADS_ERROR:
            return { ...state, isFetching: false };

        case FETCH_THREAD_REQUEST:
            return { ...state, isFetching: true, thread: null, newPost: null };
        case FETCH_THREAD_SUCCESS:
            return { ...state, isFetching: false, thread: action.thread };
        case FETCH_THREAD_ERROR:
            return { ...state, isFetching: false };

        case ADD_TAG_REQUEST:
            return { ...state, isFetching: true};
        case ADD_TAG_SUCCESS:
            return {...state,isFetching:false,thread: action.post,};
        case ADD_TAG_ERROR:
            return {isFetching: false}

        case CREATE_THREAD_REQUEST:
            return {isFetching: true}
        case CREATE_THREAD_SUCCESS:
            return {isFetching: false, newThread: action.post.hash}
        case CREATE_THREAD_ERROR:
            return {isFetching: false}


        case GETFILTERS_REQUEST:
            return { ...state, isFetching: true, filters: null };
        case GETFILTERS_SUCCESS:
            return { ...state, isFetching: false, filters: action.filtersEchoes };
        case GETFILTERS_ERROR:
            return { ...state, isFetching: false };


        


        default:
            return state
    }
}

