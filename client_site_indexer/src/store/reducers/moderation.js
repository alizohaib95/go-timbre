import {

    MODERATORS_REQUEST,
    MODERATORS_SUCCESS,
    MODERATORS_ERROR,

    BECOMEMOD_REQUEST,
    BECOMEMOD_SUCCESS,
    BECOMEMOD_ERROR,

    MODERATORPROF_REQUEST,
    MODERATORPROF_SUCCESS,
    MODERATORPROF_ERROR,

    ADDREGEXFILTER_REQUEST,
    ADDREGEXFILTER_SUCCESS,
    ADDREGEXFILTER_ERROR,

    REMOVEREGEXFILTER_REQUEST,
    REMOVEREGEXFILTER_SUCCESS,
    REMOVEREGEXFILTER_ERROR,

    UPDATEECHO_REQUEST,
    UPDATEECHO_SUCCESS,
    UPDATEECHO_ERROR,

    FOLLOWMOD_REQUEST,
    FOLLOWMOD_SUCCESS,
    FOLLOWMOD_ERROR

} from '../actions/moderation';

const initialState = {
    isProcessing: false,
    moderators: [],
};

// Todo: these just set the processing state. Add things later
export default (state = initialState, action) => {
    switch (action.type) {
        
        case MODERATORS_REQUEST:
            return {...state, isProcessing:true}
        case MODERATORS_SUCCESS:
            return { ...state, isProcessing: false, moderators: action.mods.moderators};
        case MODERATORS_ERROR:
            return { ...state, isProcessing: false };

        case BECOMEMOD_REQUEST:
            return {...state, isProcessing:true}
        case BECOMEMOD_SUCCESS:
            return { ...state, isProcessing: false, moderators: action.moderators};
        case BECOMEMOD_ERROR:
            return { ...state, isProcessing: false };

        case MODERATORPROF_REQUEST:
            return {...state, isProcessing:true, moderator: null}
        case MODERATORPROF_SUCCESS:
            return { ...state, isProcessing: false, moderator: action.mod};
        case MODERATORPROF_ERROR:
            return { ...state, isProcessing: false };

        case ADDREGEXFILTER_REQUEST:
            return {...state, isProcessing:true}
        case ADDREGEXFILTER_SUCCESS:
            return { ...state, isProcessing: false, moderator: action.filters};
        case ADDREGEXFILTER_ERROR:
            return { ...state, isProcessing: false };

        
        case REMOVEREGEXFILTER_REQUEST:
            return {...state, isProcessing:true}
        case REMOVEREGEXFILTER_SUCCESS:
            return { ...state, isProcessing: false, moderator: action.filters};
        case REMOVEREGEXFILTER_ERROR:
            return { ...state, isProcessing: false };

        
        case UPDATEECHO_REQUEST:
            return {...state, isProcessing:true}
        case UPDATEECHO_SUCCESS:
            return { ...state, isProcessing: false, moderator: action.filters};
        case UPDATEECHO_ERROR:
            return { ...state, isProcessing: false };
        


        case FOLLOWMOD_REQUEST:
            return {...state, isProcessing:true}
        case FOLLOWMOD_SUCCESS:
            return { ...state, isProcessing: false, };
        case FOLLOWMOD_ERROR:
            return { ...state, isProcessing: false };


        default:
            return state
    }
}
