import jwtDecode from 'jwt-decode';
import {
    LOGIN_REQUEST,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    SIGNUP_REQUEST,
    SIGNUP_SUCCESS,
    SIGNUP_ERROR,
    LOGOUT
} from '../actions/authentication';

const token = localStorage.getItem('token');
const user = token && jwtDecode(token);

const initialState = {
    ...(token && {
        token
    }),
    ...(user && {
        user
    })
};

export default (state = initialState, action) => {
    switch (action.type) {
        case SIGNUP_REQUEST:
        case LOGIN_REQUEST:
            return {
                ...state, loading: true
            };
        case SIGNUP_SUCCESS:
        case LOGIN_SUCCESS:
            const user = jwtDecode(action.login.token);
            let fullUser =  {...user, ...action.login}
            localStorage.setItem('token', action.login.token);
            return {
                ...state,
                loading: false,
                    token: action.login.token,
                    user: fullUser
            };

        case SIGNUP_ERROR:
        case LOGIN_ERROR:
            return {
                ...state, loading: false
            };

        case LOGOUT:
            localStorage.removeItem('token');
            return {
                ...state, token: null, user: null
            };

        default:
            return state;
    }
};
