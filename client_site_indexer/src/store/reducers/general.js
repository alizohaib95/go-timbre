import {
        

    FETCH_NBLOCKS_REQUEST,
    FETCH_NBLOCKS_SUCCESS,
    FETCH_NBLOCKS_ERROR,

    
} from '../actions/general';


const initialState = {
    isProcessing: false,
};


export default (state = initialState, action) => {
    switch (action.type) {


        
        case FETCH_NBLOCKS_REQUEST:
            return { ...state, isProcessing: true};
        case FETCH_NBLOCKS_SUCCESS:
            return { ...state, isProcessing: false, blocks: action.blocks};
        case FETCH_NBLOCKS_ERROR:
            return { ...state, isProcessing: false };  
        
        
        

        default:
            return state
    }
}