import {
    getNBlocks,
} from '../../utilities/api';


//*************////////////////////////////////////////////*************//
// Get Blockchain Blocks Action
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_NBLOCKS_REQUEST = 'FETCH_NBLOCKS_REQUEST';
export const FETCH_NBLOCKS_SUCCESS = 'FETCH_NBLOCKSSUCCESS';
export const FETCH_NBLOCKS_ERROR = 'FETCH_NBLOCKS_ERROR';

function createFetchNBlocksRequest() {
    return {
        type: FETCH_NBLOCKS_REQUEST
    }
}

function createFetchNBlocksSuccess(blocks) {
    return {
        type: FETCH_NBLOCKS_SUCCESS,
        blocks
    }
}

function createFetchNBlockssError(error) {
    return {
        type: FETCH_NBLOCKS_ERROR,
        error
    }
}

export const fetchNBlocks = () => async (dispatch, getState) => {
    dispatch(createFetchNBlocksRequest());
    try {
        const json = await getNBlocks();
        dispatch(createFetchNBlocksSuccess(json));
    } catch (error) {
        dispatch(createFetchNBlockssError(error));
    }
}
