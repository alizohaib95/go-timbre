// This file contains all the actions and action creator related to threads
import {
    getThreads,
    getThread,
    createComment,
    createThread,
    attemptLikePost,
    attemptGetFilters,
    attemptTag,
    
} from '../../utilities/api';

//*************////////////////////////////////////////////*************//
// Full Threads
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_THREADS_REQUEST = 'FETCH_THREADS_REQUEST';
export const FETCH_THREADS_SUCCESS = 'FETCH_THREADS_SUCCESS';
export const FETCH_THREADS_ERROR = 'FETCH_THREADS_ERROR';

// Action Creators
function fetchThreadsRequest()  {
    return {
        type: FETCH_THREADS_REQUEST,
        
    }
}
function fetchThreadsSuccess(threads) {
    return {
        type: FETCH_THREADS_SUCCESS,
        threads
    }
}

function fetchThreadsError(err) {
    return {
        type: FETCH_THREADS_ERROR,
        err
    }
}

// Action Dispatchers
export const fetchThreads = (api = null) => async dispatch => {
    dispatch(fetchThreadsRequest())
    try {
        const threads = await getThreads(api); // Call the api function here
        dispatch(fetchThreadsSuccess(threads.threads));
    } catch (error) {
        dispatch(fetchThreadsError(error));
    }
};


//*************////////////////////////////////////////////*************//
// Single Thread
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_THREAD_REQUEST = 'FETCH_THREAD_REQUEST';
export const FETCH_THREAD_SUCCESS = 'FETCH_THREAD_SUCCESS';
export const FETCH_THREAD_ERROR = 'FETCH_THREAD_ERROR';

// Action Creators
function fetchThreadRequest()  {
    return {
        type: FETCH_THREAD_REQUEST,
        
    }
}
function fetchThreadSuccess(thread) {
    return {
        type: FETCH_THREAD_SUCCESS,
        thread
    }
}
function fetchThreadError(err) {
    return {
        type: FETCH_THREAD_ERROR,
        err
    }
}

// Action Dispatchers
export const fetchThread = (hash) => async dispatch => {
    
    dispatch(fetchThreadRequest())
    try {
        const thread = await getThread(hash); // 
        dispatch(fetchThreadSuccess(thread));
    } catch (error) {
        dispatch(fetchThreadError(error));
    }
};



//*************////////////////////////////////////////////*************//
// Thread Creater
//*************////////////////////////////////////////////*************//

// Action Types
export const CREATE_THREAD_REQUEST = 'CREATE_THREAD_REQUEST';
export const CREATE_THREAD_SUCCESS = 'CREATE_THREAD_SUCCESS';
export const CREATE_THREAD_ERROR = 'CREATE_THREAD_ERROR';

function createThreadRequest() {
    return {
        type: CREATE_THREAD_REQUEST
    }
}

function createThreadSuccess(post) {
    return {
        type: CREATE_THREAD_SUCCESS,
        post
    }
}

function createThreadError(error) {
    return {
        type: CREATE_THREAD_ERROR,
        error
    }
}

export const attemptCreateThread = thread => async (dispatch, getState) => {
    dispatch(createThreadRequest());
    try {

        let obj = {
            "Title": thread.title,
            "Content": thread.content,
            "Author": getState().authentication.user.username,
            "Forum": "WORLDNEWS",
            "Forum_link": "timbre.io",
            "MaxCost": 100,
        }
        let c = JSON.stringify(obj)
        c = String(c)
        let user = getState().authentication.user.username
        let data = {
            "content": c,
            "parentPostHash": "",
            "threadheadPostHash": "",
            "maxCost": 100,
            "minDuration": 300,
            "username": user
        }
        const json = await createThread(data);
        dispatch(createThreadSuccess(json));
    } catch (error) {
        dispatch(createThreadError(error));
    }
}


//*************////////////////////////////////////////////*************//
// Commenter
//*************////////////////////////////////////////////*************//

// Action Types
export const CREATE_COMMENT_REQUEST = 'CREATE_COMMENT_REQUEST';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const CREATE_COMMENT_ERROR = 'CREATE_COMMENT_ERROR';

function createCommentRequest (){
    return { 
        type: CREATE_COMMENT_REQUEST 
    }
}
function createCommentSuccess(post) {
    return { 
        type: CREATE_COMMENT_SUCCESS, 
        post 
    }
}
function createCommentError(error) {
    return {
        type: CREATE_COMMENT_ERROR,
        error
    }
}

export const attemptCreateComment = comment => async (dispatch, getState) => {
  dispatch(createCommentRequest());
  try {
    
    let c = (comment.comment).concat("<author>", getState().authentication.user.username)
    c = String(c)
    let s = getState()
    let ps = String(s.threads.thread.posts[0].hash)
    let user = getState().authentication.user.username
    let data = {
        "content": c,
        "parentPostHash": ps,
        "threadheadPostHash": ps,
        "maxCost": 100,
        "minDuration": 300,
        "username": user
    }
    const json = await createComment(data);
    dispatch(createCommentSuccess(json));
  } catch (error) {
    dispatch(createCommentError(error));
    }
}



//*************////////////////////////////////////////////*************//
// Thread Like/Dislike
//*************////////////////////////////////////////////*************//


// Action Types
export const LIKE_THREAD_REQUEST = 'LIKE_THREAD_REQUEST';
export const LIKE_THREAD_SUCCESS = 'LIKE_THREAD_SUCCESS';
export const LIKE_THREAD_ERROR = 'LIKE_THREAD_ERROR';

function likeThreadRequest() {
    return {
        type: LIKE_THREAD_REQUEST
    }
}

function likeThreadSuccess(post) {
    return {
        type: LIKE_THREAD_SUCCESS,
        post
    }
}

function likeThreadError(error) {
    return {
        type: LIKE_THREAD_ERROR,
        error
    }
}

export const attemptLike = (hash,votes) => async (dispatch, getState) => {
    dispatch(likeThreadRequest());
    try {
        const { token } = getState().authentication;
        const json = await attemptLikePost(hash,votes,token);
        dispatch(likeThreadSuccess(json));
    } catch (error) {
        dispatch(likeThreadError(error));
    }
}


//*************////////////////////////////////////////////*************//
// Moderator Tag
//*************////////////////////////////////////////////*************//


// Action Types
export const ADD_TAG_REQUEST = 'ADD_TAG_REQUEST';
export const ADD_TAG_SUCCESS = 'ADD_TAG_SUCCESS';
export const ADD_TAG_ERROR = 'ADD_TAG_ERROR';

function addTagRequest() {
    return {
        type: ADD_TAG_REQUEST
    }
}

function addTagSuccess(post) {
    return {
        type: ADD_TAG_SUCCESS,
        post
    }
}

function addTagError(error) {
    return {
        type: ADD_TAG_ERROR,
        error
    }
}

export const attemptAddTag = (tag, hash) => async (dispatch, getState) => {
    dispatch(addTagRequest());
    try {
        const { token } = getState().authentication;
        const body = {'token': token, 'hash': hash, 'tag': tag }
        const post = await attemptTag(body);
        dispatch(addTagSuccess(post));
    } catch (error) {
        dispatch(addTagError(error));
    }
}



export const GETFILTERS_REQUEST = 'GETFILTERS_REQUEST';
export const GETFILTERS_SUCCESS = 'GETFILTERS_SUCCESS';
export const GETFILTERS_ERROR = 'GETFILTERS_ERROR';


function getFiltersRequest() {
    return {
        type: GETFILTERS_REQUEST
    }
}

function getFiltersSuccess(filtersEchoes) {
    return {
        type: GETFILTERS_SUCCESS,
        filtersEchoes
    }
}

function getFiltersError(error) {
    return {
        type: GETFILTERS_ERROR,
        error
    }
}



export const getFilters = () => async (dispatch, getState) => {
    dispatch(getFiltersRequest());
    try {
        let json = {
            'username': getState().authentication.user.username,
        }
        const json2 = await attemptGetFilters(json);
        dispatch(getFiltersSuccess(json2));
    } catch (error) {
        dispatch(getFiltersError(error));
    }
}