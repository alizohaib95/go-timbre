import {
    getAccount,
    makeTransaction,
    getBlockchainState,
    changeRole,
    getCommittee,
    createVote,
    fetchModerators
} from '../../utilities/api';

//*************////////////////////////////////////////////*************//
// Transaction Maker
//*************////////////////////////////////////////////*************//

// Action Types
export const CREATE_TRANSACTION_REQUEST = 'CREATE_TRANSACTION_REQUEST';
export const CREATE_TRANSACTION_SUCCESS = 'CREATE_TRANSACTION_SUCCESS';
export const CREATE_TRANSACTION_ERROR = 'CREATE_TRANSACTION_ERROR';

function createTransactionRequest() {
    return {
        type: CREATE_TRANSACTION_REQUEST
    }
}

function createTransactionSuccess(trans) {
    return {
        type: CREATE_TRANSACTION_SUCCESS,
        trans
    }
}

function createTransactionError(error) {
    return {
        type: CREATE_TRANSACTION_ERROR,
        error
    }
}

export const attemptCreateTransaction = transaction => async (dispatch, getState) => {
    dispatch(createTransactionRequest);
    try {
        let data = {
            port: String(transaction.port),
            amount: String(transaction.amount)
        }
        const json = await makeTransaction(data);
        dispatch(createTransactionSuccess(json));
    } catch (error) {
        dispatch(createTransactionError(error));
    }
}


//*************////////////////////////////////////////////*************//
// Vote Creater --------- TODO: Create function in API
//*************////////////////////////////////////////////*************//

// Action Types
export const CREATE_VOTE_REQUEST = 'CREATE_VOTE_REQUEST';
export const CREATE_VOTE_SUCCESS = 'CREATE_VOTE_SUCCESS';
export const CREATE_VOTE_ERROR = 'CREATE_VOTE_ERROR';

function createVoteRequest() {
    return {
        type: CREATE_VOTE_REQUEST
    }
}

function createVoteSuccess(vote) {
    return {
        type: CREATE_VOTE_SUCCESS,
        vote
    }
}

function createVoteError(error) {
    return {
        type: CREATE_VOTE_ERROR,
        error
    }
}

export const attemptCreateVote = vote => async (dispatch, getState) => {
    dispatch(createVoteRequest());
    try {
        
        const json = await createVote(vote);
        dispatch(createVoteSuccess(json));
    } catch (error) {
        dispatch(createVoteError(error));
    }
}


//*************////////////////////////////////////////////*************//
// Fetch Account Information
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_ACCOUNT_REQUEST = 'FETCH_ACCOUNT_REQUEST';
export const FETCH_ACCOUNT_SUCCESS = 'FETCH_ACCOUNTSUCCESS';
export const FETCH_ACCOUNT_ERROR = 'FETCH_ACCOUNT_ERROR';

function createFetchAccountRequest() {
    return {
        type: FETCH_ACCOUNT_REQUEST
    }
}

function createFetchAccountSuccess(account) {
    return {
        type: FETCH_ACCOUNT_SUCCESS,
        account
    }
}

function createFetchAccountError(error) {
    return {
        type: FETCH_ACCOUNT_ERROR,
        error
    }
}

export const fetchAccount = () => async (dispatch, getState) => {
    dispatch(createFetchAccountRequest());
    try {
        let uname = getState().authentication.user.username
        const json = await getAccount({'username': uname});
        dispatch(createFetchAccountSuccess(json));
    } catch (error) {
        dispatch(createFetchAccountError(error));
    }
}

//*************////////////////////////////////////////////*************//
// Fetch Blockchain Information
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_BLOCKCHAIN_REQUEST = 'FETCH_BLOCKCHAIN_REQUEST';
export const FETCH_BLOCKCHAIN_SUCCESS = 'FETCH_BLOCKCHAINSUCCESS';
export const FETCH_BLOCKCHAIN_ERROR = 'FETCH_BLOCKCHAIN_ERROR';

function createFetchBlockchainRequest() {
    return {
        type: FETCH_BLOCKCHAIN_REQUEST
    }
}

function createFetchBlockchainSuccess(blockchain) {
    return {
        type: FETCH_BLOCKCHAIN_SUCCESS,
        blockchain
    }
}

function createFetchBlockchainError(error) {
    return {
        type: FETCH_BLOCKCHAIN_ERROR,
        error
    }
}

export const fetchBlockchainState = () => async (dispatch, getState) => {
    dispatch(createFetchBlockchainRequest());
    try {
        const json = await getBlockchainState();
        dispatch(createFetchBlockchainSuccess(json));
    } catch (error) {
        dispatch(createFetchBlockchainError(error));
    }
}

//*************////////////////////////////////////////////*************//
// Change Roles
//*************////////////////////////////////////////////*************//

// Action Types
export const CHANGE_ROLE_REQUEST = 'CHANGE_ROLE_REQUEST';
export const CHANGE_ROLE_SUCCESS = 'CHANGE_ROLESUCCESS';
export const CHANGE_ROLE_ERROR = 'CHANGE_ROLE_ERROR';

function changeRoleRequest() {
    return {
        type: CHANGE_ROLE_REQUEST
    }
}

function changeRoleSuccess(role) {
    return {
        type: CHANGE_ROLE_SUCCESS,
        role
    }
}

function changeRoleError(error) {
    return {
        type: CHANGE_ROLE_ERROR,
        error
    }
}

export const extendRole = (role) => async (dispatch, getState) => {
    dispatch(changeRoleRequest());
    try {
        const json = await changeRole(role);
        dispatch(changeRoleSuccess(json));
    } catch (error) {
        dispatch(changeRoleError(error));
    }
}

// This should later go to another file for utilities
export const COMMITTEE_REQUEST = 'COMMITTEE_REQUEST';
export const COMMITTEE_SUCCESS = 'COMMITTEE_SUCCESS';
export const COMMITTEE_ERROR = 'COMMITTEE_ERROR';

function getCommitteeRequest() {
    return {
        type: COMMITTEE_REQUEST
    }
}

function getCommitteeSuccess(committee) {
    return {
        type: COMMITTEE_SUCCESS,
        committee
    }
}

function getCommitteeError(error) {
    return {
        type: COMMITTEE_ERROR,
        error
    }
}

export const attemptGetCommittee = () => async dispatch => {
    dispatch(getCommitteeRequest);
    try {
        const comm = await getCommittee();
        dispatch(getCommitteeSuccess(comm));
    } catch (error) {
        dispatch(getCommitteeError(error));
    }
};



