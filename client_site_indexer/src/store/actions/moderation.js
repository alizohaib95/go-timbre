import {
    fetchModerators,
    beaMod,
    fetchModeratorProfile,
    addRegexFilter,
    removeRegexFilter,
    updateEchoes,
    attemptFollowaMod
} from '../../utilities/api';

export const MODERATORS_REQUEST = 'MODERATORS_REQUEST';
export const MODERATORS_SUCCESS = 'MODERATORS_SUCCESS';
export const MODERATORS_ERROR = 'MODERATORS_ERROR';


function getModeratorsListRequest() {
    return {
        type: MODERATORS_REQUEST
    }
}

function getModeratorsListSuccess(mods) {
    return {
        type: MODERATORS_SUCCESS,
        mods
    }
}

function getModeratorsListError(error) {
    return {
        type: MODERATORS_ERROR,
        error
    }
}

export const attemptFetchMods = () => async dispatch => {
    dispatch(getModeratorsListRequest);
    try {
        const mods = await fetchModerators();
        dispatch(getModeratorsListSuccess(mods));
    } catch (error) {
        dispatch(getModeratorsListError(error));
    }
};




export const BECOMEMOD_REQUEST = 'BECOMEMOD_REQUEST';
export const BECOMEMOD_SUCCESS = 'BECOMEMOD_SUCCESS';
export const BECOMEMOD_ERROR = 'BECOMEMOD_ERROR';


function becomeModRequest() {
    return {
        type: BECOMEMOD_REQUEST
    }
}

function becomeModSuccess(mods) {
    return {
        type: BECOMEMOD_SUCCESS,
        mods
    }
}

function becomeModError(error) {
    return {
        type: BECOMEMOD_ERROR,
        error
    }
}

export const attemptBecomeAMod = (mod) => async (dispatch,getState) => {
    dispatch(becomeModRequest);
    try {
        let json = {
            'username': mod.username,
            'filters': mod.url,

        }
        const mods = await beaMod(json);
        dispatch(becomeModSuccess(mods));
    } catch (error) {
        dispatch(becomeModError(error));
    }
};

export const MODERATORPROF_REQUEST = 'MODERATORPROF_REQUEST';
export const MODERATORPROF_SUCCESS = 'MODERATORPROF_SUCCESS';
export const MODERATORPROF_ERROR = 'MODERATORPROF_ERROR';


function getModeratorProfileRequest() {
    return {
        type: MODERATORPROF_REQUEST
    }
}

function getModeratorProfileSuccess(mod) {
    return {
        type: MODERATORPROF_SUCCESS,
        mod
    }
}

function getModeratorProfileError(error) {
    return {
        type: MODERATORPROF_ERROR,
        error
    }
}

export const attemptFetchModProf = (username) => async (dispatch,getState) => {
    dispatch(getModeratorProfileRequest);
    try {
        let json  = {
            'username': username,
        }
        const mod = await fetchModeratorProfile(json);
        dispatch(getModeratorProfileSuccess(mod));
    } catch (error) {
        dispatch(getModeratorProfileError(error));
    }
};



export const ADDREGEXFILTER_REQUEST = 'ADDREGEXFILTER_REQUEST';
export const ADDREGEXFILTER_SUCCESS = 'ADDREGEXFILTER_SUCCESS';
export const ADDREGEXFILTER_ERROR = 'ADDREGEXFILTER_ERROR';


function attemptAddRegexFilterRequest() {
    return {
        type: ADDREGEXFILTER_REQUEST
    }
}

function attemptAddRegexFilterSuccess(filters) {
    return {
        type: ADDREGEXFILTER_SUCCESS,
        filters
    }
}

function attemptAddRegexFilterError(error) {
    return {
        type: ADDREGEXFILTER_ERROR,
        error
    }
}


export const attemptAddFilter = (filters) => async (dispatch, getState) => {
    dispatch(attemptAddRegexFilterRequest);
    try {
        let json = {
            'username': getState().authentication.user.username,
            'filters': filters.filters

        }
        const mod = await addRegexFilter(json);
        dispatch(attemptAddRegexFilterSuccess(mod));
    } catch (error) {
        dispatch(attemptAddRegexFilterError(error));
    }
};



export const REMOVEREGEXFILTER_REQUEST = 'REMOVEREGEXFILTER_REQUEST';
export const REMOVEREGEXFILTER_SUCCESS = 'REMOVEREGEXFILTER_SUCCESS';
export const REMOVEREGEXFILTER_ERROR = 'REMOVEREGEXFILTER_ERROR';


function attemptDeleteRegexFilterRequest() {
    return {
        type: REMOVEREGEXFILTER_REQUEST
    }
}

function attemptDeleteRegexFilterSuccess(filters) {
    return {
        type: REMOVEREGEXFILTER_SUCCESS,
        filters
    }
}

function attemptDeleteRegexFilterError(error) {
    return {
        type: REMOVEREGEXFILTER_ERROR,
        error
    }
}


export const attemptDeleteFilter = (filters) => async (dispatch, getState) => {

    dispatch(attemptDeleteRegexFilterRequest);
    try {
        let json = {
            'username': getState().authentication.user.username,
            'index': filters

        }
        const mod = await removeRegexFilter(json);
        dispatch(attemptDeleteRegexFilterSuccess(mod));
    } catch (error) {
        dispatch(attemptDeleteRegexFilterError(error));
    }
};




export const UPDATEECHO_REQUEST = 'UPDATEECHO_REQUEST';
export const UPDATEECHO_SUCCESS = 'UPDATEECHO_SUCCESS';
export const UPDATEECHO_ERROR = 'UPDATEECHO_ERROR';


function attemptUpdateEchoRequest() {
    return {
        type: UPDATEECHO_REQUEST
    }
}

function attemptUpdateEchoSuccess(filters) {
    return {
        type: UPDATEECHO_SUCCESS,
        filters
    }
}

function attemptUpdateEchoError(error) {
    return {
        type: UPDATEECHO_ERROR,
        error
    }
}


export const attemptUpdateEchoes = (filters) => async (dispatch, getState) => {

    dispatch(attemptUpdateEchoRequest);
    try {
        let json = {
            'username': getState().authentication.user.username,
            'postHash': filters

        }
        const mod = await updateEchoes(json);
        dispatch(attemptUpdateEchoSuccess(mod));
    } catch (error) {
        dispatch(attemptUpdateEchoError(error));
    }
};



export const FOLLOWMOD_REQUEST = 'FOLLOWMOD_REQUEST';
export const FOLLOWMOD_SUCCESS = 'FOLLOWMOD_SUCCESS';
export const FOLLOWMOD_ERROR = 'FOLLOWMOD_ERROR';



function attemptFollowModRequest() {
    return {
        type: FOLLOWMOD_REQUEST
    }
}

function attemptFollowModSuccess(filters) {
    return {
        type: FOLLOWMOD_SUCCESS,
        filters
    }
}

function attemptFollowModError(error) {
    return {
        type: FOLLOWMOD_ERROR,
        error
    }
}


export const attemptFollowMod = (following) => async (dispatch, getState) => {

    dispatch(attemptFollowModRequest);
    try {
        let json = {
            'username': getState().authentication.user.username,
            'following': following
        }
        const mod = await attemptFollowaMod(json);
        dispatch(attemptFollowModSuccess(mod));
    } catch (error) {
        dispatch(attemptFollowModError(error));
    }
};




