import {
    login,
    signup
} from '../../utilities/api';

//*************////////////////////////////////////////////*************//
// Authentication Actions
//*************////////////////////////////////////////////*************//

// Action Types
export const LOGIN_REQUEST = 'LOGIN_REQUEST';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';

// Action Creators
function loginRequest(){
    return {
        type: LOGIN_REQUEST
    }
}
function loginSuccess(login){
    return {
        type: LOGIN_SUCCESS,
        login
    }
}
function loginError(error){
    return {
        type: LOGIN_ERROR,
        error
}
}

// Action Dispatchers
export const attemptLogin = (user) => async dispatch => {
    dispatch(loginRequest);
    try {
        const token = await login(user.username, user.password);
        dispatch(loginSuccess(token));
    } catch (error) {
        dispatch(loginError(error));
    }
};

export const SIGNUP_REQUEST = 'SIGNUP_REQUEST';
export const SIGNUP_SUCCESS = 'SIGNUP_SUCCESS';
export const SIGNUP_ERROR = 'SIGNUP_ERROR';

function signupRequest(){ 
    return {
        type: SIGNUP_REQUEST
    }
}
function signupSuccess(login){
    return {
        type: SIGNUP_SUCCESS,
        login
    }
}
function signupError (error){
    return {
        type: SIGNUP_ERROR,
        error
    }
}

export const attemptSignup = (user) => async dispatch => {
    dispatch(signupRequest);
    try {
        const token = await signup(user.username, user.password);
        dispatch(signupSuccess(token));
    } catch (error) {
        dispatch(signupError(error));
    }
};

export const LOGOUT = 'LOGOUT';
export const logout = () => ({
    type: LOGOUT
});
