
import React from 'react'

import {
    NotificationContainer,
    NotificationManager
} from 'react-notifications';

import Notifiction from 'react-notification-modal'
import 'react-notification-modal/dist/index.css'

import { store } from 'react-notification-modal';


export const SHOW_ERROR = 'SHOW_ERROR';
const showErrorAc = error => ({
    type: SHOW_ERROR,
    error
});

export const SHOW_SUCCESS = 'SHOW_SUCCESS';
const showSuccessAc = success => ({
    type: SHOW_SUCCESS,
    success
});

export const SHOW_EXC = 'SHOW_EXC';
const showExcAc = exc => (
    {
        type: SHOW_EXC,
        exc
    }
)

export const showExc = exc => dispatch => {
    store.addNotification({
        title: "Exception!",
        status: 'New Sign Up',
        message:'Save your key',
        exception: "Please copy and save your key. You will need this to execute transactions.",
        info: <div className={'alert alert-primary overflow-auto'}>{exc.login.token}</div>,
        type: "info",
    });
    // dispatch(showExcAc(exc))
}

export const showError = error => dispatch => {
    NotificationManager.error('Error',  String(error));
    dispatch(showErrorAc(error));
};

export const showSuccess = (success, timeout = 10000, fn)  => dispatch => {
    NotificationManager.success('Success', String(success), timeout, fn);
    dispatch(showSuccessAc(success));
};
