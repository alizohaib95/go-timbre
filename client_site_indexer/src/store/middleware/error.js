import {
    LOGIN_ERROR,
    SIGNUP_ERROR,
    LOGIN_SUCCESS,
    SIGNUP_SUCCESS
} from '../actions/authentication';

import{
    FETCH_THREADS_ERROR,
    FETCH_THREAD_ERROR,
    CREATE_THREAD_SUCCESS,
} from '../actions/threads'
import {
    CREATE_TRANSACTION_SUCCESS
}
from '../actions/transactions'
import {
    showError,
    showSuccess,
    showExc,
} from '../actions/error';

import {
    CREATE_COMMENT_SUCCESS
} from '../actions/threads';

export default store => next => action => {
    next(action);
    switch (action.type) {
        case LOGIN_ERROR:
        case SIGNUP_ERROR:
            store.dispatch(showError(action.error));
            break;
        case SIGNUP_SUCCESS:
            // store.dispatch(showExc(action))
            break;
        case CREATE_COMMENT_SUCCESS:
            store.dispatch(showSuccess("Your comment was submitted. It may take atleast 6 seconds to appear."))
            break;
        case CREATE_THREAD_SUCCESS:
            store.dispatch(showSuccess("Your post was submitted. It may take atleast 6 seconds to appear."))
            break;
        case CREATE_TRANSACTION_SUCCESS:
            store.dispatch(showSuccess("Your transaction has been submitted."))
        default:
            break;
    }
};