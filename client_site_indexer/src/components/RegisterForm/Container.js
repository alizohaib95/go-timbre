import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptSignup } from '../../store/actions/authentication';
import RegisterForm from './Component';
import withAuth from '../../utilities/withAuth';

const mapDispatchToProps = {
    attemptSignup
};

const mapStateToProps = state => ({
    loading: state.authentication.loading
});

const enhance = compose(
  reduxForm({ form: 'registerForm' }),
  withAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const RegisterFormContainer = enhance(RegisterForm);

export default RegisterFormContainer;