import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptLogin } from '../../store/actions/authentication';
import LoginForm from './Component';
import withAuth from '../../utilities/withAuth';

const mapDispatchToProps = {
    attemptLogin
};

const mapStateToProps = state => ({
    loading: state.authentication.loading
});

const enhance = compose(
  reduxForm({ form: 'LoginForm' }),
  withAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const RegisterFormContainer = enhance(LoginForm);

export default RegisterFormContainer;