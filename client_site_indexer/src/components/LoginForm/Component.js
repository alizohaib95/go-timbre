import React from 'react';
import { Field, reduxForm } from 'redux-form';

class LoginForm extends React.Component {
    
    componentDidMount() {
        this.redirectIfLoggedIn();
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        this.redirectIfLoggedIn();
    }

    redirectIfLoggedIn() {
        if (this.props.token) this.props.history.push('/');
    }

    attemptLogin = user => {
        this.props.attemptLogin(user);
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.attemptLogin)
    };

    render() {
        return (
            
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <div className="card"> 
                        <div className="card-body">
                            <h5 className="card-title">Log in</h5>
                            <p className="card-text">Enter your details to log in to your account</p>
                            <form onSubmit={this.onSubmit()}>
                                <div className="form-group">
                                    <label>Username</label>
                                    <Field className="form-control" name="username" component="input" type="text" placeholder="username" required/>
                                </div>
                                <div className="form-group">
                                    <label>Password</label>
                                    <Field className="form-control" name="password" component="input" type="password" required/>
                                    <button type="submit" className="btn btn-primary float-right mt-3">Log In</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

        

        );
    }
}

export default LoginForm;