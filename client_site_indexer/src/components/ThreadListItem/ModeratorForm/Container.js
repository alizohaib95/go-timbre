import { connect } from 'react-redux';
import { compose } from 'redux';
import withAuth from '../../../utilities/withAuth';
import ModeratorForm from './Component';
import { reduxForm } from 'redux-form';
import { attemptAddTag } from '../../../store/actions/threads';

const mapDispatchToProps = { attemptAddTag };

const mapStateToProps = state => ({
    loading: state.threads.loading
});

const enhance = compose(
    reduxForm({
        form: 'moderatorForm'
    }),
    withAuth,
    connect(
        mapStateToProps,
        mapDispatchToProps,
    )
);

const ModeratorFormContainer = enhance(ModeratorForm);

export default ModeratorFormContainer;
