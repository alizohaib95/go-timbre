import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowUp,faArrowDown } from '@fortawesome/free-solid-svg-icons';
import { Field, reduxForm } from 'redux-form'
import { faEyeSlash} from '@fortawesome/free-solid-svg-icons';



class ModeratorForm extends React.Component {

    attemptAddTag = tag => {
        this.props.attemptAddTag(tag.tag, this.props.hash);
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.attemptAddTag)
    };

    mapTags = (tags) =>{
        return tags.map((tag, index) => {
            return <span className="badge badge-primary mr-1 mb-1" key={index}>{tag.tags}</span>
        })
    }

    render() {
        return (
        <div className="">
            
            {this.props.tags && <div className="float-right">{this.mapTags(this.props.tags)}</div>}
            {this.props.tags && this.props.fullText && 
                <form className="form-inline pt-3 mb-2 pb-1 ml-2" onSubmit={this.onSubmit()}>
                    
                    <div className="form-group">
                        <Field className="px-2 py-1 border-primary" name="tag" component="select" placeholder="asd">
                            <option>Select Tag</option>
                            <option value="SPAM">Spam</option>
                            <option value="ADVERTISEMENT">Advertisement</option>
                            <option value="FAKE NEWS">Fake News</option>
                            <option value="NSFW">NSFW</option>
                        </Field>

                        {/* <Field className="form-control float-left form-control-sm" name="tag" component="input" type="text" placeholder="fakenews, spam, etc." required/> */}
                        <button type="submit" className="float-left btn btn-sm btn-primary ml-1 py-1">Add Tag</button>
                    </div>
                </form>
            }
            
        </div>
        );
  }
}

export default ModeratorForm;
