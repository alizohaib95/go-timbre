import { connect } from 'react-redux';
import { compose } from 'redux';
import withAuth from '../../../utilities/withAuth';
import { attemptLike } from '../../../store/actions/threads';
import PostVote from './Component';

const mapDispatchToProps = { attemptLike };

const enhance = compose(
  withAuth,
  connect(
    null,
    mapDispatchToProps,
  )
);

const PostVoteContainer = enhance(PostVote);

export default PostVoteContainer;
