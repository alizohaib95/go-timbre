import React from 'react';
import { Field, reduxForm } from 'redux-form'
import Skeleton from 'react-loading-skeleton';

class CreateThreadForm extends React.Component {
    
    createThread = thread => {
        this.props.attemptCreateThread(thread);
    }

    componentDidUpdate(prevProps, prevState, snapshot) {
        // Redirect if not logged in
        const { token, post, history } = this.props;
        if (!token) history.push('/');
        if (post) history.push(`/${post}`);
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.createThread)
    };

    render() {
        return (
    
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <div className="card"> 
                        <div className="card-body">
                            {this.props.isFetching ? 
                            <Skeleton count={20}/> 
                            :<>
                            <h5 className="card-title">Create a Thread</h5>
                            <p className="card-text">New threads may take upto 6 seconds to appear</p>
                            <form onSubmit={this.onSubmit()} loading={this.props.isFetching.toString()}>
                                <div className="form-group">
                                    <label>Title</label>
                                    <Field className="form-control" name="title" component="input" type="text" placeholder="Lorem Ipsum" required/>
                                </div>
                                <div className="form-group">
                                    <label>Content</label>
                                    <Field className="form-control" name="content"  maxLength="200" component="textarea" as='textarea' rows="6" required/>
                                    <button type="submit" className="btn btn-primary float-right mt-3">Submit Thread</button>
                                </div>
                            </form>
                            </>
                            }
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        );
    }
}

export default CreateThreadForm;