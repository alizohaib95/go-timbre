import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptCreateThread } from '../../store/actions/threads';
import CreateThreadForm from './Component';
import withAuth from '../../utilities/withAuth';

const mapStateToProps = state => ({
  isFetching: state.threads.isFetching,
  post: state.threads.newThread
});
const mapDispatchToProps = { attemptCreateThread };

const enhance = compose(
  reduxForm({ form: 'createThread', initialValues: { type: 'text' }, }),
  withAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const CreateThreadFormContainer = enhance(CreateThreadForm);

export default CreateThreadFormContainer;