import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from '../../utilities/history';
import Header from '../Header/Container'
import HomeContainer from '../Homepage/Container';
import CreateThreadFormContainer from '../CreateThreadForm/Container'
import AmountTransFormContainer from '../AmountTransForm/Container'
import VoteTransFormContainer from '../VoteTransForm/Container'
import RoleManagerContainer from '../RoleManagerForm/Container'
import RegisterContainer from '../RegisterForm/Container'
import LoginContainer from '../LoginForm/Container'
import ModeratorContainer from '../ModeratorComponent/Container';
import BeAModContainer from '../BecomeAModerator/Container';
import {NotificationContainer} from 'react-notifications';
import ChainExplorerContainer from '../ChainExplorer/Container'
import Discover from '../Discover/Component'
import 'react-notifications/lib/notifications.css';

import Notifiction from 'react-notification-modal'
import 'react-notification-modal/dist/index.css'


const App = props => (
    
    <Router history={history}>
      <>
        <Route component={Header} />
        <Route component={NotificationContainer} / >
        <Notifiction />
        <Switch>
          <Route path='/submit' component={CreateThreadFormContainer} />
          <Route path='/transaction' component={AmountTransFormContainer} />
          <Route path='/vote' component={VoteTransFormContainer} />
          <Route path='/roles' component={RoleManagerContainer} />
          <Route path='/register' component={RegisterContainer} />
          <Route path='/login' component={LoginContainer} />
          <Route path='/explorer' component={ChainExplorerContainer} />
          {/* <Route path='/moderate' component={ModeratorContainer} /> */}
          <Route path='/becomeamoderater' component={BeAModContainer} />
          {/* <Route path='/discover' component={Discover} /> */}
          <Route
            exact
            path='/m/:username'
            render={({ match}) => (
              <ModeratorContainer urlusername={match.params.username}/>
            )}
          />
          <Route path='/' component={HomeContainer} />
        </Switch>
      </>
     </Router>
);

export default App;
