import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptCreateVote,attemptGetCommittee } from '../../store/actions/transactions';
import VoteTransForm from './Component';


export const mapStateToProps = state => ({
  isProcessing: state.transactions.isProcessing,
  committee: state.transactions.committee
});

const mapDispatchToProps = {
  attemptCreateVote,
  attemptGetCommittee
};

const enhance = compose(
  reduxForm({ form: 'createVote' }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const VoteTransFormContainer = enhance(VoteTransForm);

export default VoteTransFormContainer;