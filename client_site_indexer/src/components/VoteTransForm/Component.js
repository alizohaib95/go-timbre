import React from 'react';
import { Field, reduxForm } from 'redux-form';
import Skeleton from 'react-loading-skeleton';

class VoteTransForm extends React.Component {
  
    componentDidMount() {
        this.props.attemptGetCommittee();
    }

    createVote = vote => {
        this.props.attemptCreateVote(vote);
        return this.props.history.push("/");
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.createVote)
    };

    mapCommittee = (committee) => {
        return committee.map((member, index) => {
        return <li key={index} className = "list-group-item">Miner#{index}: {member}</li>
        });
    }

    render() {
        return (
    
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <div className="card"> 
                        <div className="card-body">
                            <h5 className="card-title">Vote a Candidate</h5>
                            <p className="card-text">Current DPOS Committee</p>

                            
                            <ul className="list-group">
                                {
                                    !this.props.committee || this.props.committee.length === 0 ?
                                        <li className = "list-group-item text-danger" >Committe Not Found</li>:
                                    this.mapCommittee(this.props.committee.candidates)
                                }
                            </ul>

                            <form onSubmit={this.onSubmit()}>
                                <div className="form-group mt-3">
                                    <label>Public Key</label>
                                    <Field className="form-control" name="publicKey" type="text" component="input" placeholder="Public key..."/>

                                </div>
                                <div className="form-group">
                                    <label>Vote Percentage</label>
                                    <Field className="form-control" name="percent" component="input" type="number"/>
                                    <button type="submit" className="btn btn-primary float-right mt-3">Send Vote</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        );
    }
}

export default VoteTransForm;