import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptBecomeAMod } from '../../store/actions/moderation.js';
import CreateThreadForm from './Component';

const mapDispatchToProps = { attemptBecomeAMod };

const enhance = compose(
  reduxForm({ form: 'createModeration' }),
  connect(
    null,
    mapDispatchToProps
  )
);

const BeAModContainer = enhance(CreateThreadForm);

export default BeAModContainer;