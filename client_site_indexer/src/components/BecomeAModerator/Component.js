import React from 'react';
import { Field, reduxForm } from 'redux-form'

class CreateThreadForm extends React.Component {
  
    createThread = thread => {
        this.props.attemptBecomeAMod(thread);
        return this.props.history.push("/");
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.createThread)
    };

    render() {
        return (        
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <div className="card"> 
                        <div className="card-body">
                            <h5 className="card-title">Register a Moderator</h5>
                            <p className="card-text">Create simple moderator</p>
                            <form onSubmit={this.onSubmit()}>
                                <div className="form-group">
                                    <label>Username</label>
                                    <Field className="form-control" name="username" component="input" type="text" placeholder="moderator name" required/>
                                </div>
                                <div className="form-group">
                                    {/* <label>Regex/Url</label> */}
                                    {/* <Field className="form-control" name="url" component="textarea" placeholder="e.g.: /p([a-z]+)ch/" required/> */}
                                    <button type="submit" className="btn btn-primary float-right mt-3">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        );
    }
}

export default CreateThreadForm;