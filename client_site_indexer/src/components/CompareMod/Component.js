import React from 'react';
import Skeleton from 'react-loading-skeleton';
import { Link } from 'react-router-dom';
import ThreadListItem from '../ThreadListItem/Component';
import ShowMore from '@tedconf/react-show-more';
import Discover from '../Discover/Component'

class CompareMod extends React.Component {

  sortByControversial = threads => {

    let sorted = threads.sort((a, b) => {
      const score1 = a.posts[0].votes.reduce((acc, v) => acc + v.vote, 0)
      const contro1 = a.posts[0].votes.length / Math.max(score1, 1)
      const score2 = b.posts[0].votes.reduce((acc, v) => acc + v.vote, 0)
      const contro2 = b.posts[0].votes.length / Math.max(score2, 1)
      return (contro2 - contro1)

    });
    return sorted

  }

  mapThreads = (threads) =>{
    if (!threads){
      return
    }
        return threads.map((thread, index) => {

            if (this.props.moderator) {
              
              return this.props.moderator.moderated_posts.map((item) => {
                if (item.hash == thread.posts[0].hash) {
                  return <div className="bg-danger mb-3 py-3 pt-4 px-4" key={index} style={{'padding': '10px'}}><ThreadListItem  user={this.props.user} key={index} {...thread} /></div>
                }
                return <div className="mb-3 " key={index} style={{'padding': '10px'}}><ThreadListItem  user={this.props.user} key={index} {...thread} /></div>

              })


            }



        })
  }


  componentDidMount() {
    this.fetchThreads();
  }

  fetchThreads = () => {
    return this.props.fetchThreads();
  }

  convertToForm(mods){
    let ob = {}
    mods.forEach(item => {
      ob[item.id] = item.filters
    })
    return ob
  }

  render() {
    return (
      <div>
        <ul className="list-group mb-3">
            {this.props.moderators && <Discover data={this.convertToForm(this.props.moderators)}/>}
        </ul>

        
        <button type="button" className="btn btn-primary btn-block" data-toggle="modal" data-target="#exampleModal2">
          COMPARE
        </button>
        <div className="modal fade bd-example-modal-xl" id="exampleModal2" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog modal-xl" role="document">
            <div className="modal-content" style={{'background': '#DAE0E6'}}>
              <div className="modal-header bg-white">
                <h5 className="modal-title" id="exampleModalLabel2">Controversial Posts - View</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body px-5">
                <ul className="list-group mb-3">

                            <ShowMore
                                items={this.sortByControversial(this.props.threads)}
                                by={10}
                            >
                                {({current,onMore,}) => (
                                    <React.Fragment>
                                        {current.map(item => (
                                            this.mapThreads([item])
                                        ))}
                                        
                                        <button
                                            className="btn btn-block btn-primary"
                                            disabled={!onMore}
                                            onClick={() => { if (!!onMore) onMore(); }}
                                        >
                                        Show More
                                        </button>
                                    </React.Fragment>
                                )}
                            </ShowMore>


                </ul>
              </div>
              
            </div>
          </div>
        </div>

      </div>
    );
  }
}

export default CompareMod;
