import { connect } from 'react-redux';
import { compose } from 'redux';
import { getModeratorProfile, attemptFollowMod, attemptFetchMods } from '../../store/actions/moderation';
import { fetchThreads, getFilters } from '../../store/actions/threads';
import CompareMod from './Component';
import withAuth from '../../utilities/withAuth';

export const mapStateToProps = state => ({
  // isProcessing: state.transactions.isProcessing,
  isFetching: state.threads.isFetching,
  threads: state.threads.threads,
  moderators: state.moderation.moderators,
  moderator: state.moderation.moderator,
});

const mapDispatchToProps = {
  attemptFetchMods,
  fetchThreads
};

const enhance = compose(
  withAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const CompareModContainer = enhance(CompareMod);

export default CompareModContainer;
