import { connect } from 'react-redux';
import { compose } from 'redux';
import { extendRole } from '../../store/actions/transactions';
import RoleManager from './Component';

const mapDispatchToProps = { extendRole };

const enhance = compose(
  connect(
    null,
    mapDispatchToProps
  )
);

const RoleManagerContainer = enhance(RoleManager);

export default RoleManagerContainer;