import React from 'react';
import ThreadListItem from '../ThreadListItem/Component';
import Skeleton from 'react-loading-skeleton';
import { faLink, faRocket, faBolt, faSignal, faUserCircle, faCogs} from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { Link } from 'react-router-dom';
import ShowMore from '@tedconf/react-show-more';


class ThreadsList extends React.Component {

    updateEchoes = (item) => {
        this.props.attemptUpdateEchoes(item)
    }

    componentDidMount() {
        this.fetchThreads()
        // this.props.fetchThreads(2);
        if (this.props.user){
            this.props.attemptFetchModProf(this.props.user.username)
        }
        
    }
    
    fetchThreads = () => {
        return this.props.fetchThreads();
    }
    sortByDate = threads => {
        return threads.sort((a, b) => {
            return b.posts[0].timestamp - a.posts[0].timestamp
        });
    }
    sortByCategory = (threads, cat) => {

        return threads.filter( threads => {
            
            let c = threads.posts[0].content
            let obj = JSON.parse(c)
            if (obj.Forum == cat){
                return true
            }
            return false
        })
    }

    sortByPopularity = threads => {

        let sorted = threads.sort((a, b) => {
            const score1 = a.posts[0].votes.reduce((acc, v) => acc + v.vote, 0)
            const score2 = b.posts[0].votes.reduce((acc, v) => acc + v.vote, 0)
            return (score2 - score1)

        });
        return sorted
        
    }
    sortByControversial = threads => {

        let sorted = threads.sort((a, b) => {
            const score1 = a.posts[0].votes.reduce((acc, v) => acc + v.vote, 0)
            const contro1 = a.posts[0].votes.length / Math.max(score1, 1)
            const score2 = b.posts[0].votes.reduce((acc, v) => acc + v.vote, 0)
            const contro2 = b.posts[0].votes.length / Math.max(score2, 1)
            return (contro2 - contro1)

        });
        return sorted

    }
    sortByDateAndFilter = threads =>{
        let sorted = this.sortByDate(threads)
        return sorted
    }

    filterForYou = threads => {
        let threads2 = threads.filter((thread) => {
            // Remove thread only if it has a tag from a top moderator
            let modstrings = this.props.moderator && this.props.moderator.following ? this.props.moderator.following : ['']
            let tags = thread.posts[0].tags.map(item => item.user)
            const found = tags.some(r => modstrings.includes(r))

            return !found

        })
        let threads3 = threads2.filter((thread) => {
            return this.props.moderator && !this.props.moderator.echoes.includes(thread.posts[0].hash)
        })
        return threads3
    }

    mapThreads = (threads) =>{

        // if (this.props.filter == true){
            // threads = threads.filter( (thread) => { return (this.props.moderator && this.props.moderator.echoes && this.props.moderator.echoes.includes(thread.posts[0].hash))  })
        // }

        return threads.map((thread, index) => {
            return <ThreadListItem  updateEchoes={this.updateEchoes} user={this.props.user} mod={this.props.moderator} key={index} {...thread} />
        })
    }

    // Will use for the other one
    // mapThreadsFiltered = (threads) =>{

    //     if (this.props.filters){
            
    //         var filters = (this.props.filters.filters) ? this.props.filters.filters: []
    //         var echoes = (this.props.filters.echoes) ? this.props.filters.echoes: []

    //         const filtered = threads.filter((thread) => {
                
    //             let bool = filters.some(keyword => thread.posts[0].content.toLowerCase().includes(keyword))
    //             let bool2 = echoes.some(keyword => thread.posts[0].hash.toLowerCase().includes(keyword))
    //             return bool || !bool2 
    //         });

    //         threads = filtered


    //     }

    //     if (this.props.filter == true){
    //         threads = threads.filter( (thread) => { return (this.props.moderator && this.props.moderator.echoes && this.props.moderator.echoes.includes(thread.posts[0].hash))  })
    //     }

    //     return threads.map((thread, index) => {
    //         return <ThreadListItem  updateEchoes={this.updateEchoes} user={this.props.user} key={index} {...thread} />
    //     })
    // }
    render() {
        
        if (this.props.isFetching) return <Skeleton count={20}/>;
        if (!this.props.threads || this.props.threads.length === 0) return <p>Threads not found..</p>;
        
        let threads = this.props.threads.filter((thread) => {
            // Remove thread only if it has a tag from a top moderator
            let modstrings = this.props.moderators.map(item => item.id)
            let tags = thread.posts[0].tags.map(item=>item.user)
            const found = tags.some(r => modstrings.includes(r))
            
            return !found

        })
        threads = threads.filter( (thread) =>  {
            if (this.props.moderator){
                return !this.props.moderator.echoes.includes(thread.posts[0].hash)
            }
            return true
            
        })
        let sortedByDate = this.sortByDateAndFilter(threads)

        return ( 
            
            <div>
            
            { this.props.user &&
            <div className="card mb-3 py-0 pt-3 pb-2">
                <div className="card-body mb-0 py-0">
                    <form>
                        <div className="form-group row mb-0 px-auto align-baseline" >
                            <div className="col-11 ml-0 mr-0">
                                <Link to="/submit" className="">

                                <div className="input-group">
                                    <div className="input-group-prepend">
                                    <div className="input-group-text"><small><strong>NEW</strong></small></div>
                                    </div>
                                    
                                    <input type="text" className="form-control bg-light" id="inlineFormInputGroup" placeholder="Create a Post" />
                                </div>
                                </Link>

                                
                            </div>
                            <div className="col-1 ml-0 pl-0">
                                <Link to="/submit" className="btn btn-primary mb-2">
                                    <FontAwesomeIcon icon={ faLink }/>
                                </Link>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            }

            {(!this.props.filter ) ?
                <ul className="nav nav-pills mb-3 bg-white px-2 py-2" id="pills-tab" role="tablist">
                    <li className="nav-item " role="presentation">
                        <a className="nav-link active" id="pills-profile-tab" data-toggle="pill" href="#pills-popular" role="tab" aria-controls="pills-profile" aria-selected="false"><FontAwesomeIcon icon={ faRocket } className="mr-2"/>Popular</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a className="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-latest" role="tab" aria-controls="pills-home" aria-selected="true"><FontAwesomeIcon icon={ faSignal } className="mr-2"/>Latest</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a className="nav-link" id="pills-conv-tab" data-toggle="pill" href="#pills-controversial" role="tab" aria-controls="pills-home" aria-selected="true"><FontAwesomeIcon icon={ faBolt } className="mr-2"/>Controversial</a>
                    </li>
                    {this.props.user && <li className="nav-item" role="presentation">
                        <a className="nav-link" id="pills-you-tab" data-toggle="pill" href="#pills-foryou" role="tab" aria-controls="pills-home" aria-selected="true"><FontAwesomeIcon icon={ faUserCircle } className="mr-2"/>For You</a>
                    </li>}
                    {/* <li className="nav-item" role="presentation">
                        <a className="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-world" role="tab" aria-controls="pills-home" aria-selected="true">World News</a>
                    </li>
                    <li className="nav-item" role="presentation">
                        <a className="nav-link" id="pills-home-tab" data-toggle="pill" href="#pills-politics" role="tab" aria-controls="pills-home" aria-selected="true">Politics</a>
                    </li> */}
                    <li className="nav-item ml-auto" role="presentation">
                        <a className="nav-link" id="pills-raw-tab" data-toggle="pill" href="#pills-raw" role="tab" aria-controls="pills-home" aria-selected="true"><FontAwesomeIcon icon={ faCogs } className="mr-2"/>Raw</a>
                    </li>
                    
                    
                </ul>
                
                :<></>
            }
            
                <div className="tab-content" id="pills-tabContent">
                    <div className="tab-pane fade show active" id="pills-popular" role="tabpanel" aria-labelledby="pills-popular-tab">
                        {/* <div>{this.mapThreads() }</div> */}
                        <ShowMore
                                items={this.sortByPopularity(sortedByDate)}
                                by={10}
                            >
                                {({current,onMore,}) => (
                                    <React.Fragment>
                                        {current.map(item => (
                                            this.mapThreads([item])
                                        ))}
                                        
                                        <button 
                                            className="btn btn-block btn-primary"
                                            disabled={!onMore}
                                            onClick={() => { if (!!onMore) onMore(); }}
                                        >
                                        Show More
                                        </button>
                                    </React.Fragment>
                                )}
                            </ShowMore>
                    </div>
                    <div className="tab-pane fade" id="pills-latest" role="tabpanel" aria-labelledby="pills-latest-tab">
                        {/* <div>{this.mapThreads(sortedByDate)}</div> */}

                            <ShowMore
                                items={sortedByDate}
                                by={10}
                            >
                                {({current,onMore,}) => (
                                    <React.Fragment>
                                        {current.map(item => (
                                            this.mapThreads([item])
                                        ))}
                                        
                                        <button 
                                            class = "btn btn-block btn-primary"
                                            disabled={!onMore}
                                            onClick={() => { if (!!onMore) onMore(); }}
                                        >
                                        Show More
                                        </button>
                                    </React.Fragment>
                                )}
                            </ShowMore>

                    </div>
                    <div className="tab-pane fade" id="pills-controversial" role="tabpanel" aria-labelledby="pills-popular-tab">
                        {/* <div>{this.mapThreads(this.sortByControversial(sortedByDate))}</div> */}

                            <ShowMore
                                items={this.sortByControversial(sortedByDate)}
                                by={10}
                            >
                                {({current,onMore,}) => (
                                    <React.Fragment>
                                        {current.map(item => (
                                            this.mapThreads([item])
                                        ))}
                                        
                                        <button
                                            className="btn btn-block btn-primary"
                                            disabled={!onMore}
                                            onClick={() => { if (!!onMore) onMore(); }}
                                        >
                                        Show More
                                        </button>
                                    </React.Fragment>
                                )}
                            </ShowMore>

                    </div>
                    {this.props.user && <div className="tab-pane fade" id="pills-foryou" role="tabpanel" aria-labelledby="pills-popular-tab">
                        {/* <div>{this.mapThreads(this.filterForYou(this.props.threads))}</div> */}
                        
                        <ShowMore
                                items={this.filterForYou(this.props.threads)}
                                by={10}
                            >
                                {({current,onMore,}) => (
                                    <React.Fragment>
                                        {current.map(item => (
                                            this.mapThreads([item])
                                        ))}
                                        
                                        <button 
                                            class = "btn btn-block btn-primary"
                                            disabled={!onMore}
                                            onClick={() => { if (!!onMore) onMore(); }}
                                        >
                                        Show More
                                        </button>
                                    </React.Fragment>
                                )}
                            </ShowMore>

                    </div>}
                    {/* <div className="tab-pane fade" id="pills-world" role="tabpanel" aria-labelledby="pills-popular-tab">
                        <div>{this.mapThreads(this.sortByCategory(threads, "WORLDNEWS"))}</div>
                    </div>
                    <div className="tab-pane fade" id="pills-politics" role="tabpanel" aria-labelledby="pills-popular-tab">
                        <div>{this.mapThreads(this.sortByCategory(this.props.threads, "POLITICS"))}</div>
                    </div> */}
                    <div className="tab-pane fade" id="pills-raw" role="tabpanel" aria-labelledby="pills-home-tab">
                        {/* <div>{this.mapThreads(this.sortByDate(this.props.threads))}</div> */}

                        <ShowMore
                                items={this.sortByDate(this.props.threads)}
                                by={10}
                            >
                                {({current,onMore,}) => (
                                    <React.Fragment>
                                        {current.map(item => (
                                            this.mapThreads([item])
                                        ))}
                                        
                                        <button 
                                            className="btn btn-block btn-primary"
                                            disabled={!onMore}
                                            onClick={() => { if (!!onMore) onMore(); }}
                                        >
                                        Show More
                                        </button>
                                    </React.Fragment>
                                )}
                            </ShowMore>
                        
                    </div>
                    
                    
                </div>
            </div>

        )
        
        

        
    }
}

export default ThreadsList;