import { compose } from 'redux';
import { connect } from 'react-redux';
import { fetchThreads, getFilters } from '../../store/actions/threads';
import ThreadsList from './Component';
import { attemptFetchModProf, attemptAddFilter, attemptDeleteFilter, attemptUpdateEchoes } from '../../store/actions/moderation';
import withAuth from '../../utilities/withAuth';

export const mapStateToProps = state => ({
    isFetching: state.threads.isFetching,
    threads: state.threads.threads,
    filters: state.threads.filters,
    moderator: state.moderation.moderator,
    moderators: state.moderation.moderators.sort( (b,a)=> a.followers.length - b.followers.length).slice(0,10)
});

const mapDispatchToProps = {
  fetchThreads,
  attemptFetchModProf,
  attemptUpdateEchoes,
  getFilters
};


const enhance = compose(
  withAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);



const ThreadsListContainer = enhance(ThreadsList);

export default ThreadsListContainer;
