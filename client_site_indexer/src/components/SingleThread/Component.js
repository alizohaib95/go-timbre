import React from 'react';
import ThreadListItem from '../ThreadListItem/Component';
import Skeleton from 'react-loading-skeleton';
import CommentFormContainer from '../CommentForm/Container'
import CommentList from '../CommentList/Component'

class SingleThread extends React.Component {
  updateEchoes = (item) => {
    this.props.attemptUpdateEchoes(item)
  }
  componentDidMount() {
    // Id coming from url
    this.props.fetchThread(this.props.id);
    if (this.props.user) {
      this.props.attemptFetchModProf(this.props.user.username)
    }
  }
  render() {
    if (this.props.isFetching) return <Skeleton count={10}/>;
    if (!this.props.thread) return <div className="card"><div className="card-body text-center">Post does not exist</div></div>
    return (
      <>
        <ThreadListItem {...this.props.thread} fullText={true} user={this.props.user} mod={this.props.moderator} updateEchoes={this.updateEchoes}/>
        {this.props.token && <CommentFormContainer history={this.props.history}/>}
        <CommentList comments={this.props.thread.posts}/>
        
      </>
    );
  }
}

export default SingleThread;
