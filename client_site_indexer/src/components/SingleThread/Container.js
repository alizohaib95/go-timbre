import { connect } from 'react-redux';
import { compose } from 'redux';
import { fetchThread } from '../../store/actions/threads';
import { attemptUpdateEchoes, attemptFetchModProf } from '../../store/actions/moderation';
import SingleThread from './Component';
import withAuth from '../../utilities/withAuth';


export const mapStateToProps = state => ({
  isFetching: state.threads.isFetching,
  thread: state.threads.thread,
  moderator: state.moderation.moderator
});

const mapDispatchToProps = { fetchThread, attemptUpdateEchoes, attemptFetchModProf};
const enhance = compose(
  withAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const SingleThreadContainer = enhance(SingleThread);

export default SingleThreadContainer;
