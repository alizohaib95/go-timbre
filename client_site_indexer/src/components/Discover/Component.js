import React from 'react';
import {InteractiveForceGraph, ForceGraphNode, ForceGraphLink} from 'react-vis-force';
var Jaccard = require("jaccard-index");



class Discover extends React.Component {

    constructor(props) {
        super(props);
        if (props.data) {
            this.state = {
                data: props.data,
            };
        }
    }

    componentDidMount(){
        this.setState({data: this.props.data})
        this.doSetup()
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        // this.props.attemptFetchModProf(this.props.urlusername)
        if (this.props.data !== nextProps.data) {
            this.setState({
                data: this.props.data,
            });
            this.doSetup()
        }

    }
    

    doSetup(){
        if (!this.props.data){
            return
        }
        let items = Object.keys(this.state.data); // item1, item2, item3
        const getLog = (item) => {
            // return Promise.resolve(this.state.data[item]); // async loading
            return this.state.data[item]; // sync loading
        }
        var options = {
            getLog: getLog
        };

        const showResult = (links) => {
            console.log(JSON.stringify(links, null, 2));
            this.setState({
                logs: links
            })
            // process.exit(0);
        }
        Jaccard(options).getLinks(items).then((l)=>{
            this.setState({logs: l})
        });

    }


    showResult = (links) => {
        return links
    }

    
    
    mapNodes = (logs) =>{

        let items = Object.keys(this.state.data);


        let all = items.map((item,index) => {
            return   <ForceGraphNode r={7} key={index} node={{ id: item }} fill="#007bff" />

        })
        let b = []
        if(this.state.logs){
            b = this.state.logs.map((item, index)    =>{
                return  <ForceGraphLink fill="red" key={index} link={{ source: item.source, target: item.target, value: item.value*10 }} />

            })
        }

        return all.concat(b)
        // return <h1>asd</h1>
    }

    

    render() {
        return (
            
                    <div className="card"> 
                        <div className="card-body px-0 py-0">
                            <InteractiveForceGraph  
                            // labelAttr="label"
                            showLabels
                            opacityFactor={1}
                            zoom
                            zoomOptions={{minScale: 1, maxScale: 5, onZoom: ()=> 'zoomed' , onPan: () => 'panned'}}
                            simulationOptions={{ 
                                animate: true, 
                                width: 300, 
                                height: 150,
                                
                            }}
                            
                            >
                            {this.state.logs && this.mapNodes(this.state.logs)}

                            </InteractiveForceGraph>
                        </div>
                    </div>
            

        

        );
    }
}

export default Discover;

