import { connect } from 'react-redux';
import { compose } from 'redux';
import { getModeratorProfile, attemptFollowMod, attemptFetchMods } from '../../store/actions/moderation';
import ModeratorList from './Component';
import withAuth from '../../utilities/withAuth';

export const mapStateToProps = state => ({
  // isProcessing: state.transactions.isProcessing,
  moderators: state.moderation.moderators,
});

const mapDispatchToProps = {
  attemptFetchMods
};

const enhance = compose(
  withAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const ModeratorListContainer = enhance(ModeratorList);

export default ModeratorListContainer;
