import React from 'react';
import { Link } from 'react-router-dom';


class ModeratorList extends React.Component {

  mapMods = (mods, n) => {
  
      return mods.map ( (mod, index) => {
        if (index>n){return}
        return <li key ={index} className = "text-small list-group-item d-flex justify-content-between align-items-center overflow-auto"><Link to={"/m/"+mod.id}>/m/{mod.id}</Link> <span className="float-right"><small>FOLLOWERS</small>: {mod.followers.length} </span> </li>
      } )
  }

  componentDidMount() {
    this.props.attemptFetchMods();
  }
  render() {
    
    
    if (!this.props.moderators || this.props.moderators.length === 0) return <p> Moderators not found.. </p>;

    let sortedMods = this.props.moderators.sort((b,a) => a.followers.length - b.followers.length)
    return (
      <div>
        <ul className="list-group mb-3">
            {
                this.mapMods(sortedMods, 10)
            }
        </ul>

        
        <button type="button" className="btn btn-primary btn-block" data-toggle="modal" data-target="#exampleModal">
          VIEW ALL MODERATORS
        </button>
        <div className="modal fade" id="exampleModal" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
          <div className="modal-dialog" role="document">
            <div className="modal-content">
              <div className="modal-header">
                <h5 className="modal-title" id="exampleModalLabel">All Moderators</h5>
                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div className="modal-body">
                <ul className="list-group mb-3">
                {
                  this.mapMods(this.props.moderators, 100)
                }
                </ul>
              </div>
              
            </div>
          </div>
        </div>

        {/* {
          this.props.user && this.props.user.IsModerator &&
            <Link to='/becomeamoderater'>
                <span className="btn btn-block btn-primary mb-2">ADD A MODERATOR</span>
            </Link>
        } */}
      </div>
    );
  }
}

export default ModeratorList;
