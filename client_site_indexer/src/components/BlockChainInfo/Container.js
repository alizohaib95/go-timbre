import { connect } from 'react-redux';
import { compose } from 'redux';
import { fetchBlockchainState } from '../../store/actions/transactions';
import BlockchainInfo from './Component';

export const mapStateToProps = state => ({
  isProcessing: state.transactions.isProcessing,
  blockchain: state.transactions.blockchain
});

const mapDispatchToProps = { fetchBlockchainState };

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const BlockchainInfoContainer = enhance(BlockchainInfo);

export default BlockchainInfoContainer;
