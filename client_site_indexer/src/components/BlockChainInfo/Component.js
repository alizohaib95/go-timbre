import React from 'react';
import Skeleton from 'react-loading-skeleton';
import { Link } from 'react-router-dom';

class BlockchainInfo extends React.Component {
  componentDidMount() {  
    this.props.fetchBlockchainState();
  }
  render() {
    if (this.props.isProcessing) return <Skeleton count={3}/>;
    if (!this.props.blockchain) return <p className="text-danger"> Stats not found </p>;
    return (
      <div>
        <ul className="list-group mb-3">
            <li className="text-small list-group-item d-flex justify-content-between align-items-center">
                Height: <strong>{this.props.blockchain.height}</strong>
            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center">
            <small className="overflow-auto">{this.props.blockchain.tail}</small>
            </li>
            <li className="list-group-item d-flex justify-content-between align-items-center">
                Forks: <strong>{this.props.blockchain.forks}</strong>
            </li>
            <Link to='/explorer'>
              <span className="btn btn-block btn-primary mt-3">BLOCKCHAIN EXPLORER</span>
            </Link>
            
        </ul>
      </div>
    );
  }
}

export default BlockchainInfo;
