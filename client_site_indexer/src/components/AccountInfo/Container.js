import { connect } from 'react-redux';
import { compose } from 'redux';
import { fetchAccount } from '../../store/actions/transactions';
import AccountInfo from './Component';
import withAuth from '../../utilities/withAuth';

export const mapStateToProps = state => ({
  isProcessing: state.transactions.isProcessing,
  account: state.transactions.account
});

const mapDispatchToProps = { fetchAccount };

const enhance = compose(
  withAuth,
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const AccountInfoContainer = enhance(AccountInfo);

export default AccountInfoContainer;
