import React from 'react';
import Skeleton from 'react-loading-skeleton';

class AccountInfo extends React.Component {
  componentDidMount() {
    if (this.props.token){
      this.props.fetchAccount();
    }
  }
  render() {
    if (this.props.isProcessing) return <Skeleton count={3}/>;
    if (!this.props.account) return <p className="text-danger"> Account Not found </p>;
    return (
      <div>
        <ul className="list-group mb-3">
            <li id="address-bar" className="text-small list-group-item d-flex justify-content-between align-items-center overflow-auto">
                Address: <strong>{this.props.account.address}</strong>
            </li>
            <li id="money-bar" className="list-group-item d-flex justify-content-between align-items-center">
                Karma: 
                <span className="badge badge-primary badge-pill">{this.props.account.balance}</span>
            </li>
            <li id="nodetype-bar" className="list-group-item d-flex justify-content-between align-items-center">
                Node Type: <strong>{this.props.account.node_type}</strong>
            </li>
        </ul>
      </div>
    );
  }
}

export default AccountInfo;
