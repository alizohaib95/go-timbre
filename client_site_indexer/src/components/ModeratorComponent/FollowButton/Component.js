import React from 'react';

class Follow extends React.Component {
  constructor(props) {
    super(props);
    const didVote = Follow.existingVote(props);
    
    const followers = props.votes.length
    this.state = {
      score: followers,
      didVote,
    };
  }

  static existingVote({ user, votes }) {
    const existingVote =
      user && votes && votes.find(vote => vote === user.username);
    return existingVote ? 1 : 0;
  }

  componentWillUpdate(nextProps, nextState, nextContext) {
    if (this.props.score !== nextProps.score) {
      const didVote = Follow.existingVote(nextProps);
      this.setState({
        score: nextProps.score,
        didVote,
      });
    } else if (this.props.token !== nextProps.token && !nextProps.token) {
      this.setState({
        didVote: false,
      });
    }
  }

  castVote(vote) {
    const {id, token } = this.props;
    if (token) {
      this.props.attemptFollowMod(this.props.urlname);
      this.props.attemptFetchModProf(this.props.urlname)
      this.setState({
        score: this.state.score + vote - this.state.didVote,
        didVote: vote,
      });
    }
  }

  upvote = () => this.castVote(this.state.didVote ? 0 : 1);
  downvote = () => this.castVote(this.state.didVote ? 0 : -1);

  render() {
    
    if (!this.props.user){
      return <span className="d-flex mx-auto justify-content-center mb-2"><strong>{this.state.score ? this.state.score : 0} Followers</strong></span>
    }

    const isLoop = this.props.urlname == this.props.user.username
    if (isLoop){
      return <span className="d-flex mx-auto justify-content-center mb-2"><strong>{this.state.score ? this.state.score : 0} Followers</strong></span>
    }

    return (
      <div className="">
        {this.state.didVote ? 
          <button className="btn btn-block btn-secondary mb-2" onClick={this.downvote}> UNFOLLOW</button>
        :  <button className="btn btn-block btn-primary mb-2" onClick={this.upvote}> FOLLOW</button>
        }
        <span className="d-flex mx-auto justify-content-center mb-2"><strong>{this.state.score ? this.state.score : 0} Followers</strong></span>
      </div>
    );
  }
}

export default Follow;
