import { connect } from 'react-redux';
import { compose } from 'redux';
import withAuth from '../../../utilities/withAuth';
import { attemptFetchModProf, attemptFollowMod } from '../../../store/actions/moderation';
import Follow from './Component';

const mapDispatchToProps = { attemptFollowMod, attemptFetchModProf };

const enhance = compose(
  withAuth,
  connect(
    null,
    mapDispatchToProps,
  )
);

const FollowContainer = enhance(Follow);

export default FollowContainer;
