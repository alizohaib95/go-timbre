import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptFetchModProf, attemptAddFilter, attemptDeleteFilter, attemptFollowMod } from '../../store/actions/moderation';
import ModeratorComp from './Component';
import { attemptCreateTransaction } from '../../store/actions/transactions';

export const mapStateToProps = state => ({
  // isProcessing: state.transactions.isProcessing,
    moderator: state.moderation.moderator,
});

const mapDispatchToProps = {
  attemptFetchModProf,
  attemptAddFilter,
  attemptDeleteFilter,
  attemptFollowMod,
  attemptCreateTransaction
};

const enhance = compose(
  reduxForm({ form: 'addRegexFilter' }),
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const ModeratorContainer = enhance(ModeratorComp);

export default ModeratorContainer;
