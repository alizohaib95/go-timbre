import React from 'react';
import { Field, reduxForm } from 'redux-form';
import {Query, Builder, BasicConfig, Utils as QbUtils} from 'react-awesome-query-builder';
import 'react-awesome-query-builder/css/antd.less';
import "antd/dist/antd.css";
import 'react-awesome-query-builder/css/styles.scss';
import 'react-awesome-query-builder/css/compact_styles.scss'; //optional, for more compact styles
import merge from 'lodash/merge';
import ThreadsListContainer from '../ThreadsList/Container';
import ModeratorList from '../ModeratorList/Container';
import CompareModContainer from '../CompareMod/Container'
import FollowContainer from './FollowButton/Container';

const PrettyPrintJson = ({data}) => (<div><pre>{JSON.stringify(data, null, 2) }</pre></div>);


class ModeratorComponent extends React.Component {
    
    constructor(props) {
        super(props);
        let stream2 = {}
        if (props.moderator_id){
            stream2 = {
                "moderator_id": props.moderator.moderator_id,
                "active_forums": props.moderator.active_forums,
                "votes": props.moderator.votes,
                "followers": props.moderator.followers,
                "moderated_posts": props.moderator.moderated_posts.map(post => {
                    delete post["rootThreadHash"]
                    return post
                })
            }
        }
        
        this.state = {
          stream: stream2,
        };
    }
    
    componentDidMount() {
        this.props.attemptFetchModProf(this.props.urlusername)
    }

    componentWillUpdate(nextProps, nextState, nextContext) {
        // this.props.attemptFetchModProf(this.props.urlusername)
        if (this.props.moderator !== nextProps.moderator) {
            
            let stream2 = {
                "moderator_id": nextProps.moderator.moderator_id,
                "active_forums": nextProps.moderator.active_forums,
                "votes": nextProps.moderator.votes,
                "followers": nextProps.moderator.followers,
                "moderated_posts": nextProps.moderator.moderated_posts.map(post => {
                    delete post["rootThreadHash"]
                    return post
                })
            }
            this.setState({
                stream: stream2,
            });
        }
        
    }
    

    downloadFile = async () => {
        const fileName = "stream";
        const json = JSON.stringify(this.state.stream);
        const blob = new Blob([json],{type:'application/json'});
        const href = await URL.createObjectURL(blob);
        const link = document.createElement('a');
        link.href = href;
        link.download = fileName + ".json";
        document.body.appendChild(link);
        link.click();
        document.body.removeChild(link);
    }


    createTransaction = transaction => {
        if(this.props.moderator){
            let trans = {'port' : this.props.moderator.publicKey, 'amount': transaction.amount}
            this.props.attemptCreateTransaction(trans)
            .then(() => {
                this.props.reset()
            })
        }
        
        // this.props.history.push("/"); // This should change later -> what happens if transac fails?
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.createTransaction)
    };

    render() {

        return (
        <div className="container">
            <div className="row">
                <div className="col-md-8">
                            
                    <div className="card">
                        <div className = "card-header">
                            <h5 className="d-inline ">Moderator Stream: <strong>{this.props.urlusername}</strong></h5>
                            
                            <button onClick={this.downloadFile} className="btn btn-primary float-right btn-sm ">DOWNLOAD JSON</button>
                        </div>
                        <div className="card-body">
                           
                            <span className="card-text">
                                    
                                <PrettyPrintJson data={this.state.stream}/>
                            </span>
                            
                            <button href="#" className="btn btn-disabled btn-primary ml-3" disabled>API ENDPOINT</button>
                        </div>
                    </div>

                    
                </div>
                <div className="col-md-4">
                    {this.state.stream && this.state.stream.followers &&
                        <>
                        <FollowContainer votes={this.props.moderator.followers} urlname={this.props.urlusername}/>
                        <div className="card mb-2">
                            <div className = "card-body ">
                                <h5 className="card-title">Send Tip</h5>
                                <form onSubmit={this.onSubmit()}>
                                
                                    <div className="form-group">
                                        <Field className="form-control col-auto" placeholder="100" name="amount" component="input" type="number"/>
                                        
                                    </div>
                                    <button type="submit" className="btn btn-primary btn-block">TRANSFER COINS</button>
                                </form>
                            </div>
                        </div>
                        </>  

                    }   

                    <div className="card mb-2">
                        <div className = "card-body ">
                            <h5 className="card-title">Similar Moderators</h5>
                                <CompareModContainer />
                        </div>
                    </div>  

                    <div className="card mb-2">
                        <div className = "card-body ">
                            <h5 className="card-title">Moderators</h5>
                                <ModeratorList />
                        </div>
                    </div>

                    
                    

                </div>
            </div>
            
        </div>
        );


    }
}

export default ModeratorComponent;