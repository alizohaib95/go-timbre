import React from 'react';  
import { Link } from 'react-router-dom';
import Logo from '../../timbre-logo.jpg';
import { disableBodyScroll, enableBodyScroll } from "body-scroll-lock";
import SearchBarMain from './Search/Component';
import { Route } from 'react-router-dom';


import Tour from "reactour";

const tourConfig = [
  {
    selector: '#account-info',
    content: `This section shows your Timbre account details on the current indexer`
  },
  {
    selector: '#address-bar',
    content: `This is your public key that will be used as an identifier on any transaction`
  },
  {
    selector: '#money-bar',
    content: `Each new account gets 10000000 Karma. You can use it to create content on Timbre`
  },
  {
    selector: '#nodetype-bar',
    content: `By default, you sign up as a subscriber. Since Timbre supports multiple account roles(Miner, Storage Provider, Bandwidth Provider), you can download our client from the Github repo to get started with a new accounr role.`
  },
  {
    selector: '#voting-bar',
    content: `Mining turns and consenus in Timbre is implemented through Delegated Proof Of Stake(DPOS), through this you can vote(according to your stake/karma value) for a candidate opting for mining`
  },
  {
    selector: '#thread-bar',
    content: `You can create a post through this section. All posts in Timbre are paid i.e poster pays for the storage and addition of the post to the blockchain.`
  },
  {
    selector: '#moderator-bar',
    content: `This section lists the top 10 moderators. In Timbre, popular moderators influence the site-wide content i.e posts blacklisted by the top 10 moderators won't be seen in the popular content section. However they can viewed through the raw posts.`
  },
  {
    selector: '#blockchain-bar',
    content: `Any Timbre forum runs on a separate blockchain. This section shows the statistcs for the active blockchain and one can explore the transactions appended to the chain throught the blockchain explorer. `
  },
  {
    selector: '#pills-profile-tab',
    content: `This section shows the posts according to popularity which is defined in terms of score i.e upvotes - downvotes`
  },
  {
    selector: '#pills-home-tab',
    content: `This section shows the posts in chronological order`
  },
  {
    selector: '#pills-conv-tab',
    content: `This section shows the posts in ordered in terms of the controverial nature of the content. Controverial score is defined by: abs(upvotes - downvotes) / total votes`
  },
  {
    selector: '#pills-you-tab',
    content: `This section shows posts filtered by the moderator streams of the moderators you follow.`
  },
  {
    selector: '#pills-raw-tab',
    content: `This section shows posts without filtering. All available posts on the chain are shown here`
  },
];



class Header extends React.Component {

  constructor() {
    super();
    this.state = {
      isTourOpen: false,
      isShowingMore: false
    };
  }

  disableBody = target => disableBodyScroll(target);
  enableBody = target => enableBodyScroll(target);

  toggleShowMore = () => {
    this.setState(prevState => ({
      isShowingMore: !prevState.isShowingMore
    }));
  };

  closeTour = () => {
    this.setState({
      isTourOpen: false
    });
  };

  openTour = () => {
    this.setState({
      isTourOpen: true
    });
  };

  render(){
    const { isTourOpen, isShowingMore } = this.state;
    const accentColor = "#007bff";
  return (
    <div className="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
      
      <h5 className="my-0 mr-md-auto font-weight-normal"><Link to='/'><img src={Logo}></img></Link>
      
      {this.props.user && 
        <Route exact path='/' component={()=> <a href="#" className="btn btn-secondary btn-sm ml-4 py-0 pb-1 pt-1" onClick={this.openTour}> <small>Tutorial</small></a> } />
      
      }
      </h5>
      
      
      {this.props.user &&<div className="mr-4">
        <Route exact path='/' component={()=> <SearchBarMain source={this.props.threads}></SearchBarMain>} />
        </div>
      }
      
      <nav className="my-2 my-md-0 pull-right fl">
    </nav>
    {
      this.props.user ?
      (
        <>
        <span>Hi, <strong>{this.props.user.username}</strong></span>
    
        <Tour
          onRequestClose={this.closeTour}
          steps={tourConfig}
          isOpen={isTourOpen}
          maskClassName="mask"
          className="helper"
          rounded={5}
          accentColor={accentColor}
          onAfterOpen={this.disableBody}
          onBeforeClose={this.enableBody}
        />
        <a href="#" className="btn btn-primary ml-3" onClick={this.props.logout}> Log Out</a>
        </>
      )
      :
      ( 
        <>
        <Link to="/login">
          <span className="btn btn-primary mr-2" href="#">Sign in</span> 
        </Link>
        <Link to="/register">
          <span className="btn btn-primary" href="#">Sign up</span>
        </Link>
        </>
      )

    }
  
    </div>)
  }
}

export default Header;









