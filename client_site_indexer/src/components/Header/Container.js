import { connect } from 'react-redux';
import { compose } from 'redux';
import withAuth from '../../utilities/withAuth';
import { logout } from '../../store/actions/authentication';
import Header from './Component';

export const mapStateToProps = state => ({
  // isProcessing: state.transactions.isProcessing,
  threads: state.threads.threads
});
const mapDispatchToProps = { logout };

const enhance = compose(
  withAuth,
  connect(mapStateToProps, mapDispatchToProps)
);

const HeaderContainer = enhance(Header);

export default HeaderContainer;