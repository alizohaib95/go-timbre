import { connect } from 'react-redux';
import { compose } from 'redux';
import Home from './Component';
import withAuth from '../../utilities/withAuth';
import { logout } from '../../store/actions/authentication';

const mapDispatchToProps = {
    logout
};

const enhance = compose(
    withAuth,
    connect(null, mapDispatchToProps)
);


const HomeContainer = enhance(Home);

export default HomeContainer;