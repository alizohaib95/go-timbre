import React from 'react';
import { Route } from 'react-router-dom';
import ThreadsListContainer from '../ThreadsList/Container';
import SingleThreadContainer from '../SingleThread/Container'
import { Link } from 'react-router-dom';
import AccountInfoContainer from '../AccountInfo/Container'
import BlockchainInfoContainer from '../BlockChainInfo/Container'
import ModeratorList from '../ModeratorList/Container'
import SelectSearch from 'react-select-search';


const Home = (props) => {
    return (<div className="container">
        <div className="row">
            <div className="col-md-8">                      
                <Route exact path='/' component={ThreadsListContainer} />
                <Route
                    exact
                    path='/:post'
                    render={({ match, history }) => (
                        <SingleThreadContainer id={match.params.post} history={props.history} />
                    )}
                />

            </div>

            <div className="col-md-4">
                {props.user &&
                <div className="card mb-2" id="account-info">
                    <div className="card-body">
                        <h5 className="card-title mb-2">Account Information</h5>
                            <AccountInfoContainer />
                            <Link to='/vote'>
                                <span id="voting-bar" className="btn btn-block btn-primary mb-2">VOTE A CANDIDATE</span>
                            </Link>
                    </div>
                </div>
                }
                {
                    props.user &&
                <div className="card mb-2" id="thread-bar" >
                    <div className="card-body">
                        <h5 className="card-title">Start a thread</h5>
                        <p className="card-text">New threads may take upto 6 seconds to appear</p>
                        
                        <Link to='/submit'>
                            <span className="btn btn-block btn-primary">CREATE A POST</span>
                        </Link>
                        
                    </div>
                </div>
                }
                {props.user &&
                <div className="card mb-2" id="moderator-bar">
                    <div className = "card-body ">
                        <h5 className="card-title">Popular Moderators</h5>
                            <ModeratorList />
                    </div>
                </div>
                }
                <div className="card" id="blockchain-bar">
                    <div className="card-body ">
                        <h5 className="card-title">Blockchain Statistics</h5>
                        <BlockchainInfoContainer />
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>)
};

export default Home;
