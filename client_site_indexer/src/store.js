import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import threads from './store/reducers/threads';
import authentication from './store/reducers/authentication';
import transactions from './store/reducers/transactions';
import moderation from './store/reducers/moderation';
import form from './store/reducers/form';
import thunk from 'redux-thunk';
import errorMiddleware from './store/middleware/error';
import general from './store/reducers/general';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
        combineReducers({ form, threads, transactions,authentication, general, moderation }),
                composeEnhancers(
                    applyMiddleware(thunk, errorMiddleware)
                )
            );