// Change this when moving to production

// Pass port argument as : REACT_APP_API=8888 npm start
const PORT = process.env.REACT_APP_API || 7200;
const PORT2 = process.env.REACT_APP_API || 7400;
const url = 'http://localhost:'.concat(String(PORT), '/v1')
const url2 = 'http://localhost:'.concat(String(PORT2), '/v1')

const baseUrl = process.env.NODE_ENV === 'development' ? url :`http://localhost:7200/v1`;
const baseUrl2 = process.env.NODE_ENV === 'development' ? url2 : `http://localhost:7400/v1`;

const methods = {
    get: async function (endpoint, token = null, api = null) {
        const options = {
            method: 'GET',
            // Add headers for indexer authen
        };
        
        if (api===2){
            const response = await fetch(`${baseUrl2}/${endpoint}`, options);
            const json = await response.json();
            if (!response.ok) throw Error(json.message);
            return json;
        }else{
            const response2 = await fetch(`${baseUrl}/${endpoint}`, options);
            const json2 = await response2.json();
            if (!response2.ok) throw Error(json2.message);
            return json2;
        }
        
        return null
        
    },

    post: async function (endpoint, body, token = null, api=null) {
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // Add headers for indexer authen
            },
            body: JSON.stringify(body)
        };

        const response = await fetch(`${baseUrl}/${endpoint}`, options);
        // const response2 = await fetch(`${baseUrl2}/${endpoint}`, options);
        const json = await response.json();

        if (!response.ok) {
            if (response.status === 422) {
                json.errors.forEach(error => {
                    throw Error(`${error.param} ${error.msg}`);
                });
            }

            throw Error(json.message);
        }

        return json;
    },

};


export async function getAccount(username) {
    return await methods.post(`AccountViaUsername`,username);
}

export async function getBlockchainState() {
    return await methods.get(`current/state`);
}

export async function getThread(roothash) {
    return await methods.get(`thread?threadroothash=${roothash}`);
}

export async function getThreads(api=null) {
    return await methods.get(`threads`, null, api);
}

export async function createThread(body) {
    return await methods.post('PostViaIndexer', body);
}

export async function createComment(comment) {
    return await methods.post(`PostViaIndexer`, comment);
}

export async function makeTransaction(transaction){
    return await methods.post(`TransferMoney`, transaction);
}

export async function changeRole(role) {
    return await methods.post(`Role`, role);
}

export async function getCommittee() {
    return await methods.get(`committee`);
}

// Add function for voting
// ----------
export async function createVote(vote) {
    return await methods.post(`VoteViaIndexer`, vote);
}

export async function login(username, password) {
    const json = await methods.post('Login', {
        username,
        password
    });
    return json;
}

export async function signup(username, password) {
    const json = await methods.post('Register', {
        username,
        password
    });
    return json;
}

export async function attemptLikePost(hash, votes, token) {
    const json = await methods.post('LikeAPost', {
        token,
        hash,
        votes
    });
    return json.token;
}

export async function fetchModerators(){
    return await methods.get(`GetMods`)
}

export async function beaMod(body) {
    return await methods.post(`AddMod`, body);
}

export async function fetchModeratorProfile(body) {
    return await methods.post(`GetMod`, body);
}

export async function addRegexFilter(body) {
    return await methods.post(`AddFilterToMod`, body);
}

export async function removeRegexFilter(body){
    return await methods.post(`RemoveFilterFromMod`, body);
}

export async function updateEchoes(body){
    return await methods.post(`UpdateEchoes`, body)
}

export async function attemptFollowaMod(body) {
    return await methods.post(`FollowMod`, body)
}

export async function attemptGetFilters(body) {
    return await methods.post(`GetAllFilters`, body)
}

export async function attemptTag(body) {
    return await methods.post(`TagPost`, body)
}

export async function getNBlocks(blocks) {
    return await methods.get(`nblocks?N=5`);
}

