import {connect} from 'react-redux';

// This just sets an authentication token and user to the component
export default function withAuth(WrappedComponent) {
    const mapStateToProps = state => ({
        token: state.authentication.token,
        user: state.authentication.user,
    });

    return connect(mapStateToProps)(WrappedComponent);
}
