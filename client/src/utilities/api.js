// Change this when moving to production

// Pass port argument as : REACT_APP_API=8888 npm start
const PORT = process.env.REACT_APP_API || 7200;
const url = 'http://localhost:'.concat(String(PORT), '/v1')

const baseUrl = process.env.NODE_ENV === 'development' ? url :`http://localhost:7200/v1`;

const methods = {
    get: async function (endpoint, token = null) {
        const options = {
            method: 'GET',
            // Add headers for indexer authen
        };

        const response = await fetch(`${baseUrl}/${endpoint}`, options);
        const json = await response.json();

        if (!response.ok) throw Error(json.message);
        return json;
    },

    post: async function (endpoint, body, token = null) {
        const options = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                // Add headers for indexer authen
            },
            body: JSON.stringify(body)
        };

        const response = await fetch(`${baseUrl}/${endpoint}`, options);
        const json = await response.json();

        if (!response.ok) {
            if (response.status === 422) {
                json.errors.forEach(error => {
                    throw Error(`${error.param} ${error.msg}`);
                });
            }

            throw Error(json.message);
        }

        return json;
    },

};


export async function getAccount() {
    return await methods.get(`account`);
}

export async function getStorageAccount(){
    return await methods.get('GetStorageProviderStats')
}

export async function getBlockchainState() {
    return await methods.get(`current/state`);
}

export async function getThread(roothash) {
    return await methods.get(`thread?threadroothash=${roothash}`);
}

export async function getThreads() {
    return await methods.get(`threads`);
}

export async function createThread(body) {
    return await methods.post('Post', body);
}

export async function createComment(comment) {
    return await methods.post(`Post`, comment);
}

export async function makeTransaction(transaction){
    return await methods.post(`TransferMoney`, transaction);
}

export async function changeRole(role) {
    return await methods.post(`Role`, role);
}

export async function sendStorageOffer(offer){
    return await methods.post(`SendStorageOffer`, offer);
}

export async function getNBlocks(blocks){
    return await methods.get(`nblocks?N=5`);
}

export async function getCommittee(blocks) {
    return await methods.get(`committee`);
}

export async function createVote(vote) {
    return await methods.post(`VoteViaIndexer`, vote);
}

export async function removePost(post) {
    methods.post(`removepost`, post)
}
