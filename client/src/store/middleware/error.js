
import{
    FETCH_THREADS_ERROR,
    FETCH_THREAD_ERROR,
} from '../actions/threads'
import {
    showError,
    showSuccess
} from '../actions/error';

import {
    SEND_STORAGE_SUCCESS,
    SEND_STORAGE_ERROR
} from '../actions/general';

import {
    CREATE_COMMENT_SUCCESS
} from '../actions/threads';

export default store => next => action => {
    next(action);
    switch (action.type) {
        case SEND_STORAGE_ERROR:
        case FETCH_THREADS_ERROR:
            store.dispatch(showError("Unable to retrieve threads"));
            break;
        case FETCH_THREAD_ERROR:
            store.dispatch(showError("Unable to retrieve thread"));
            break;
        case SEND_STORAGE_SUCCESS:
            store.dispatch(showSuccess("Storage Offer Sent"));
            break
        case CREATE_COMMENT_SUCCESS:
            store.dispatch(showSuccess("Comment Submitted"));
        break
        default:
            break;
    }
};