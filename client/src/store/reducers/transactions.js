import {
    CREATE_TRANSACTION_REQUEST,
    CREATE_TRANSACTION_SUCCESS,
    CREATE_TRANSACTION_ERROR,

    CREATE_VOTE_REQUEST,
    CREATE_VOTE_SUCCESS,
    CREATE_VOTE_ERROR,

    FETCH_ACCOUNT_REQUEST,
    FETCH_ACCOUNT_SUCCESS,
    FETCH_ACCOUNT_ERROR,

    FETCH_BLOCKCHAIN_REQUEST,
    FETCH_BLOCKCHAIN_SUCCESS,
    FETCH_BLOCKCHAIN_ERROR,

    COMMITTEE_REQUEST,
    COMMITTEE_SUCCESS,
    COMMITTEE_ERROR

} from '../actions/transactions';

const initialState = {
    isProcessing: false,
    committee: [],
};


// Todo: these just set the processing state. Add things later
export default (state = initialState, action) => {
    switch (action.type) {
        case CREATE_TRANSACTION_REQUEST:
            return { ...state, isProcessing: true};
        case CREATE_TRANSACTION_SUCCESS:
            return { ...state, isProcessing: false};
        case CREATE_TRANSACTION_ERROR:
            return { ...state, isProcessing: false };


        case CREATE_VOTE_REQUEST:
            return { ...state, isProcessing: true};
        case CREATE_VOTE_SUCCESS:
            return { ...state, isProcessing: false};
        case CREATE_VOTE_ERROR:
            return { ...state, isProcessing: false };
        

        case FETCH_BLOCKCHAIN_REQUEST:
            return { ...state, isProcessing: true};
        case FETCH_BLOCKCHAIN_SUCCESS:
            return { ...state, isProcessing: false, blockchain: action.blockchain};
        case FETCH_BLOCKCHAIN_ERROR:
            return { ...state, isProcessing: false };


        case COMMITTEE_REQUEST:
            return {...state, isProcessing:true}
        case COMMITTEE_SUCCESS:
            return { ...state, isProcessing: false, committee: action.committee};
        case COMMITTEE_ERROR:
            return { ...state, isProcessing: false };

        default:
            return state
    }
}
