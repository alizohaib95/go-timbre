// This file contains all reducer logic related to threads
import {
    FETCH_THREADS_REQUEST,
    FETCH_THREADS_SUCCESS,
    FETCH_THREADS_ERROR,
    FETCH_THREAD_REQUEST,
    FETCH_THREAD_SUCCESS,
    FETCH_THREAD_ERROR,

} from '../actions/threads';

const initialState = {
    isFetching: false,
    threads: []
};

export default (state = initialState, action) => {
    switch (action.type) {
        case FETCH_THREADS_REQUEST:
            return { ...state, isFetching: true};
        case FETCH_THREADS_SUCCESS:
            return { ...state, isFetching: false, threads: action.threads };
        case FETCH_THREADS_ERROR:
            return { ...state, isFetching: false };

        case FETCH_THREAD_REQUEST:
            return { ...state, isFetching: true, thread: null };
        case FETCH_THREAD_SUCCESS:
            return { ...state, isFetching: false, thread: action.thread };
        case FETCH_THREAD_ERROR:
            return { ...state, isFetching: false };

        // Todo: State not being updated for new thread events

        default:
            return state
    }
}

