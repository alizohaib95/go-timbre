import {
        
    FETCH_ACCOUNT_REQUEST,
    FETCH_ACCOUNT_SUCCESS,
    FETCH_ACCOUNT_ERROR,

    FETCH_STORAGE_ACCOUNT_REQUEST,
    FETCH_STORAGE_ACCOUNT_SUCCESS,
    FETCH_STORAGE_ACCOUNT_ERROR,

    FETCH_NBLOCKS_REQUEST,
    FETCH_NBLOCKS_SUCCESS,
    FETCH_NBLOCKS_ERROR,

    
} from '../actions/general';


const initialState = {
    isProcessing: false,
};


export default (state = initialState, action) => {
    switch (action.type) {

        case FETCH_ACCOUNT_REQUEST:
            return { ...state, isProcessing: true};
        case FETCH_ACCOUNT_SUCCESS:
            return { ...state, isProcessing: false, account: action.account};
        case FETCH_ACCOUNT_ERROR:
            return { ...state, isProcessing: false };  

        case FETCH_STORAGE_ACCOUNT_REQUEST:
            return { ...state, isProcessing: true};
        case FETCH_STORAGE_ACCOUNT_SUCCESS:
            return { ...state, isProcessing: false, storage_account: action.account};
        case FETCH_STORAGE_ACCOUNT_ERROR:
            return { ...state, isProcessing: false };  

        
        case FETCH_NBLOCKS_REQUEST:
            return { ...state, isProcessing: true};
        case FETCH_NBLOCKS_SUCCESS:
            return { ...state, isProcessing: false, blocks: action.blocks};
        case FETCH_NBLOCKS_ERROR:
            return { ...state, isProcessing: false };  
        
        
        

        default:
            return state
    }
}