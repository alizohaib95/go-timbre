import {
    NotificationContainer,
    NotificationManager
} from 'react-notifications';

export const SHOW_ERROR = 'SHOW_ERROR';
const showErrorAc = error => ({
    type: SHOW_ERROR,
    error
});

export const SHOW_SUCCESS = 'SHOW_SUCCESS';
const showSuccessAc = success => ({
    type: SHOW_SUCCESS,
    success
});

export const showError = error => dispatch => {
    NotificationManager.error('Error',  String(error));
    dispatch(showErrorAc(error));
};

export const showSuccess = success => dispatch => {
    NotificationManager.success('Success', String(success));
    dispatch(showSuccessAc(success));
};


