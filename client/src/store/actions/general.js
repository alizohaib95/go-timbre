import {
    getAccount,
    getStorageAccount,
    sendStorageOffer,
    getNBlocks,
} from '../../utilities/api';


//*************////////////////////////////////////////////*************//
// Fetch Account Information
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_ACCOUNT_REQUEST = 'FETCH_ACCOUNT_REQUEST';
export const FETCH_ACCOUNT_SUCCESS = 'FETCH_ACCOUNTSUCCESS';
export const FETCH_ACCOUNT_ERROR = 'FETCH_ACCOUNT_ERROR';

function createFetchAccountRequest() {
    return {
        type: FETCH_ACCOUNT_REQUEST
    }
}

function createFetchAccountSuccess(account) {
    return {
        type: FETCH_ACCOUNT_SUCCESS,
        account
    }
}

function createFetchAccountError(error) {
    return {
        type: FETCH_ACCOUNT_ERROR,
        error
    }
}

export const fetchAccount = () => async (dispatch, getState) => {
    dispatch(createFetchAccountRequest());
    try {
        const json = await getAccount();
        dispatch(createFetchAccountSuccess(json));
    } catch (error) {
        dispatch(createFetchAccountError(error));
    }
}




//*************////////////////////////////////////////////*************//
// Fetch Storage Account Information
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_STORAGE_ACCOUNT_REQUEST = 'FETCH_STORAGE_ACCOUNT_REQUEST';
export const FETCH_STORAGE_ACCOUNT_SUCCESS = 'FETCH_STORAGE_ACCOUNT_SUCCESS';
export const FETCH_STORAGE_ACCOUNT_ERROR = 'FETCH_STORAGE_ACCOUNT_ERROR';

function createFetchStorageAccountRequest() {
    return {
        type: FETCH_STORAGE_ACCOUNT_REQUEST
    }
}

function createFetchStorageAccountSuccess(account) {
    return {
        type: FETCH_STORAGE_ACCOUNT_SUCCESS,
        account
    }
}

function createFetchStorageAccountError(error) {
    return {
        type: FETCH_STORAGE_ACCOUNT_ERROR,
        error
    }
}

export const fetchStorageAccount = () => async (dispatch, getState) => {
    dispatch(createFetchStorageAccountRequest());
    try {
        const json = await getStorageAccount();
        dispatch(createFetchStorageAccountSuccess(json));
    } catch (error) {
        dispatch(createFetchStorageAccountError(error));
    }
}



//*************////////////////////////////////////////////*************//
// Change Roles
//*************////////////////////////////////////////////*************//

// Action Types
export const SEND_STORAGE_REQUEST = 'SEND_STORAGE_REQUEST';
export const SEND_STORAGE_SUCCESS = 'SEND_STORAGE_SUCCESS';
export const SEND_STORAGE_ERROR = 'SEND_STORAGE_ERROR';

function sendStorageRequest() {
    return {
        type: SEND_STORAGE_REQUEST
    }
}

function sendStorageSuccess(storage) {
    return {
        type: SEND_STORAGE_SUCCESS,
        storage
    }
}

function sendStorageError(error) {
    return {
        type: SEND_STORAGE_ERROR,
        error
    }
}

export const attemptSendStorageOffer = (storageOff) => async (dispatch, getState) => {
    dispatch(sendStorageRequest());
    try {
        storageOff['maxDuration'] = 3600*24;
        storageOff['size'] = parseInt(storageOff['size']*1000000)
        const json = await sendStorageOffer(storageOff);
        dispatch(sendStorageSuccess(json));
    } catch (error) {
        dispatch(sendStorageError(error));
    }
}



//*************////////////////////////////////////////////*************//
// Get Blockchain Blocks Action
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_NBLOCKS_REQUEST = 'FETCH_NBLOCKS_REQUEST';
export const FETCH_NBLOCKS_SUCCESS = 'FETCH_NBLOCKSSUCCESS';
export const FETCH_NBLOCKS_ERROR = 'FETCH_NBLOCKS_ERROR';

function createFetchNBlocksRequest() {
    return {
        type: FETCH_NBLOCKS_REQUEST
    }
}

function createFetchNBlocksSuccess(blocks) {
    return {
        type: FETCH_NBLOCKS_SUCCESS,
        blocks
    }
}

function createFetchNBlockssError(error) {
    return {
        type: FETCH_NBLOCKS_ERROR,
        error
    }
}

export const fetchNBlocks = () => async (dispatch, getState) => {
    dispatch(createFetchNBlocksRequest());
    try {
        const json = await getNBlocks();
        dispatch(createFetchNBlocksSuccess(json));
    } catch (error) {
        dispatch(createFetchNBlockssError(error));
    }
}
