// This file contains all the actions and action creator related to threads
import {
    getThreads,
    getThread,
    createComment,
    createThread,
    removePost
    
} from '../../utilities/api';

//*************////////////////////////////////////////////*************//
// Full Threads
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_THREADS_REQUEST = 'FETCH_THREADS_REQUEST';
export const FETCH_THREADS_SUCCESS = 'FETCH_THREADS_SUCCESS';
export const FETCH_THREADS_ERROR = 'FETCH_THREADS_ERROR';

// Action Creators
function fetchThreadsRequest()  {
    return {
        type: FETCH_THREADS_REQUEST,
        
    }
}
function fetchThreadsSuccess(threads) {
    return {
        type: FETCH_THREADS_SUCCESS,
        threads
    }
}

function fetchThreadsError(err) {
    return {
        type: FETCH_THREADS_ERROR,
        err
    }
}

// Action Dispatchers
export const fetchThreads = () => async dispatch => {

    dispatch(fetchThreadsRequest())
    try {
        const threads = await getThreads(); // Call the api function here
        dispatch(fetchThreadsSuccess(threads.threads));
    } catch (error) {
        dispatch(fetchThreadsError(error));
    }
};


//*************////////////////////////////////////////////*************//
// Single Thread
//*************////////////////////////////////////////////*************//

// Action Types
export const FETCH_THREAD_REQUEST = 'FETCH_THREAD_REQUEST';
export const FETCH_THREAD_SUCCESS = 'FETCH_THREAD_SUCCESS';
export const FETCH_THREAD_ERROR = 'FETCH_THREAD_ERROR';

// Action Creators
function fetchThreadRequest()  {
    return {
        type: FETCH_THREAD_REQUEST,
        
    }
}
function fetchThreadSuccess(thread) {
    return {
        type: FETCH_THREAD_SUCCESS,
        thread
    }
}
function fetchThreadError(err) {
    return {
        type: FETCH_THREAD_ERROR,
        err
    }
}

// Action Dispatchers
export const fetchThread = (hash) => async dispatch => {
    
    dispatch(fetchThreadRequest())
    try {
        const thread = await getThread(hash); // 
        dispatch(fetchThreadSuccess(thread));
    } catch (error) {
        dispatch(fetchThreadError(error));
    }
};


//*************////////////////////////////////////////////*************//
// Commenter
//*************////////////////////////////////////////////*************//

// Action Types
export const CREATE_COMMENT_REQUEST = 'CREATE_COMMENT_REQUEST';
export const CREATE_COMMENT_SUCCESS = 'CREATE_COMMENT_SUCCESS';
export const CREATE_COMMENT_ERROR = 'CREATE_COMMENT_ERROR';

function createCommentRequest (){
    return { 
        type: CREATE_COMMENT_REQUEST 
    }
}
function createCommentSuccess(post) {
    return { 
        type: CREATE_COMMENT_SUCCESS, post 
    }
}
function createCommentError(error) {
    return {
        type: CREATE_COMMENT_ERROR,
        error
    }
}

export const attemptCreateComment = comment => async (dispatch, getState) => {
  dispatch(createCommentRequest());
  try {
    let c = (comment.comment).concat("<author>", 'client1')
    c = String(c)
    let s = getState()
    let ps = String(s.threads.thread.posts[0].hash)
    // let user = getState().authentication.user.username
    let data = {
        "content": c,
        "parentPostHash": ps,
        "threadheadPostHash": ps,
        "maxCost": 100,
        "minDuration": 300,
        "username": 'client1'
    }
    const json = await createComment(data);
    dispatch(createCommentSuccess(json));
  } catch (error) {
      dispatch(createCommentError(error));
  }
}


//*************////////////////////////////////////////////*************//
// Thread Creater
//*************////////////////////////////////////////////*************//

// Action Types
export const CREATE_THREAD_REQUEST = 'CREATE_THREAD_REQUEST';
export const CREATE_THREAD_SUCCESS = 'CREATE_THREAD_SUCCESS';
export const CREATE_THREAD_ERROR = 'CREATE_THREAD_ERROR';

function createThreadRequest() {
    return {
        type: CREATE_THREAD_REQUEST
    }
}

function createThreadSuccess(post) {
    return {
        type: CREATE_THREAD_SUCCESS,
        post
    }
}

function createThreadError(error) {
    return {
        type: CREATE_THREAD_ERROR,
        error
    }
}

export const attemptCreateThread = thread => async (dispatch, getState) => {
    dispatch(createThreadRequest());
    try {

        let obj = {
            "Title": thread.title,
            "Content": thread.content,
            "Author": 'client1',
            "Forum": "WORLDNEWS",
            "Forum_link": "timbre.io",
            "MaxCost": parseInt(thread.price)
        }
        let c = JSON.stringify(obj)
        c = String(c)
        // let user = getState().authentication.user.username
        let data = {
            "content": c,
            "parentPostHash": "",
            "threadheadPostHash": "",
            "maxCost": thread.price,
            "minDuration": 300,
            "username": 'client1'
        }
        const json = await createThread(data);
        dispatch(createThreadSuccess(json));
    } catch (error) {
        dispatch(createThreadError(error));
    }
}




//*************////////////////////////////////////////////*************//
// Thread Remover
//*************////////////////////////////////////////////*************//

// Action Types
export const REMOVE_THREAD_REQUEST = 'REMOVE_THREAD_REQUEST';
export const REMOVE_THREAD_SUCCESS = 'REMOVE_THREAD_SUCCESS';
export const REMOVE_THREAD_ERROR = 'REMOVE_THREAD_ERROR';

function removeThreadRequest() {
    return {
        type: REMOVE_THREAD_REQUEST
    }
}

function removeThreadSuccess(post) {
    return {
        type: REMOVE_THREAD_SUCCESS,
        post
    }
}

function removeThreadError(error) {
    return {
        type: REMOVE_THREAD_ERROR,
        error
    }
}

export const attemptRemoveThread = thread => async (dispatch, getState) => {
    dispatch(removeThreadRequest());
    try {

        let obj = {
            "postHash": thread,
        }
        const json = await removePost(obj);
        dispatch(removeThreadSuccess(json));
    } catch (error) {
        dispatch(removeThreadError(error));
    }
}

