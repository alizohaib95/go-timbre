import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import threads from './store/reducers/threads';
import transactions from './store/reducers/transactions';
import general from './store/reducers/general';
import form from './store/reducers/form';
import thunk from 'redux-thunk';
import errorMiddleware from './store/middleware/error';

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(
        combineReducers({ form, threads, transactions, general }),
                composeEnhancers(
                    applyMiddleware(thunk, errorMiddleware)
                )
            );