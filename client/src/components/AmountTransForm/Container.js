import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptCreateTransaction } from '../../store/actions/transactions';
import AmountTransForm from './Component';

const mapDispatchToProps = { attemptCreateTransaction };

const enhance = compose(
  reduxForm({ form: 'amountTransaction' }),
  connect(
    null,
    mapDispatchToProps
  )
);

const AmountTransFormContainer = enhance(AmountTransForm);

export default AmountTransFormContainer;