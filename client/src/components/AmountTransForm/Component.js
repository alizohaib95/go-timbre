import React from 'react';
import { Field, reduxForm } from 'redux-form'

class AmountTransForm extends React.Component {
  
    createTransaction = transaction => {
        this.props.attemptCreateTransaction(transaction);
        this.props.history.push("/"); // This should change later -> what happens if transac fails?
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.createTransaction)
    };

    render() {
        return (
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <div className="card"> 
                        <div className="card-body">
                            <h5 className="card-title">Make a Transaction</h5>
                            <p className="card-text">New transactions may take a while to get added to the blockchain</p>
                            <form onSubmit={this.onSubmit()}>
                                <div className="form-group">
                                    <label>User Public Key</label>
                                    < Field className = "form-control"
                                    name = "port"
                                    type = "text"
                                    component = "input"
                                    placeholder = "" / >
                                </div>
                                <div className="form-group">
                                    <label>Amount</label>
                                    <Field className="form-control" name="amount" component="input" type="number"/>
                                    <button type="submit" className="btn btn-primary float-right mt-3">Send Coins</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}

export default AmountTransForm;