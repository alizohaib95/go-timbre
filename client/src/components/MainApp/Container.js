import { connect } from 'react-redux';
import App from './Component';

const AppContainer = connect(null)(App);

export default AppContainer;
