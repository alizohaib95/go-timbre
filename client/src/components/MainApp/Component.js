import React from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from '../../utilities/history';
import Header from '../Header/Component'
import Home from '../Homepage/Component';
import CreateThreadFormContainer from '../CreateThreadForm/Container'
import AmountTransFormContainer from '../AmountTransForm/Container'
import VoteTransFormContainer from '../VoteTransForm/Container'
import RoleManagerContainer from '../RoleManagerForm/Container'
import StorageAccountInfoContainer from '../StorageAccountInfo/Container'
import ChainExplorerContainer from '../ChainExplorer/Container'
import {NotificationContainer, NotificationManager} from 'react-notifications';
import Notifiction from 'react-notification-modal'

import 'react-notifications/lib/notifications.css';


const App = props => (
    <Router history={history}>
      <>
        <Route component={Header} />
        <Route component={NotificationContainer} / >
        <Route component={Notifiction} />
        <Switch>
          
          <Route path='/submit' component={CreateThreadFormContainer} />
          <Route path='/transaction' component={AmountTransFormContainer} />
          <Route path='/vote' component={VoteTransFormContainer} />
          <Route path='/roles' component={RoleManagerContainer} />
          <Route path='/storage' component={StorageAccountInfoContainer} />
          <Route path='/explorer' component={ChainExplorerContainer} />

          <Route path='/' component={Home} />
        </Switch>
      </>
     </Router>
);

export default App;
