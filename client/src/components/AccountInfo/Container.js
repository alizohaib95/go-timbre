import { connect } from 'react-redux';
import { compose } from 'redux';
import { fetchAccount } from '../../store/actions/general';
import AccountInfo from './Component';

export const mapStateToProps = state => ({
  isProcessing: state.general.isProcessing,
  account: state.general.account
});

const mapDispatchToProps = { fetchAccount };

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const AccountInfoContainer = enhance(AccountInfo);

export default AccountInfoContainer;
