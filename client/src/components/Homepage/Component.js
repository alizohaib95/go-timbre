import React from 'react';
import { Route } from 'react-router-dom';
import ThreadsListContainer from '../ThreadsList/Container';
import SingleThreadContainer from '../SingleThread/Container'
import { Link } from 'react-router-dom';
import AccountInfoContainer from '../AccountInfo/Container'
import BlockchainInfoContainer from '../BlockChainInfo/Container'


const Home = () => (
    <div className="container">
        <div className="row">
            <div className="col-md-8">      
                

                <Route exact path='/' component={ThreadsListContainer} />
                <Route
                    exact
                    path='/:post'
                    render={({ match, history }) => (
                        <SingleThreadContainer id={match.params.post} history={history} />
                    )}
                />
                

            </div>
            <div className="col-md-4">
                <div className="card mb-2" >
                    <div className="card-body">
                        <h5 className="card-title mb-2">Account Information</h5>
                        <AccountInfoContainer />

                        <Link to='/transaction'>
                            <span className="btn btn-block btn-primary mb-2">TRANSFER COINS</span>
                        </Link>
                        <Link to='/vote'>
                            <span className="btn btn-block btn-primary mb-2">VOTE A CANDIDATE</span>
                        </Link>
                        <Link to='/roles'>
                            <span className="btn btn-block btn-primary mb-2">CHANGE ROLES</span>
                        </Link>
                        <Link to='/storage'>
                            <span className="btn btn-block btn-primary mb-2">STORAGE DASHBOARD</span>
                        </Link>
                        <Link to='/explorer'>
                            <span className="btn btn-block btn-primary mb-2">BLOCKCHAIN EXPLORER</span>
                        </Link>
                        
                    </div>
                </div>
                <div className="card mb-2" >
                    <div className="card-body">
                        <h5 className="card-title">Start a thread</h5>
                        <p className="card-text">New threads may take upto 6 seconds to appear</p>
                        <Link to='/submit'>
                            <span className="btn btn-block btn-primary">CREATE A POST</span>
                        </Link>
                    </div>
                </div>
                <div className="card">
                    <div className="card-body ">
                        <h5 className="card-title">Blockchain Statistics</h5>
                        <BlockchainInfoContainer />
                        
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
);

export default Home;
