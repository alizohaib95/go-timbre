import React from 'react';
import { Field, reduxForm } from 'redux-form'

class CommentForm extends React.Component {
  
    createComment = comment => {
        this.props.attemptCreateComment(comment);
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.createComment)
    };

    render() {
        return (
        <form onSubmit={this.onSubmit()}>

            <div className="card mb-3">
                <div className="card-body">
                    <Field className="form-control" name="comment" component="textarea" placeholder="Enter your comment"/>
                </div>
                <div className="card-footer py-2">
                    <small className="text-muted"><button type="submit" className="btn btn-primary btn-sm float-right d-block mt-1">Comment</button></small>
                </div>
            </div>
            
        </form>
        );
    }
}

export default CommentForm;