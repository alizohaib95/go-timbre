import React from 'react';
import ThreadListItem from '../ThreadListItem/Component';
import Skeleton from 'react-loading-skeleton';
import ShowMore from '@tedconf/react-show-more';


class ThreadsList extends React.Component {

    componentDidMount() {
        this.fetchThreads()
    }
    fetchThreads = () => {
        return this.props.fetchThreads();
    }
    sortByDate = threads => {
        // return threads.sort((a, b) => {
        //     return new Date(a.posts[0].timestamp / 1000000) - new Date(b.posts[0].timestamp / 1000000)
            
        // });

        return threads.sort((a, b) => {
            return b.posts[0].timestamp - a.posts[0].timestamp
        });
    }
    mapThreads = (threads) =>(
        threads.map((thread, index) => {
            return <ThreadListItem key={index} {...thread} />
        })
    );
    render() {
        if (this.props.isFetching) return <Skeleton count={20}/>;
        if (!this.props.threads || this.props.threads.length === 0) return <p>Threads not found..</p>;
        return (
            <div>
                {/* {this.mapThreads(this.sortByDate(this.props.threads))} */}

            <ShowMore
                items={this.sortByDate(this.props.threads)}
                by={10}
            >
                {({current,onMore,}) => (
                    <React.Fragment>
                        {current.map(item => (
                            this.mapThreads([item])
                        ))}
                        
                        <button 
                            class = "btn btn-block btn-primary"
                            disabled={!onMore}
                            onClick={() => { if (!!onMore) onMore(); }}
                        >
                        Show More
                        </button>
                    </React.Fragment>
                )}
            </ShowMore>

            </div>)
    }
}

export default ThreadsList;