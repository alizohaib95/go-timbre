import { connect } from 'react-redux';
import { fetchThreads } from '../../store/actions/threads';
import ThreadsList from './Component';

export const mapStateToProps = state => ({
    isFetching: state.threads.isFetching,
    threads: state.threads.threads,
});

const mapDispatchToProps = { fetchThreads };

const ThreadsListContainer = connect(
  mapStateToProps,
  mapDispatchToProps
)(ThreadsList);

export default ThreadsListContainer;
