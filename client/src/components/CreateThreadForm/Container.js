import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptCreateThread } from '../../store/actions/threads';
import CreateThreadForm from './Component';

const mapDispatchToProps = { attemptCreateThread };

const enhance = compose(
  reduxForm({ form: 'createThread' }),
  connect(
    null,
    mapDispatchToProps
  )
);

const CreateThreadFormContainer = enhance(CreateThreadForm);

export default CreateThreadFormContainer;