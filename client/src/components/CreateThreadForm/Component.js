import React from 'react';
import { Field, reduxForm } from 'redux-form'

const required = value => value ? undefined : 'Required';
class CreateThreadForm extends React.Component {
  
    createThread = thread => {
        this.props.attemptCreateThread(thread);
        this.props.history.push("/");
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.createThread)
    };

    render() {
        return (
    
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <div className="card"> 
                        <div className="card-body">
                            <h5 className="card-title">Create a Thread</h5>
                            <p className="card-text">New threads may take upto 6 seconds to appear</p>
                            <form onSubmit={this.onSubmit()}>
                                <div className="form-group">
                                    <label>Title</label>
                                    <Field className="form-control" name="title" component="input" type="text" placeholder="Lorem Ipsum" required/>
                                </div>
                                <div className="form-group">
                                    <label>Price</label>
                                    <Field className="form-control mb-2" name="price" component="input" type="number" required/>
                                    
                                    <label>Duration <small>in seconds</small></label>
                                    <Field className="form-control mb-2" name="duration" component="input" type="number" required/>
                                    <label>Content</label>
                                    <Field className="form-control" name="content" component="textarea" required/>
                                    <button type="submit" className="btn btn-primary float-right mt-3">Submit Thread</button>

                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        

        );
    }
}

export default CreateThreadForm;