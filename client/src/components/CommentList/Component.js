import React from 'react';
import moment from 'moment';


const CommentListItem = ({hash, content, timestamp}) => {
  let tstamp = new Date(timestamp*1000); // dividing by 100000 to convert nano
  let tstampstring = tstamp.toGMTString();
  let contentAndAuthor = content.split("<author>")
  return (
    <div className="card mb-2">
      <div className="card-body">
        <p className="card-text">{contentAndAuthor[0]}</p>
      </div>
      <div className="card-footer">
        <small className="text-muted">Posted by: <strong>{contentAndAuthor[1]}</strong> | {moment(tstampstring).fromNow()}</small>
      </div>
    </div>)
};

const mapComments = comments =>{
    let a = comments.map((comment, index) => (
    <CommentListItem key={index} {...comment} />
  ))
  return a;
};



const sortByDate = comments =>{
  return comments.sort((a, b) => new Date(b.timestamp) - new Date(a.timestamp));
}

const CommentList = ({ comments }) =>{
  if (!comments || comments.length == 1) return <div className="card"><div className="card-body text-center">No comments found</div></div>
  return comments && <div>{mapComments( (sortByDate(comments)).slice(1) )}</div>;
}
  

export default CommentList;
