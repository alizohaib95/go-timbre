import { connect } from 'react-redux';
import { compose } from 'redux';
import { fetchNBlocks } from '../../store/actions/general';
import ChainExplorer from './Component';

export const mapStateToProps = state => ({
  isProcessing: state.general.isProcessing,
  blocks: state.general.blocks
});

const mapDispatchToProps = { fetchNBlocks };

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const ChainExplorerContainer = enhance(ChainExplorer);

export default ChainExplorerContainer;
