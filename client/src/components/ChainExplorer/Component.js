import React from 'react';
import Skeleton from 'react-loading-skeleton';
import StorageOfferFormContainer from '../StorageOfferForm/Container'

const mapDeals = deals => {
  let a = deals.map((deal, index) => {
    let tstamp = new Date(deal.timestamp / 1000000)
    let tstampstring = tstamp.toGMTString()
    let tstamp2 = new Date(deal.expiryTime / 1000000)
    let tstampstring2 = tstamp2.toGMTString()
      return (
      <div className="card mb-2" key={index}>
        <div className="card-body">
          <p className="card-text"><strong>Hash:</strong> {deal.name}</p>
          <p className="card-text"><strong>Timestamp:</strong> {deal.timestamp}</p>
          <span href="#" className="btn btn-sm mb-2 btn-danger">Expiring on: {tstampstring2}</span>
        </div>
      </div>
      )
  })
  return a;
};


const mapTransactions = transactions => {
  let a = transactions.map((transaction, index) => {
    let tstamp = new Date(transaction.timeStamp / 1000000)
    let tstampstring = tstamp.toGMTString()
      return (
      <div className="card mb-2" key={index}>
        <div className="card-body">
          <p className="card-text"><strong>From:</strong> {transaction.body.payer_id}
          <br></br>
          <strong>To:</strong> {transaction.body.recipient_id}
          <br></br>
          <strong>Amount:</strong> {transaction.body.decibels}
          <br></br>
          <strong>TimeStamp:</strong> {tstampstring}
          </p>
        </div>
      </div>
      )
  })
  return a;
};

const mapBlocks = blocks => {
  let a = blocks.map((block, index) => {
    let tstamp = new Date(block.header.timestamp / 1000000)
    let tstampstring = tstamp.toGMTString()
      return (
      <div className="card mb-3" key={block.header.timestamp.toString()}>
        <div className="card-header" id="headingOne">
          
          <h6 className="mb-2">
              Block Height: <span className="font-weight-light">{block.header.height}</span>
              <br></br>
              Hash: <span className="font-weight-light">{block.hash}</span>
              <br></br>
              Parent Hash: <span className="font-weight-light">{block.header.parent_hash}</span>
              <br></br>
              Round Number: <span className="font-weight-light">{block.header.roundNum}</span>
              <br></br>
              Mined by: <span className="font-weight-light">{block.header.miner_public_key}</span>
              <br></br>
              <hr></hr>

              <span className="btn btn-sm btn-success">Timestamp: {tstampstring} </span>
              <button className="btn btn-primary btn-sm ml-3 float-right" data-toggle="collapse" data-target={"#Block"+index} aria-expanded="false" aria-controls={'Block'+index}>
              Click To Explore
            </button>

          </h6>

          
          
        </div>

        <div id={'Block'+index} className="collapse" aria-labelledby="headingOne" data-parent="#accordion">
          <div className="card-body">
            <div className="row">
              <div className="col-md-6">
                <h6>Deals</h6>
                {block.data.deals ? 

                  mapDeals(block.data.deals)
                
                  :<span>Deals not found in the block</span>
                }
              </div>
              <div className="col-md-6">
                <h6>Transactions</h6>
                {block.data.transactions ? 

                  mapTransactions(block.data.transactions)
                  :<span>Transactions not found in the block</span>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
      )
  })

  return a;
};




class ChainExplorer extends React.Component {
  componentDidMount() {
    this.props.fetchNBlocks();
  }
  render() {
    
    if (this.props.isProcessing) return <div className="container"><Skeleton count={5}/></div>;
    // if (!this.props.storageAccount) return <div className="container"><p className="text-danger"> Account Not found </p></div>;
    
    return (
      <div className="container">
        <h2>Blockchain Explorer <span className="badge badge-secondary float-right">Last 10 Blocks</span></h2>
        <hr></hr>
        <div className="row mb-2">
          <div className="col-md-10">
            <div id="accordion">
            
            {
              this.props.blocks?
                mapBlocks(this.props.blocks.blocks)
                : <span>No blocks available</span>
            }

          </div>
          </div>
        </div>        
      </div>
    );
  }
}



export default ChainExplorer;

