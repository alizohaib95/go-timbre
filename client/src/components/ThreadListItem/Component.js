import React from 'react';
import { Link } from 'react-router-dom';
import moment from 'moment';
import ReactMarkdown from 'react-markdown';
import breaks from 'remark-breaks';
import { Field, reduxForm } from 'redux-form'
// import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
// import { faEyeSlash, faEye, faMoneyBill, faMoneyBillAlt, faDollarSign} from '@fortawesome/free-solid-svg-icons';
import sizeof from 'object-sizeof'


class ThreadListItem extends React.Component {

    render() {
    
        let {posts,mod,user} = this.props
        let root = posts[0]
        let blueorgray = mod && mod.echoes && mod.echoes.includes(root.hash)? false: true
        let textBool = blueorgray ? 'Hide' : 'Unhide'
        // let icon = blueorgray ? faEyeSlash : faEye
        let tstamp = new Date(root.timestamp * 1000); // dividing by 100000 to convert nano
        let tstampstring = tstamp.toGMTString();
        let length = posts ? posts.length - 1 : 0;
        
        var post = JSON.parse(root.content);
        let sze = sizeof(post.Content)
        let postvalue = sze + post.MaxCost


        return (<div className="card mb-2 border-white">
            <div className="row no-gutters">
                {/* <div className="col-md-1 card-footer py-0 pt-2 border-right border-top-0">
                </div> */}
                <div className="col-md-12">
                <div className="card-body pt-3 pb-0">
                    {post.Forum == "POLITICS" ?
                    <a href={`http://localhost:3005/${root.hash}` }>
                        <h6 className="card-title">{post.Title}</h6>
                    </a>
                    :<Link to={`/${root.hash}`}>
                        <h6 className="card-title">{post.Title}</h6>
                    </Link>
                    }   
                    {!this.props.fullText ? 
                        <p className="overflow-hidden text-truncate text-nowrap">
                            {post.Content}
                        </p>
                    :

                        <ReactMarkdown
                            source={post.Content}
                            plugins={[breaks]}
                            unwrapDisallowed
                        />    
                    }

                </div>
                <div className="card-footer pb-2 pt-1">
                    <span className="card-text"><small className="text-muted"> Karma Value: <strong>{postvalue}</strong></small> | <small className="text-muted">Posted by: <strong>{post.Author}</strong>  | <strong>{moment(tstampstring).fromNow()}</strong> | in <strong>{post.Forum}</strong> | <strong>{length}</strong> Comments</small>
                    </span>
                    
                </div>
                
                
                
                </div>
            </div>
        </div>);
    }
}



export default ThreadListItem;

