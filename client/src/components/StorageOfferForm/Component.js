import {
    NotificationContainer,
    NotificationManager
} from 'react-notifications';
import React from 'react';
import { Field, reduxForm } from 'redux-form'

class StorageOfferForm extends React.Component {
  
    createOffer = offer => {
        this.props.attemptSendStorageOffer(offer).then( () => {
            this.props.reset()
        })
        // this.props.history.push("/"); // This should change later -> what happens if transac fails?
    }

    onSubmit = () => {
        return this.props.handleSubmit(this.createOffer)
    };

    render() {
        return (
            <div>
            <h4 className="card-title">Create Storage Offer</h4>
            <hr></hr>
            <div className="card mb-3"> 
                <div className="card-body">
                    {/* <p className="card-text">Offer created will be sent to the miners</p> */}
                    <form onSubmit={this.onSubmit()}>
                        <div className="form-group">
                                <label>Price <small>(Decibels/MB)</small></label>
                            <Field className = "form-control"
                            name = "minPrice"
                            type = "number"
                            component = "input"
                            required
                            placeholder = "Market Price: 100" / >
                        </div>
                        {/* <div className="form-group">
                                <label>Max Duration<small>(seconds)</small></label>
                                <Field className="form-control" name="maxDuration" component="input" type="number" required/>
                            
                        </div> */}
                        <div className="form-group">
                                <label>Size <small>(MB)</small></label>
                                <Field className="form-control" name="size" placeholder="Max Available: 200MB" component="input" type="number" required/>
                            <button type="submit" className="btn btn-block btn-primary float-right mt-3">Send Offer</button>

                        </div>
                    </form>
                </div>
            </div>
            </div>
        );
    }
}

export default StorageOfferForm;