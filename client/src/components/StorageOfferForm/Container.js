import { connect } from 'react-redux';
import { compose } from 'redux';
import { reduxForm } from 'redux-form';
import { attemptSendStorageOffer } from '../../store/actions/general';
import StorageOfferForm from './Component';

const mapDispatchToProps = { attemptSendStorageOffer };

const enhance = compose(
  reduxForm({ form: 'sendStorageOfferForm' }),
  connect(
    null,
    mapDispatchToProps
  )
);

const StorageOfferFormContainer = enhance(StorageOfferForm);

export default StorageOfferFormContainer;