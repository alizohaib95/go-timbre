import { connect } from 'react-redux';
import { compose } from 'redux';
import { fetchStorageAccount } from '../../store/actions/general';
import StorageAccountInfo from './Component';
import { attemptRemoveThread } from '../../store/actions/threads';

export const mapStateToProps = state => ({
  isProcessing: state.general.isProcessing,
  storageAccount: state.general.storage_account
});

const mapDispatchToProps = { fetchStorageAccount, attemptRemoveThread };

const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const StorageAccountInfoContainer = enhance(StorageAccountInfo);

export default StorageAccountInfoContainer;
