import React from 'react';
import Skeleton from 'react-loading-skeleton';
import ShowMore from '@tedconf/react-show-more';
import StorageOfferFormContainer from '../StorageOfferForm/Container'
import Notifiction from 'react-notification-modal'
import 'react-notification-modal/dist/index.css'

import {
  store
} from 'react-notification-modal';

const mapDeals = deals => {
  let a = deals.map((deal, index) => {
    let tstamp = new Date(deal.expiryTime *1000)
    let tstampstring = tstamp.toGMTString()
      return (
      <div className="card mb-2 py-0" key={index}>
        <div className="card-body py-0">
          <p className="card-text mt-3"><strong>Hash:</strong> {deal.dealHash}</p>
          <p className="card-title mb-2"><span href="#" className="btn btn-sm mb-2 btn-danger ">Expires: {tstampstring}</span></p>

        </div>
      </div>
      )
  })
  return a;
};



class StorageAccountInfo extends React.Component {
  componentDidMount() {
    this.props.fetchStorageAccount();
  }
  removePost = (post) => {
    this.props.attemptRemoveThread(post)
  }

  ShowWarning = (hash) => {
    store.addNotification({
          title: "Exception!",
          status: 'Post Deletion',
          message:'',
          exception: "Are you sure you want to delete the post with hash:",
          info: <div className={'alert alert-danger overflow-auto'}>{hash} 
            <button className="btn btn-danger ml-3 btn-sm" onClick={()=> {
              this.removePost(hash)
              store.removeNotification()
            }} >Yes</button>
            <button className={'btn btn-primary btn-sm ml-2'} onClick={()=> store.removeNotification()}> No</button>
          </div>,
          type: "exception",
      });
  }

  mapPosts = posts => {
    let a = posts.map((post, index) => {
    var c = JSON.parse(post.content);
      
      return (
        <div className="card mb-2" key={index}>
          <div className="card-body">
            <button onClick={() => this.ShowWarning(post.hash) }className="btn btn-sm mb-2 btn-danger float-right ml-3">Remove Post</button>
            {/* <h6 className="card-title"><strong>Title:</strong> {c.Title} </h6> */}
            <p className="card-text mb-0"><strong> Title: </strong><a href={'/' + post.hash}>{c.Title}</a> </p >
            <p className="card-text mb-0"><strong> Author:</strong> {c.Author}</p >
            <p className="card-text mb-0"><strong>Hash:</strong> <small>{post.hash}</small></p>
            <p className="card-text"><strong>Size:</strong> {post.size} Bytes </p>
          </div>
        </div>
      )
    })
    return a;
  };
  render() {

    if (this.props.isProcessing) return <div className="container"><Skeleton count={3}/></div>;
    if (!this.props.storageAccount) return <div className="container"><p className="text-danger"> Account Not found </p></div>;
    
    return (
      <div className="container">
        <h2>Storage Provider <span className="badge badge-secondary float-right">Dashboard<small></small></span></h2>
        <hr></hr>
        <div className="row mb-2">
          <div className="col-md-3">
            <div className="card text-white bg-primary mb-3 text-center">
              <div className="card-header">Active Deals</div>
              <div className="card-body">
                <h1 className="card-title">{this.props.storageAccount.activeDeals ? (this.props.storageAccount.activeDeals.length) : "0"}</h1>
              </div>
            </div>
          </div>

          <div className="col-md-3">
            <div className="card text-white bg-success mb-3 text-center" >
              <div className="card-header">Posts Stored</div>
              <div className="card-body">
                <h1 className="card-title">{this.props.storageAccount.storedPosts ? (this.props.storageAccount.storedPosts.length) : "0"}</h1>
              </div>
            </div>
          </div>

          <div className="col-md-3">
            <div className="card text-white bg-secondary mb-3 text-center" >
              <div className="card-header">Chunks Allocated<small>(1KB/Chunk)</small></div>
              <div className="card-body">
                <h1 className="card-title"> {this.props.storageAccount.chunks ? this.props.storageAccount.chunks : "0"}</h1>
              </div>
            </div>
          </div>
      
          


          <div className="col-md-3">
            <div className="card text-white bg-danger mb-3 text-center" >
              <div className="card-header">Total Capacity(MB)</div>
              <div className="card-body">
                <h1 className="card-title overflow-auto">{this.props.storageAccount.totalCapacity ? (this.props.storageAccount.totalCapacity)/1000000 : "0"}</h1>
              </div>
            </div>
            {/* <small className="pull-right float-right">values in bytes</small> */}
          </div>

        </div>


        <div className="row">
          <div className="col-sm-8">
            <h4 className="mb-3">Active Deals <span className="badge badge-secondary ml-2">{this.props.storageAccount.activeDeals ? (this.props.storageAccount.activeDeals.length) : "0"}</span></h4>
            <hr></hr>
            
            {this.props.storageAccount.activeDeals ? 
                // mapDeals(this.props.storageAccount.activeDeals)
                <ShowMore
                    items={this.props.storageAccount.activeDeals}
                    by={5}
                >
                    {({current,onMore,}) => (
                        <React.Fragment>
                            {current.map(item => (
                                mapDeals([item])
                            ))}
                            
                            <button 
                                className = "btn btn-block btn-primary"
                                disabled={!onMore}
                                onClick={() => { if (!!onMore) onMore(); }}
                            >
                            Show More
                            </button>
                        </React.Fragment>
                    )}
                </ShowMore>
              : <span>No active deals</span>
            }

            <h4 className="mt-5">Posts Stored <span className="badge badge-secondary ml-3">{this.props.storageAccount.storedPosts ? (this.props.storageAccount.storedPosts.length) : "0"}</span></h4>
            <hr></hr>
            {this.props.storageAccount.storedPosts ?
              // mapPosts(this.props.storageAccount.storedPosts)
              <ShowMore
                    items={this.props.storageAccount.storedPosts}
                    by={5}
                >
                    {({current,onMore,}) => (
                        <React.Fragment>
                            {current.map(item => (
                                this.mapPosts([item])
                            ))}
                            
                            <button 
                                className = "btn btn-block btn-primary"
                                disabled={!onMore}
                                onClick={() => { if (!!onMore) onMore(); }}
                            >
                            Show More
                            </button>
                        </React.Fragment>
                    )}
                </ShowMore>

              : <span>No posts stored</span>
            }

            {/* <h4 className="mt-3">Posts Served <span className="badge badge-secondary float-right">0</span></h4>
            <hr></hr>
            <span>No posts being served </span> */}
          </div>
          <div className="col-sm-4">
            <StorageOfferFormContainer />
            {this.props.storageAccount.lastOffer ?
                <div className="card mb-2">
                    <div className="card-body">
                        <h5 className="card-title mb-2">Current Offer</h5>
                            
                        <ul className="list-group">
                          <li className="text-small list-group-item"><small><strong>ID: </strong>{this.props.storageAccount.lastOffer.spid}</small></li>
                          <li className="text-small list-group-item"><small><strong>Price: </strong>{this.props.storageAccount.lastOffer.min_price}</small></li>
                          <li className="text-small list-group-item"><small><strong>Size: </strong>{this.props.storageAccount.lastOffer.size / 1000000} MB</small></li>
                          {/* <li className="text-small list-group-item"><small><strong>Duration: </strong>{this.props.storageAccount.lastOffer.max_duration}</small></li> */}
                        </ul>
                    </div>
                </div>
            : <small>No Offers Exist</small>
            }
            
          </div>
        </div>
      </div>
    );
  }
}

export default StorageAccountInfo;
