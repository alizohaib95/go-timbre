import React from 'react';
import ThreadListItem from '../ThreadListItem/Component';
import Skeleton from 'react-loading-skeleton';
import CommentFormContainer from '../CommentForm/Container'
import CommentList from '../CommentList/Component'

class SingleThread extends React.Component {
  
  componentDidMount() {
    // Id coming from url
    this.props.fetchThread(this.props.id);
    
  }
  render() {
    if (this.props.isFetching) return <Skeleton count={10}/>;
    if (!this.props.thread) return <div className="card"><div className="card-body text-center">Post does not exist</div></div>
    return (
      <>
        <ThreadListItem {...this.props.thread} fullText={true} />
        <CommentFormContainer history={this.props.history}/>
        <CommentList comments={this.props.thread.posts}/>
        
      </>
    );
  }
}

export default SingleThread;
