import { connect } from 'react-redux';
import { compose } from 'redux';
import { fetchThread } from '../../store/actions/threads';
import SingleThread from './Component';

export const mapStateToProps = state => ({
  isFetching: state.threads.isFetching,
  thread: state.threads.thread
});

const mapDispatchToProps = { fetchThread };
const enhance = compose(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )
);

const SingleThreadContainer = enhance(SingleThread);

export default SingleThreadContainer;
