import React from 'react';
import { Field, reduxForm } from 'redux-form'

class RoleManager extends React.Component {
  
    extendToSp = () => {
        this.props.extendRole({'role':'SP'})
        this.props.history.push("/");
    };
    
    extendToMiner = () => {
        this.props.extendRole({'role':'MINER'})
        this.props.history.push("/");
    };

    render() {
        return (
        <div className="container">
            <div className="row">
                <div className="col-md-6 offset-md-3">
                    <div className="card"> 
                        <div className="card-body">
                            <h5 className="card-title">Extend Roles</h5>
                            <button onClick={this.extendToMiner} className="btn btn-block btn-primary">BECOME A MINER</button>
                            <button onClick={this.extendToSp} className="btn btn-block btn-primary">BECOME A STORAGE PROVIDER</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        );
    }
}

export default RoleManager;